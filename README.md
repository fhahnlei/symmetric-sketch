# Symmetric Sketch

Code for the paper "Symmetry-driven 3D Reconstruction from Concept Sketches"

Since the publication of the paper, we have compiled a more user-friendly sketch-based modeling library. It is also easier to install.
You can find it here:
https://gitlab.inria.fr/D3/pysbm
