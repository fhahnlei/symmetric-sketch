import bpy
import bmesh
import os
import bpy_extras
from mathutils import Matrix
from mathutils import Vector
import numpy as np
import json, pickle
#from scipy.spatial.transform import Rotation

#---------------------------------------------------------------
# 3x4 P matrix from Blender camera
#---------------------------------------------------------------

def align_vectors(a, b, weights=None, return_sensitivity=False):

    """Estimate a rotation to optimally align two sets of vectors.
    Find a rotation between frames A and B which best aligns a set of
    vectors `a` and `b` observed in these frames. The following loss
    function is minimized to solve for the rotation matrix
    :math:`C`:
    .. math::
        L(C) = \\frac{1}{2} \\sum_{i = 1}^{n} w_i \\lVert \\mathbf{a}_i -
        C \\mathbf{b}_i \\rVert^2 ,
    where :math:`w_i`'s are the `weights` corresponding to each vector.
    The rotation is estimated with Kabsch algorithm [1]_.
    Parameters
    ----------
    a : array_like, shape (N, 3)
        Vector components observed in initial frame A. Each row of `a`
        denotes a vector.
    b : array_like, shape (N, 3)
        Vector components observed in another frame B. Each row of `b`
        denotes a vector.
    weights : array_like shape (N,), optional
        Weights describing the relative importance of the vector
        observations. If None (default), then all values in `weights` are
        assumed to be 1.
    return_sensitivity : bool, optional
        Whether to return the sensitivity matrix. See Notes for details.
        Default is False.
    Returns
    -------
    estimated_rotation : `Rotation` instance
        Best estimate of the rotation that transforms `b` to `a`.
    rmsd : float
        Root mean square distance (weighted) between the given set of
        vectors after alignment. It is equal to ``sqrt(2 * minimum_loss)``,
        where ``minimum_loss`` is the loss function evaluated for the
        found optimal rotation.
    sensitivity_matrix : ndarray, shape (3, 3)
        Sensitivity matrix of the estimated rotation estimate as explained
        in Notes. Returned only when `return_sensitivity` is True.
    Notes
    -----
    This method can also compute the sensitivity of the estimated rotation
    to small perturbations of the vector measurements. Specifically we
    consider the rotation estimate error as a small rotation vector of
    frame A. The sensitivity matrix is proportional to the covariance of
    this rotation vector assuming that the vectors in `a` was measured with
    errors significantly less than their lengths. To get the true
    covariance matrix, the returned sensitivity matrix must be multiplied
    by harmonic mean [3]_ of variance in each observation. Note that
    `weights` are supposed to be inversely proportional to the observation
    variances to get consistent results. For example, if all vectors are
    measured with the same accuracy of 0.01 (`weights` must be all equal),
    then you should multiple the sensitivity matrix by 0.01**2 to get the
    covariance.
    Refer to [2]_ for more rigorous discussion of the covariance
    estimation.
    References
    ----------
    .. [1] https://en.wikipedia.org/wiki/Kabsch_algorithm
    .. [2] F. Landis Markley,
            "Attitude determination using vector observations: a fast
            optimal matrix algorithm", Journal of Astronautical Sciences,
            Vol. 41, No.2, 1993, pp. 261-280.
    .. [3] https://en.wikipedia.org/wiki/Harmonic_mean
    """
    a = np.asarray(a)
    if a.ndim != 2 or a.shape[-1] != 3:
        raise ValueError("Expected in `a` to have shape (N, 3), "
                         "got {}".format(a.shape))
    b = np.asarray(b)
    if b.ndim != 2 or b.shape[-1] != 3:
        raise ValueError("Expected input `b` to have shape (N, 3), "
                         "got {}.".format(b.shape))

    if a.shape != b.shape:
        raise ValueError("Expected inputs `a` and `b` to have same shapes"
                         ", got {} and {} respectively.".format(
                            a.shape, b.shape))

    if weights is None:
        weights = np.ones(len(b))
    else:
        weights = np.asarray(weights)
        if weights.ndim != 1:
            raise ValueError("Expected `weights` to be 1 dimensional, got "
                             "shape {}.".format(weights.shape))
        if weights.shape[0] != b.shape[0]:
            raise ValueError("Expected `weights` to have number of values "
                             "equal to number of input vectors, got "
                             "{} values and {} vectors.".format(
                                weights.shape[0], b.shape[0]))
        if (weights < 0).any():
            raise ValueError("`weights` may not contain negative values")

    B = np.einsum('ji,jk->ik', weights[:, None] * a, b)
    u, s, vh = np.linalg.svd(B)

    # Correct improper rotation if necessary (as in Kabsch algorithm)
    if np.linalg.det(u @ vh) < 0:
        s[-1] = -s[-1]
        u[:, -1] = -u[:, -1]

    C = np.dot(u, vh)

    if s[1] + s[2] < 1e-16 * s[0]:
        print("Optimal rotation is not uniquely or poorly defined "
                      "for the given sets of vectors.")

    rmsd = np.sqrt(max(
        np.sum(weights * np.sum(b ** 2 + a ** 2, axis=1)) - 2 * np.sum(s),
        0))

    if return_sensitivity:
        zeta = (s[0] + s[1]) * (s[1] + s[2]) * (s[2] + s[0])
        kappa = s[0] * s[1] + s[1] * s[2] + s[2] * s[0]
        with np.errstate(divide='ignore', invalid='ignore'):
            sensitivity = np.mean(weights) / zeta * (
                    kappa * np.eye(3) + np.dot(B, B.T))
        return C, rmsd, sensitivity
    else:
        print(C)
        return C, rmsd

# Build intrinsic camera parameters from Blender camera data
#
# See notes on this in
# blender.stackexchange.com/questions/15102/what-is-blenders-camera-projection-matrix-model
def get_calibration_matrix_K_from_blender(camd):
    f_in_mm = camd.lens
    scene = bpy.context.scene
    resolution_x_in_px = scene.render.resolution_x
    resolution_y_in_px = scene.render.resolution_y
    scale = scene.render.resolution_percentage / 100
    sensor_width_in_mm = camd.sensor_width
    sensor_height_in_mm = camd.sensor_height
    pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
    if (camd.sensor_fit == 'VERTICAL'):
        # the sensor height is fixed (sensor fit is horizontal),
        # the sensor width is effectively changed with the pixel aspect ratio
        s_u = resolution_x_in_px * scale / sensor_width_in_mm / pixel_aspect_ratio
        s_v = resolution_y_in_px * scale / sensor_height_in_mm
    else: # 'HORIZONTAL' and 'AUTO'
        # the sensor width is fixed (sensor fit is horizontal),
        # the sensor height is effectively changed with the pixel aspect ratio
        pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
        s_u = resolution_x_in_px * scale / sensor_width_in_mm
        s_v = resolution_y_in_px * scale * pixel_aspect_ratio / sensor_height_in_mm


    # Parameters of intrinsic calibration matrix K
    alpha_u = f_in_mm * s_u
    alpha_v = f_in_mm * s_v
    u_0 = resolution_x_in_px * scale / 2
    v_0 = resolution_y_in_px * scale / 2
    skew = 0 # only use rectangular pixels

    K = Matrix(
        ((alpha_u, skew,    u_0),
        (    0  , alpha_v, v_0),
        (    0  , 0,        1 )))
    return K

def get_3x4_RT_matrix_from_blender(cam):
    # bcam stands for blender camera
    R_bcam2cv = Matrix(
        ((1, 0,  0),
         (0, -1, 0),
         (0, 0, -1)))

    # Transpose since the rotation is object rotation,
    # and we want coordinate rotation
    # R_world2bcam = cam.rotation_euler.to_matrix().transposed()
    # T_world2bcam = -1*R_world2bcam * location
    #
    # Use matrix_world instead to account for all constraints
    location, rotation = cam.matrix_world.decompose()[0:2]
    R_world2bcam = rotation.to_matrix().transposed()

    # Convert camera location to translation vector used in coordinate changes
    # T_world2bcam = -1*R_world2bcam*cam.location
    # Use location from matrix_world to account for constraints:
    T_world2bcam = -1*R_world2bcam @ location

    # Build the coordinate transform matrix from world to computer vision camera
    # NOTE: Use * instead of @ here for older versions of Blender
    # TODO: detect Blender version
    R_world2cv = R_bcam2cv@R_world2bcam
    T_world2cv = R_bcam2cv@T_world2bcam

    # put into 3x4 matrix
    RT = Matrix((
        R_world2cv[0][:] + (T_world2cv[0],),
        R_world2cv[1][:] + (T_world2cv[1],),
        R_world2cv[2][:] + (T_world2cv[2],)
         ))
    return RT

def get_3x4_P_matrix_from_blender(cam):
    K = get_calibration_matrix_K_from_blender(cam.data)
    RT = get_3x4_RT_matrix_from_blender(cam)
    return K@RT, K, RT

# ----------------------------------------------------------
# Alternate 3D coordinates to 2D pixel coordinate projection code
# adapted from https://blender.stackexchange.com/questions/882/how-to-find-image-coordinates-of-the-rendered-vertex?lq=1
# to have the y axes pointing up and origin at the top-left corner
def project_by_object_utils(cam, point):
    scene = bpy.context.scene
    co_2d = bpy_extras.object_utils.world_to_camera_view(scene, cam, point)
    render_scale = scene.render.resolution_percentage / 100
    render_size = (
            int(scene.render.resolution_x * render_scale),
            int(scene.render.resolution_y * render_scale),
            )
    return Vector((co_2d.x * render_size[0], render_size[1] - co_2d.y * render_size[1]))


def compute_inverse_matrices(r_t, k):

    r_t_inv = np.ones(shape=(4, 4))
    r_t_inv[:3, :] = r_t
    r_t_inv[3, :3] = 0.0
    r_t_inv = np.linalg.inv(r_t_inv)
    k_inv = np.linalg.inv(k)
    return r_t_inv, k_inv

def lift_point(p, lambda_val, k_inv, r_t_inv):
    u, v = p[0], p[1]

    p_cam = np.dot(k_inv, np.array([[u], [v], [1.0]]))
    p_cam *= lambda_val
    p_cam = np.expand_dims(p_cam, 0)

    p_world = np.ones(shape=(4, 1))
    p_world[:3] = p_cam
    p_world = np.dot(r_t_inv, p_world)
    p_world[:] /= p_world[3]
    p_world = p_world[:3]
    p_world = np.transpose(p_world)
    return p_world[0]


def look_at(obj_camera, point):
    loc_camera = obj_camera.matrix_world.to_translation()
    direction = point - loc_camera
    # point the cameras '-Z' and use its 'Y' as up
    rot_quat = direction.to_track_quat('-Z', 'Y')
    # assume we're using euler rotation
    obj_camera.rotation_euler = rot_quat.to_euler()

def project_point(p, proj_mat):
    hom_p = np.ones(4)
    hom_p[:3] = p
    proj_p = np.dot(proj_mat, hom_p)
    return proj_p[:2] / proj_p[2]

def calibrate_camera(cam_param, folder):
    fov = cam_param["fov"]
    cam = bpy.data.objects['Camera']
    cam.data.sensor_width = 1
    cam.data.sensor_height = 1
    cam.rotation_mode = "XYZ"
    #cam.location = [-0.82328509792654525, -0.58680546433041747, -0.749133031137398]
    cam.location = cam_param["C"]
    cam_pos = np.array(cam_param["C"])
    #cam.data.lens = 885.72437566790518
    P, K, RT = get_3x4_P_matrix_from_blender(cam)
    K = np.array(cam_param["K"])
    #print("K")
    #print(K)
    #K = np.array([[885.72437566790518,0,356.35539412325687],[0,885.72437566790518,-276.38582435039547],[0,0,1]])

    fx, fy = K[0,0], K[1,1]
    cx, cy = K[0,2], K[1,2]
    if fx > fy:
        bpy.context.scene.render.pixel_aspect_y = fx/fy
    elif fx < fy:
        bpy.context.scene.render.pixel_aspect_x = fy/fx
    width = 512 #fov 29.0416d
    height = 512

    cam.data.lens_unit = 'FOV'
    bpy.context.scene.render.resolution_x = width
    bpy.context.scene.render.resolution_y = height
    max_resolution = max(width, height)
    cam.data.angle = 2 * np.arctan(width / (2 * K[0, 0]))
    #cam.data.angle = 2 * np.arctan(471.034823785905 / (2 * K[0, 0]))
    #print(np.rad2deg(cam.data.angle))
    #shift_x: -0.166805, shift_y: -0.946853
    #cam.data.angle = np.deg2rad(35.1282)
    #print(np.rad2deg(cam.data.angle))
    cam.data.shift_x = -(cx - width / 2.0) / max_resolution
    cam.data.shift_y = (cy - height / 2.0) / max_resolution
    bpy.context.view_layer.update()

    # find correct rotation
    r_t_inv, k_inv = compute_inverse_matrices(RT, K)
    bpy.context.view_layer.update()

    points_2d = np.load(os.path.join(folder, "points_2d.npy"))
    lifted_points_3d = np.array([lift_point(p, 1.0, k_inv, r_t_inv) for p in points_2d])
    for p_id in range(len(lifted_points_3d)):
        v = lifted_points_3d[p_id] - cam_pos
        v /= np.linalg.norm(v)
        #lifted_points_3d[p_id] = cam_pos + v
        lifted_points_3d[p_id] = v
        #lifted_points_3d[p_id] /= np.linalg.norm(lifted_points_3d[p_id])
        #lifted_points_3d[p_id] = cam_pos + lifted_points_3d[p_id]/np.linalg.norm(lifted_points_3d[p_id])
    np.save(os.path.join(folder, "lifted_points_3d.npy"), lifted_points_3d)

    points_3d = np.load(os.path.join(folder, "points_3d.npy"))
    rot_mat, _ = align_vectors(points_3d, lifted_points_3d)
    #print(rot_mat)
    rot_mat = Matrix(rot_mat)
    #print(rot_mat.to_euler())
    rot_mat_euler = rot_mat.to_euler()
    #print(cam.rotation_euler)
    cam.rotation_euler.rotate(rot_mat_euler)
    #print(cam.rotation_euler)
    bpy.context.view_layer.update()
    bpy.context.view_layer.update()
    P, K, RT = get_3x4_P_matrix_from_blender(cam)
    K = np.array(cam_param["K"])
    r_t_inv, k_inv = compute_inverse_matrices(RT, K)
    return k_inv, r_t_inv, P

def sketch_mesh_to_gpencil(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='GPENCIL')
    #bpy.context.object.data.pixel_factor = 0.1
    #bpy.ops.object.gpencil_modifier_add(type='GP_OPACITY')
    #bpy.context.object.grease_pencil_modifiers["Opacity"].factor = 0.5

    #ob = bpy.context.view_layer.objects.active

    #bpy.ops.object.select_all(action='DESELECT')
    #ob.select_set(True)
    #bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    #if mat is None:
    #    mat = new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    #ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')

def sketch_mesh_to_tubes(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='CURVE')

    ob = bpy.context.view_layer.objects.active
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    #if mat is None:
    #    mat = new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')


MATERIAL_FILE_PATH = os.path.join("/Users/fhahnlei/Pictures/symmetric_sketch/materials.blend")
#MATERIAL_FILE_PATH = os.path.join("/" ,"user", "fhahnlei", "home", "Documents", "wires_python", "scripts", "materials.blend")

def load_mat(mat_name):
    path = MATERIAL_FILE_PATH + "\\Material\\"
    bpy.ops.wm.append(filename=mat_name, directory=path)
    mat = bpy.data.materials.get(mat_name)
    return mat

def load_sketch_mesh_obj(ob, thickness=0.001, pressure=None, color=None):

    mat = load_mat("stroke-black")
    mat = mat.copy()
    mat.name = ob.name
    tree = mat.node_tree
    if not pressure is None:
        tree.nodes["Mix Shader"].inputs[0].default_value = pressure
    if not color is None:
        tree.nodes["Principled BSDF"].inputs[0].default_value = (color[0], color[1], color[2], 1)
    #bpy.data.materials["stroke-black"].node_tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob

def load_sketch_mesh(sketch_name, sketch_obj_file, thickness=0.001, pressure=None, color=None):

    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF', axis_forward="Y", axis_up="Z")
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}"
    # Apply your transform to correct axis, eg:
    #ob.rotation_euler[0] = np.deg2rad(90) # Correct axis orientations
    #bpy.ops.object.transform_apply() # Apply all transforms

    mat = load_mat("stroke-black")
    mat = mat.copy()
    mat.name = ob.name
    tree = mat.node_tree
    if not pressure is None:
        tree.nodes["Mix Shader"].inputs[0].default_value = pressure
    if not color is None:
        tree.nodes["Principled BSDF"].inputs[0].default_value = (color[0], color[1], color[2], 1)
    #bpy.data.materials["stroke-black"].node_tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob


def load_plane(sketch_name, sketch_obj_file, axis):

    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF', axis_forward="Y", axis_up="Z")
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}"
    # Apply your transform to correct axis, eg:
    #ob.rotation_euler[0] = np.deg2rad(90) # Correct axis orientations
    #bpy.ops.object.transform_apply() # Apply all transforms

    mat = load_mat("plane-"+axis)

    #sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob

def setup_scene(envmap_path=None):

    bpy.context.scene.render.film_transparent = True

    # Environment lighting
    if envmap_path is not None:
        bpy.context.scene.world.use_nodes = True
        node_tex = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeTexEnvironment")
        node_tex.image = bpy.data.images.load(envmap_path)
        node_tree = bpy.context.scene.world.node_tree
        # Tweak saturation
        node_hsv = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeHueSaturation")
        node_hsv.inputs[1].default_value = 0.2 # Set saturation
        node_tree.links.new(node_tex.outputs['Color'], node_hsv.inputs['Color'])
        node_tree.links.new(node_hsv.outputs['Color'], node_tree.nodes['Background'].inputs['Color'])

    bpy.ops.object.select_all(action = 'SELECT')
    bpy.data.objects.get("Camera").select_set(False)
    #bpy.data.objects.get("Camera_2").select_set(False)
    bpy.ops.object.delete()

def projective_stroke_animation(s, s_id, cam_pos, f_start, f_end, mat_name="plane-y", symmetry_index=-1):

    # construct triangle mesh
    s_m = bmesh.new()
    triangle_points = []
    # make triangle longer than just the stroke
    s_0 = s[0] - cam_pos
    s_0 = cam_pos + 3.0*s_0
    s_1 = s[-1] - cam_pos
    s_1 = cam_pos + 3.0*s_1
    if symmetry_index > -1:
        s_0[symmetry_index] *= -1
        s_1[symmetry_index] *= -1
        cam_pos[symmetry_index] *= -1
    triangle_points.append(s_m.verts.new(cam_pos))
    triangle_points.append(s_m.verts.new(s_0))
    triangle_points.append(s_m.verts.new(s_1))
    s_m.faces.new(triangle_points)
    me = meshes.new('placeholder_mesh')
    mesh_obj = objects.new('triangle_s_'+str(s_id), me)
    s_m.to_mesh(me)
    s_m.free()

    bpy.context.scene.collection.objects.link(mesh_obj)
    mat = load_mat(mat_name)
    mesh_obj.active_material = mat
#
#    # material
#    # animation
    # create a second triangle to boolean diff it from the first
    translation_vec = (s_0 + s_1)/2 -cam_pos
    s_m = bmesh.new()
    triangle_points = []
    # make triangle longer than just the stroke
    #triangle_points.append(s_m.verts.new(s_0 - translation_vec))
    #triangle_points.append(s_m.verts.new(s_0))
    #triangle_points.append(s_m.verts.new(s_1))
    #triangle_points.append(s_m.verts.new(s_0 - translation_vec))
    #triangle_points.append(s_m.verts.new(s_1))
    #triangle_points.append(s_m.verts.new(s_1 - translation_vec))
    cam_vec = translation_vec/np.linalg.norm(translation_vec)
    up_vec = s_0 - s_1
    up_vec /= np.linalg.norm(up_vec)
    side_vec = np.cross(cam_vec, up_vec)
    side_vec /= np.linalg.norm(side_vec)

    a = s_1 - translation_vec + side_vec*0.1
    b = s_1 + side_vec*0.1
    c = s_1 - side_vec*0.1
    d = s_1 - translation_vec - side_vec*0.1

    e = s_0 - translation_vec + side_vec*0.1
    f = s_0 + side_vec*0.1
    g = s_0 - side_vec*0.1
    h = s_0 - translation_vec - side_vec*0.1

    triangle_points.append(s_m.verts.new(a))
    triangle_points.append(s_m.verts.new(b))
    triangle_points.append(s_m.verts.new(c))
    triangle_points.append(s_m.verts.new(d))

    triangle_points.append(s_m.verts.new(e))
    triangle_points.append(s_m.verts.new(f))
    triangle_points.append(s_m.verts.new(g))
    triangle_points.append(s_m.verts.new(h))

    s_m.edges.new([triangle_points[0], triangle_points[1]])
    s_m.edges.new([triangle_points[1], triangle_points[2]])
    s_m.edges.new([triangle_points[2], triangle_points[3]])
    s_m.edges.new([triangle_points[3], triangle_points[0]])

    s_m.edges.new([triangle_points[0], triangle_points[4]])
    s_m.edges.new([triangle_points[1], triangle_points[5]])
    s_m.edges.new([triangle_points[2], triangle_points[6]])
    s_m.edges.new([triangle_points[3], triangle_points[7]])

    s_m.edges.new([triangle_points[4], triangle_points[5]])
    s_m.edges.new([triangle_points[5], triangle_points[6]])
    s_m.edges.new([triangle_points[6], triangle_points[7]])
    s_m.edges.new([triangle_points[7], triangle_points[4]])
    s_m.faces.new([triangle_points[0], triangle_points[1], triangle_points[2], triangle_points[3]])
    s_m.faces.new([triangle_points[4], triangle_points[5], triangle_points[6], triangle_points[7]])
    s_m.faces.new([triangle_points[0], triangle_points[1], triangle_points[5], triangle_points[4]])
    s_m.faces.new([triangle_points[0], triangle_points[4], triangle_points[7], triangle_points[3]])
    s_m.faces.new([triangle_points[1], triangle_points[2], triangle_points[6], triangle_points[5]])
    s_m.faces.new([triangle_points[3], triangle_points[2], triangle_points[6], triangle_points[7]])

    me = meshes.new('placeholder_mesh')
    bool_mesh_obj = objects.new('triangle_s_bool_'+str(s_id), me)
    s_m.to_mesh(me)
    s_m.free()
    bpy.context.scene.collection.objects.link(bool_mesh_obj)

    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = bool_mesh_obj
    bool_mesh_obj.select_set(True)
    #bpy.ops.object.modifier_add(type='SOLIDIFY')
    #bool_mesh_obj.modifiers["Solidify"].use_rim_only = True
    #bool_mesh_obj.modifiers["Solidify"].offset = 1.0
    bpy.context.object.keyframe_insert(data_path='location', frame=f_start)
    bpy.context.object.location = np.array(bpy.context.object.location) + translation_vec 
    bpy.context.object.keyframe_insert(data_path='location', frame=f_end)


    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = mesh_obj
    mesh_obj.select_set(True)
    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["triangle_s_bool_"+str(s_id)]
    bpy.data.objects["triangle_s_bool_"+str(s_id)].hide_set(True)
    bpy.data.objects["triangle_s_bool_"+str(s_id)].hide_render = True
#    bpy.ops.transform.translate(value=(0.852882, 0.614148, 0.180108), orient_axis_ortho='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

def rotate_obj(obj, angle, f_start, f_end):
    bpy.context.scene.tool_settings.transform_pivot_point = 'CURSOR'
    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=f_start)
    bpy.context.object.rotation_euler[2] = 6.28319
    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=f_end)

def camera_change(new_pos, f_start, f_end, old_pos=None):
    if not old_pos is None:
        bpy.data.objects["Camera"].rotation_euler = np.deg2rad(old_pos[1])
        bpy.data.objects["Camera"].location = old_pos[0]
    bpy.data.objects["Camera"].keyframe_insert(data_path='location', frame=f_start)
    bpy.data.objects["Camera"].keyframe_insert(data_path='rotation_euler', frame=f_start)
    bpy.data.objects["Camera"].rotation_euler = np.deg2rad(new_pos[1])
    bpy.data.objects["Camera"].location = new_pos[0]
    bpy.data.objects["Camera"].keyframe_insert(data_path='location', frame=f_end)
    bpy.data.objects["Camera"].keyframe_insert(data_path='rotation_euler', frame=f_end)

def alpha_change(mat_name, alpha_start, alpha_end, f_start, f_end):
    bpy.data.materials[mat_name].node_tree.nodes["Mix Shader"].inputs[0].default_value = alpha_start
    bpy.data.materials[mat_name].node_tree.nodes["Mix Shader"].inputs[0].keyframe_insert(data_path='default_value', frame=f_start)
    bpy.data.materials[mat_name].node_tree.nodes["Mix Shader"].inputs[0].default_value = alpha_end
    bpy.data.materials[mat_name].node_tree.nodes["Mix Shader"].inputs[0].keyframe_insert(data_path='default_value', frame=f_end)
    #bpy.data.materials[mat_name].keyframe_insert(data_path='node_tree.nodes["Mix Shader"].inputs[0].default_value', frame=f_end)
#
#def appear(obj_name, f_start, f_end):

if __name__ == "__main__":
    import sys
    argv = sys.argv
    argv = argv[argv.index("--") + 1:]  # get all args after "--"
    sketch_id = argv[0]
    folder = os.path.join("/Users/fhahnlei/Documents/wires_python/data", str(sketch_id))
    #folder = os.path.join("/user/fhahnlei/home/Documents/wires_python/data", str(sketch_id))
    blender_folder = os.path.join(folder, "blender", "candidates")
    pickle_folder = os.path.join(folder, "pickle")
    with open(os.path.join(folder, "sketch.json"), "rb") as fp:
        sketch = json.load(fp)
    #load_sketch_mesh("sketch", os.path.join(blender_folder, "sketch.obj"))
    # load pressure
    if os.path.exists(os.path.join(folder, "blender", "pressures.npy")):
        pressures = np.load(os.path.join(folder, "blender", "pressures.npy"))
    if os.path.exists(os.path.join(blender_folder, "candidate_pressures.npy")):
        candidate_pressures = np.load(os.path.join(blender_folder, "candidate_pressures.npy"))
    meshes = bpy.data.meshes
    objects = bpy.data.objects
    bpy.context.scene.frame_start = 0
    bpy.context.scene.frame_end = 350

    #1) setup scene
    width = 1080
    setup_scene(os.path.join("/Applications/Blender.app/Contents/Resources/3.1/datafiles/studiolights/world/interior.exr"))
    #setup_scene(os.path.join("/user/fhahnlei/home/blender-3.1.2-linux-x64/3.1/datafiles/studiolights/world/interior.exr"))
    calibrate_camera(sketch["cam_param"], os.path.join(folder, "blender"))
    k_inv, rt_inv, proj_mat = calibrate_camera(sketch["cam_param"], os.path.join(folder, "blender"))
    bpy.context.scene.render.resolution_x = width
    bpy.context.scene.render.resolution_y = width
    bpy.ops.import_scene.obj(filepath="film.obj", split_mode='OFF', axis_forward="Y", axis_up="Z")
    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
    objects["film"].location = objects["Camera"].location
    objects["film"].rotation_euler = (np.deg2rad(-90.8646), np.deg2rad(11.7565), np.deg2rad(223.217))
    #objects["film"].rotation_euler = objects["Camera"].rotation_euler
    #objects["film"].rotation_euler = (-90.8646, 11.7565, 223.217)
    objects["film"].scale = (0.17, 0.17, 0.17)
    view_initial = [np.array(objects["Camera"].location), np.array(objects["Camera"].rotation_euler)]
    cam_pos = np.array(objects["Camera"].location)
    view_shoulder = [[-0.634934, -1.3573, -0.89773],
                     [-120.863, -2.89551, 156.434]]
    view_frontal = [[0.0, -2.05484, -0.389188],
                    [-101.238, 0, 179.837]]
    view_candidate = [[-0.385109, -1.12222, -1.27678],
                      [-137.638, -0.000003, 153.437]]
    
    size = 0.0005

    #2) construct image as a plane
    image_strokes_3d = []
    for s_id, s in enumerate(sketch["strokes_topology"][:13]):
        s_m = bmesh.new()
        points_3d = []
        image_stroke_points_3d = []
        for p in s["points2D"]:
            if type(p) == dict:
                points_3d.append(s_m.verts.new(lift_point([p["x"], p["y"]], 0.5, k_inv, rt_inv)))
                #print(p["x"], p["y"], project_by_object_utils(cam, points_3d[-1].co))
                #points_2d.append(cam.world_to_camera_view(points_3d[-1]))
                image_stroke_points_3d.append(lift_point([p["x"], p["y"]], 0.5, k_inv, rt_inv))
        if len(points_3d) < 2:
            continue
        image_strokes_3d.append(np.array(image_stroke_points_3d))

        for i in range(len(points_3d)-1):
            s_m.edges.new([points_3d[i], points_3d[i+1]])

        me = meshes.new('placeholder_mesh')
        mesh_obj = objects.new('s_'+str(s_id), me)
        s_m.to_mesh(me)
        s_m.free()

        bpy.context.scene.collection.objects.link(mesh_obj)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = mesh_obj
        mesh_obj.select_set(True)
        bpy.ops.object.convert(target='CURVE')
        mesh_obj = load_sketch_mesh_obj(mesh_obj, thickness=size, pressure=1.0)
    # construct image frame
    canvas_size = 512
    strokes = [[[0, 0], [0, canvas_size]],
               [[0, 0], [canvas_size, 0]],
               [[canvas_size, 0], [canvas_size, canvas_size]],
               [[canvas_size, canvas_size], [0, canvas_size]]]
    for s_id, s in enumerate(strokes):
        s_m = bmesh.new()
        points_3d = []
        for p in s:
            points_3d.append(s_m.verts.new(lift_point([p[0], p[1]], 0.5, k_inv, rt_inv)))
            #print(p["x"], p["y"], project_by_object_utils(cam, points_3d[-1].co))
            #points_2d.append(cam.world_to_camera_view(points_3d[-1]))
        #if len(points_3d) < 2:
        #    continue

        for i in range(len(points_3d)-1):
            s_m.edges.new([points_3d[i], points_3d[i+1]])

        me = meshes.new('placeholder_mesh')
        mesh_obj = objects.new('image_frame_'+str(s_id), me)
        s_m.to_mesh(me)
        s_m.free()

        bpy.context.scene.collection.objects.link(mesh_obj)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = mesh_obj
        mesh_obj.select_set(True)
        bpy.ops.object.convert(target='CURVE')
        mesh_obj = load_sketch_mesh_obj(mesh_obj, thickness=size, pressure=1.0)

    #3) load candidates
    stroke_collection = bpy.data.collections.new("strokes")
    bpy.context.scene.collection.children.link(stroke_collection)
    clean_stroke_collection = bpy.data.collections.new("clean_strokes")
    bpy.context.scene.collection.children.link(clean_stroke_collection)
    for x in os.listdir(blender_folder):
        if not ".obj" in x and not "candidate_corr_" in x:
            continue
        if "bootstrapped" in x:
            s_id_0 = int(x.split("_")[3])
            s_id_1 = int(x.split("_")[4])
            l_id = int(x.split("_")[5].split(".obj")[0])
        else:
            s_id_0 = int(x.split("_")[2])
            s_id_1 = int(x.split("_")[3])
            l_id = int(x.split("_")[4].split(".obj")[0])
        if not (s_id_0 == 0 and s_id_1 == 1 and l_id == 0):
            continue
        if not s_id_0 in [0, 1] or not s_id_1 in [0,1] or not l_id == 0:
            continue
        obj_name = "cand_"+str(s_id_0)+"_"+str(s_id_1)+"_"+str(l_id)
        load_sketch_mesh(obj_name, os.path.join(blender_folder, x),
                         #thickness=size*candidate_pressures[s_id_0], pressure=candidate_pressures[s_id_0])
                         thickness=size, pressure=1.0)
        stroke_collection.objects.link(bpy.data.objects[obj_name])
        try:
            bpy.context.scene.collection.objects.unlink(bpy.data.objects[obj_name])
        except:
            try:
                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects[obj_name])
            except:
                continue
    #for x in os.listdir(os.path.join(folder, "blender")):
    #    if not ".obj" in x:
    #        continue
    #    if "stroke_" in x and not "diff" in x and not "reconstructed" in x and not "original" in x:
    #        stroke_id = int(x.split("_")[1].split(".obj")[0])
    #        size = 0.0005
    #        load_sketch_mesh("stroke_"+str(stroke_id), os.path.join(folder, "blender", x),
    #                         thickness=size*pressures[stroke_id], pressure=pressures[stroke_id])
    #        clean_stroke_collection.objects.link(bpy.data.objects["stroke_"+str(stroke_id)])
    #        try:
    #            bpy.context.scene.collection.objects.unlink(bpy.data.objects["stroke_"+str(stroke_id)])
    #        except:
    #            try:
    #                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["stroke_"+str(stroke_id)])
    #            except:
    #                continue

    #4) candidate construction animation
    # highlight the two strokes and the candidate stroke
    ob = bpy.data.objects["s_0"]
    ob.data.bevel_depth = 0.002
    mat = ob.active_material
    tree = mat.node_tree
    tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
    tree.nodes["Principled BSDF"].inputs[0].default_value = (217,95,2,1)
    ob = bpy.data.objects["s_1"]
    ob.data.bevel_depth = 0.002
    mat = ob.active_material
    tree = mat.node_tree
    tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
    tree.nodes["Principled BSDF"].inputs[0].default_value = (117,112,179,1)
    ob = bpy.data.objects["cand_0_1_0"]
    ob.data.bevel_depth = 0.007
    #projective_stroke_animation(image_strokes_3d[2], 2, cam_pos, f_start=0, f_end=60)
    camera_change(view_shoulder, 10, 40)
    projective_stroke_animation(image_strokes_3d[1], 1, cam_pos, f_start=50, f_end=80, mat_name="plane-z")
    projective_stroke_animation(image_strokes_3d[0], 0, cam_pos, f_start=90, f_end=120)
    camera_change(view_frontal, 130, 150, old_pos=view_shoulder)
    alpha_change("plane-y", 0.2, 0.0, 160, 180)
    # reflect strokes s1
    projective_stroke_animation(image_strokes_3d[1], 3, cam_pos, f_start=0, f_end=1, mat_name="plane-z", symmetry_index=0)
    # shape key
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = bpy.data.objects["triangle_s_1"]
    bpy.data.objects["triangle_s_1"].select_set(True)
    bpy.ops.object.shape_key_add(from_mix=False)
    bpy.data.objects["triangle_s_1"].select_set(False)
    bpy.data.objects["triangle_s_3"].select_set(True)
    bpy.data.objects["triangle_s_1"].select_set(True)
    bpy.ops.object.join_shapes()
    bpy.data.objects["triangle_s_3"].hide_set(True)
    bpy.data.objects["triangle_s_3"].hide_render = True
    bpy.data.objects["triangle_s_3"].select_set(False)
    bpy.data.objects["triangle_s_1"].select_set(True)
    bpy.data.shape_keys["Key"].key_blocks["triangle_s_3"].value = 0.0
    bpy.context.object.data.shape_keys.key_blocks["triangle_s_3"].keyframe_insert(data_path='value', frame=190)
    bpy.data.shape_keys["Key"].key_blocks["triangle_s_3"].value = 1.0
    bpy.context.object.data.shape_keys.key_blocks["triangle_s_3"].keyframe_insert(data_path='value', frame=210)
    camera_change(view_candidate, 220, 250, old_pos=view_frontal)
    alpha_change("plane-y", 0.0, 0.2, 260, 290)
    alpha_change("cand_0_1_0", 0.0, 1.0, 300, 330)
    camera_change(view_initial, 340, 360, old_pos=view_candidate)
    alpha_change("triangle", 0.0, 1.0, 300, 330)


    for x in os.listdir(os.path.join(blender_folder, "..")):
        if "plane_" in x:
            plane_id = int(x.split("_")[2].split(".obj")[0])
            if plane_id != 0:
                continue
            axis = x.split("_")[1]
            load_plane("plane_"+str(plane_id), os.path.join(blender_folder, "..", x), axis)
#
#        if "diff_ours_" in x:
#            stroke_id = int(x.split("_")[-1].split(".obj")[0])
#            load_sketch_mesh("diff_ours_"+str(stroke_id), os.path.join(blender_folder, x),
#                             thickness=0.001, pressure=None, color=diff_colors[stroke_id])
#            ours_collection.objects.link(bpy.data.objects["diff_ours_"+str(stroke_id)])
#            try:
#                bpy.context.scene.collection.objects.unlink(bpy.data.objects["diff_ours_"+str(stroke_id)])
#            except:
#                try:
#                    bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["diff_ours_"+str(stroke_id)])
#                except:
#                    continue
#
#        if "diff_wires_" in x:
#            stroke_id = int(x.split("_")[-1].split(".obj")[0])
#            print(stroke_id)
#            load_sketch_mesh("diff_wires_"+str(stroke_id), os.path.join(blender_folder, x),
#                             thickness=0.001, pressure=None, color=diff_colors[stroke_id])
#            wires_collection.objects.link(bpy.data.objects["diff_wires_"+str(stroke_id)])
#            try:
#                bpy.context.scene.collection.objects.unlink(bpy.data.objects["diff_wires_"+str(stroke_id)])
#            except:
#                try:
#                    bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["diff_wires_"+str(stroke_id)])
#                except:
#                    continue
#
#    #3)
#    for x in os.listdir(blender_folder):
#        if not ".obj" in x:
#            continue
#        if "non_reconstructed_" in x:
#            stroke_id = int(x.split("_")[-1].split(".obj")[0])
#            obj = bpy.data.objects["non_reconstructed_stroke_"+str(stroke_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    #for s_id in only_processed_by_ours:
    #    try:
    #        obj = bpy.data.objects["diff_ours_"+str(s_id)]
    #        obj.data.bevel_depth = 0.001
    #        mat = obj.active_material
    #        tree = mat.node_tree
    #        c = diff_colors[s_id]
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
    #        tree.nodes["Principled BSDF"].inputs[0].default_value = (55/256,126/256,184/256, 1)

    #        load_sketch_mesh("missing_stroke_"+str(s_id), os.path.join(blender_folder, "stroke_diff_ours_"+str(s_id)+".obj"),
    #                         thickness=0.001, pressure=None, color=(55/256,126/256,184/256))
    #        missing_collection.objects.link(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        try:
    #            bpy.context.scene.collection.objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        except:
    #            try:
    #                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #            except:
    #                continue
    #    except:
    #        continue
    #for s_id in only_processed_by_wires:
    #    try:
    #        obj = bpy.data.objects["diff_wires_"+str(s_id)]
    #        obj.data.bevel_depth = 0.001
    #        mat = obj.active_material
    #        tree = mat.node_tree
    #        c = diff_colors[s_id]
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
    #        tree.nodes["Principled BSDF"].inputs[0].default_value = (55/256,126/256,184/256, 1)

    #        load_sketch_mesh("missing_stroke_"+str(s_id), os.path.join(blender_folder, "stroke_diff_wires_"+str(s_id)+".obj"),
    #                         thickness=0.001, pressure=None, color=(55/256,126/256,184/256))
    #        missing_collection.objects.link(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        try:
    #            bpy.context.scene.collection.objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        except:
    #            try:
    #                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #            except:
    #                continue
    #    except:
    #        continue
#    for s_id in range(len(diff_colors)):
#        try:
#            obj = bpy.data.objects["diff_ours_"+str(s_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            c = diff_colors[s_id]
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#            obj = bpy.data.objects["diff_wires_"+str(s_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            c = diff_colors[s_id]
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#        except:
#            continue
#    for s_id in range(len(pressures)):
#        try:
#            obj = bpy.data.objects["stroke_"+str(s_id)]
#            obj.data.bevel_depth = 0.001*pressures[s_id]
#            #mat = obj.active_material
#            #tree = mat.node_tree
#            #c = diff_colors[s_id]
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#        except:
#            continue
#    for s_id, c in enumerate(per_stroke_batch_color):
#        try:
#            obj = bpy.data.objects["stroke_"+str(s_id)]
#            #obj.data.bevel_depth = 0.001
#            #mat = load_mat("emission")
#            mat = obj.active_material
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#            #tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)
#            #mat = mat.copy()
#            tree = mat.node_tree
#            #tree.nodes["Emission"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Emission"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            tree.nodes["Emission"].inputs[1].default_value = 2.0
#            obj.active_material = mat
#        except:
#            continue

#    # single image render
#    render_folder = os.path.join(blender_folder, "turntable_small")
#    #render_folder = os.path.join(blender_folder, "turntable")
#    if not os.path.exists(render_folder):
#        os.mkdir(render_folder)
#    tmp_file_name = os.path.join(render_folder, "input_sketch.png")
#    print(tmp_file_name)
#    bpy.data.scenes['Scene'].render.filepath = tmp_file_name
#    bpy.ops.render.render(write_still = True)

#    # animation render
#    blender_folder = os.path.join(folder, "blender")
#    if os.path.exists(os.path.join(blender_folder, "bbox_center.npy")):
#        bbox_center = np.load(os.path.join(blender_folder, "bbox_center.npy"))
#    bpy.context.scene.frame_start = 0
#    bpy.context.scene.frame_end = 359
#
#    bpy.ops.object.empty_add(type='PLAIN_AXES', align='WORLD', location=(bbox_center[0], bbox_center[1], bbox_center[2]), scale=(1, 1, 1))
#    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=0)
#    bpy.context.object.rotation_euler[2] = 6.28319
#    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=359)
#
#
#    fcurves = bpy.context.object.animation_data.action.fcurves
#    for fcurve in fcurves:
#        for kf in fcurve.keyframe_points:
#            kf.interpolation = 'LINEAR'
#    for obj in stroke_collection.all_objects:
#        obj.select_set(True)
#    for obj in clean_stroke_collection.all_objects:
#        obj.select_set(True)
#    #bpy.ops.outliner.collection_objects_select()
#    #stroke_collection.collection_objects_select()
#    bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)
#    #bpy.ops.anim.change_frame(90)
#    #bpy.data.scenes['Scene'].camera = camera
#    #render_folder = os.path.join(blender_folder, "turntable_small")
#    # RENDER
#    render_folder = os.path.join(blender_folder, "turntable_1080")
#    if not os.path.exists(render_folder):
#        os.mkdir(render_folder)
#    for i in range(360):
#        #bpy.data.scenes['Scene'].frame_set(bpy.data.scenes['Scene'].frame_current + 1)
#        bpy.data.scenes['Scene'].frame_current = i
#        tmp_file_name = os.path.join(render_folder, str(np.char.zfill(str(i), 4))+".png")
#        print(tmp_file_name)
#        bpy.data.scenes['Scene'].render.filepath = tmp_file_name
#        bpy.ops.render.render(write_still = True)
