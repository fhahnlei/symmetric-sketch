def test():
    import numpy as np
    import bpy
    scene = bpy.context.scene
    K = np.array([[885.72437566790518,0,356.35539412325687],[0,885.72437566790518,-276.38582435039547],[0,0,1]])
    R = np.array([[-0.69386634829913174,0.71995560577811935,-0.014608778411940981],[-0.010137621554909861,0.010518794412793691,0.99989328610272743],[0.720032443246301,0.69394040138383817,-1.9081958235744878E-17]])
    location = np.array([-0.82328509792654525,-0.58680546433041747,-0.749133031137398])
    bpy.ops.object.add(
        type='CAMERA',
        location=location)
    ob = bpy.context.object
    ob.name = 'CamFrom3x4PObj'
    cam = ob.data
    cam.name = 'CamFrom3x4P'
    f_in_mm = K[0,0]
    sensor_width_in_mm = K[1,1]*K[0,2] / (K[0,0]*K[1,2])
    cam.type = 'PERSP'
    cam.lens = f_in_mm
    cam.lens_unit = 'MILLIMETERS'
    cam.sensor_width  = sensor_width_in_mm
    ob.matrix_world = location*R
    # Display
    cam.show_name = True
    # Make this the current camera
    scene.camera = ob
    bpy.context.scene.update()


import numpy as np
K = np.array([[885.72437566790518,0,356.35539412325687],[0,885.72437566790518,-276.38582435039547],[0,0,1]])
R = np.array([[-0.69386634829913174,0.71995560577811935,-0.014608778411940981],[-0.010137621554909861,0.010518794412793691,0.99989328610272743],[0.720032443246301,0.69394040138383817,-1.9081958235744878E-17]])
location = np.array([-0.82328509792654525,-0.58680546433041747,-0.749133031137398])
print(location*R.T)
