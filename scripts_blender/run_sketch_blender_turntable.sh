#!/usr/bin/env zsh
sketch_id=$1

echo $sketch_id
#if [[ ! -f ../data/$sketch_id/batches_results_normal.json ]]
#then
#  return
#fi
#
#python export_obj.py --sketch_id=$sketch_id
##/Applications/Blender.app/Contents/MacOS/Blender -b -P blender_turntable.py -- $sketch_id
#return
/Applications/Blender.app/Contents/MacOS/Blender -b -P blender_turntable.py -- $sketch_id

#cd ../data/$sketch_id/blender/turntable_small
#cd ../data/$sketch_id/blender/turntable
cd ../data/$sketch_id/blender/turntable_1080
#convert input_sketch.png -background white -flatten input_sketch.png
# ADD white background
#for i in $(ls *.png)
#do
#  echo $i
#  convert $i -background white -flatten $i
#done

# CONVERT to video
#/usr/local/bin/./ffmpeg -y -i %04d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4
#/usr/local/bin/./ffmpeg -y -i %04d.png -c:v libx264 -vf fps=24 -pix_fmt yuv420p out.mp4
#/usr/local/bin/./ffmpeg -f lavfi -i color=c=white:s=1080x1080:r=24 -pattern_type glob -framerate 24 -i %04d.png -vcodec libx264 -filter_complex '[0:v][1:v]overlay=shortest=1,format=yuv420p[out]' -map '[out]' -r 24 out.mp4
#/usr/local/bin/./ffmpeg  -framerate 24 -i ./*.png -vcodec libx264 -filter_complex '[0:v][1:v]overlay=shortest=1,format=yuv420p[out]' -map '[out]' -r 24 out.mp4
cd ../../../../scripts
