import json
from time import time
from copy import deepcopy
from skspatial.objects import Plane
from shapely.geometry import MultiPoint
import polyscope as ps
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os, sys, inspect
import pickle

VERBOSE = False

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
import tools_3d
import symmetry_tools
import non_symmetric_tools
import sketch_wires

class CurveIntersections:
    def __init__(self, inter_3d=[], s_endpoint=[], s_id=-1):
        self.inter_3d = inter_3d
        self.s_endpoint = s_endpoint
        self.s_id = s_id

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--sketch_id", default=0, type=int, help="sketch_id")
parser.add_argument("--verbose", default=0, type=int, help="sketch_id")
args = parser.parse_args()
sketch_id = args.sketch_id
#sketch_id = 26
if args.verbose > 0:
    VERBOSE = True
print("sketch_id", sketch_id)

#sketch_id = 1
folder = os.path.join("../data", str(sketch_id))
pickle_folder = os.path.join(folder, "pickle")
#with open(os.path.join(folder, "batches_results_normal.json"), "rb") as fp:
with open(os.path.join(folder, "batches_results_post.json"), "rb") as fp:
    batches_results = json.load(fp)
with open(os.path.join(pickle_folder, "pre_processed_sketch_2.pkl"), "rb") as fp:
    sketch = pickle.load(fp)
with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
    camera = pickle.load(fp)
batch = batches_results[-1]
fixed_strokes = deepcopy(batch["fixed_strokes"])

proxies = batch["final_proxies"]
my_dpi = 100
for p_id, p in enumerate(proxies):
    if p is not None and len(p) > 0 and len(fixed_strokes[p_id]) == 0:
        fixed_strokes[p_id] = p

non_fixed_stroke_ids = [s_id for s_id, s in enumerate(fixed_strokes)
                        if len(s) == 0]
#print(non_fixed_stroke_ids)

nb_strokes = len(sketch.strokes)
blue_normal = np.array(sns.color_palette("Set1", n_colors=2*nb_strokes))

if VERBOSE:
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes, #color_process=lambda s: "black",
                             color_process=lambda s: "black",
                             norm_global=True)
    if len(non_fixed_stroke_ids) > 0:
        sketch.display_strokes_2(fig=fig, ax=axes, #color_process=lambda s: blue_normal[s.id],
                                 #color_process=lambda s: ["green", "blue", "red", "yellow", "", "cyan"][s.axis_label],
                                 color_process=lambda s: "red",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=non_fixed_stroke_ids)
    #for inter in sketch.intersection_graph.get_intersections():
    #    if inter.stroke_ids[0] in non_fixed_stroke_ids or \
    #        inter.stroke_ids[1] in non_fixed_stroke_ids:
    #        if inter.is_tangential:
    #            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="red",
    #                                   zorder=5))
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
    tmp_file_name = os.path.join(folder, "non_symmetric_strokes.png")
    plt.savefig(tmp_file_name, dpi=my_dpi)
    #plt.show()
    plt.close(fig)
#exit()

#strokes_3d = [s["points_3d"] for s in results["strokes"]]
total_time = time()

planes = []
intersection_3ds = np.array([inter[0] for batch in batches_results for inter in batch["intersections"]]).tolist()
intersection_ids = np.array([inter[1] for batch in batches_results for inter in batch["intersections"]]).tolist()
with open(os.path.join(folder, "batches_results_normal.json"), "rb") as fp:
    batches_results = json.load(fp)
intersection_3ds += [inter[0] for batch in batches_results for inter in batch["intersections"]]
intersection_ids += [inter[1] for batch in batches_results for inter in batch["intersections"]]
intersection_3ds = np.array(intersection_3ds)
intersection_ids = np.array(intersection_ids)
with open(os.path.join(folder, "batches_results_post.json"), "rb") as fp:
    batches_results = json.load(fp)
symmetry_tools.assign_ellipse_minor_axes(sketch, camera, alignment_threshold=15.0,
                                         VERBOSE=False, data_folder=folder)


plane_counter = 0
for vec_id, inter in enumerate(intersection_3ds):
    inter_id = intersection_ids[vec_id]
    inter_2d = sketch.intersection_graph.get_intersections([inter_id])[0]
    s1_id, s2_id = inter_2d.stroke_ids
    if len(fixed_strokes[s1_id]) == 0 or len(fixed_strokes[s2_id]) == 0:
        continue
    if sketch.strokes[s1_id].axis_label == 5:
        continue
    plane_p = np.mean(inter)
    plane_p = inter
    s_1 = np.array(fixed_strokes[s1_id])
    vec_1 = s_1[-1] - s_1[0]
    if np.isclose(np.linalg.norm(vec_1), 0.0):
        continue
    vec_1 /= np.linalg.norm(vec_1)
    s_2 = np.array(fixed_strokes[s2_id])
    vec_2 = s_2[-1] - s_2[0]
    if np.isclose(np.linalg.norm(vec_2), 0.0):
        continue
    vec_2 /= np.linalg.norm(vec_2)
    plane_n = np.cross(vec_2, vec_1)
    if np.isclose(np.linalg.norm(plane_n), 0.0):
        continue
    plane_n /= np.linalg.norm(plane_n)
    #if plane_n.dot(np.array([1, 0, 0])) < 0.0:
    if plane_n.dot(np.array(camera.view_dir)) < 0.0:
        plane_n *= -1
    plane_counter += 1
    planes.append([plane_p, plane_n])

for vec_id, inter in enumerate(intersection_3ds):
    plane_p = inter
    for i in range(3):
        plane_n = np.zeros(3)
        plane_n[i] = 1.0
        planes.append([plane_p, plane_n])

planes = np.array(planes)
# cluster planes
pc = np.array([p for s_id, s in enumerate(fixed_strokes)
               for p in s
               if sketch.strokes[s_id].axis_label < 5 and len(s) > 0])
bbox = tools_3d.bbox_from_points(pc)
tools_3d.scale_bbox(bbox, 0.1)
bbox_diag = tools_3d.bbox_diag(bbox)

clustered_planes, clustered_plane_ids = symmetry_tools.cluster_planes(planes, epsilon=0.025*bbox_diag)
#clustered_planes, clustered_plane_ids = symmetry_tools.cluster_planes(planes, epsilon=0.001*bbox_diag)
#print("clustering: before ", len(planes), "after", len(clustered_planes))

ps.init()
#if VERBOSE:
#    for plane_id, (plane_point, plane_normal) in enumerate(planes):
#        ps.register_point_cloud("intersections", intersection_3ds)
#        for s_id, s in enumerate(fixed_strokes):
#            if len(s) == 0:
#                continue
#            nodes_array = np.array(s)
#            edges_array = np.array([[i, i+1] for i in range(len(nodes_array)-1)])
#            ps.register_curve_network("stroke_"+str(s_id), nodes=nodes_array,
#                                      edges=edges_array,
#                                      enabled=True,
#                                      color=blue_normal[s_id])
#        bbox_inters = symmetry_tools.bbox_plane_intersection(bbox, plane_point, plane_normal)
#        if len(bbox_inters) == 4:
#            bbox_inters = np.array(bbox_inters)
#            point_a = bbox_inters[0]
#            max_dist = np.linalg.norm(point_a - bbox_inters[1])
#            max_id = 1
#            for i in [2, 3]:
#                dist = np.linalg.norm(point_a - bbox_inters[i])
#                if dist > max_dist:
#                    max_id = i
#                    max_dist = dist
#            indices = [1, 2, 3]
#            indices.remove(max_id)
#            faces = np.array([[0, indices[0], max_id], [0, max_id, indices[1]]])
#            #if VERBOSE:
#            ps.register_surface_mesh(str(plane_id), vertices=bbox_inters,
#                                     faces=faces,
#                                     color=blue_normal[clustered_plane_ids[plane_id]],
#                                     enabled=True)
#    ps.show()
#
#if VERBOSE:
#    ps.remove_all_structures()
#    for plane_id, (plane_point, plane_normal) in enumerate(clustered_planes):
#        bbox_inters = symmetry_tools.bbox_plane_intersection(bbox, plane_point, plane_normal)
#        if len(bbox_inters) == 4:
#            bbox_inters = np.array(bbox_inters)
#            point_a = bbox_inters[0]
#            max_dist = np.linalg.norm(point_a - bbox_inters[1])
#            max_id = 1
#            for i in [2, 3]:
#                dist = np.linalg.norm(point_a - bbox_inters[i])
#                if dist > max_dist:
#                    max_id = i
#                    max_dist = dist
#            indices = [1, 2, 3]
#            indices.remove(max_id)
#            faces = np.array([[0, indices[0], max_id], [0, max_id, indices[1]]])
#            #if VERBOSE:
#            ps.register_surface_mesh("cluster_plane_"+str(plane_id), vertices=bbox_inters,
#                                     faces=faces)
#    ps.show()

foreshortenings = [1.0 - tools_3d.get_foreshortening_max(cand, camera.cam_pos)/tools_3d.line_3d_length(cand)
                   for cand in fixed_strokes if len(cand) > 0]
dist_ratios = [sketch.strokes[s_id].length()/tools_3d.line_3d_length(cand)
               for s_id, cand in enumerate(fixed_strokes) if len(cand) > 0]
median_dist_ratio = np.median(dist_ratios)

bbox_center = (bbox[:3]+bbox[3:])/2
#print("len(planes)", len(planes))
reconstructed_stroke_ids = []
parallel_intersections = {}
for inter in sketch.intersection_graph.get_intersections():
    if hasattr(inter, "is_parallel") and inter.is_parallel:
        if not inter.stroke_ids[0] in parallel_intersections.keys():
            parallel_intersections[inter.stroke_ids[0]] = [inter]
        else:
            parallel_intersections[inter.stroke_ids[0]].append(inter)
        if not inter.stroke_ids[1] in parallel_intersections.keys():
            parallel_intersections[inter.stroke_ids[1]] = [inter]
        else:
            parallel_intersections[inter.stroke_ids[1]].append(inter)

for s_id, s in enumerate(fixed_strokes):
    if VERBOSE:
        print(s_id, sketch.strokes[s_id].axis_label)
    #if VERBOSE and s_id != 26:
    #    continue
    ps.remove_all_structures()
    if len(s) > 0:
        continue
    #if VERBOSE:
    #    fig, axes = plt.subplots(nrows=1, ncols=1)
    #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
    #                        bottom=0.0,
    #                        top=1.0)
    #    sketch.display_strokes_2(fig=fig, ax=axes, #color_process=lambda s: "black",
    #                             color_process=lambda s: "black",
    #                             norm_global=True)
    #    sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "red",
    #                             display_strokes=[s_id],
    #                             linewidth_data=lambda s: 3.0, norm_global=True)

    #    axes.set_xlim(0, sketch.width)
    #    axes.set_ylim(sketch.height, 0)
    #    axes.axis("equal")
    #    axes.axis("off")
    #    plt.show()

    stroke_candidates = []
    #plane_points = np.array([plane_p for plane_p, plane_n in planes])
    #ps.register_point_cloud("plane_pts", plane_points.reshape(-1, 3))
    # Plane-based candidates
    plane_based_candidate_ids = []
    new_pts = np.array(sketch.strokes[s_id].linestring.linestring)
    # if an ellipse is overdrawn with an already existing ellipse, just paste it
    if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
        intersections = parallel_intersections[s_id]
        found_match = False
        for inter in intersections:
            #print(inter.stroke_ids)
            other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
            if inter.is_parallel and sketch.strokes[other_s_id].is_ellipse() and len(fixed_strokes[other_s_id]) > 0:
                plane_n = Plane.best_fit(fixed_strokes[other_s_id]).normal
                lifted_ellipse = np.array(camera.lift_polyline_to_plane(new_pts, fixed_strokes[other_s_id][0], plane_n))
                fixed_strokes[s_id] = lifted_ellipse
                found_match = True
                break
        # also check for overlap with other ellipses
        for other_s_id, other_s in enumerate(sketch.strokes):
            if not other_s.is_ellipse():
                continue
            if len(fixed_strokes[other_s_id]) == 0:
                continue
            overlap = MultiPoint(new_pts).convex_hull.intersects(other_s.linestring.linestring.convex_hull)
            if overlap:
                plane_n = Plane.best_fit(fixed_strokes[other_s_id]).normal
                lifted_ellipse = np.array(camera.lift_polyline_to_plane(new_pts, fixed_strokes[other_s_id][0], plane_n))
                fixed_strokes[s_id] = lifted_ellipse
                found_match = True
                break
        if found_match:
            continue

    if sketch.strokes[s_id].axis_label < 5:
        new_pts = np.array([new_pts[0], new_pts[-1]])
    for plane_id, plane in enumerate(clustered_planes):
        plane_p = plane[0]
        plane_n = plane[1]
        plane_n /= np.linalg.norm(plane_n)
        #if sketch.strokes[s_id].axis_label < 3:
        #    axis_vec = np.zeros(3)
        #    axis_vec[sketch.strokes[s_id].axis_label] = 1.0
        #    if sketch.strokes[s_id].axis_label < 3 and not np.isclose(np.abs(np.dot(plane_n, axis_vec)), 0.0, atol=1e-1):
        #        continue
        #if VERBOSE and s_id == 30:
        #    print(plane_id, plane_n, len(stroke_candidates))
        #    print(plane_n)
        #    print(np.dot(plane_n, axis_vec))
        proj_s = np.array(camera.lift_polyline_to_plane(new_pts, plane_p, plane_n))
        plane_based_candidate_ids.append(len(stroke_candidates))
        stroke_candidates.append(proj_s)
    # Intersection-based candidates
    # gather intersections with fixed strokes
    inter_3ds = []
    for inter in sketch.intersection_graph.get_intersections_by_stroke_id(s_id):
        other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
        if len(fixed_strokes[other_s_id]) == 0:
            continue
        other_s = np.array(fixed_strokes[other_s_id])
        #inter_3ds.append(camera.lift_point_close_to_polyline(inter.inter_coords,
        #                                                     other_s))
        inter_3ds.append(CurveIntersections(
            camera.lift_point_close_to_polyline(inter.inter_coords,other_s),
            other_s[-1], other_s_id))
    #inter_3ds = np.array(inter_3ds)
    #print("len(inter_3ds)", len(inter_3ds))
    # construct candidates from intersections
    if not (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()):
        inter_candidates = non_symmetric_tools.get_intersection_based_candidates(
            new_pts, sketch.strokes[s_id].axis_label,
            inter_3ds, camera, sketch.strokes[s_id].axis_label)
        stroke_candidates += inter_candidates
    #print("len(stroke_candidates)")
    #print(len(stroke_candidates))

    inter_params = []
    for inter in sketch.intersection_graph.get_intersections_by_stroke_id(s_id):
        inter_params.append(np.array(inter.mid_inter_param)[np.array(inter.stroke_ids) == s_id][0])
    if len(inter_params) == 0:
        max_line_coverage = 0.0
    else:
        max_line_coverage = max(inter_params) - min(inter_params)
    candidate_scores = []

    fix_inters = [[] for i in range(len(sketch.intersection_graph.get_intersections_by_stroke_id(s_id)))]
    for inter_id, inter in enumerate(sketch.intersection_graph.get_intersections_by_stroke_id(s_id)):
        other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
        if len(fixed_strokes[other_s_id]) == 0:
            continue
        other_s = np.array(fixed_strokes[other_s_id])
        fix_inter = np.array(camera.lift_point_close_to_polyline(
            inter.inter_coords, other_s))
        fix_inters[inter_id] = fix_inter

    candidate_has_intersection = np.zeros(len(stroke_candidates), dtype=np.bool)
    candidate_has_intersection[:] = False
    foreshortenings = np.zeros(len(stroke_candidates))
    out_of_box_cam_cand_ids = []
    for cand_id, cand in enumerate(stroke_candidates):
        # bbox check

        #if s_id == 46 and cand_id == 4:
        #    checks = []
        #    print(cand)
        #    for p in cand:
        #        checks.append(np.any(p < bbox[:3]) or np.any(p > bbox[3:]))
        #    print(checks)
        #    print(tools_3d.all_out_of_bbox(cand, bbox))
        #    #exit()
        # foreshortening
        #print(cand_id)
        #foreshortening = np.exp(-tools_3d.get_foreshortening(cand, camera.cam_pos)/len(cand))
        foreshortening = 1.0 - tools_3d.get_foreshortening_max(cand, camera.cam_pos)/tools_3d.line_3d_length(cand)
        foreshortenings[cand_id] = foreshortening
        if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
            u, v = tools_3d.get_basis_for_planar_point_cloud(cand)
            plane_n = np.cross(u, v)
            plane_n /= np.linalg.norm(plane_n)
            if sketch.strokes[s_id].closest_major_axis_deviation < 15.0 and sketch.strokes[s_id].closest_major_axis_id != np.argmax(np.abs(plane_n)):
                candidate_scores.append(0.0)
                continue
            ecc = tools_3d.get_ellipse_eccentricity(cand, u, v)
            score = np.exp(-ecc)
            foreshortenings[cand_id] = score

        if tools_3d.all_out_of_bbox(cand, bbox):
            out_of_box_cam_cand_ids.append(cand_id)
            candidate_scores.append(0.0)
            continue

        # Line coverage
        line_coverage = 0.0
        own_intersections_3d = []
        if max_line_coverage > 0.0 or (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()):
            inter_params = []
            for inter_id, inter in enumerate(sketch.intersection_graph.get_intersections_by_stroke_id(s_id)):
                other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
                if not (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()) and \
                        (sketch.strokes[other_s_id].axis_label == 5 and sketch.strokes[other_s_id].is_ellipse()):
                    continue
                #print(other_s_id)
                if len(fixed_strokes[other_s_id]) == 0:
                    continue
                other_s = np.array(fixed_strokes[other_s_id])
                #fix_inter = np.array(camera.lift_point_close_to_polyline(
                #    inter.inter_coords, other_s))
                fix_inter = fix_inters[inter_id]
                if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                    own_inter = camera.lift_point_to_plane(inter.inter_coords, cand[0], plane_n)
                else:
                    own_inter = camera.lift_point_close_to_polyline(
                        inter.inter_coords, cand)
                #print(own_inter, fix_inter)
                if own_inter is None or fix_inter is None:
                    continue
                own_inter = np.array(own_inter)
                #if cand_id == 14:
                #    print(other_s_id)
                #    print("inter_dist", np.linalg.norm(fix_inter-own_inter))
                #    print(tools_3d.line_3d_length(other_s))
                #    print(tools_3d.line_3d_length(cand))
                if np.linalg.norm(fix_inter-own_inter) < 0.1*max(tools_3d.line_3d_length(other_s),
                                                                  tools_3d.line_3d_length(cand)):
                    inter_params.append(np.array(inter.mid_inter_param)[np.array(inter.stroke_ids) == s_id][0])
                    own_intersections_3d.append(own_inter)
            #print("len(inter_params)", len(inter_params))
            if len(inter_params) > 0:
                line_coverage = (max(inter_params) - min(inter_params))/max_line_coverage
                candidate_has_intersection[cand_id] = True
                #print("has inter", s_id)
            else:
                candidate_scores.append(0.0)
                continue
            #if line_coverage > 0.0:
            #    candidate_has_intersection[cand_id] = True
        ell_coverage = 0.0
        if (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()) and len(own_intersections_3d) > 1:
            projected_inters = np.array([camera.project_point(inter) for inter in own_intersections_3d])
            ell_area = sketch.strokes[s_id].linestring.linestring.convex_hull.area
            inter_area = MultiPoint(projected_inters).convex_hull.area
            ell_coverage = inter_area/ell_area

        # check if any points lie behind the camera
        cam_vecs = np.array([p-np.array(camera.cam_pos) for p in cand])
        cam_vecs[:] /= np.linalg.norm(cam_vecs, axis=0)
        bbox_cam_vec = bbox_center - np.array(camera.cam_pos)
        bbox_cam_vec /= np.linalg.norm(bbox_cam_vec)
        dot_prods = np.dot(cam_vecs, bbox_cam_vec)
        if not(np.all(dot_prods < 0) or np.all(dot_prods > 0)):
            candidate_has_intersection[cand_id] = False

        #print("foreshortening", tools_3d.get_foreshortening_max(cand, camera.cam_pos), tools_3d.line_3d_length(cand))
        #if foreshortening < 0.5:
        #    candidate_has_intersection[cand_id] = False

        if sketch.strokes[s_id].axis_label < 3:
            # axis_alignment
            axis_alignment = 0.0
            vec = cand[-1] - cand[0]
            if np.isclose(np.linalg.norm(vec), 0.0):
                candidate_scores.append(0.0)
                continue
            vec /= np.linalg.norm(vec)
            axis_vec = np.zeros(3)
            axis_vec[sketch.strokes[s_id].axis_label] = 1.0
            axis_alignment = np.abs(np.dot(axis_vec, vec))
            if axis_alignment > 0.95:
                axis_alignment = 1.0
            #if axis_alignment < 0.5:
            #    candidate_scores.append(0.0)
            #    continue

            #print(line_coverage, axis_alignment, foreshortening)
            candidate_scores.append(0.4*line_coverage + 0.4*axis_alignment + 0.2*foreshortening)
            #print(candidate_scores[-1])
        else:
            dist_ratio = sketch.strokes[s_id].length()/tools_3d.line_3d_length(cand)
            #print(line_coverage, foreshortening, dist_ratio)
            #print("median_dist_ratio", median_dist_ratio, abs(median_dist_ratio - dist_ratio)/median_dist_ratio)
            #if abs(median_dist_ratio - dist_ratio)/median_dist_ratio > 0.1:
            #    candidate_scores.append(0.0)
            #elif line_coverage < 0.5 or foreshortening < 0.1:
            #    candidate_scores.append(0.0)
            #else:
            if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                #candidate_scores.append(0.8*ell_coverage + 0.2*foreshortening)
                candidate_scores.append(1.0*ell_coverage)
            else:
                candidate_scores.append(0.8*line_coverage + 0.2*foreshortening)
            #print(candidate_scores[-1])
            #candidate_scores.append(foreshortening)

    #print("len(candidate_scores")
    #print(len(candidate_scores))
    #if VERBOSE and s_id == 30:
    #    print(sketch.strokes[s_id].axis_label)
    #    print(foreshortenings)

    #    print(candidate_has_intersection)
    #    print(np.sum(candidate_has_intersection))
    #    print(plane_based_candidate_ids)
    #    ps.init()
    #    ps.remove_all_structures()
    #    for cand_id, cand in enumerate(stroke_candidates):
    #        if cand_id in out_of_box_cam_cand_ids:
    #            continue
    #        if np.isclose(candidate_scores[cand_id], 0.0):
    #            continue
    #        edges_array = np.array([[i, i+1] for i in range(len(cand)-1)])
    #        cand_ps = ps.register_curve_network("cand_"+str(cand_id), nodes=cand,
    #                                            edges=edges_array,
    #                                            enabled=True,
    #                                            color=blue_normal[s_id])
    #        cand_ps.add_scalar_quantity("score",
    #                                    np.repeat(candidate_scores[cand_id], len(cand)),
    #                                    enabled=True, vminmax=(0, 1))
    #    for tmp_s_id, s in enumerate(fixed_strokes):
    #        if len(s) == 0:
    #            continue
    #        nodes_array = np.array(s)
    #        edges_array = np.array([[i, i+1] for i in range(len(nodes_array)-1)])
    #        ps.register_curve_network("stroke_"+str(tmp_s_id), nodes=nodes_array,
    #                                  edges=edges_array,
    #                                  enabled=True,
    #                                  color=blue_normal[tmp_s_id])
    #    ps.register_point_cloud("cam_pos", np.array([camera.cam_pos]))
    #    ps.show()
    if len(candidate_scores) == 0:
        continue
    median_foreshortening = np.median(foreshortenings)
    for cand_id in range(len(stroke_candidates)):
        if foreshortenings[cand_id] < median_foreshortening:
            candidate_has_intersection[cand_id] = False
    #if s_id == 50:
    #    print(median_foreshortening)
    #    print(foreshortenings)
    #    print(candidate_has_intersection)
    if np.sum(candidate_has_intersection) > 0:
        stroke_candidates = np.array(stroke_candidates)[candidate_has_intersection]
        print(len(candidate_scores), len(candidate_has_intersection))
        candidate_scores = np.array(candidate_scores)[candidate_has_intersection]
    #print("len(candidate_scores)")
    #print(len(candidate_scores))

    if len(candidate_scores) == 0:
        continue
    best_score = np.max(candidate_scores)
    #print("best_score", best_score)
    #print("s_id", s_id)

    #if best_score < 0.5:
    #    continue

    #print("best_score", best_score)
    #if best_score < 0.8:
    #if candidate_has_intersection[np.argmax(candidate_scores)]:
    #    continue
    best_cand = stroke_candidates[np.argmax(candidate_scores)]
    #results["strokes"][s_id]["points_3d"] = best_cand.tolist()
    fixed_strokes[s_id] = np.array(best_cand)
    reconstructed_stroke_ids.append(s_id)
    #if not s_id in [50, 51, 52]:
    continue

    if len(inter_3ds) > 0:# and VERBOSE:
        fixed_inters = np.array([inter.inter_3d for inter in inter_3ds])
        ps.register_point_cloud("fixed_intersections", fixed_inters)
    edges_array = np.array([[i, i+1] for i in range(len(best_cand)-1)])
    cand_ps = ps.register_curve_network("best_cand", nodes=best_cand,
                                        edges=edges_array,
                                        enabled=True,
                                        color=blue_normal[s_id])
    cand_ps.add_scalar_quantity("score",
                                np.repeat(np.max(candidate_scores), len(cand)),
                                enabled=True, vminmax=(0, 1))
    for cand_id, cand in enumerate(stroke_candidates):
        edges_array = np.array([[i, i+1] for i in range(len(cand)-1)])
        cand_ps = ps.register_curve_network("cand_"+str(cand_id), nodes=cand,
                                  edges=edges_array,
                                  enabled=True,
                                  color=blue_normal[s_id])
        cand_ps.add_scalar_quantity("score",
                                   np.repeat(candidate_scores[cand_id], len(cand)),
                                   enabled=True, vminmax=(0, 1))
if VERBOSE:
    ps.init()
    ps.remove_all_structures()
    for s_id, s in enumerate(fixed_strokes):
        if len(s) == 0:
            continue
        nodes_array = np.array(s)
        edges_array = np.array([[i, i+1] for i in range(len(nodes_array)-1)])
        ps.register_curve_network("stroke_"+str(s_id), nodes=nodes_array,
                                  edges=edges_array,
                                  enabled=True,
                                  color=blue_normal[s_id])
    ps.register_point_cloud("cam_pos", np.array([camera.cam_pos]))
    ps.show()
#exit()

total_time = time() - total_time
times_file_name = os.path.join(folder, "runtimes_post_missing_strokes")
np.save(times_file_name, np.array([total_time]))
print("here")

#batches_results[-1]["with_non_symmetric_strokes"] = [np.array(s).tolist() for s in fixed_strokes if len(s) > 0]
batches_results[-1]["with_non_symmetric_strokes"] = [np.array(s).tolist() for s in fixed_strokes]
#for s_id, s in enumerate(fixed_strokes):
#    if len(s) == 0:
#        print(s_id)
with open(os.path.join(folder, "batches_results_non_symmetric.json"), "w") as fp:
    json.dump(batches_results, fp)
#print("HERE")

if VERBOSE:
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes, #color_process=lambda s: "black",
                             color_process=lambda s: "black",
                             norm_global=True)
    sketch.display_strokes_2(fig=fig, ax=axes, #color_process=lambda s: blue_normal[s.id],
                             #color_process=lambda s: ["green", "blue", "red", "yellow", "", "cyan"][s.axis_label],
                             color_process=lambda s: "red",
                             linewidth_data=lambda s: 3.0, norm_global=True,
                             display_strokes=reconstructed_stroke_ids)
    #for inter in sketch.intersection_graph.get_intersections():
    #    if inter.stroke_ids[0] in non_fixed_stroke_ids or \
    #        inter.stroke_ids[1] in non_fixed_stroke_ids:
    #        if inter.is_tangential:
    #            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="red",
    #                                   zorder=5))
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
    tmp_file_name = os.path.join(folder, "non_symmetric_strokes_reconstructed.png")
    plt.savefig(tmp_file_name, dpi=my_dpi)
    #plt.show()
    plt.close(fig)
exit()

#pc = np.array([p for s in results["strokes"] for p in s["points_3d"]
#               if s["axis_label"] < 5 and len(s["points_3d"]) > 0])
#bbox = tools_3d.bbox_from_points(pc)
#bbox_diag = tools_3d.bbox_diag(bbox)
#epsilon = 0.05*bbox_diag
#fine_pc = []
#for s in results["strokes"]:
#    if s["axis_label"] < 5 and len(s["points_3d"]) > 0:
#        seg = symmetry_tools.equi_resample_polyline(np.array(s["points_3d"]), epsilon)
#        for p in seg:
#            fine_pc.append(p)
#fine_pc = np.array(fine_pc)

#if VERBOSE:
#ps.remove_all_structures()
#for s_id, s in enumerate(fixed_strokes):
#    if len(s) == 0:
#        continue
#    nodes_array = np.array(s)
#    edges_array = np.array([[i, i+1] for i in range(len(nodes_array)-1)])
#    ps.register_curve_network("stroke_"+str(s_id), nodes=nodes_array,
#                              edges=edges_array,
#                              enabled=True,
#                              color=blue_normal[s_id])
#ps.show()
exit()
#ps.register_point_cloud("pc", pc)
#ps.register_point_cloud("fine_pc", fine_pc)

#plane1 = pyrsc.Plane()
#for i in range(5):
#    best_eq, best_inliers = plane1.fit(fine_pc, 0.01*bbox_diag)
#    print(best_eq)
#    print(best_inliers)
#    inlier_pc = fine_pc[best_inliers]
#    mask = np.ones(len(fine_pc), np.bool)
#    mask[best_inliers] = 0
#    fine_pc = fine_pc[mask]
#    ps.register_point_cloud("plane_"+str(i), inlier_pc)

if VERBOSE:
    ps.show()
output_file_name = os.path.join(output_folder, file_name.split("/")[-1])
print(output_file_name)
with open(output_file_name, 'w') as output_file:
    json.dump(results, output_file)
