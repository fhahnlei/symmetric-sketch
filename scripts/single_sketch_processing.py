import os, sys, inspect
import networkx as nx
import pickle
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
import sketch_wires
import matplotlib.pyplot as plt
import numpy as np
import symmetry_tools
import time_series_analysis_copy
from copy import deepcopy
import polyscope as ps
import tools_3d
import seaborn as sns
import cluster_proxies
import gurobi_models
from itertools import accumulate, product
from symmetry_tools import get_per_correspondence_sym_scores
from time import process_time, time
from pre_processing import pre_process, get_planes_scale_factors
from scipy.spatial import distance
import json
from skspatial.objects import Plane
import utils_plot
from candidate_correspondences import candidate_correspondences_2d_and_straight_3d, process_candidate_correspondences_curves

sym_plane_size = 0.05
sys.setrecursionlimit(10000)

PLOT_DEBUG = True
PLOT_STROKE_SEQUENCE = False
INTERSECTION_CONSTRAINT = False
PARTIAL_CURVES = True
max_scale_factor_modes = 3
max_nb_strokes = 30
# major-axis-aligned lines and curves
max_axes = [0, 1, 2, 3]
SKETCH_CONNECTIVITY = True
LINE_COVERAGE = True
SKETCH_COMPACITY = False

#TODO
SKETCH_SYMMETRY = False
SYMMETRIC_CURVE_FILTERING = False

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--file_name", default="toto", help="concept_sketch file name")
parser.add_argument("--max_scale_factor_modes", default=3, type=int, help="max_scale_factor_modes")
parser.add_argument("--max_nb_strokes", default=20, type=int, help="max_nb_strokes")
parser.add_argument("--include_curves", default=0, type=int, help="include_curves")
parser.add_argument("--plot_debug", default=0, type=int, help="plot_debug")
parser.add_argument("--intersection_constraint", default=1, type=int, help="intersection_constraint")
parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
parser.add_argument("--main_axis", default=-1, type=int, help="sketch_id")
parser.add_argument("--main_axis_file", default="", type=str, help="sketch_id")
parser.add_argument("--bootstrap_file", default="", type=str, help="")
parser.add_argument("--compute_batches", default=-1, type=int, help="sketch_id")
parser.add_argument("--recompute_candidates", default=-1, type=int, help="sketch_id")
parser.add_argument("--only_preprocessing", default=-1, type=int, help="sketch_id")
parser.add_argument("--filter_concept_lines", default=-1, type=int, help="sketch_id")

parser.add_argument("--ablate_line_coverage", default=-1, type=int, help="sketch_id")
parser.add_argument("--ablate_proximity", default=-1, type=int, help="sketch_id")
parser.add_argument("--ablate_curve_corr", default=-1, type=int, help="sketch_id")
parser.add_argument("--ablate_eccentricity", default=-1, type=int, help="sketch_id")
parser.add_argument("--ablate_tangential_intersections", default=-1, type=int, help="sketch_id")
parser.add_argument("--ablate_anchoring", default=-1, type=int, help="sketch_id")
parser.add_argument("--dry_run", default=-1, type=int, help="sketch_id")

args = parser.parse_args()

DRY_RUN = args.dry_run > 0
ABLATE_LINE_COVERAGE = args.ablate_line_coverage > 0
ABLATE_PROXIMITY = args.ablate_proximity > 0
ABLATE_CURVE_CORR = args.ablate_curve_corr > 0
ABLATE_ECCENTRICITY = args.ablate_eccentricity > 0
ABLATE_TANGENTIAL_INTERSECTIONS = args.ablate_tangential_intersections > 0
ABLATE_ANCHORING = args.ablate_anchoring > 0

if args.file_name != "toto":
    file_name = args.file_name
if args.max_scale_factor_modes != 3:
    max_scale_factor_modes = args.max_scale_factor_modes
if args.max_nb_strokes != 20:
    max_nb_strokes = args.max_nb_strokes
main_axis = args.main_axis
bootstrap_file = args.bootstrap_file
if args.include_curves == 1:
    max_axes.append(5)
if args.plot_debug == 0:
    PLOT_DEBUG = False
if args.intersection_constraint == 1:
    INTERSECTION_CONSTRAINT = True
elif args.intersection_constraint == 0:
    INTERSECTION_CONSTRAINT = False
sketch_id = args.sketch_id
#sketch_id = 60
#main_axis = 0

#regions [[0, 22], [23, 53], [54, 70], [71, 101]]
#regions [[0, 11], [12, 22], [23, 39], [40, 53], [54, 69], [70, 85], [86, 101]]
#regions [[0, 11], [12, 22], [23, 39], [40, 53], [54, 70], [71, 85], [86, 101]]

PLOT_DEBUG = False
#INTERSECTION_CONSTRAINT = False
max_axes.append(5)
max_axes = np.unique(max_axes).tolist()
selected_planes = [0, 1, 2]

data_folder = os.path.join("..", "data", str(sketch_id))
pickle_folder = os.path.join(data_folder, "pickle")
if not os.path.exists(pickle_folder):
    os.mkdir(pickle_folder)


file_name = os.path.join(data_folder, "sketch.svg")
file_name = os.path.join(data_folder, "sketch.json")
print("file_name", file_name)
if bootstrap_file != "":
    bootstrap_file = os.path.join(data_folder, bootstrap_file)
    with open(bootstrap_file, "r") as fp:
        bootstrap_data = json.load(fp)

if args.main_axis_file != "":
    main_axis = int(np.load(os.path.join(data_folder, args.main_axis_file))[0])
print("main_axis", main_axis)

PRE_PROCESS_SKETCH = False
GATHER_CANDIDATE_CORRESPONDENCES = False
PROCESS_CANDIDATE_CORRESPONDENCES_CURVES = False
if args.only_preprocessing >= 0:
    PRE_PROCESS_SKETCH = True
if args.recompute_candidates >= 0:
    GATHER_CANDIDATE_CORRESPONDENCES = True
    PROCESS_CANDIDATE_CORRESPONDENCES_CURVES = True
COMPUTE_BATCHES = False
if args.compute_batches >= 0:
    COMPUTE_BATCHES = True
#DRY_RUN = False

TRIPLE_INTERSECTIONS = True

ps.init()
if PLOT_DEBUG:
    ps.init()

total_time = time()
pre_process_time = time()
if PRE_PROCESS_SKETCH:
    sketch, camera, extreme_intersections_distances_per_stroke, stroke_lengths = pre_process(file_name, data_folder,
                                                                                             args.filter_concept_lines > 0)
    blue_normal = np.array(sns.color_palette("Set1", n_colors=len(sketch.strokes)))
    for s_id, s in enumerate(sketch.strokes):
        s.color = blue_normal[s_id]
    # save sketch and camera
    with open(os.path.join(pickle_folder, "camera.pkl"), "wb") as fp:
        pickle.dump(camera, fp)
    with open(os.path.join(pickle_folder, "extreme_intersections_distances_per_stroke.pkl"), "wb") as fp:
        pickle.dump(extreme_intersections_distances_per_stroke, fp)
    with open(os.path.join(pickle_folder, "stroke_lengths.pkl"), "wb") as fp:
        pickle.dump(stroke_lengths, fp)
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "wb") as fp:
        for s in sketch.intersection_graph.strokes:
            s.fitter = None
        for s in sketch.strokes:
            s.fitter = None
        pickle.dump(sketch, fp)
else:
    #with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
    #    camera = pickle.load(fp)
    #with open(os.path.join("../reconstructions_wires/full_json/designer2_chair_04_rough_full.json"), "r") as fp:
    _, _, camera = sketch_wires.read_data_wires(file_name)
    with open(os.path.join(pickle_folder, "extreme_intersections_distances_per_stroke.pkl"), "rb") as fp:
        extreme_intersections_distances_per_stroke = pickle.load(fp)
    with open(os.path.join(pickle_folder, "stroke_lengths.pkl"), "rb") as fp:
        stroke_lengths = pickle.load(fp)
    with open(os.path.join(pickle_folder, "pre_processed_sketch_2.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
        #for s in sketch.strokes:
        #    s.resample_rdp(0.0001)
        #sketch.fit_strokes()
    blue_normal = np.array(sns.color_palette("Set1", n_colors=len(sketch.strokes)))
    for s_id, s in enumerate(sketch.strokes):
        s.color = blue_normal[s_id]
    utils_plot.sketch_plot_simple(sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "reloaded_sketch.png"),
                                  color_fun=lambda s: s.color)
    utils_plot.sketch_plot_intersections(sketch, VERBOSE=False,
                                         output_file_name=os.path.join(data_folder, "reloaded_intersections_2.png"))
    #utils_plot.sketch_plot_intersections_triple(sketch, VERBOSE=False,
    #                                            output_file_name=os.path.join(data_folder, "intersections_triple.png"))
if args.only_preprocessing > 0:
    exit()
per_stroke_triple_intersections = []
if TRIPLE_INTERSECTIONS:
    per_stroke_triple_intersections = symmetry_tools.prepare_triple_intersections(sketch)
    #print(per_stroke_triple_intersections)
    #for s in per_stroke_triple_intersections:
    #    print(s)
pre_process_time = time() - pre_process_time
#utils_plot.sketch_plot_per_stroke_triple_intersections(sketch, per_stroke_triple_intersections)

#inter = sketch.intersection_graph.get_intersections([162])[0]
#print(inter)
#print(inter.mid_inter_param)
#print(inter.inter_params)
#s_id = 27
#s = sketch.strokes[27]
#neighbour = 18
#max_acc_radius = max(s.get_acc_radius(), sketch.strokes[neighbour].get_acc_radius())
#inter_lines = sketch.strokes[neighbour].get_intersection_fuzzy(
#    s, max_acc_radius)
#print(len(inter_lines))
## get parameters, lying on both lines
#first_inter_params = []
#snd_inter_params = []
#for inter_line in inter_lines:
#    first_params = [sketch.strokes[neighbour].linestring.project(Point(p)) for p in inter_line]
#    snd_params = [s.linestring.project(Point(p)) for p in inter_line]
#    if len(first_params) > 0:
#        first_inter_params.append([min(first_params), max(first_params)])
#    if len(snd_params) > 0:
#        snd_inter_params.append([min(snd_params), max(snd_params)])
#first_mid_inter_params = [(inter_param[0]+inter_param[-1])/2.0 for inter_param in first_inter_params]
#if sketch.strokes[neighbour].is_ellipse():
#    first_mid_inter_params = [inter_param[0] for inter_param in first_inter_params]
#snd_mid_inter_params = [(inter_param[0]+inter_param[-1])/2.0 for inter_param in snd_inter_params]
#if s.is_ellipse():
#    snd_mid_inter_params = [inter_param[0] for inter_param in first_inter_params]
#if not sketch.strokes[neighbour].is_ellipse() and s.is_ellipse():
#    inter_coords = np.array([sketch.strokes[neighbour].eval_polyline(mid_inter_param) for mid_inter_param in first_mid_inter_params])
#else:
#    inter_coords = [s.eval_polyline(mid_inter_param) for mid_inter_param in snd_mid_inter_params]
#print(first_mid_inter_params)
#plt.rcParams["figure.figsize"] = (10, 10)
#pickle_folder = os.path.join(data_folder, "pickle")
#fig, axes = plt.subplots(nrows=1, ncols=1)
#fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
#                    bottom=-0.1,
#                    top=1.0)
#sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#0000003D")
#
#axes.scatter(inter_coords[:, 0], inter_coords[:, 1])
#for inter_id in [162]:
#    inter = sketch.intersection_graph.get_intersections([inter_id])[0]
#    if 18 in inter.stroke_ids:
#        axes.add_artist(Circle(xy=inter.inter_coords, radius=2, color="red", zorder=5))
#
#axes.set_xlim(0, sketch.width)
#axes.set_ylim(sketch.height, 0)
#axes.axis("equal")
#axes.axis("off")
#plt.show()
#exit()

ref_correspondences = []
log_text = ""
if DRY_RUN and os.path.exists(os.path.join(pickle_folder, "reference_correspondences.pkl")):
    with open(os.path.join(pickle_folder, "reference_correspondences.pkl"), "rb") as fp:
        ref_correspondences = pickle.load(fp)

#s_id = 63
#other_s_id = 58
#s_id = 30
#other_s_id = 29
#axis_id = 2
#intersect = symmetry_tools.curve_symmetry_candidate(sketch.strokes[s_id], sketch.strokes[other_s_id],
#                                                    symmetry_tools.get_vanishing_triangle(
#                                                        sketch.strokes[s_id], np.array(camera.vanishing_points_coords[axis_id]),
#                                                        epsilon=1.5),
#                                                    axis_id,
#                                                    sketch, camera, dist_threshold=0.15, VERBOSE=True)
#print("intersect", intersect)
#exit()
#print("intersect", intersect)
##exit()
#detected_curve_correspondences = []
#cmap = sns.color_palette(list(sns.color_palette("Set1", n_colors=8))+
#                         list(sns.color_palette("Dark2", n_colors=8))+
#                         list(sns.color_palette("Accent", n_colors=8)),
#                         n_colors=len(sketch.strokes))
#for axis_id in range(3):
#    rot_angles = []
#    angle_stroke_ids = []
#    rot_vecs = []
#    for s_id, s in enumerate(sketch.strokes):
#        if not s.axis_label == 5 or s.is_ellipse():
#            continue
#        for other_s_id, other_s in enumerate(sketch.strokes):
#            if other_s_id >= s_id:
#                continue
#            if not other_s.axis_label == 5 or other_s.is_ellipse():
#                continue
#            intersect = symmetry_tools.curve_symmetry_candidate(s, other_s,
#                                                                symmetry_tools.get_vanishing_triangle(
#                                                                    s, np.array(camera.vanishing_points_coords[axis_id]),
#                                                                    epsilon=1.5),
#                                                                axis_id,
#                                                                sketch, camera, dist_threshold=0.15, VERBOSE=False)
#
#            if intersect:
#                print(s_id, other_s_id, axis_id)
#                detected_curve_correspondences.append([axis_id, s_id, other_s_id])
#
#wrong_correspondences = []
#missing_correspondences = []
#for corr in detected_curve_correspondences:
#    if not ref_correspondences[corr[0]].has_edge(corr[1], corr[2]):
#        wrong_correspondences.append(corr)
#        file_name = os.path.join("../data", "wrong_curve_correspondences", "_".join([str(sketch_id), str(corr[1]), str(corr[2]), str(corr[0])]))
#        utils_plot.sketch_plot_highlight(sketch, [corr[1], corr[2]], output_file_name=file_name, VERBOSE=False)
#for corr in detected_curve_correspondences:
#    file_name = os.path.join("../data", "detected_curve_correspondences", "_".join([str(sketch_id), str(corr[1]), str(corr[2]), str(corr[0])]))
#    utils_plot.sketch_plot_highlight(sketch, [corr[1], corr[2]], output_file_name=file_name, VERBOSE=False)
#for axis_id, ref_graph in enumerate(ref_correspondences):
#    for edge in ref_graph.edges():
#        if not(sketch.strokes[edge[0]].axis_label == 5 and not sketch.strokes[edge[0]].is_ellipse()):
#            continue
#        corr = [axis_id, max(edge), min(edge)]
#        if not corr in detected_curve_correspondences:
#            missing_correspondences.append(corr)
#            file_name = os.path.join("../data", "missing_curve_correspondences",
#                                     "_".join([str(sketch_id), str(edge[0]), str(edge[1]), str(axis_id)]))
#            utils_plot.sketch_plot_highlight(sketch, [edge[0], edge[1]], output_file_name=file_name, VERBOSE=False)
#print("detected_curve_correspondences")
#print(len(detected_curve_correspondences))
#print(len(wrong_correspondences))
#print("wrong_correspondences")
#print(wrong_correspondences)
#print(len(wrong_correspondences))
#print("missing_correspondences")
#print(missing_correspondences)
#print(len(missing_correspondences))
#
#exit()

symmetry_tools.assign_ellipse_minor_axes(sketch, camera, alignment_threshold=15.0,
                                         VERBOSE=False, data_folder=data_folder)

best_transformations = []
per_axis_per_stroke_candidate_correspondences = [[] for i in range(3)]

per_stroke_proxies = [[] for s_id in range(len(sketch.strokes))]
per_stroke_proxie_cand_ids = [[] for s_id in range(len(sketch.strokes))]
# list of (i_1, i_2, c_1, c_2, l, p_1, p_2)
global_candidate_correspondences = []

nb_planes = len(selected_planes)

per_axis_per_stroke_candidate_reconstructions = [[[] for s_id in range(len(sketch.strokes))]
                                                 for plane_id in range(nb_planes)]

gather_time = time()
print("GATHER")
if GATHER_CANDIDATE_CORRESPONDENCES:
    candidate_correspondences_2d_and_straight_3d(sketch, camera,
                                                 data_folder,
                                                 global_candidate_correspondences,
                                                 per_axis_per_stroke_candidate_reconstructions,
                                                 selected_planes, max_axes)
    with open(os.path.join(pickle_folder, "global_candidate_correspondences.pkl"), "wb") as fp:
        pickle.dump(global_candidate_correspondences, fp)
    with open(os.path.join(pickle_folder, "per_axis_per_stroke_candidate_reconstructions.pkl"), "wb") as fp:
        pickle.dump(per_axis_per_stroke_candidate_correspondences, fp)
else:
    with open(os.path.join(pickle_folder, "global_candidate_correspondences.pkl"), "rb") as fp:
        global_candidate_correspondences = pickle.load(fp)
    with open(os.path.join(pickle_folder, "per_axis_per_stroke_candidate_reconstructions.pkl"), "rb") as fp:
        per_axis_per_stroke_candidate_correspondences = pickle.load(fp)

if len(ref_correspondences) > 0:
    # check for missing 2d correspondences
    log_text += "Missing 2D correspondences\n"
    global_candidate_correspondence_graphs = [nx.Graph() for i in range(3)]
    for corr in global_candidate_correspondences:
        global_candidate_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
    for axis_id, ref_graph in enumerate(ref_correspondences):
        for edge in ref_graph.edges():
            if not global_candidate_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
                log_text += str(edge)+ ", axis "+ str(axis_id)+"\n"
                print(edge)
                ref_correspondences[axis_id].remove_edge(edge[0], edge[1])
    log_text += "\n"

if PROCESS_CANDIDATE_CORRESPONDENCES_CURVES:
    curve_epsilons = process_candidate_correspondences_curves(sketch, camera,
                                                              data_folder,
                                                              global_candidate_correspondences,
                                                              per_axis_per_stroke_candidate_reconstructions,
                                                              per_stroke_proxies)
    per_correspondence_sym_scores = get_per_correspondence_sym_scores(
        sketch, curve_epsilons, global_candidate_correspondences)
    with open(os.path.join(pickle_folder, "global_candidate_correspondences_curves.pkl"), "wb") as fp:
        pickle.dump(global_candidate_correspondences, fp)
    with open(os.path.join(pickle_folder, "per_axis_per_stroke_candidate_reconstructions_curves.pkl"), "wb") as fp:
        pickle.dump(per_axis_per_stroke_candidate_correspondences, fp)
    with open(os.path.join(pickle_folder, "per_stroke_proxies.pkl"), "wb") as fp:
        pickle.dump(per_stroke_proxies, fp)
    with open(os.path.join(pickle_folder, "per_correspondence_sym_scores.pkl"), "wb") as fp:
        pickle.dump(per_correspondence_sym_scores, fp)
else:
    with open(os.path.join(pickle_folder, "global_candidate_correspondences_curves.pkl"), "rb") as fp:
        global_candidate_correspondences = pickle.load(fp)
    with open(os.path.join(pickle_folder, "per_axis_per_stroke_candidate_reconstructions_curves.pkl"), "rb") as fp:
        per_axis_per_stroke_candidate_correspondences = pickle.load(fp)
    with open(os.path.join(pickle_folder, "per_stroke_proxies.pkl"), "rb") as fp:
        per_stroke_proxies = pickle.load(fp)
    with open(os.path.join(pickle_folder, "per_correspondence_sym_scores.pkl"), "rb") as fp:
        per_correspondence_sym_scores = pickle.load(fp)

#print(per_correspondence_sym_scores)
symmetry_tools.filter_weak_curve_correspondences(per_correspondence_sym_scores, global_candidate_correspondences,
                                                 sketch)
gather_time = time() - gather_time
#for corr_id, corr in enumerate(global_candidate_correspondences):
#    if (corr[0] == 23 and corr[1] == 16 and corr[4] == 0) or \
#        (corr[0] == 26 and corr[1] == 16 and corr[4] == 0):
#        print(corr[:2])
#        print(per_correspondence_sym_scores[corr_id])
#        utils_plot.plot_curves([corr[2], corr[3]], name_prefix="s_")
#        ps.show()
#exit()

#ps.init()
#for corr_id, corr in enumerate(global_candidate_correspondences):
#    if 27 in [corr[0], corr[1]] and 25 in [corr[0], corr[1]]:
#    #if 27 in [corr[0], corr[1]]:
#        for curve_id, curve in enumerate([corr[2], corr[3]]):
#            curve = np.array(curve)
#            c = ps.register_curve_network(str(corr_id)+"_"+str(corr[curve_id]),
#                                         curve, edges=np.array([[i, i+1] for i in range(len(curve)-1)]))
#    if 27 in [corr[0], corr[1]] and 25 in [corr[0], corr[1]]:
#        print(corr)
##            u, v = tools_3d.get_basis_for_planar_point_cloud(curve)
##            ecc = tools_3d.get_ellipse_eccentricity(curve, u, v)
##            score = np.exp(-ecc)
##            eccies = np.zeros(len(curve))
##            eccies[:] = ecc
##            c.add_scalar_quantity("ecc", eccies, defined_on="nodes", enabled=True)
#ps.show()

print("len(global_candidate_correspondences)")
print(len(global_candidate_correspondences))

if len(ref_correspondences) > 0:
    # check for missing 2d correspondences
    log_text += "Missing 3D reconstructions\n"
    global_candidate_correspondence_graphs = [nx.Graph() for i in range(3)]
    for corr in global_candidate_correspondences:
        global_candidate_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
    for axis_id, ref_graph in enumerate(ref_correspondences):
        for edge in ref_graph.edges():
            if not global_candidate_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
                log_text += str(edge)+ ", axis "+ str(axis_id)+"\n"
                ref_correspondences[axis_id].remove_edge(edge[0], edge[1])
    log_text += "\n"


if COMPUTE_BATCHES:
    # batches = time_series_analysis_copy.get_batch_decomposition(
    #     global_candidate_correspondences, data_folder, sketch=sketch, min_dist=15)
    batches = time_series_analysis_copy.get_batch_decomposition(
        global_candidate_correspondences,
        #[corr for corr in global_candidate_correspondences if corr[4] == main_axis],
        data_folder, sketch=sketch, min_dist=10)
    #global_candidate_correspondences, data_folder, sketch=sketch, min_dist=10)
    print("batches", batches)
    with open(os.path.join(pickle_folder, "batches.pkl"), "wb") as fp:
        pickle.dump(batches, fp)
    #exit()
else:
    with open(os.path.join(pickle_folder, "batches.pkl"), "rb") as fp:
        batches = pickle.load(fp)
#batches = [[0, 20]]

print("batches", batches)
#exit()

#print(len(global_candidate_correspondences))
#for corr in global_candidate_correspondences:
#    if 1 in corr[:2] and 4 in corr[:2]:
#        print(corr)
#for corr in global_candidate_correspondences:
#    if 45 == corr[0] and 29 == corr[1] and corr[4] == 1:
#        print(corr)
#print(len(global_candidate_correspondences))
#exit()

fixed_strokes = [[] for i in range(len(sketch.strokes))]
if bootstrap_file != "":
    for batch in bootstrap_data:
        for s_id, s in enumerate(batch["final_proxies"]):
            if len(s) > 0:
                #print(np.array(s))
                fixed_strokes[s_id] = np.array(s)

fixed_intersections = []
fixed_line_coverages = np.zeros(len(sketch.strokes))
fixed_planes = []
fixed_planes_scale_factors = []
fixed_intersections = []
# get different scaling factors for axes 1 and 2
# loop through scaling factors and solve BIP

batches_data = {"batches": [],
                "proj_mat": camera.proj_mat}
batch_0_diag = -1
blue_normal = np.array(sns.color_palette("Set1", n_colors=len(sketch.strokes)))

final_correspondence_graphs = [nx.Graph(), nx.Graph(), nx.Graph()]
#if np.sum([len(missing_correspondences[i]) for i in range(3)]) > 0:
#    exit()
#exit()

accumulated_obj_value = 0

reconstruction_time = time()
batches_results = []
for batch_id, batch in enumerate(batches):
    #if batch_id > 1:
    #    exit()
    batch_folder = os.path.join(data_folder, "batch_"+str(batch_id))
    if bootstrap_file != "":
        batch_folder = os.path.join(data_folder, "bootstrap_batch_"+str(batch_id))
    if not os.path.exists(batch_folder):
        os.mkdir(batch_folder)
    #if batch_id > 0:
    #    exit()
    #    break
    print("BATCH #", batch_id, ":", batch)
    enforce_strokes = []
    #if 96 > batch[0] and 96 <= batch[1]:
    #    enforce_strokes = [27, 96]

    if PLOT_DEBUG:
        non_fixed_strokes = [s_id for s_id in range(0, batch[0])
                             if len(fixed_strokes[s_id]) == 0]
        utils_plot.sketch_plot_highlight(sketch, non_fixed_strokes,
                                         VERBOSE=False,
                                         output_file_name=os.path.join(batch_folder, "batch_"+str(batch_id)+"_non_fixed_strokes.png"))

    scale_factors_time = time()
    per_axis_per_stroke_candidate_reconstructions = symmetry_tools.update_candidate_strokes(
        fixed_strokes, global_candidate_correspondences, batch, len(sketch.strokes))
    nb_rec = [len(s) for axis_id in range(3) for s in per_axis_per_stroke_candidate_reconstructions[axis_id]]
    planes_scale_factors = get_planes_scale_factors(
        sketch, camera, batch, batch_id, selected_planes, fixed_strokes,
        fixed_planes_scale_factors, per_axis_per_stroke_candidate_reconstructions,
        max_scale_factor_modes, batch_folder, PLOT_DEBUG=False)
    if main_axis != -1 and batch_id > 0:
        planes_scale_factors[main_axis] = [planes_scale_factors[main_axis][0]]
    print("scale_factors_time ", time() - scale_factors_time)

    print("planes_scale_factors")
    print(planes_scale_factors)

    planes_scale_factors_numbers = [range(len(planes_scale_factor))
                                    for planes_scale_factor in planes_scale_factors]
    best_comb = -1
    best_obj_value = -10000.0
    ref_best_obj_value = -1.0
    ref_best_comb = -1
    total_start_time = time()
    nb_planes_comb = len(list(product(*planes_scale_factors_numbers)))

    #for planes_comb_id, planes_comb in enumerate(planes_combs):
    if batch_id > 0:
        orig_plane_triplet = symmetry_tools.get_plane_triplet(
            np.array(camera.cam_pos), planes_scale_factors[0][0],
            planes_scale_factors[1][0], planes_scale_factors[2][0])
        already_distant_plane_triplet = False
    planes_combs = list(product(*planes_scale_factors_numbers))
    #if batch_id == 1:
    #    planes_combs = [planes_combs[1], planes_combs[5]]
    #for planes_comb_id, planes_comb in enumerate(product(*planes_scale_factors_numbers)):
    for planes_comb_id, planes_comb in enumerate(planes_combs):
        #if DRY_RUN:
        #    ref_log_file = os.path.join(data_folder, "ref_log.txt")
        #    if os.path.exists(ref_log_file):
        #        file1 = open(ref_log_file)
        #        next_line_comb = False
        #        for l in file1.readlines():
        #            if next_line_comb:
        #                #c = tuple(np.array(l.split("(")[-1].split(")")[0].split(", "), dtype=int))
        #                c = tuple(np.array(l.split("(")[1].split(")")[0].split(", "), dtype=int))
        #                best_comb = c
        #                break
        #            if "Batch_id: "+str(batch_id) in l:
        #                next_line_comb = True
        #        file1.close()
        #        #break

        #if batch_id == 0:
        #    best_comb = (0, 1, 0)
        #if batch_id == 1:
        #best_comb = (0, 0, 1)
        #break
        #plane_time = time()
        print(str(planes_comb_id)+"/"+str(nb_planes_comb))
        print(planes_comb)
        print("best_obj_value", best_obj_value)
        #if batch_id == 1:
        #    break
        planes_point_normal = []
        #all_refl_mats = [[], [], []]
        #if batch_id > 0:
        #    for fixed_p_s_f in fixed_planes_scale_factors:
        #        planes_point_normal = []
        #        for i in range(3):
        #            focus_vp = i
        #            sym_plane_point = np.zeros(3, dtype=np.float)
        #            sym_plane_normal = np.zeros(3, dtype=np.float)
        #            sym_plane_normal[focus_vp] = 1.0
        #            sym_plane_point = camera.cam_pos + \
        #                              fixed_p_s_f[i]*(np.array(sym_plane_point) - camera.cam_pos)
        #            planes_point_normal.append([sym_plane_point, sym_plane_normal])
        #        for i, (p, n) in enumerate(planes_point_normal):
        #            refl_mat = tools_3d.get_reflection_mat(p, n)
        #            all_refl_mats[i].append(refl_mat)

        #    planes_point_normal = []
        #    for i in range(3):
        #        focus_vp = i
        #        sym_plane_point = np.zeros(3, dtype=np.float)
        #        sym_plane_normal = np.zeros(3, dtype=np.float)
        #        sym_plane_normal[focus_vp] = 1.0
        #        sym_plane_point = camera.cam_pos + \
        #                          planes_scale_factors[i][planes_comb[i]]*(np.array(sym_plane_point) - camera.cam_pos)
        #        planes_point_normal.append([sym_plane_point, sym_plane_normal])
        #    for i, (p, n) in enumerate(planes_point_normal):
        #        refl_mat = tools_3d.get_reflection_mat(p, n)
        #        all_refl_mats.append(refl_mat)
        check_time = time()

        refl_mats = []

        planes_point_normal = []
        if batch_id > 0 or np.sum([len(fixed_strokes[i]) for i in range(len(fixed_strokes))]) > 0:
            for i in range(3):
                focus_vp = i
                sym_plane_point = np.zeros(3, dtype=np.float)
                sym_plane_normal = np.zeros(3, dtype=np.float)
                sym_plane_normal[focus_vp] = 1.0
                sym_plane_point = camera.cam_pos + \
                                  planes_scale_factors[i][planes_comb[i]]*(np.array(sym_plane_point) - camera.cam_pos)
                planes_point_normal.append([sym_plane_point, sym_plane_normal])
            for p, n in planes_point_normal:
                refl_mat = tools_3d.get_reflection_mat(p, n)
                refl_mats.append(refl_mat)
            current_plane_triplet = symmetry_tools.get_plane_triplet(
                np.array(camera.cam_pos),
                planes_scale_factors[0][planes_comb[0]],
                planes_scale_factors[1][planes_comb[1]],
                planes_scale_factors[2][planes_comb[2]],
            )
            #plane_triplet_dist = np.linalg.norm(orig_plane_triplet - current_plane_triplet)
        local_planes_scale_factors = [planes_scale_factors[0][planes_comb[0]],
                                      planes_scale_factors[1][planes_comb[1]],
                                      planes_scale_factors[2][planes_comb[2]]]
        all_planes_scale_factors = deepcopy(fixed_planes_scale_factors)
        all_planes_scale_factors.append(local_planes_scale_factors)
        #print("check_time", time()-check_time)
        check_time = time()
        if DRY_RUN:
            local_candidate_correspondences, correspondence_ids = symmetry_tools.copy_correspondences_batch(
                global_candidate_correspondences, batch, fixed_strokes, refl_mats,
                local_planes_scale_factors,
                camera, sketch, ref_correspondences=ref_correspondences)
        else:
            local_candidate_correspondences, correspondence_ids = symmetry_tools.copy_correspondences_batch(
                global_candidate_correspondences, batch, fixed_strokes, refl_mats,
                local_planes_scale_factors,
                camera, sketch)
        #print("check_time", time()-check_time)
        check_time = time()
        if len(local_candidate_correspondences) == 0:
            continue
        local_per_correspondence_sym_scores = [per_correspondence_sym_scores[corr_id]
                                               for corr_id in correspondence_ids]

        correspondences, per_stroke_candidates = symmetry_tools.extract_correspondence_information(
            local_candidate_correspondences, sketch, VERBOSE=False)

        per_stroke_proxies = [[] for s_id in range(len(sketch.strokes))]

        #for corr in global_candidate_correspondences:
        #    if 28 in corr[:2]:
        #        print(corr)
        #        exit()

        cluster_proxies.cluster_proxy_strokes(local_candidate_correspondences,
                                                            per_stroke_proxies, sketch)
        #print("check_time", time()-check_time)
        check_time = time()
        #print("plane_time", time()-plane_time)
        #plane_time = time()
        ## debugging sanity check
        #for axis_id, ref_graph in enumerate(ref_correspondences):
        #    for s_id in range(len(sketch.strokes)):
        #        if not (s_id >= batch[0] and s_id <= batch[1]):
        #            continue
        #        if not ref_graph.has_node(s_id):
        #            continue
        #        ideal_neighbors = list(ref_graph.neighbors(s_id))
        #        if any(i < batch[0] or i > batch[1] for i in ideal_neighbors):
        #            continue
        #        real_proxies = {}
        #        for corr in local_candidate_correspondences:
        #            #if s_id == 40 and corr[0] == 40 and corr[1] == 29 and corr[4] == 0:
        #            #    print(corr)
        #            if corr[4] != axis_id:
        #                continue
        #            if corr[5] == -1:
        #                continue
        #            other_s_id = -1
        #            if corr[0] == s_id:
        #                other_s_id = corr[1]
        #            elif corr[1] == s_id:
        #                other_s_id = corr[0]
        #            else:
        #                continue
        #            for proxy_id in corr[5]:
        #                if proxy_id in real_proxies.keys():
        #                    real_proxies[proxy_id].append(other_s_id)
        #                else:
        #                    real_proxies[proxy_id] = [other_s_id]
        #        found_any_good_proxy = False
        #        for proxy in real_proxies.values():
        #            if all(i in proxy for i in ideal_neighbors):
        #                found_any_good_proxy = True
        #                continue
        #        if not found_any_good_proxy:
        #            print("Missing proxy clustering: s_id", s_id, ", axis_id", axis_id, ", ideal_neighbors", ideal_neighbors, ", real_proxies", real_proxies)
        # debugging sanity check
        #if len(ref_correspondences) > 0:
        #    for s_id in range(len(sketch.strokes)):
        #        if s_id < batch[0] or s_id > batch[1]:
        #            continue
        #        ideal_neighbors = []
        #        for axis_id, ref_graph in enumerate(ref_correspondences):
        #            if not (s_id >= batch[0] and s_id <= batch[1]):
        #                continue
        #            if not ref_graph.has_node(s_id):
        #                continue
        #            for i_n in list(ref_graph.neighbors(s_id)):
        #                ideal_neighbors.append((i_n, axis_id))
        #                #if any(i < batch[0] or i > batch[1] for i in ideal_neighbors):
        #        #    continue
        #        real_proxies = {}
        #        for corr in local_candidate_correspondences:
        #            #if corr[4] != axis_id:
        #            #    continue
        #            if corr[5] == -1:
        #                continue
        #            other_s_id = -1
        #            if corr[0] == s_id:
        #                other_s_id = corr[1]
        #            elif corr[1] == s_id:
        #                other_s_id = corr[0]
        #            else:
        #                continue
        #            for proxy_id in corr[5]:
        #                if proxy_id in real_proxies.keys():
        #                    real_proxies[proxy_id].append((other_s_id, corr[4]))
        #                else:
        #                    real_proxies[proxy_id] = [(other_s_id, corr[4])]
        #        found_any_good_proxy = False
        #        for proxy in real_proxies.values():
        #            if all(i in proxy for i in ideal_neighbors):
        #                found_any_good_proxy = True
        #                continue
        #        if not found_any_good_proxy:
        #            print("Missing proxy clustering: s_id", s_id, ", ideal_neighbors", ideal_neighbors, ", real_proxies", real_proxies)
        #exit()

        eccentricity_weights = [[] for i in range(len(sketch.strokes))]
        for s_id, s in enumerate(sketch.strokes):
            if not s.is_ellipse():
                continue
            for p in per_stroke_proxies[s_id]:
                u, v = tools_3d.get_basis_for_planar_point_cloud(p)
                ecc = tools_3d.get_ellipse_eccentricity(p, u, v)
                score = np.exp(-ecc)
                eccentricity_weights[s_id].append(score)
        #is_ellipse_time = time()
        for s in sketch.strokes:
            tmp_check = s.is_ellipse()
        #print("is_ellipse_time", time()-is_ellipse_time)
        #print("plane_time", time()-plane_time)
        #plane_time = time()

        intersections_3d = []
        intersections_3d_simple = []
        inter_time = time()
        intersections_3d_simple = symmetry_tools.get_intersections_simple_batch(
            per_stroke_proxies, sketch, camera, batch, fixed_strokes)
        #print("inter_time", time()-inter_time)
        check_time = time()
        #print("plane_time", time()-plane_time)
        #print(len(intersections_3d_simple))
        #exit()
        #plane_time = time()
        #candidates_folder = os.path.join(planes_comb_folder, "candidates")
        #if not os.path.exists(candidates_folder):
        #    os.mkdir(candidates_folder)
        #utils_plot.plot_candidate_correspondences(
        #    sketch, correspondences, local_planes_scale_factors, camera,
        #    intersections_3d_simple, candidates_folder)

        if batch_id == 0:
            min_dists = []
            epsilons = []
            for inter in intersections_3d_simple:
                #print(inter.cam_depths)
                first_cam_depths = inter.cam_depths[0]
                if len(first_cam_depths) == 0:
                    first_cam_depths = [inter.fix_depth]
                snd_cam_depths = inter.cam_depths[1]
                if len(snd_cam_depths) == 0:
                    snd_cam_depths = [inter.fix_depth]
                min_dists.append(np.min(distance.cdist(np.array(first_cam_depths).reshape(-1, 1),
                                                       np.array(snd_cam_depths).reshape(-1, 1))))
                epsilons.append(inter.epsilon)
            #median_dist = np.quantile(min_dists, 0.8)
        #print("median_dist", median_dist)
        #for inter_id in range(len(intersections_3d_simple)):
        #    intersections_3d_simple[inter_id].epsilon = median_dist

        line_coverages_simple = []
        if LINE_COVERAGE:
            line_coverages_simple = symmetry_tools.get_line_coverages_simple(intersections_3d_simple, sketch,
                                                                                       extreme_intersections_distances_per_stroke)
        #print("check_time", time()-check_time)
        check_time = time()

        per_axis_median_lengths = []

        print("start model construction")
        suppress_correspondences = []
        obj_value, final_correspondences, final_proxis, final_intersections, final_line_coverages, \
        final_end_intersections, solution_terms, final_full_anchored_stroke_ids, final_half_anchored_stroke_ids, final_triple_intersections = \
            gurobi_models.get_best_correspondences_simple_batch(
                local_candidate_correspondences, per_stroke_proxies, intersections_3d_simple, line_coverages_simple,
                local_per_correspondence_sym_scores,
                batch, fixed_strokes, fixed_intersections, sketch, batch_folder,
                sketch_connectivity=SKETCH_CONNECTIVITY, return_final_strokes=True,
                line_coverage=LINE_COVERAGE, eccentricity_weights=eccentricity_weights,
                stroke_lengths=stroke_lengths, intersection_constraint=INTERSECTION_CONSTRAINT,
                best_obj_value=best_obj_value-1,
                sketch_compacity=SKETCH_COMPACITY, camera=camera, per_axis_median_lengths=per_axis_median_lengths,
                end_intersections=False, per_stroke_triple_intersections=per_stroke_triple_intersections,
                ref_correspondences=ref_correspondences,
                main_axis=main_axis,
                ABLATE_LINE_COVERAGE=ABLATE_LINE_COVERAGE,
                ABLATE_PROXIMITY=ABLATE_PROXIMITY,
                ABLATE_CURVE_CORR=ABLATE_CURVE_CORR,
                ABLATE_ECCENTRICITY=ABLATE_ECCENTRICITY,
                ABLATE_TANGENTIAL_INTERSECTIONS=ABLATE_TANGENTIAL_INTERSECTIONS,
                ABLATE_ANCHORING=ABLATE_ANCHORING
            )

        print(solution_terms)
        print("obj_value", obj_value)
        print()
        #if len(ref_correspondences) > 0:
        #    ref_obj_value, ref_final_correspondences, ref_final_proxis, ref_final_intersections, \
        #    ref_final_line_coverages, ref_solution_terms = \
        #        symmetry_sketch_selection_gurobi.get_best_correspondences_simple_batch(
        #            local_candidate_correspondences, per_stroke_proxies, intersections_3d_simple, line_coverages_simple,
        #            local_per_correspondence_sym_scores,
        #            batch, fixed_strokes, sketch, batch_folder,
        #            sketch_connectivity=SKETCH_CONNECTIVITY, return_final_strokes=True,
        #            line_coverage=LINE_COVERAGE, eccentricity_weights=eccentricity_weights,
        #            stroke_lengths=stroke_lengths, intersection_constraint=INTERSECTION_CONSTRAINT,
        #            best_obj_value=best_obj_value-1, ref_correspondences=ref_correspondences,
        #            enforce_strokes=enforce_strokes)
        #    if ref_obj_value > ref_best_obj_value:
        #        ref_best_obj_value = ref_obj_value
        #        ref_best_comb = planes_comb
        #if DRY_RUN:
        #    obj_value = ref_obj_value
        if np.isclose(obj_value, -1.0):
            continue
        #if batch_id == 1:
        #    planes_comb_folder = os.path.join(batch_folder, "planes_comb_"+str(planes_comb_id))
        #    if not os.path.exists(planes_comb_folder):
        #        os.mkdir(planes_comb_folder)
        #    utils_plot.plot_solutions(sketch, final_correspondences, local_planes_scale_factors,
        #                              camera, final_intersections, final_proxis, fixed_strokes, planes_comb_folder)

        ## debugging sanity check
        #final_correspondence_graphs = [nx.Graph() for i in range(3)]
        #for corr in final_correspondences:
        #    final_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
        #for axis_id, ref_graph in enumerate(ref_correspondences):
        #    for edge in ref_graph.edges():
        #        if min(edge) >= batch[0] and max(edge) <= batch[1]:
        #            if not final_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
        #                print("Missing correspondence: ", edge, ", axis ", axis_id)
        #solution_folder = os.path.join(planes_comb_folder, "solution")
        #if not os.path.exists(solution_folder):
        #    os.mkdir(solution_folder)
        #utils_plot.plot_solutions(sketch, final_correspondences, local_planes_scale_factors,
        #                          camera, final_intersections, final_proxis, fixed_strokes, solution_folder)
        #exit()

        #if obj_value-plane_penalizer > best_obj_value:
        if obj_value > best_obj_value:
            print("new_best_comb", planes_comb)
            best_obj_value = obj_value
            best_comb = planes_comb
        #break

    accumulated_obj_value += best_obj_value
    print("best_comb")
    print(best_comb)
    print("best_scale_factors")
    if best_comb == -1:
        fixed_planes_scale_factors.append([])
        continue
    print(planes_scale_factors[0][best_comb[0]])
    print(planes_scale_factors[1][best_comb[1]])
    print(planes_scale_factors[2][best_comb[2]])
    if len(ref_correspondences) > 0:
        # check if the best plane has been chosen
        if ref_best_comb == -1:
            ref_best_comb = best_comb
        log_text += "Batch_id: "+str(batch_id)+", "+str(batch)+"\nbest_comb: "+str(best_comb)+", ref_best_comb: "+str(ref_best_comb)+"\n"
        log_text += "\n"

    local_planes_scale_factors = [planes_scale_factors[0][best_comb[0]],
                                  planes_scale_factors[1][best_comb[1]],
                                  planes_scale_factors[2][best_comb[2]]]
    # output best alignment
    #local_candidate_correspondences = global_candidate_correspondences.copy()
    best_planes = []
    best_planes_point_normal = []
    for i in selected_planes:
        focus_vp = i
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[focus_vp] = 1.0
        sym_plane_point = camera.cam_pos + \
                          planes_scale_factors[selected_planes[i]][best_comb[selected_planes[i]]]*(np.array(sym_plane_point) - camera.cam_pos)
        best_planes.append(Plane(sym_plane_point, sym_plane_normal))
        best_planes_point_normal.append([sym_plane_point, sym_plane_normal])
    refl_mats = []
    for p, n in best_planes_point_normal:
        refl_mat = tools_3d.get_reflection_mat(p, n)
        refl_mats.append(refl_mat)
    #all_refl_mats = [[], [], []]
    #planes_point_normal = []
    #if batch_id > 0:
    #    for fixed_p_s_f in fixed_planes_scale_factors:
    #        planes_point_normal = []
    #        for i in range(3):
    #            focus_vp = i
    #            sym_plane_point = np.zeros(3, dtype=np.float)
    #            sym_plane_normal = np.zeros(3, dtype=np.float)
    #            sym_plane_normal[focus_vp] = 1.0
    #            sym_plane_point = camera.cam_pos + \
    #                              fixed_p_s_f[i]*(np.array(sym_plane_point) - camera.cam_pos)
    #            planes_point_normal.append([sym_plane_point, sym_plane_normal])
    #        for i, (p, n) in enumerate(planes_point_normal):
    #            refl_mat = tools_3d.get_reflection_mat(p, n)
    #            all_refl_mats[i].append(refl_mat)

    #    planes_point_normal = []
    #    for i in range(3):
    #        focus_vp = i
    #        sym_plane_point = np.zeros(3, dtype=np.float)
    #        sym_plane_normal = np.zeros(3, dtype=np.float)
    #        sym_plane_normal[focus_vp] = 1.0
    #        sym_plane_point = camera.cam_pos + \
    #                          planes_scale_factors[i][planes_comb[i]]*(np.array(sym_plane_point) - camera.cam_pos)
    #        planes_point_normal.append([sym_plane_point, sym_plane_normal])
    #    for i, (p, n) in enumerate(planes_point_normal):
    #        refl_mat = tools_3d.get_reflection_mat(p, n)
    #        all_refl_mats.append(refl_mat)
    #all_planes_scale_factors = deepcopy(fixed_planes_scale_factors)
    #all_planes_scale_factors.append([planes_scale_factors[0][best_comb[0]],
    #                                 planes_scale_factors[1][best_comb[1]],
    #                                 planes_scale_factors[2][best_comb[2]]])

    if DRY_RUN:
        local_candidate_correspondences, correspondence_ids = symmetry_tools.copy_correspondences_batch(
            global_candidate_correspondences, batch, fixed_strokes, refl_mats,
            [planes_scale_factors[0][best_comb[0]],
             planes_scale_factors[1][best_comb[1]],
             planes_scale_factors[2][best_comb[2]]],
            camera, sketch, ref_correspondences=ref_correspondences)
    else:
        local_candidate_correspondences, correspondence_ids = symmetry_tools.copy_correspondences_batch(
            global_candidate_correspondences, batch, fixed_strokes, refl_mats,
            [planes_scale_factors[0][best_comb[0]],
             planes_scale_factors[1][best_comb[1]],
             planes_scale_factors[2][best_comb[2]]],
            camera, sketch)
    solution_folder = os.path.join(batch_folder, "solution")
    if not os.path.exists(solution_folder):
        os.mkdir(solution_folder)
    local_per_correspondence_sym_scores = [per_correspondence_sym_scores[corr_id]
                                           for corr_id in correspondence_ids]

    correspondences, per_stroke_candidates = symmetry_tools.extract_correspondence_information(
        local_candidate_correspondences, sketch, VERBOSE=False)
    per_stroke_proxies = [[] for s_id in range(len(sketch.strokes))]
    cluster_proxies.cluster_proxy_strokes(local_candidate_correspondences,
                                                        per_stroke_proxies, sketch)
    # debugging sanity check
    if len(ref_correspondences) > 0:
        # check for missing 2d correspondences
        log_text += "Missing local_candidate_correspondences\n"
        global_candidate_correspondence_graphs = [nx.Graph() for i in range(3)]
        for corr in local_candidate_correspondences:
            global_candidate_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
        for axis_id, ref_graph in enumerate(ref_correspondences):
            for edge in ref_graph.edges():
                if edge[0] > batch[1] or edge[1] > batch[1]:
                    continue
                if not(np.any(np.logical_and(np.array(edge) >= batch[0], np.array(edge) <= batch[1]))):
                    continue
                if not global_candidate_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
                    log_text += str(edge)+ ", axis "+ str(axis_id)+"\n"
                    ref_correspondences[axis_id].remove_edge(edge[0], edge[1])
        log_text += "\n"

    missing_proxy_clustering_stroke_ids= set()
    if len(ref_correspondences) > 0:
        log_text += "Missing proxy clusterings\n"
        for s_id in range(len(sketch.strokes)):
            if s_id < batch[0] or s_id > batch[1]:
                continue
            ideal_neighbors = []
            for axis_id, ref_graph in enumerate(ref_correspondences):
                if not (s_id >= batch[0] and s_id <= batch[1]):
                    continue
                if not ref_graph.has_node(s_id):
                    continue
                for i_n in list(ref_graph.neighbors(s_id)):
                    ideal_neighbors.append((i_n, axis_id))
                    #if any(i < batch[0] or i > batch[1] for i in ideal_neighbors):
            if len(ideal_neighbors) == 0:
                continue
            #    continue
            real_proxies = {}
            for corr in local_candidate_correspondences:
                #if corr[4] != axis_id:
                #    continue
                tmp_i = 0
                if corr[1] == s_id:
                    tmp_i = 1
                if corr[5+tmp_i] == -1:
                    continue
                other_s_id = -1
                if corr[0] == s_id:
                    other_s_id = corr[1]
                elif corr[1] == s_id:
                    other_s_id = corr[0]
                else:
                    continue
                for proxy_id in corr[5+tmp_i]:
                    if proxy_id in real_proxies.keys():
                        real_proxies[proxy_id].append((other_s_id, corr[4]))
                    else:
                        real_proxies[proxy_id] = [(other_s_id, corr[4])]
            found_any_good_proxy = False
            for proxy in real_proxies.values():
                if all(i in proxy for i in ideal_neighbors):
                    found_any_good_proxy = True
                    continue
            if not found_any_good_proxy:
                log_text += "s_id: " + str(s_id)+ ", ideal_neighbors"+ str(ideal_neighbors)+ ", real_proxies"+str(real_proxies)+"\n"
                missing_proxy_clustering_stroke_ids.add(s_id)
                #print("Missing proxy clustering: s_id", s_id, ", ideal_neighbors", ideal_neighbors, ", real_proxies", real_proxies)
        log_text += "\n"

    eccentricity_weights = [[] for i in range(len(sketch.strokes))]
    for s_id, s in enumerate(sketch.strokes):
        #print("s_id", s_id)
        if not s.is_ellipse():
            continue
        for p in per_stroke_proxies[s_id]:
            u, v = tools_3d.get_basis_for_planar_point_cloud(p)
            ecc = tools_3d.get_ellipse_eccentricity(p, u, v)
            # we want a score close to 1 for low eccentricites, i.e., for circular ellipses
            score = np.exp(-ecc)
            eccentricity_weights[s_id].append(score)
    intersections_3d_simple = symmetry_tools.get_intersections_simple_batch(per_stroke_proxies,
                                                                            sketch, camera, batch, fixed_strokes)
    if batch_id == 0:
        min_dists = []
        for inter in intersections_3d_simple:
            first_cam_depths = inter.cam_depths[0]
            if len(first_cam_depths) == 0:
                first_cam_depths = [inter.fix_depth]
            snd_cam_depths = inter.cam_depths[1]
            if len(snd_cam_depths) == 0:
                snd_cam_depths = [inter.fix_depth]
            min_dists.append(np.min(distance.cdist(np.array(first_cam_depths).reshape(-1, 1),
                                                   np.array(snd_cam_depths).reshape(-1, 1))))
            #min_dists.append(np.min(distance.cdist(np.array(inter.cam_depths[0]).reshape(-1, 1),
            #                                       np.array(inter.cam_depths[1]).reshape(-1, 1))))
        #median_dist = np.quantile(min_dists, 0.8)
    #print("median_dist", median_dist)
    #for inter_id in range(len(intersections_3d_simple)):
    #    intersections_3d_simple[inter_id].epsilon = median_dist

    line_coverages_simple = symmetry_tools.get_line_coverages_simple(intersections_3d_simple, sketch,
                                                                               extreme_intersections_distances_per_stroke)
    # debugging sanity check
    local_candidate_correspondence_graphs = [nx.Graph() for i in range(3)]
    for corr in local_candidate_correspondences:
        local_candidate_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
    missing_correspondences = [[] for i in range(3)]
    for axis_id, ref_graph in enumerate(ref_correspondences):
        for edge in ref_graph.edges():
            if max(edge) <= batch[1] and min(edge) >= batch[0]:
                if not local_candidate_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
                    print("Missing candidate correspondence: ", edge, ", axis ", axis_id)
                    missing_correspondences[axis_id].append(edge)
    #if np.sum([len(missing_correspondences[i]) for i in range(3)]) > 0:
    #    exit()


    bip_start_time = process_time()
    per_axis_median_lengths = []
    print(best_obj_value)
    #if batch_id == 1:
    #    utils_plot.plot_candidate_correspondences(
    #        sketch, correspondences, local_planes_scale_factors, camera,
    #        intersections_3d_simple, solution_folder)
    #if not DRY_RUN:
    suppress_correspondences = []
    obj_value, final_correspondences, final_proxis, final_intersections, final_line_coverages, \
    final_end_intersections, solution_terms, final_full_anchored_stroke_ids, final_half_anchored_stroke_ids, final_triple_intersections = \
        gurobi_models.get_best_correspondences_simple_batch(
            local_candidate_correspondences, per_stroke_proxies, intersections_3d_simple, line_coverages_simple,
            local_per_correspondence_sym_scores,
            batch, fixed_strokes, fixed_intersections, sketch, batch_folder,
            sketch_connectivity=SKETCH_CONNECTIVITY, return_final_strokes=True,
            line_coverage=LINE_COVERAGE, eccentricity_weights=eccentricity_weights,
            stroke_lengths=stroke_lengths, intersection_constraint=INTERSECTION_CONSTRAINT,
            best_obj_value=best_obj_value-1, sketch_compacity=SKETCH_COMPACITY, camera=camera,
            per_axis_median_lengths=per_axis_median_lengths, end_intersections=False,
            ref_correspondences=ref_correspondences,
            per_stroke_triple_intersections=per_stroke_triple_intersections,
            main_axis=main_axis,
            ABLATE_LINE_COVERAGE=ABLATE_LINE_COVERAGE,
            ABLATE_PROXIMITY=ABLATE_PROXIMITY,
            ABLATE_CURVE_CORR=ABLATE_CURVE_CORR,
            ABLATE_ECCENTRICITY=ABLATE_ECCENTRICITY,
            ABLATE_TANGENTIAL_INTERSECTIONS=ABLATE_TANGENTIAL_INTERSECTIONS,
            ABLATE_ANCHORING=ABLATE_ANCHORING
        )

    print("total time: ", time() - total_start_time)
    print("final obj_value", obj_value)
    print(solution_terms)
    ## debugging sanity check
    #final_correspondence_graphs = [nx.Graph() for i in range(3)]
    #for corr in final_correspondences:
    #    final_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
    #for axis_id, ref_graph in enumerate(ref_correspondences):
    #    for edge in ref_graph.edges():
    #        if min(edge) >= batch[0] and max(edge) <= batch[1]:
    #            if not final_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
    #                print("Missing correspondence: ", edge, ", axis ", axis_id)
    #if len(ref_correspondences) > 0:
    #    ref_obj_value, ref_final_correspondences, ref_final_proxis, ref_final_intersections, \
    #    ref_final_line_coverages, ref_solution_terms = \
    #        symmetry_sketch_selection_gurobi.get_best_correspondences_simple_batch(
    #            local_candidate_correspondences, per_stroke_proxies, intersections_3d_simple, line_coverages_simple,
    #            local_per_correspondence_sym_scores,
    #            batch, fixed_strokes, sketch, batch_folder,
    #            sketch_connectivity=SKETCH_CONNECTIVITY, return_final_strokes=True,
    #            line_coverage=LINE_COVERAGE, eccentricity_weights=eccentricity_weights,
    #            stroke_lengths=stroke_lengths, intersection_constraint=INTERSECTION_CONSTRAINT,
    #            best_obj_value=best_obj_value-1, ref_correspondences=ref_correspondences, enforce_strokes=enforce_strokes)
    #    print("total time: ", time() - total_start_time)
    #    print("final obj_value", ref_obj_value)
    #    print(ref_solution_terms)
    #    if DRY_RUN:
    #        final_correspondences = ref_final_correspondences
    #        final_proxis = ref_final_proxis
    #        final_line_coverages = ref_final_line_coverages
    #    #print(final_proxis)
    #    #exit()
    if TRIPLE_INTERSECTIONS:
        utils_plot.plot_anchored_strokes(sketch, final_full_anchored_stroke_ids, final_half_anchored_stroke_ids, final_triple_intersections,
                                         output_file_name=os.path.join(solution_folder, "anchored_strokes.png"))

    if LINE_COVERAGE:
        #print("Line-coverages")
        for s_i, final_line_coverage in enumerate(final_line_coverages):
            if final_line_coverage > 0.0:
                fixed_line_coverages[s_i] = final_line_coverage
        #    print(s_i, final_line_coverage)
        #for s_i, final_proxy in enumerate(final_proxis):
        #    print(s_i, final_proxy)
        #for s_i, lc in enumerate(fixed_line_coverages):
        #    print(s_i, lc)

    # debugging sanity check
    if len(ref_correspondences) > 0:
        log_text += "Missing correspondences after BIP\n"
        final_correspondence_graphs = [nx.Graph() for i in range(3)]
        for corr in final_correspondences:
            final_correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
        for axis_id, ref_graph in enumerate(ref_correspondences):
            for edge in ref_graph.edges():
                #print(edge)
                if edge[0] > batch[1] or edge[1] > batch[1]:
                    continue
                if not(np.any(np.logical_and(np.array(edge) >= batch[0], np.array(edge) <= batch[1]))):
                    continue
                if not final_correspondence_graphs[axis_id].has_edge(edge[0], edge[1]):
                    log_text += str(edge)+ ", axis "+ str(axis_id)
                    if edge[0] in missing_proxy_clustering_stroke_ids:
                        log_text += ", proxy_problem s_id: "+str(edge[0])
                    if edge[1] in missing_proxy_clustering_stroke_ids:
                        log_text += ", proxy_problem s_id: "+str(edge[1])
                    log_text += "\n"
                    #print("Missing correspondence: ", edge, ", axis ", axis_id)
        log_text += "\n"
    #if np.sum([len(missing_correspondences[i]) for i in range(3)]) > 0:
    #    exit()
    #exit()
    #if PLOT_DEBUG and len(ref_correspondences) > 0:
    #    utils_plot.plot_comparison_3d(ref_final_proxis, ref_final_correspondences,
    #                                  final_proxis, final_correspondences, sketch, fixed_strokes)

    #if PLOT_DEBUG:
    #try:
    utils_plot.plot_solutions(sketch, final_correspondences, local_planes_scale_factors,
                            camera, final_intersections, final_proxis, fixed_strokes, solution_folder)
    #except:
    #    continue
    #utils_plot.plot_candidate_correspondences(
    #    sketch, correspondences, local_planes_scale_factors, camera,
    #    intersections_3d_simple, solution_folder)

    results = {"final_proxies": []}
    for s_id, s in enumerate(sketch.strokes[:batch[1]+1]):
        if final_proxis[s_id] is not None:
            results["final_proxies"].append(final_proxis[s_id].tolist())
        elif len(fixed_strokes[s_id]) > 0:
            results["final_proxies"].append(fixed_strokes[s_id].tolist())
        else:
            results["final_proxies"].append([])
    results["ref_final_proxies"] = []
    for inter in final_intersections:
        fixed_intersections.append(inter[2])
    #if len(ref_correspondences) > 0:
    #    for s_id, s in enumerate(sketch.strokes[:batch[1]+1]):
    #        if ref_final_proxis[s_id] is not None:
    #            results["ref_final_proxies"].append(ref_final_proxis[s_id].tolist())
    #        elif len(fixed_strokes[s_id]) > 0:
    #            results["ref_final_proxies"].append(fixed_strokes[s_id].tolist())
    #        else:
    #            results["ref_final_proxies"].append([])
    results["symmetry_planes"] = [{"plane_normal": list(plane.normal),
                                   "signed_distance": plane.distance_point_signed([0, 0, 0]),
                                   #"vertices": best_planes_geometry[plane_id][0].tolist(),
                                   #"faces": best_planes_geometry[plane_id][1].tolist()
                                   }
                                  for plane_id, plane in enumerate(best_planes)]
    results["symmetry_correspondences"] = [{"stroke_id_0": int(corr[0]),
                                            "stroke_id_1": int(corr[1]),
                                            "stroke_3d_0": corr[2].tolist(),
                                            "stroke_3d_1": corr[3].tolist(),
                                            "symmetry_plane_id": corr[4],
                                            # get connectivity information in cas of self-symmetric strokes
                                            "self_sym_inter_id_0": int(corr[5]),
                                            "self_sym_inter_id_1": int(corr[6])
                                            }
                                           for corr in final_correspondences]
    results["local_correspondences"] = [{"stroke_id_0": int(corr[0]),
                                         "stroke_id_1": int(corr[1]),
                                         "stroke_3d_0": corr[2].tolist(),
                                         "stroke_3d_1": corr[3].tolist(),
                                         "symmetry_plane_id": corr[4],
                                         # get connectivity information in cas of self-symmetric strokes
                                         "self_sym_inter_id_0": int(corr[5][0]) if type(corr[5])==list else corr[5],
                                         "self_sym_inter_id_1": int(corr[6][0]) if type(corr[6])==list else corr[6]
                                         }
                                        for corr in local_candidate_correspondences]
    results["final_correspondences"] = [{"stroke_id_0": int(corr[0]),
                                         "stroke_id_1": int(corr[1]),
                                         "stroke_3d_0": corr[2].tolist(),
                                         "stroke_3d_1": corr[3].tolist(),
                                         "symmetry_plane_id": int(corr[4]),
                                         # get connectivity information in cas of self-symmetric strokes
                                         "self_sym_inter_id_0": int(corr[5][0]) if type(corr[5])==list else int(corr[5]),
                                         "self_sym_inter_id_1": int(corr[6][0]) if type(corr[6])==list else int(corr[6])
                                         }
                                        for corr in final_correspondences]
    #if len(ref_correspondences) > 0:
    #    results["ref_final_correspondences"] = [{"stroke_id_0": int(corr[0]),
    #                                             "stroke_id_1": int(corr[1]),
    #                                             "stroke_3d_0": corr[2].tolist(),
    #                                             "stroke_3d_1": corr[3].tolist(),
    #                                             "symmetry_plane_id": int(corr[4]),
    #                                             # get connectivity information in cas of self-symmetric strokes
    #                                             "self_sym_inter_id_0": int(corr[5][0]) if type(corr[5])==list else int(corr[5]),
    #                                             "self_sym_inter_id_1": int(corr[6][0]) if type(corr[6])==list else int(corr[6])
    #                                             }
    #                                            for corr in ref_final_correspondences]
    #else:
    #    results["ref_final_correspondences"] = 0

    results["batch_indices"] = [int(batch[0]), int(batch[1])]
    results["fixed_strokes"] = [s.tolist() if len(s) > 0 else [] for s in fixed_strokes ]
    results["intersections"] = symmetry_tools.relift_intersections(
        final_intersections, fixed_strokes, final_proxis, sketch, camera)
    batches_results.append(results)

    for s_id, proxy in enumerate(final_proxis):
        if proxy is None:
            continue
        if s_id <= batch[1]:
            fixed_strokes[s_id] = proxy

    fixed_planes_scale_factors.append([planes_scale_factors[0][best_comb[0]],
                                       planes_scale_factors[1][best_comb[1]],
                                       planes_scale_factors[2][best_comb[2]]])

if main_axis != -1 and args.main_axis_file == "":
    accumulated_obj_value_file_name = os.path.join(data_folder, "main_axis_"+str(main_axis)+"_acc_obj_value")
    np.save(accumulated_obj_value_file_name, np.array(accumulated_obj_value))

reconstruction_time = time() - reconstruction_time
total_time = time() - total_time
times = np.array([total_time, pre_process_time, gather_time, reconstruction_time])
times_file_name = os.path.join(data_folder, "runtimes")
if bootstrap_file != "":
    times_file_name = os.path.join(data_folder, "runtimes_bootstrapped")
np.save(times_file_name, times)

output_file_name = os.path.join(data_folder, "batches_results_normal.json")
if bootstrap_file != "":
    output_file_name = os.path.join(data_folder, "batches_results_bootstrapped.json")

if DRY_RUN:
    output_file_name = os.path.join(data_folder, "batches_results_ref.json")
if DRY_RUN and bootstrap_file != "":
    output_file_name = os.path.join(data_folder, "batches_results_bootstrapped_ref.json")

if ABLATE_LINE_COVERAGE:
    output_file_name = output_file_name.replace(".json", "_ablate_line_coverage.json")
if ABLATE_ECCENTRICITY:
    output_file_name = output_file_name.replace(".json", "_ablate_eccentricity.json")
if ABLATE_PROXIMITY:
    output_file_name = output_file_name.replace(".json", "_ablate_proximity.json")
if ABLATE_ANCHORING:
    output_file_name = output_file_name.replace(".json", "_ablate_anchoring.json")
if ABLATE_CURVE_CORR:
    output_file_name = output_file_name.replace(".json", "_ablate_curve_corr.json")
if ABLATE_TANGENTIAL_INTERSECTIONS:
    output_file_name = output_file_name.replace(".json", "_ablate_tangential_intersections.json")

print("output_file_name")
print(output_file_name)

with open(output_file_name, "w") as fp:
    json.dump(batches_results, fp, indent=4)
with open(os.path.join(data_folder, "ref_log.txt"), "w") as fp:
    fp.write(log_text)