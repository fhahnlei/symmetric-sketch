import os
import numpy as np
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
args = parser.parse_args()

data_folder = os.path.join("..", "data", str(args.sketch_id))
main_axis_0 = int(np.load(os.path.join(data_folder, "main_axis_0_acc_obj_value.npy")))
main_axis_1 = int(np.load(os.path.join(data_folder, "main_axis_1_acc_obj_value.npy")))
best_main_axis = np.array([np.argmax([main_axis_0, main_axis_1])])
np.save(os.path.join(data_folder, "main_axis"), best_main_axis)