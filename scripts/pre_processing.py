import pickle
import os, sys, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
import fitCurves, bezier
import sketch_wires
import matplotlib.pyplot as plt
import numpy as np
from pystrokeproc.sketch_core import Sketch, Stroke
import utils_plot
import symmetry_tools
sys.setrecursionlimit(50000)

def pre_process(file_name, data_folder, filter_concept_lines=False, max_axes=[0, 1, 2, 3, 4, 5]):
    if "svg" in file_name:
        original_sketch, camera = sketch_wires.process_raw_sketch(file_name)
    elif "json" in file_name:
        original_sketch, sketch3d, camera = sketch_wires.read_data_wires(file_name)
        #original_sketch.strokes = original_sketch.strokes[:15]
    print(len(original_sketch.strokes))
    if filter_concept_lines:
        concept_line_ids = np.load(os.path.join(data_folder, "concept_line_ids.npy"))
        del_ids = []
        for s_id, s in enumerate(original_sketch.strokes):
            if np.any([orig_s_id in concept_line_ids for orig_s_id in s.original_id]):
                continue
            del_ids.append(s_id)
        for del_id in reversed(np.sort(del_ids).tolist()):
            del original_sketch.strokes[del_id]
        original_sketch.update_stroke_indices()
    utils_plot.sketch_plot_acc_radius(original_sketch,
                                      output_file_name=os.path.join(data_folder, "acc_radius_beginning.png"),
                                      VERBOSE=False)

    original_sketch.set_is_curved_parameters("pca", 0.04)
    max_acc_radius = np.max([s.acc_radius for s in original_sketch.strokes])
    #print("max_acc_radius", max_acc_radius)
    #original_sketch.shave_empty_strokes(2*max_acc_radius)
    original_sketch.shave_empty_strokes(2*max_acc_radius)
    #selected_strokes = np.arange(43)
    #selected_strokes = [7, 8, 10, 11, 13, 14, 18, 19, 20, 21, 22, 23, 92]
    #selected_strokes = [0, 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14]
    #original_sketch.strokes = [s for s_id, s in enumerate(original_sketch.strokes)
    #                           if s_id in selected_strokes]
    #svg_file_name = "../figures/sketch_svgs/"+designer+"_"+object+".svg"


    utils_plot.sketch_plot_simple(original_sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "input_sketch.png"))
    utils_plot.sketch_plot_simple(original_sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "vanishing_lines.png"),
                                  color_fun=lambda s: ["green", "blue", "red", "yellow"][s.axis_label] if s.axis_label < 4 else "black")

    print("original nb_strokes", len(original_sketch.strokes))
    intermediate_strokes = [s for s_id, s in enumerate(original_sketch.strokes)
                            if s.axis_label in max_axes]
                            #if s.axis_label < max_axes]
    intermediate_stroke_ids = [s_id for s_id, s in enumerate(original_sketch.strokes)
                               if s.axis_label in max_axes]
    sketch = Sketch()
    sketch.height = original_sketch.height
    sketch.width = original_sketch.width
    sketch.strokes = [s for s_id, s in enumerate(intermediate_strokes)
                      if s.axis_label in max_axes]
    for vec_id, s in enumerate(intermediate_strokes):
        sketch.strokes[vec_id].original_points = [p.coords
                                                  for p in s.points_list]

    # idealize straight lines
    #for s in original_sketch.strokes:
    #    if s.axis_label > 3:
    #        continue
    #    ideal_line = np.array(s.resample_ideal_line())
    #    axis_label = s.axis_label
    #    width = s.width
    #    acc_radius = s.acc_radius
    #    id = s.id
    #    s.from_array(ideal_line)
    #    s.id = id
    #    s.acc_radius = acc_radius
    #    s.axis_label = axis_label
    #    s.width = width

    sketch.update_stroke_indices()
    sketch.resample_rdp(epsilon=1.0)
    #print("before shave")
    sketch.shave_empty_strokes()
    print("sketch_length", len(sketch.strokes))
    utils_plot.sketch_plot_simple(sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "processed_sketch.png"))

    #utils_plot.sketch_plot_simple(original_sketch, VERBOSE=True,color_fun=lambda s: "red" if s.id == 719 else "black")
    # check if there's a problem with cornucopia fitting of strokes
    print("trying to delete strokes")
    # DEBUG
    #del_strokes = []
    #for s_id, s in enumerate(sketch.strokes):
    #    coords = np.array([p.coords for p in s.points_list])
    #    pts_string = "".join([",["+str(p[0])+","+str(p[1])+"]" for p in coords])
    #    return_value = os.system("python3 -c \"from pystrokeproc.sketch_core import Stroke; tmp_s = Stroke([]); tmp_s.from_array(["+pts_string[1:]+"]); tmp_s.fit()\"")
    #    #print(s_id, return_value)
    #    if return_value != 0:
    #        del_strokes.append(s_id)
    #for del_id in reversed(del_strokes):
    #    del sketch.strokes[del_id]
    sketch.fit_strokes()
    print("before inter_graph")
    sketch.compute_intersection_graph()
    sketch_wires.cluster_straight_lines(sketch)

    sketch.update_stroke_indices()
    utils_plot.sketch_plot_simple(sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "straight_line_clustering.png"))
    sketch.compute_intersection_graph()

    print("After straight-line clustering nb_strokes", len(sketch.strokes))

    sketch.fit_strokes()
    sketch.resample_fitting(1.0)
    sketch.compute_intersection_graph()

    # curve-clustering simple absorption strategy
    sketch_wires.cluster_curves(sketch, VERBOSE=False)
    sketch.update_stroke_indices()
    #DEBUG
    #sketch_wires.cluster_curves_strokeaggregator(sketch, data_folder, VERBOSE=False)
    #sketch.update_stroke_indices()
    utils_plot.sketch_plot_simple(sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "curve_clustering.png"))
    print("After curve clustering nb_strokes", len(sketch.strokes))

    stroke_original_pts =[[] for i in range(len(sketch.strokes))]
    print("curve_ids", [i for i, s in enumerate(sketch.strokes) if s.axis_label == 5 or s.is_ellipse()])

    # fit beziers to curves
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5 or s.is_ellipse():
            continue
        points = np.array([p.coords for p in s.points_list])
        stroke_original_pts[s_id] = points
        bez = np.array(fitCurves.generate_bezier_without_tangents(points))
        #bez = np.array(fitCurves.fitCurve(points, maxError=1.0)).reshape(-1, 2)
        #bez = np.array(s.get_bezier()).reshape(-1, 2)
        c_points = []
        for c_1_i in range(int(len(bez)/4)):
            for t in np.linspace(0.0, 1.0, num=40):
            #for t in np.arange(0.0, 1.0, 0.1):
                c_points.append(bezier.q(bez[4*c_1_i:4*(c_1_i+1)+1], t))
        s.bezier = bez

        new_s = Stroke([])
        new_s.original_points = s.original_points
        new_s.bezier = bez
        new_s.original_id = s.original_id
        new_s.from_array(c_points)
        for pt_id in range(len(c_points)):
            new_s.points_list[pt_id].add_data("pressure", 1.0)
        new_s.id = s.id
        new_s.acc_radius = s.acc_radius
        new_s.axis_label = 5
        new_s.width = s.width
        #new_s.ellipse_fitting = s.get_ellipse_fitting()
        sketch.strokes[s_id] = new_s

    print("Final nb_strokes", len(sketch.strokes))
    utils_plot.sketch_plot_simple(sketch, VERBOSE=False,
                                  output_file_name=os.path.join(data_folder, "curve_fitting.png"))

    individual_lines_folder = os.path.join(data_folder, "individual_lines")
    if not os.path.exists(individual_lines_folder):
        os.mkdir(individual_lines_folder)
    utils_plot.sketch_plot_successive(sketch, individual_lines_folder)

    sketch.compute_intersection_graph(extended=True)
    #sketch.compute_intersection_graph()
    print("after extended")

    sketch_wires.compute_tangential_intersections(sketch, VERBOSE=False)
    for inter in sketch.intersection_graph.get_intersections():
        inter.is_extended = False
    utils_plot.sketch_plot_intersections(sketch, output_file_name=os.path.join(data_folder, "intersection_all.png"),
                                         VERBOSE=False)
    utils_plot.sketch_plot_acc_radius(original_sketch,
                                      output_file_name=os.path.join(data_folder, "acc_radius_intersections.png"),
                                      VERBOSE=False)

    # prune intersection graph of unlikely intersections
    likely_inter_ids = sketch_wires.get_likely_intersections(sketch)
    likely_inter_ids = likely_inter_ids.tolist()
    #likely_inter_ids = [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 21, 26, 27, 29, 31, 32, 37, 38, 40, 41, 42, 43, 44, 45]

    # get rid of intersections between same axes
    same_axis_inter_ids = [inter.inter_id
                           for inter in sketch.intersection_graph.get_intersections()
                           if sketch.strokes[inter.stroke_ids[0]].axis_label == sketch.strokes[inter.stroke_ids[1]].axis_label
                           and sketch.strokes[inter.stroke_ids[0]].axis_label != 5 and not inter.is_tangential]
                           #and sketch.strokes[inter.stroke_ids[0]].axis_label != 5]
    likely_inter_ids += same_axis_inter_ids
    likely_inter_ids += [inter.inter_id for inter in sketch.intersection_graph.get_intersections()
                         if inter.is_tangential]
    likely_inter_ids = np.unique(likely_inter_ids).tolist()
    for del_id in sorted(same_axis_inter_ids, reverse=True):
        if del_id in likely_inter_ids:
            likely_inter_ids.remove(del_id)

    likely_inter_ids = np.array(likely_inter_ids)
    unlikely_inter_ids = [inter.inter_id for inter in sketch.intersection_graph.get_intersections()
                          if not inter.inter_id in likely_inter_ids]
    sketch.intersection_graph.remove_intersections(unlikely_inter_ids)
    print("final_nb_intersections: ", len(sketch.intersection_graph.get_intersections()))

    # for the line-coverage part of the score function, we need the arc_distance for
    # the two most extreme intersections for each stroke
    extreme_intersections_distances_per_stroke = []
    stroke_lengths = []
    for s_id in range(len(sketch.strokes)):
        stroke_lengths.append(sketch.strokes[s_id].length())
        if len(sketch.intersection_graph.node_strokes[s_id].inter_ids) < 2:
            #stroke_lengths.append(sketch.strokes[s_id].length())
            extreme_intersections_distances_per_stroke.append([0, 1])
            continue
        inter_set = sketch.intersection_graph.get_intersections(inter_ids=sketch.intersection_graph.node_strokes[s_id].inter_ids)
        arc_params = [inter.mid_inter_param[np.argwhere(np.array(inter.stroke_ids) == s_id).flatten()[0]]
                      for inter in inter_set if not inter.is_extended]
        #print(s_id, arc_params)
        if len(arc_params) < 1:
            extreme_intersections_distances_per_stroke.append([0, 1])
            #stroke_lengths.append(sketch.strokes[s_id].length())
            continue
        #is_extended = [inter.is_extended for inter in inter_set]
        #stroke_ids = [inter.stroke_ids for inter in inter_set]
        #print(s_id)
        #print(is_extended)
        #print(stroke_ids)
        #print(arc_params)
        #extreme_intersections_distances_per_stroke.append(np.max(arc_params) - np.min(arc_params))
        extreme_intersections_distances_per_stroke.append([np.min(arc_params), np.max(arc_params)])
        #stroke_lengths.append(np.linalg.norm(inter_set[np.argmax(arc_params)].inter_coords \
        #                                     - inter_set[np.argmin(arc_params)].inter_coords))
    stroke_lengths = np.array(stroke_lengths)
    stroke_lengths[:] /= np.max(stroke_lengths)

    utils_plot.sketch_plot_intersections(sketch, output_file_name=os.path.join(data_folder, "intersection_filtered.png"),
                                         VERBOSE=False)
    return sketch, camera, extreme_intersections_distances_per_stroke, stroke_lengths

def get_planes_scale_factors(sketch, camera, batch, batch_id, selected_planes, fixed_strokes,
                            fixed_planes_scale_factors,
                            per_axis_per_stroke_candidate_reconstructions, max_scale_factor_modes,
                            batch_folder="", PLOT_DEBUG=True):
    planes_scale_factors = []
    start_plane = 1
    if batch_id > 0:
        start_plane = 0
    for plane_id in selected_planes:
        if np.sum([len(fixed_strokes[i]) for i in range(len(fixed_strokes))]) == 0:#batch_id == 0:
            if plane_id == 0:
                planes_scale_factors.append([1.0])
                continue
            tmp_max_scale_factor_modes = 4
            intersections_3d, scale_factors = symmetry_tools.get_intersections_scale_factors(
                per_axis_per_stroke_candidate_reconstructions, selected_planes[0], plane_id,
                sketch, camera, batch=batch)
        else:
            tmp_max_scale_factor_modes = max_scale_factor_modes
            intersections_3d, scale_factors = symmetry_tools.get_intersections_scale_factors_fixed_strokes(
                per_axis_per_stroke_candidate_reconstructions, fixed_strokes, batch, plane_id,
                sketch, camera)
            #print(scale_factors)
        # filter intersection outliers
        median_scale_factor = np.median(scale_factors)
        del_scale_factors = [i for i, scale_factor in enumerate(scale_factors)
                             if scale_factor < 0.5*median_scale_factor or scale_factor > 1.5*median_scale_factor]
        for i in sorted(del_scale_factors, reverse=True):
            #del intersections_3d[i]
            del scale_factors[i]
        n, bins = np.histogram(scale_factors, bins=40)
        new_scale_factors = []
        for i in range(len(bins)-1):
            bin_start = bins[i]
            bin_end = bins[i+1]
            already_picked_inters = set()
            for scale_id in range(len(scale_factors)):
                if scale_factors[scale_id] >= bin_start and scale_factors[scale_id] <= bin_end and \
                        not intersections_3d[scale_id].inter_id in already_picked_inters:
                    already_picked_inters.add(intersections_3d[scale_id].inter_id)
                    new_scale_factors.append(scale_factors[scale_id])
        if PLOT_DEBUG:
            plt.hist(scale_factors, bins=bins, color="blue")
            plt.hist(new_scale_factors, bins=bins, color="red")
            plt.savefig(os.path.join(batch_folder, "scale_factors_"+str(plane_id)+".png"))
            plt.close()
        n, bins = np.histogram(new_scale_factors, bins=bins)

        #print(n)
        #print(bins)
        modes_ranked = np.flip(np.argsort(n))
        scale_factors = [(bins[modes_ranked[i]+1] + bins[modes_ranked[i]])/2.0
                         for i in range(tmp_max_scale_factor_modes)]
        #for i in range(min(tmp_max_scale_factor_modes, len(modes_ranked)))]
        if batch_id > 0 and len(fixed_planes_scale_factors) > 0 and len(fixed_planes_scale_factors[-1]) > 0:
            scale_factors.insert(0, fixed_planes_scale_factors[-1][plane_id])
        planes_scale_factors.append(scale_factors)
    # for each plane, pick out 5 most important alignment, i.e., 5 most dominant modes
    return planes_scale_factors

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=0, type=int, help="sketch_id")
    args = parser.parse_args()
    #sketch_id = 10
    data_folder = os.path.join("..", "data", str(args.sketch_id))
    pickle_folder = os.path.join(data_folder, "pickle")
    file_name = os.path.join(data_folder, "sketch.json")
    print(data_folder, pickle_folder, file_name)
    sketch, camera, extreme_intersections_distances_per_stroke, stroke_lengths = pre_process(file_name, data_folder)
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "wb") as fp:
        for s in sketch.intersection_graph.strokes:
            s.fitter = None
        for s in sketch.strokes:
            s.fitter = None
        pickle.dump(sketch, fp)
