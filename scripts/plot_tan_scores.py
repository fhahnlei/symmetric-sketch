import os, sys, inspect
import matplotlib.cm as cm
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
import symmetry_tools
from shapely.geometry import MultiLineString, LineString, Point, MultiPoint
from shapely.geometry.multilinestring import MultiLineString as MultiMultiLinestring
import seaborn as sns
import numpy as np
import pickle
sys.setrecursionlimit(10000)
import utils_plot
import matplotlib.pyplot as plt
from pystrokeproc.sketch_intersection_graph import EdgeIntersection

cmap = sns.color_palette("icefire", as_cmap=True)
cmap = sns.mpl_palette("icefire", as_cmap=True)

def plot_tan_scores(folder, VERBOSE=False):
    pickle_folder = os.path.join(folder, "pickle")
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)

    # also affect edge intersections
    max_inter_id = np.max([inter.inter_id for inter in sketch.intersection_graph.get_intersections()])
    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
    for s_id, s in enumerate(sketch.strokes):
        for other_s_id, other_s in enumerate(sketch.strokes):
            if other_s_id == s_id:
                continue
            #print(s_id, other_s_id)
            #if s_id in [122, 123] and other_s_id in [122, 123]:
            #    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
            acc_radius = 2*max(sketch.strokes[s_id].acc_radius, sketch.strokes[other_s_id].acc_radius)
            intersection = other_s.linestring.linestring.intersection(s.linestring.linestring.buffer(acc_radius))
            intersection_s = s.linestring.linestring.intersection(other_s.linestring.linestring.buffer(acc_radius))
            inter_ratio = intersection.length/other_s.linestring.linestring.length
            #if s_id in [122, 123] and other_s_id in [122, 123]:
            #    print(inter_ratio)
            if inter_ratio > 0.1 or (sketch.strokes[s_id].is_ellipse() or sketch.strokes[other_s_id].is_ellipse() and inter_ratio > 0.0):
                tan_score = symmetry_tools.two_curves_tan_score(s, other_s, sketch, VERBOSE=False)
                #print(tan_score)
                #print(s_id, other_s_id, inter_ratio)
                color = cmap(1.0-tan_score/90.0)
                if tan_score > 30.0 or tan_score < 0.0:
                    continue

                intersection_mid_points = []
                if type(intersection) == MultiLineString or type(intersection) == MultiMultiLinestring:
                    for l in intersection.geoms:
                        plt.plot(np.array(l.coords)[:, 0],
                                 np.array(l.coords)[:, 1],
                                 c=color, lw=3)
                        intersection_mid_points.append(l.interpolate(0.5, normalized=True))
                else:
                    plt.plot(np.array(intersection.coords)[:, 0],
                             np.array(intersection.coords)[:, 1],
                             c=color, lw=3)
                    intersection_mid_points.append(intersection.interpolate(0.5, normalized=True))
                for inter_coord in intersection_mid_points:
                    edge_inter = EdgeIntersection(inter_id=max_inter_id+1,
                                                  inter_params=[],
                                                  mid_inter_param=[],
                                                  inter_coords=np.array(inter_coord),
                                                  stroke_ids=[max(s_id, other_s_id), min(s_id, other_s_id)],
                                                  acc_radius = acc_radius,
                                                  adjacent_inter_ids=None)
                    edge_inter.is_parallel = True
                    edge_inter.is_tangential = False
                    edge_inter.is_extended = False
                    edge_inter.is_triplet = False
                    edge_inter.parallel_inter_ratio = inter_ratio
                    edge_inter.parallel_tan_score = tan_score
                    sketch.intersection_graph.edge_intersections[max_inter_id+1] = edge_inter
                    max_inter_id += 1

                # plot the counterpart
                if type(intersection_s) == MultiLineString or type(intersection_s) == MultiMultiLinestring:
                    for l in intersection_s.geoms:
                        plt.plot(np.array(l.coords)[:, 0],
                                 np.array(l.coords)[:, 1],
                                 c=color, lw=3)
                else:
                    plt.plot(np.array(intersection_s.coords)[:, 0],
                             np.array(intersection_s.coords)[:, 1],
                             c=color, lw=3)
    #utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)

    #norm = Normalize(vmin=0, vmax=90)
    #plt.colorbar(cm.ScalarMappable(cmap=cmap, norm=norm))
    plt.colorbar(cm.ScalarMappable(cmap=cmap))
    plt.gcf().set_size_inches((512 / 100, 512 / 100))
    #plt.show()
    tmp_file_name = os.path.join(folder, "tan_scores.png")
    plt.savefig(tmp_file_name, dpi=100)
    plt.close(plt.gcf())
    with open(os.path.join(pickle_folder, "pre_processed_sketch_2.pkl"), "wb") as fp:
        pickle.dump(sketch, fp)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    #sketch_id = 1
    print("sketch_id", sketch_id)
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    plot_tan_scores(folder, VERBOSE=False)
