#!/usr/bin/env zsh
sketch_id=$1

echo $sketch_id
if [[ ! -d ../data/$sketch_id ]]
then
  return
fi
if [[ ! -f ../data/$sketch_id/pickle/pre_processed_sketch_2.pkl ]]
then
  python single_sketch_processing.py --sketch_id=$sketch_id --only_preprocessing=1
  python plot_tan_scores.py --sketch_id=$sketch_id
fi

# figure out best main_axis
python single_sketch_processing.py --sketch_id=$sketch_id --main_axis=0 --compute_batches=1 --recompute_candidates=1
python single_sketch_processing.py --sketch_id=$sketch_id --main_axis=1
python save_best_main_axis.py --sketch_id=$sketch_id
python single_sketch_processing.py --sketch_id=$sketch_id --main_axis_file="main_axis.npy"

# reconstruct the rest
python single_sketch_processing.py --sketch_id=$sketch_id --bootstrap_file="batches_results_normal.json"
python post_symmetrize.py --sketch_id=$sketch_id
python reconstruct_missing_strokes.py --sketch_id=$sketch_id

# visualizations
python ../src/utils_plot.py --sketch_id=$sketch_id --batches_file="batches_results_normal.json"
python ../src/utils_plot.py --sketch_id=$sketch_id --batches_file="batches_results_bootstrapped.json"
python ../scripts_other/choose_correspondences.py --sketch_id=$sketch_id

for turntable_folder in ../data/$sketch_id/turntable_2d_non_symmetric
do
  echo $turntable_folder
  if [[ -d $turntable_folder ]]
  then
    #cd $turntable_folder
    convert -delay 45 -loop 0 $turntable_folder/{000..359..5}.png $turntable_folder/anim.gif
    #cd ../../scripts
    #break
  fi
done