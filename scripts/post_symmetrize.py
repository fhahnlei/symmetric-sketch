import os, sys, inspect
from time import time
from copy import deepcopy
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
from pystrokeproc.sketch_io import SketchSerializer as skio
import symmetry_tools
from shapely.geometry import LineString, MultiPoint
import tools_3d
import seaborn as sns
import json
import polyscope as ps
import numpy as np
import pickle
import utils_plot
import matplotlib.pyplot as plt
from pystrokeproc.sketch_core import Stroke

def symmetrize(folder, VERBOSE=False):
    with open(os.path.join(pickle_folder, "pre_processed_sketch_2.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)

    symmetry_tools.assign_ellipse_minor_axes(sketch, cam, alignment_threshold=15.0,
                                             VERBOSE=False, data_folder=folder)
    strokes_3d = []
    refl_mats = []
    unique_symm_planes = []
    per_stroke_nb_corr = np.zeros(len(sketch.strokes), dtype=int)
    intersection_3ds = {}

    intersection_ids = []
    with open(os.path.join(folder, "batches_results_normal.json"), "rb") as fp:
        batches_results = json.load(fp)
    for batch in batches_results:
        for inter in batch["intersections"]:
            intersection_ids.append(inter[-1])
            if inter[-1] in intersection_3ds.keys():
                intersection_3ds[inter[-1]].append(inter[0])
            else:
                intersection_3ds[inter[-1]] = [inter[0]]
        for symm_plane in batch["symmetry_planes"]:
            unique = True
            for u_plane in unique_symm_planes:
                if np.argmax(symm_plane["plane_normal"]) == u_plane[0] and np.isclose(u_plane[1], symm_plane["signed_distance"], atol=1e-4):
                    unique = False
            if not unique:
                continue
            n = np.array(symm_plane["plane_normal"])
            p = -n*symm_plane["signed_distance"]
            refl_mat = tools_3d.get_reflection_mat(p, n)
            refl_mats.append(refl_mat)
            unique_symm_planes.append([np.argmax(symm_plane["plane_normal"]), symm_plane["signed_distance"]])
        for corr in batch["final_correspondences"]:
            if corr["stroke_id_0"] != corr["stroke_id_1"]:
                per_stroke_nb_corr[corr["stroke_id_0"]] += 1
                per_stroke_nb_corr[corr["stroke_id_1"]] += 1


    with open(os.path.join(folder, "batches_results_bootstrapped.json"), "rb") as fp:
        batches_results = json.load(fp)
    for batch in batches_results:
        for inter in batch["intersections"]:
            intersection_ids.append(inter[-1])
            if inter[-1] in intersection_3ds.keys():
                intersection_3ds[inter[-1]].append(inter[0])
            else:
                intersection_3ds[inter[-1]] = [inter[0]]
        for symm_plane in batch["symmetry_planes"]:
            unique = True
            for u_plane in unique_symm_planes:
                if np.argmax(symm_plane["plane_normal"]) == u_plane[0] and np.isclose(u_plane[1], symm_plane["signed_distance"], atol=1e-4):
                    unique = False
            if not unique:
                continue
            n = np.array(symm_plane["plane_normal"])
            p = -n*symm_plane["signed_distance"]
            refl_mat = tools_3d.get_reflection_mat(p, n)
            refl_mats.append(refl_mat)
            unique_symm_planes.append([np.argmax(symm_plane["plane_normal"]), symm_plane["signed_distance"]])
        for corr in batch["final_correspondences"]:
            if corr["stroke_id_0"] != corr["stroke_id_1"]:
                per_stroke_nb_corr[corr["stroke_id_0"]] += 1
                per_stroke_nb_corr[corr["stroke_id_1"]] += 1

    #for s_id, nb_corr in enumerate(per_stroke_nb_corr):
    #    print(s_id, nb_corr)
    final_batch = batches_results[-1]
    strokes_3d = [[] for i in sketch.strokes]
    for s_id, s in enumerate(final_batch["fixed_strokes"]):
        strokes_3d[s_id] = s
    for s_id, s in enumerate(final_batch["final_proxies"]):
        if len(s) > 0:
            strokes_3d[s_id] = s

    # reconstruct line-coverages
    intersection_ids = np.unique(intersection_ids)
    line_coverages = [[] for i in sketch.strokes]
    intersections = sketch.intersection_graph.get_intersections(intersection_ids)
    for inter in intersections:
        for vec_id, s_id in enumerate(inter.stroke_ids):
            #plt.plot(np.array(sketch.strokes[38].linestring.linestring.coords)[:, 0],
            #         np.array(sketch.strokes[38].linestring.linestring.coords)[:, 1])
            if not(hasattr(inter, "is_parallel") and inter.is_parallel):
                # double-check if lines are close enough
                #if len(intersection_3ds[inter.inter_id]) > 2:
                #    inter_dist = np.linalg.norm(np.array(intersection_3ds[inter.inter_id][0]) -
                #                                np.array(intersection_3ds[inter.inter_id][1]))
                #    if s_id == 38:
                #        print("inter_dist", inter_dist)
                #        print()

                #    if inter_dist > 0.1*min(tools_3d.line_3d_length(strokes_3d[inter.stroke_ids[0]]),
                #                            tools_3d.line_3d_length(strokes_3d[inter.stroke_ids[1]])):
                #        continue
                line_coverages[s_id].append(inter.mid_inter_param[vec_id])
                #if s_id == 38:
                #    plt.scatter([inter.inter_coords[0]], [inter.inter_coords[1]])
                #    #print(inter.stroke_ids, inter.mid_inter_param)
            #if s_id == 38:
            #    plt.show()

    extreme_intersections_distances_per_stroke = []
    for s_id in range(len(sketch.strokes)):
        if len(sketch.intersection_graph.node_strokes[s_id].inter_ids) < 2:
            extreme_intersections_distances_per_stroke.append([0, 1])
            continue
        inter_set = sketch.intersection_graph.get_intersections(inter_ids=sketch.intersection_graph.node_strokes[s_id].inter_ids)
        arc_params = [inter.mid_inter_param[np.argwhere(np.array(inter.stroke_ids) == s_id).flatten()[0]]
                      for inter in inter_set]# if not inter.is_extended]
        if len(arc_params) < 1:
            extreme_intersections_distances_per_stroke.append([0, 1])
            continue
        extreme_intersections_distances_per_stroke.append(np.max(arc_params)-np.min(arc_params))


    print(extreme_intersections_distances_per_stroke)
    line_coverages = [(np.max(lc) - np.min(lc))/extreme_intersections_distances_per_stroke[s_id]
                      if len(lc) > 2 else 0.0 for s_id, lc in enumerate(line_coverages)]

    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
    for s_id in range(len(sketch.strokes)):
        if line_coverages[s_id] < 0.5 and per_stroke_nb_corr[s_id] < 3:
            plt.plot(np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 0],
                     np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 1],
                     c='r',
                     lw=2)
    plt.gcf().set_size_inches((512 / 100, 512 / 100))
    #plt.show()
    tmp_file_name = os.path.join(folder, "post_weak_strokes.png")
    plt.savefig(tmp_file_name, dpi=100)
    plt.close(plt.gcf())
    #exit()

    total_time = time()
    ellipse_coverages = np.zeros(len(sketch.strokes))
    all_intersections = sketch.intersection_graph.get_intersections()
    all_inter_ids = [inter_id for inter_id in sketch.intersection_graph.edge_intersections.keys()]
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label == 5 and s.is_ellipse():
            ellipse_inters = sketch.intersection_graph.get_intersections_by_stroke_id(stroke_id=s_id)
            filtered_ellipse_inter_coords = [ell_inter.inter_coords for ell_inter in ellipse_inters if ell_inter.inter_id in intersection_ids]
            ellipse_inter_ids = [inter_id for inter_id in intersection_ids if s_id in sketch.intersection_graph.edge_intersections[inter_id].stroke_ids]
            filtered_ellipse_inter_coords = [sketch.intersection_graph.edge_intersections[inter_id].inter_coords
                                             for inter_id in ellipse_inter_ids]
            ellipse_coverages[s_id] = MultiPoint(filtered_ellipse_inter_coords).convex_hull.area/s.linestring.linestring.convex_hull.area
            print(s_id, ellipse_coverages[s_id])
            print(len(filtered_ellipse_inter_coords))
    if VERBOSE:
        print(line_coverages)
    print(line_coverages[38])
    #print(extreme_intersections_distances_per_stroke[38])
    #exit()

    #ps.init()
    #utils_plot.plot_curves(strokes_3d, "strokes_")
    #ps.show()

    # pre-process lift intersections
    lifted_inters = {}
    for inter in sketch.intersection_graph.get_intersections():
        if (sketch.strokes[inter.stroke_ids[0]].axis_label == 5 and sketch.strokes[inter.stroke_ids[0]].is_ellipse()) or \
                (sketch.strokes[inter.stroke_ids[1]].axis_label == 5 and sketch.strokes[inter.stroke_ids[1]].is_ellipse()):
            continue
        inter_lifted_0 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, strokes_3d[inter.stroke_ids[0]]))
        inter_lifted_1 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, strokes_3d[inter.stroke_ids[1]]))
        lifted_inters[inter.inter_id] = {inter.stroke_ids[0]: inter_lifted_0,
                                         inter.stroke_ids[1]: inter_lifted_1}


    per_stroke_explanations = [[] for i in sketch.strokes]

    #print(symmetry_tools.two_curves_tan_score(sketch.strokes[17], sketch.strokes[18], sketch, VERBOSE=False))
    for refl_mat_id, refl_mat in enumerate(refl_mats):
        print(refl_mat_id, len(refl_mats))
        for s_id, s in enumerate(strokes_3d):
            if len(s) == 0:
                continue
            refl_s = tools_3d.apply_hom_transform_to_points(s, refl_mat)
            refl_proj_s = np.array(cam.project_polyline(refl_s))
            acc_radius = 2*sketch.strokes[s_id].acc_radius
            covered_strokes = []
            for other_s_id, other_s in enumerate(sketch.strokes):
                #print(s_id, other_s_id)
                if (other_s.axis_label < 5 and not sketch.strokes[s_id].axis_label < 5) or (not other_s.axis_label < 5 and sketch.strokes[s_id].axis_label < 5):
                    continue
                if other_s.axis_label == 5 and sketch.strokes[s_id].axis_label == 5 and ((other_s.is_ellipse() and not sketch.strokes[s_id].is_ellipse()) or ( not other_s.is_ellipse() and sketch.strokes[s_id].is_ellipse())):
                    continue
                if other_s_id == s_id:
                    continue
                if (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()):
                    continue
                if other_s.linestring.linestring.intersects(LineString(refl_proj_s).buffer(acc_radius)):
                    inter_ratio = other_s.linestring.linestring.intersection(LineString(refl_proj_s).buffer(acc_radius)).length/other_s.linestring.linestring.length
                    #if other_s_id == 18 and s_id == 17:
                    #    print("inter_ratio", inter_ratio)
                    #if s_id == 42 and other_s_id == 43:
                    #    print(refl_mat)
                    #    print(arc_params)
                    #    print(np.max(arc_params) - np.min(arc_params))
                    #    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
                    #    proj_s = np.array(cam.project_polyline(s))
                    #    plt.plot(proj_s[:, 0], proj_s[:, 1], c="r", lw=2)
                    #    plt.plot(refl_proj_s[:, 0], refl_proj_s[:, 1], c="blue", lw=2)
                    #    for covered_s_id in covered_strokes:
                    #        plt.plot(np.array(sketch.strokes[covered_s_id].linestring.linestring.coords)[:, 0],
                    #                 np.array(sketch.strokes[covered_s_id].linestring.linestring.coords)[:, 1], c="g",
                    #                 lw=2)
                    #    plt.show()
                    if inter_ratio > 0.5:
                        refl_stroke = Stroke([])
                        refl_stroke.from_array(refl_proj_s)
                        refl_stroke.acc_radius = sketch.strokes[s_id].acc_radius
                        refl_stroke.id = sketch.strokes[s_id].id
                        tan_score = symmetry_tools.two_curves_tan_score(refl_stroke, sketch.strokes[other_s_id], sketch, VERBOSE=False)
                        if tan_score > 10.0 or tan_score < 0.0:
                            continue
                    else:
                        continue
                else:
                    continue

                # would the reflection be better? check the new line-coverage!
                other_s_pts = np.array(other_s.linestring.linestring.coords)
                if sketch.strokes[s_id].axis_label < 5:
                    other_s_pts = np.array([other_s_pts[0], other_s_pts[-1]])
                lifted_other_s = np.array([cam.lift_point_close_to_polyline(p, refl_s) for p in other_s_pts])
                #print(other_s_id, len(lifted_other_s))
                if line_coverages[s_id] < line_coverages[other_s_id]:
                    continue
                arc_params = []
                for inter in sketch.intersection_graph.get_intersections_by_stroke_id(stroke_id=other_s_id):
                    if (sketch.strokes[inter.stroke_ids[0]].axis_label == 5 and sketch.strokes[inter.stroke_ids[0]].is_ellipse()) or \
                            (sketch.strokes[inter.stroke_ids[1]].axis_label == 5 and sketch.strokes[inter.stroke_ids[1]].is_ellipse()):
                        continue
                    #if sketch.strokes[inter.stroke_ids[0]].axis_label == sketch.strokes[inter.stroke_ids[1]].axis_label:
                    #    continue
                    #s_1 = strokes_3d[np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != other_s_id][0]]
                    #if len(s_1) == 0:
                    #    continue
                    #inter_lifted_1 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, s_1))
                    inter_lifted_0 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, lifted_other_s))
                    inter_lifted_1 = lifted_inters[inter.inter_id][np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != other_s_id][0]]
                    try:
                        dist = inter_lifted_0 - inter_lifted_1
                    except:
                        continue
                    #if np.linalg.norm(inter_lifted_0 - inter_lifted_1) < 0.1*max(tools_3d.line_3d_length(refl_s), tools_3d.line_3d_length(lifted_other_s)):
                    if np.linalg.norm(inter_lifted_0 - inter_lifted_1) < 0.1*tools_3d.line_3d_length(refl_s):
                        arc_params.append(np.array(inter.mid_inter_param)[np.array(inter.stroke_ids) == other_s_id][0])
                #if other_s_id == 18:
                #    print(s_id)
                #    print(arc_params)
                #    print((np.max(arc_params) - np.min(arc_params))/extreme_intersections_distances_per_stroke[s_id])
                if sketch.strokes[other_s_id].axis_label == 5 and sketch.strokes[other_s_id].is_ellipse():
                    if len(arc_params) < 1:
                        continue
                else:
                    if len(arc_params) < 2:
                        continue
                    new_lc = (np.max(arc_params) - np.min(arc_params))/extreme_intersections_distances_per_stroke[other_s_id]
                    if other_s_id == 38:
                        print(new_lc, s_id)
                    if np.isclose(line_coverages[other_s_id], 0.0) and new_lc < 0.5:
                        continue
                    if new_lc <= line_coverages[other_s_id]:
                        continue
                #if other_s_id == 24:
                #    print(arc_params)
                #    print(s_id)
                #    print(np.max(arc_params) - np.min(arc_params))
                covered_strokes.append(other_s_id)
                per_stroke_explanations[s_id].append(
                    {"other_s_id": other_s_id,
                     "lifted_other_s": lifted_other_s}
                )

                #print(inter_ratio)
                #print(symmetry_tools.two_curves_tan_score(sketch.strokes[s_id], sketch.strokes[other_s_id], sketch, VERBOSE=False))
                #    ps.init()
                #    utils_plot.plot_curves(strokes_3d, "strokes_")
                #    utils_plot.plot_curves([strokes_3d[s_id], lifted_other_s, refl_s], "new_strokes_")
                #    ps.show()
            #per_stroke_explanations[s_id] += covered_strokes
            #exit()
    #for s_id, covered_strokes in enumerate(per_stroke_explanations):
    #    per_stroke_explanations[s_id] = np.unique(covered_strokes)
    cmap = sns.color_palette("viridis", as_cmap=True)
    nb_covered_strokes = [len(exp) for exp in per_stroke_explanations]
    max_covered_strokes = np.max(nb_covered_strokes)
    min_covered_strokes = np.min(nb_covered_strokes)
    #utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
    #for s_id, s in enumerate(sketch.strokes):
    #    plt.plot(np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 0],
    #             np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 1],
    #             c=cmap((nb_covered_strokes[s_id]-min_covered_strokes)/(max_covered_strokes-min_covered_strokes)),
    #             lw=2)
    #plt.show()
    #print(per_stroke_explanations)

    #covered_folder = os.path.join(folder, "covered_strokes")
    #if not os.path.exists(covered_folder):
    #    os.mkdir(covered_folder)
    ## plot which strokes are covered by which other strokes
    #for s_id, s in enumerate(sketch.strokes):
    #    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
    #    plt.plot(np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 0],
    #             np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 1],
    #             c="r", lw=2)
    #    for cov_s_id in per_stroke_explanations[s_id]:
    #        plt.plot(np.array(sketch.strokes[cov_s_id].linestring.linestring.coords)[:, 0],
    #                 np.array(sketch.strokes[cov_s_id].linestring.linestring.coords)[:, 1],
    #                 c="g", lw=2)
    #    plt.savefig(os.path.join(covered_folder, str(np.char.zfill(str(s_id), 3))+".png"))

    print(line_coverages[18])
    priority_list = list(reversed(np.argsort(nb_covered_strokes).tolist()))
    replaced_strokes = []
    new_strokes_3d = deepcopy(strokes_3d)
    #print(per_stroke_explanations[17])
    for s_id in priority_list:
        if s_id in replaced_strokes:
            continue
        if nb_covered_strokes[s_id] < 1:
            continue
        for exp in per_stroke_explanations[s_id]:
            sub_s_id = exp["other_s_id"]
            #print(sub_s_id)

            if nb_covered_strokes[sub_s_id] < 2:
                if per_stroke_nb_corr[sub_s_id] > 2:
                    continue
                if ((sketch.strokes[sub_s_id].axis_label == 5 and sketch.strokes[sub_s_id].is_ellipse()) and per_stroke_nb_corr[sub_s_id] > 1) or line_coverages[sub_s_id] > 0.5:
                    continue
                # replace 3d stroke
                if sub_s_id in replaced_strokes:
                    continue
                #if VERBOSE:
                print("replace ", sub_s_id, "by", s_id)
                replaced_strokes.append(sub_s_id)
                new_strokes_3d[sub_s_id] = [exp["lifted_other_s"] for exp in per_stroke_explanations[s_id] if exp["other_s_id"] == sub_s_id][0]

    # process ellipses
    for other_s_id, other_s in enumerate(sketch.strokes):
        #if other_s_id > 40:
        #    break
        #print(other_s_id)
        print(other_s_id)
        if other_s.axis_label == 5 and other_s.is_ellipse():
            if ellipse_coverages[other_s_id] > 0.5:
                continue
            best_coverage_s_id_refl_mat = []
            if per_stroke_nb_corr[other_s_id] > 0:
                continue
            for refl_mat_id, refl_mat in enumerate(refl_mats):
                for s_id, s in enumerate(strokes_3d):
                    if other_s_id == s_id:
                        continue
                    if ellipse_coverages[s_id] < 0.5:
                        continue
                    if not (sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse()):
                        continue
                    #print(other_s.is_ellipse())
                    #print(other_s.closest_major_axis_deviation)
                    if not(other_s.closest_major_axis_deviation < 15.0 and sketch.strokes[s_id].closest_major_axis_deviation < 15.0) \
                            or other_s.closest_major_axis_id != sketch.strokes[s_id].closest_major_axis_id:
                        continue
                    if len(s) == 0:
                        continue
                    #print(s_id)
                    refl_s = tools_3d.apply_hom_transform_to_points(s, refl_mat)
                    refl_proj_s = np.array(cam.project_polyline(refl_s))
                    ell_s = LineString(refl_proj_s).convex_hull
                    ell_other_s = sketch.strokes[other_s_id].linestring.linestring.convex_hull
                    if ell_s.intersects(ell_other_s):
                        if ell_s.intersection(ell_other_s).area/ell_other_s.area < 0.4:
                            continue
                    else:
                        continue
                    #lifted_other_s = np.array([cam.lift_point_close_to_polyline(p, refl_s) for p in np.array(other_s.linestring.linestring.coords)])
                    lifted_other_s = np.array([cam.lift_point_close_to_polyline_v2(p, refl_s) for p in np.array(other_s.linestring.linestring.coords)])

                    ell_inter_coords = []
                    lifted_inters = []
                    for inter in sketch.intersection_graph.get_intersections_by_stroke_id(stroke_id=other_s_id):
                        inter_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != other_s_id][0]
                        if (sketch.strokes[inter_s_id].axis_label == 5 and sketch.strokes[inter_s_id].is_ellipse()):
                            continue
                        #if sketch.strokes[inter.stroke_ids[0]].axis_label == sketch.strokes[inter.stroke_ids[1]].axis_label:
                        #    continue
                        s_1 = strokes_3d[np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != other_s_id][0]]
                        if len(s_1) == 0:
                            continue
                        #inter_lifted_0 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, lifted_other_s))
                        inter_lifted_0 = cam.lift_point_close_to_polyline_v2(inter.inter_coords, lifted_other_s)
                        if sketch.strokes[inter_s_id].axis_label == 5 and sketch.strokes[inter_s_id].is_ellipse():
                            continue
                        #    inter_lifted_1 = cam.lift_point_close_to_polyline_v2(inter.inter_coords, s_1)
                        else:
                            inter_lifted_1 = np.array(cam.lift_point_close_to_polyline(inter.inter_coords, s_1))
                        try:
                            dist = inter_lifted_0 - inter_lifted_1
                        except:
                            continue
                        #if np.linalg.norm(inter_lifted_0 - inter_lifted_1) < 0.1*max(tools_3d.line_3d_length(refl_s), tools_3d.line_3d_length(lifted_other_s)):
                        if np.linalg.norm(inter_lifted_0 - inter_lifted_1) < 0.1*tools_3d.line_3d_length(s_1):
                            ell_inter_coords.append(inter.inter_coords)
                            lifted_inters.append(inter_lifted_0)
                            lifted_inters.append(inter_lifted_1)
                        # get area spun by intersections to judge the quality of the ellipse
                    if len(ell_inter_coords) < 2:
                        continue
                    ell_coverage = MultiPoint(ell_inter_coords).convex_hull.area/other_s.linestring.linestring.convex_hull.area
                    #print("ell_coverage", ell_coverage)
                    #ps.init()
                    #utils_plot.plot_curves(strokes_3d)
                    #utils_plot.plot_curves([lifted_other_s], "lifted_")
                    #ps.register_point_cloud("inters", np.array(lifted_inters))
                    #ps.show()
                    #print(s_id, refl_mat)
                    best_coverage_s_id_refl_mat.append([s_id, refl_mat_id, ell_coverage])
                    #per_stroke_explanations[s_id].append(
                    #    {"other_s_id": other_s_id,
                    #     "lifted_other_s": lifted_other_s}
                    #)
            if len(best_coverage_s_id_refl_mat) > 0:
                max_ell_cov_id = np.argmax([ell_cov[-1] for ell_cov in best_coverage_s_id_refl_mat])
                replacer_s_id = best_coverage_s_id_refl_mat[max_ell_cov_id][0]
                refl_s = tools_3d.apply_hom_transform_to_points(strokes_3d[replacer_s_id],
                                                                refl_mats[best_coverage_s_id_refl_mat[max_ell_cov_id][1]])
                lifted_other_s = np.array([cam.lift_point_close_to_polyline_v2(p, refl_s) for p in np.array(other_s.linestring.linestring.coords)])
                new_strokes_3d[other_s_id] = lifted_other_s
                replaced_strokes.append(other_s_id)
                print("replaced", other_s_id, "by", replacer_s_id)
                print(other_s.closest_major_axis_id, other_s.closest_major_axis_deviation)
                print(sketch.strokes[replacer_s_id].closest_major_axis_id, sketch.strokes[replacer_s_id].closest_major_axis_deviation)

    utils_plot.sketch_plot_simple(sketch, VERBOSE=True, plot=False)
    for s_id in replaced_strokes:
        plt.plot(np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 0],
                 np.array(sketch.strokes[s_id].linestring.linestring.coords)[:, 1],
                 c='r',
                 lw=2)
    plt.gcf().set_size_inches((512 / 100, 512 / 100))
    #plt.show()
    tmp_file_name = os.path.join(folder, "post_symmetrized_strokes.png")
    plt.savefig(tmp_file_name, dpi=100)

    if VERBOSE:
        ps.init()
        utils_plot.plot_curves(strokes_3d, "old_strokes_")
        utils_plot.plot_curves(new_strokes_3d, "new_strokes_", color=(1, 0, 0))
        ps.show()

    total_time = time() - total_time
    times_file_name = os.path.join(folder, "runtimes_post_symmetrize")
    np.save(times_file_name, np.array([total_time]))
    batches_results[-1]["fixed_strokes"] = [np.array(s).tolist() for s in new_strokes_3d]
    batches_results[-1]["replaced_strokes"] = np.array(replaced_strokes).tolist()
    with open(os.path.join(folder, "batches_results_post.json"), "w") as fp:
        json.dump(batches_results, fp, indent=4)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    #sketch_id = 1
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    symmetrize(folder, VERBOSE=False)
