import os, sys, inspect
from math import acos
from skspatial.objects import Plane
from sklearn.neighbors import NearestNeighbors
from trimesh.registration import procrustes
from copy import deepcopy
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
from pystrokeproc.sketch_io import SketchSerializer as skio
import sketch_wires
import matplotlib.patheffects as pe
from matplotlib.collections import LineCollection
import tools_3d
import seaborn as sns
import json
import polyscope as ps
import numpy as np
import pickle
import utils_plot
import matplotlib.pyplot as plt

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

my_dpi = 100

def prep_rendering():
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glClearColor(0.5, 0.5, 0.5, 1.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)
    glEnable(GL_CULL_FACE)
    glCullFace(GL_BACK)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_LINE_SMOOTH)
    glEnable(GL_POLYGON_SMOOTH)
    glEnable(GL_MULTISAMPLE)

id_file_list = [
    "student8_house_rough_full.json",
    "designer2_armchair_02_rough_full.json",
    "Prof2task2_printer_02_rough_full.json",
    "student8_hairdryer_rough_full.json",
    "Professional2_vacuum_cleaner_rough_full.json",
    "Professional6_mouse_rough_full.json",
    "designer2_bookshelves_rough_full.json",
    "designer2_trash_bin_rough_full.json",
    "Professional3_tubes_rough_full.json",
    "designer2_chair_04_rough_full.json",
    "designer2_guitar_01_rough_full.json",
    "designer2_printer_02_rough_full.json",
    "Prof2task2_guitar_01_rough_full.json",
    "Professional1_house_rough_full.json",
    "designer2_cabinet_02_rough_full.json",
    "Prof2task2_cabinet_01_rough_full.json",
    "Professional2_house_rough_full.json",
    "designer2_cabinet_01_rough_full.json",
    "student1_house_rough_full.json",
    "student1_vacuum_cleaner_rough_full.json",
    "student3_wobble_surface_rough_full.json",
    "student4_house_rough_full.json",
    "student5_house_rough_full.json",
    "student9_house_rough_full.json",
    "student8_mouse_rough_full.json",
    "designer2_lamp_rough_full.json",
    "student9_tubes_rough_full.json",
    "Professional6_house_rough_full.json",
    "Professional5_wobble_surface_rough_full.json",
    "Professional5_house_rough_full.json",
    "Professional4_tubes_rough_full.json",
    "Professional3_house_rough_full.json",
    "student9_bumps_rough_full.json",
    "student7_vacuum_cleaner_rough_full.json",
    "student6_house_rough_full.json",
    "student1_shampoo_bottle_rough_full.json",
    "Professional6_hairdryer_rough_full.json",
    "Professional4_vacuum_cleaner_rough_full.json",
    "Professional4_hairdryer_rough_full.json",
    "Professional3_mixer_rough_full.json",
    "Professional3_bumps_rough_full.json",
    "student7_wobble_surface_rough_full.json",
    "student2_house_rough_full.json",
    "Professional6_wobble_surface_rough_full.json",
    "Professional6_shampoo_bottle_rough_full.json",
    "Professional1_tubes_rough_full.json",
    "Prof5task2_car_02_rough_full.json",
    "Prof2task2_chair_01_bestScore_full.json",
    "student3_house_bestScore_full.json",
    "",
    "student5_potato_chip_bestScore_full.json",
    "student5_wobble_surface_bestScore_full.json",
    "Professional2_shampoo_bottle_bestScore_full.json",
    "",
    "Professional2_potato_chip_bestScore_full.json",
    "",
    "Professional3_vacuum_cleaner_bestScore_full.json",
    "",
    "student4_potato_chip_bestScore_full.json",
    "",
    "",
    "Professional3_hairdryer_bestScore_full.json",
    "",
    "",
    "",
    "Professional3_potato_chip_bestScore_full.json",
    "Prof2task2_lamp_bestScore_full.json",
    "student3_waffle_iron_bestScore_full.json",
    "Professional5_hairdryer_bestScore_full.json",
    "",
    "student4_wobble_surface_bestScore_full.json",
    "",
    "",
    "Professional1_vacuum_cleaner_bestScore_full.json",
    "Professional3_shampoo_bottle_bestScore_full.json",
    "",
    "",
    "",
    "",
    "student7_shampoo_bottle_bestScore_full.json",
    "Professional2_waffle_iron_bestScore_full.json",
    "",
    "",
    "",
    "",
    "Professional2_hairdryer_bestScore_full.json",
    "Professional2_mouse_bestScore_full.json",
    "student2_wobble_surface_bestScore_full.json",
    "",
    "",
    "",
    "designer2_chair_01_bestScore_full.json",
    "",
    "Professional3_waffle_iron_bestScore_full.json",
]
#id_file_list = [
#    "student8_house_rough_full.json",
#    "designer2_armchair_02_rough_full.json",
#    "Prof2task2_printer_02_rough_full.json",
#    "student8_hairdryer_rough_full.json",
#    "Professional2_vacuum_cleaner_rough_full.json",
#    "Professional6_mouse_rough_full.json",
#    "designer2_bookshelves_rough_full.json",
#    "designer2_trash_bin_rough_full.json",
#    "Professional3_tubes_rough_full.json",
#    "designer2_chair_04_rough_full.json",
#    "designer2_guitar_01_rough_full.json",
#    "designer2_printer_02_rough_full.json",
#    "Prof2task2_guitar_01_rough_full.json",
#    "Professional1_house_rough_full.json",
#    "designer2_cabinet_02_rough_full.json",
#    "Prof2task2_cabinet_01_rough_full.json",
#    "Professional2_house_rough_full.json",
#    "designer2_cabinet_01_rough_full.json",
#    "student1_house_rough_full.json",
#    "student1_vacuum_cleaner_rough_full.json",
#    "student3_wobble_surface_rough_full.json",
#    "student4_house_rough_full.json",
#    "student5_house_rough_full.json",
#    "student9_house_rough_full.json",
#    "student8_mouse_rough_full.json",
#    "designer2_lamp_rough_full.json",
#    "student9_tubes_rough_full.json",
#    "Professional6_house_rough_full.json",
#    "Professional5_wobble_surface_rough_full.json",
#    "Professional5_house_rough_full.json",
#    "Professional4_tubes_rough_full.json",
#    "Professional3_house_rough_full.json",
#    "student9_bumps_rough_full.json",
#    "student7_vacuum_cleaner_rough_full.json",
#    "student6_house_rough_full.json",
#    "student1_shampoo_bottle_rough_full.json",
#    "Professional6_hairdryer_rough_full.json",
#    "Professional4_vacuum_cleaner_rough_full.json",
#    "Professional4_hairdryer_rough_full.json",
#    "Professional3_mixer_rough_full.json",
#    "Professional3_bumps_rough_full.json",
#    "student7_wobble_surface_rough_full.json",
#    "student2_house_rough_full.json",
#    "Professional6_wobble_surface_rough_full.json",
#    "Professional6_shampoo_bottle_rough_full.json",
#    "Professional1_tubes_rough_full.json",
#    "Prof5task2_car_02_rough_full.json"]

def chamfer_distance(x, y, metric='l2', direction='bi', return_pointwise_distances=False):
    """Chamfer distance between two point clouds
    Parameters
    ----------
    x: numpy array [n_points_x, n_dims]
        first point cloud
    y: numpy array [n_points_y, n_dims]
        second point cloud
    metric: string or callable, default ‘l2’
        metric to use for distance computation. Any metric from scikit-learn or scipy.spatial.distance can be used.
    direction: str
        direction of Chamfer distance.
            'y_to_x':  computes average minimal distance from every point in y to x
            'x_to_y':  computes average minimal distance from every point in x to y
            'bi': compute both
    Returns
    -------
    chamfer_dist: float
        computed bidirectional Chamfer distance:
            sum_{x_i \in x}{\min_{y_j \in y}{||x_i-y_j||**2}} + sum_{y_j \in y}{\min_{x_i \in x}{||x_i-y_j||**2}}
    """

    if direction == 'y_to_x':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        chamfer_dist = np.mean(min_y_to_x)
        if return_pointwise_distances:
            return chamfer_dist, min_y_to_x
    elif direction == 'x_to_y':
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_x_to_y)
        if return_pointwise_distances:
            return chamfer_dist, min_x_to_y
    elif direction == 'bi':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_y_to_x) + np.mean(min_x_to_y)
        if return_pointwise_distances:
            return chamfer_dist, min_x_to_y, min_y_to_x
    else:
        raise ValueError("Invalid direction type. Supported types: \'y_x\', \'x_y\', \'bi\'")

    return chamfer_dist

def remove_sharp_turns(original_sketch):

    # detect sharp turns
    #utils_plot.sketch_plot_simple(original_sketch, plot=False)
    for s_id, s in enumerate(original_sketch.strokes):
        removed_point = True
        while removed_point:
            removed_point = False
            points = np.array([p.coords for p in original_sketch.strokes[s_id].points_list])
            #points = np.array(s.linestring.linestring.coords)
            sharp_points = np.zeros(len(points), dtype=bool)
            #plt.plot(points[:, 0], points[:, 1])
            for p_id in range(len(s.points_list)-2):

                seg_one = points[p_id:p_id+2]
                seg_two = points[p_id+1:p_id+3]
                #print(seg_one)
                #print(seg_two)
                v1 = seg_one[1] - seg_one[0]
                v1 /= np.linalg.norm(v1)
                v2 = seg_two[1] - seg_two[0]
                v2 /= np.linalg.norm(v2)
                #angle = np.rad2deg(acos(np.abs(np.dot(v1, v2))))
                angle = np.rad2deg(acos(np.minimum(1.0, np.dot(v1, v2))))
                #print(np.rad2deg(acos(np.dot(v1, v2))))
                #print(angle)
                if np.abs(angle) > 90:
                    #print(s_id)
                    sharp_points[p_id+1] = True
                    #plt.plot(seg_one[:, 0], seg_one[:, 1], lw=3)
                    #plt.plot(seg_two[:, 0], seg_two[:, 1], lw=3)
            for p_id in range(len(points)-1):
                if sharp_points[p_id] and sharp_points[p_id+1]:
                    sharp_points[p_id] = False
            #if s_id == 12:
            #    plt.scatter(points[sharp_points][:, 0], points[sharp_points][:, 1])
            #    print(sharp_points)
            for p_id in reversed(range(len(points))):
                if sharp_points[p_id]:
                    #if s_id == 12:
                    #    print(len(original_sketch.strokes[s_id].points_list))
                    #    print(p_id)
                    del original_sketch.strokes[s_id].points_list[p_id]
                    removed_point = True
            #if s_id == 12:
            #    points = np.array([p.coords for p in original_sketch.strokes[s_id].points_list])
            #    plt.plot(points[:, 0], points[:, 1], lw=3)
    #plt.show()

def get_wires_strokes(file_name, with_remove_sharp_turns=False):
    original_sketch, sketch_3d, cam = sketch_wires.read_data_wires(file_name, include_dict_points=True)
    if with_remove_sharp_turns:
        remove_sharp_turns(original_sketch)

    new_fixed_strokes = []
    for s_id, s in enumerate(sketch_3d.strokes):
        #if len(s.points_list) == 0:
        #    continue
        original_s = original_sketch.strokes[s_id]
        #print(original_s.id)
        original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])

        new_fixed_strokes.append([p.coords for p in s.points_list])

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes

def get_symm_strokes(file_name, pickle_folder, with_remove_sharp_turns=False):
    with open(file_name, "rb") as fp:
        batches_results = json.load(fp)
    fixed_strokes = batches_results[-1]["with_non_symmetric_strokes"]
    #proxies = batches_results[-1]["final_proxies"]
    #for p_id, p in enumerate(proxies):
    #    if p is not None and len(p) > 0:
    #        fixed_strokes[p_id] = p

    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    sketch_file_name = os.path.join(folder, "sketch.json")
    print(sketch_file_name)
    original_sketch, _, _ = sketch_wires.read_data_wires(sketch_file_name, include_dict_points=True)
    if with_remove_sharp_turns:
        remove_sharp_turns(original_sketch)
    print(len(original_sketch.strokes))
    new_fixed_strokes = [[] for i in original_sketch.strokes]
    new_pressures = [[] for i in original_sketch.strokes]
    for s_id, s in enumerate(fixed_strokes):
        for orig_s_id in sketch.strokes[s_id].original_id:
            if len(s) == 0:
                new_fixed_strokes[orig_s_id] = []
                new_pressures[orig_s_id] = []
                continue
            original_s = [tmp_s for tmp_s in original_sketch.strokes if tmp_s.original_id[0] == orig_s_id][0]
            #original_pts = np.array(original_s.linestring.linestring)
            original_pts = np.array([p.coords for p in original_s.points_list])
            original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])
            lifted_points = []
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                if not np.isclose(np.linalg.norm(line_v), 0.0):
                    line_v /= np.linalg.norm(line_v)
                    lifted_points = np.array([cam.lift_point_close_to_line(
                        p, line_p, line_v) for p in original_pts])
            else:
                if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                    plane = Plane.best_fit(np.array(s))
                    lifted_points = cam.lift_polyline_to_plane(original_pts, s[0], plane.normal)
                else:
                    lifted_points = np.array([cam.lift_point_close_to_polyline(
                        p, np.array(s)) for p in original_pts])
            new_fixed_strokes[orig_s_id] = lifted_points
            new_pressures[orig_s_id] = original_pressures

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes, new_pressures

def turntable_opengl(fixed_strokes, pressures, folder_name, colormap, sketch, cam, points):

    pygame.init()
    display = (512, 512)
    #display = (1024, 1024)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)
    glClearColor(1.0, 1.0, 1.0, 1.0)
    prep_rendering()

    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2

    gluPerspective(cam.fov, (display[0] / display[1]), 0.001, 10.0)
    up_vec = np.zeros(3)
    up_vec[2] = 1.0
    gluLookAt(cam.cam_pos[0], cam.cam_pos[1], cam.cam_pos[2],
              bbox_center[0], bbox_center[1], bbox_center[2],
              up_vec[0], up_vec[1], up_vec[2])
#    glMatrixMode(GL_PROJECTION)
#    glLoadIdentity()
#    rot_mat = np.zeros([4, 4])
#    rot_mat[:3, :3] = cam.rot_mat
#    rot_mat[3, 3] = 1
#    rot_mat[3, :3] = cam.cam_pos
#    glLoadMatrixf(rot_mat)
    #print(cam.proj_mat)
    #proj_mat = np.zeros([4, 4])
    #proj_mat[:3,:] = cam.proj_mat
    ##proj_mat[3, 3] = 1.0
    #glLoadMatrixf(proj_mat)

    cmap = sns.color_palette("magma", as_cmap=True)
    cmap = sns.diverging_palette(220, 20, as_cmap=True, center="dark")
    cmap = sns.color_palette("icefire", as_cmap=True)
    max_dist = np.max(colormap)
    min_dist = np.min(colormap)
    thresh = np.load(os.path.join(folder_name, "..", "thresh.npy"))[0]

    #bandwidth = estimate_bandwidth(colormap.reshape(-1, 1), quantile=0.3)
    #ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    #ms.fit(colormap.reshape(-1, 1))
    #labels = ms.labels_
    #min_cluster_id = np.argmin(ms.cluster_centers_)

    #for angle_id, angle in enumerate([0, 45, -45]):
    #for angle_id, angle in enumerate(np.linspace(0, 45, 45)):
    angles = list(range(0, 45)) + list(reversed(list(range(-45, 45)))) + list(range(-45, 0))
    for angle_id, angle in enumerate(angles):
        #print(angle)
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(angle))
        #print(angle)
        for s_id, s in enumerate(fixed_strokes):
            #print(s_id, s)
            if len(s) == 0:
                continue
            s = np.array(s)
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #widths = np.array([(pressures[s_id][p_id]+pressures[s_id][p_id+1])/2
            #                   for p_id in range(len(proj_s)-1)])
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)

            color = (0, 0, 0, 1.0)
            glLineWidth(2)
            #color = (0, 0, 0, pressures[s_id][0])
            #glLineWidth(1+pressures[s_id][0])
            diff = (colormap[s_id]-min_dist)/(max_dist-min_dist)
            if diff >= thresh:
                color = (217/256, 95/256, 2/256, 1.0)
            #if labels[s_id] != min_cluster_id:
            #    color = (217/256, 95/256, 2/256, 1.0)
            glBegin(GL_LINE_STRIP)
            for p_id, p in enumerate(s):
                #glColor4f(0, 0, 0, pressures[s_id][p_id])
                glColor4f(color[0], color[1], color[2], color[3])
                glVertex3f(p[0], p[1], p[2]);
            glEnd()

            #colors = np.zeros([len(widths), 4])
            diff = colormap[s_id]
            #c = cmap((colormap[s_id]-min_dist)/(max_dist-min_dist))
            #colors = [c for i in segs]
            #if diff >= 0.5:
            #print(colors)
            #colors[:, -1] = widths
            #print(colors)
            #widths *= 1.2
            #widths *= 3.0
            #path_effects=[pe.Stroke(linewidth=3, foreground='black'), pe.Normal()])
        pixels = glReadPixels(0, 0, display[0], display[1], format=GL_RGB, type=GL_FLOAT)
        axes.imshow(pixels)
        plt.gca().invert_xaxis()
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        tmp_file_name = os.path.join(folder_name, str(np.char.zfill(str(angle_id), 3))+".png")
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)
    pygame.quit()

def turntable(fixed_strokes, folder_name, colormap, sketch, cam, points):

    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2
    cmap = sns.color_palette("magma", as_cmap=True)
    cmap = sns.diverging_palette(220, 20, as_cmap=True, center="dark")
    cmap = sns.color_palette("icefire", as_cmap=True)
    max_dist = np.max(colormap)
    min_dist = np.min(colormap)
    thresh = np.load(os.path.join(folder_name, "diff_study_threshold.npy"))[0]

    #bandwidth = estimate_bandwidth(colormap.reshape(-1, 1), quantile=0.3)
    #ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    #ms.fit(colormap.reshape(-1, 1))
    #labels = ms.labels_
    #min_cluster_id = np.argmin(ms.cluster_centers_)

    #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
    for angle_id, angle in enumerate([0, 45, -45]):
        #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(angle))
        #print(angle)
        for s_id, s in enumerate(fixed_strokes):
            #print(s_id, s)
            if len(s) == 0:
                continue
            s = np.array(s)
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)
            segs = [[proj_s[p_id], proj_s[p_id+1]] for p_id in range(len(proj_s)-1)]
            #colors = np.zeros([len(widths), 4])
            diff = (colormap[s_id]-min_dist)/(max_dist-min_dist)
            #diff = colormap[s_id]
            #c = cmap((colormap[s_id]-min_dist)/(max_dist-min_dist))
            #colors = [c for i in segs]
            if diff >= thresh:
                colors = [(1, 0, 0) for i in segs]
            #if labels[s_id] != min_cluster_id:
            #    colors = [(1, 0, 0) for i in segs]
            #else:
            #    colors = [(0, 0, 0) for i in segs]
            #print(colors)
            #colors[:, -1] = widths
            #print(colors)
            #widths *= 1.2
            #widths *= 3.0
            lc = LineCollection(segs, linewidths=np.repeat(2, len(colors)), colors=colors)
                                #path_effects=[pe.Stroke(linewidth=3, foreground='black'), pe.Normal()])
            axes.add_collection(lc)
        axes.set_xlim(x_lim)
        axes.set_ylim(y_lim)
        axes.set_aspect("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(folder_name, str(np.char.zfill(str(angle_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)

def get_trick_strokes(folder):
    with open(os.path.join(folder, "batches_results_normal_correct.json"), "rb") as fp:
        batches_results = json.load(fp)
    fixed_strokes = [p for p in batches_results[-1]["final_proxies"] if len(p) > 0]
    with open(os.path.join(folder, "batches_results_normal_fail.json"), "rb") as fp:
        batches_results = json.load(fp)
    fixed_strokes_trick = [p for p in batches_results[-1]["final_proxies"] if len(p) > 0]

    return fixed_strokes, fixed_strokes_trick


def diff_visu(folder, wires_file_name):
    #symm_strokes = get_symm_strokes(os.path.join(folder, "batches_results_bootstrapped.json"),
    #                                os.path.join(folder, "pickle"))
    wires_strokes = get_wires_strokes(wires_file_name)
    symm_strokes, symm_pressures = get_symm_strokes(os.path.join(folder, "batches_results_non_symmetric.json"),
                                    os.path.join(folder, "pickle"))
    #symm_strokes, wires_strokes = get_trick_strokes(folder)
    #symm_pressures = [[] for i in range(len(symm_strokes))]

    print(len(symm_pressures))
    print("len(wires_strokes)")
    print(len(wires_strokes))
    print(len(symm_strokes))
    transform_distances = []
    transformations = []
    for s_id, s in enumerate(wires_strokes):
        if len(s) == 0 or len(symm_strokes[s_id]) != len(s):
            continue
        #transformation, other_s_transformed, _ = procrustes(symm_strokes[s_id], s)
        try:
            transformation, other_s_transformed, _ = procrustes(s, symm_strokes[s_id])
        except:
            continue
        transformed_symm_strokes = []
        nb_close_strokes = 0
        for symm_s_id, symm_s in enumerate(wires_strokes):
            if len(symm_s) == 0 or len(symm_strokes[symm_s_id]) != len(symm_s):
                transformed_symm_strokes.append([])
                continue
            tmp_pts = np.ones([len(symm_s), 4])
            tmp_pts[:, :3] = np.array(symm_s)
            tmp_pts = np.dot(transformation, tmp_pts.T).T
            if len(wires_strokes[s_id]) > 0:
                chamfer_dist = chamfer_distance(tmp_pts[:, :3], symm_strokes[symm_s_id])
                if chamfer_dist < 0.1*tools_3d.line_3d_length(wires_strokes[symm_s_id]):
                    nb_close_strokes += 1
            transformed_symm_strokes.append(tmp_pts[:, :3])
        transform_distances.append(nb_close_strokes)
        transformations.append(transformation)
        #utils_plot.plot_curves(transformed_symm_strokes, "transformed_symm_")
        #ps.show()
    #print(transform_distances)
    transformation = transformations[np.argmax(transform_distances)]
    #transformation = np.identity(4)
    wires_strokes = get_wires_strokes(wires_file_name, with_remove_sharp_turns=True)
    symm_strokes, symm_pressures = get_symm_strokes(os.path.join(folder, "batches_results_non_symmetric.json"),
                                                    os.path.join(folder, "pickle"),
                                                    with_remove_sharp_turns=True)
    transformed_symm_strokes = []
    for symm_s_id, symm_s in enumerate(wires_strokes):
        if len(symm_s) == 0:
            transformed_symm_strokes.append([])
            continue
        tmp_pts = np.ones([len(symm_s), 4])
        tmp_pts[:, :3] = np.array(symm_s)
        tmp_pts = np.dot(transformation, tmp_pts.T).T
        transformed_symm_strokes.append(tmp_pts[:, :3])

    for s_id in range(len(wires_strokes)):
        if len(wires_strokes[s_id]) == 0 or len(symm_strokes[s_id]) == 0:
            wires_strokes[s_id] = []
            transformed_symm_strokes[s_id] = []
            symm_strokes[s_id] = []

    wires_strokes = transformed_symm_strokes
    # create diff map
    chamfer_dists = np.zeros(len(wires_strokes), dtype=float)
    for s_id, s in enumerate(wires_strokes):
        if len(s) > 0:
            chamfer_dists[s_id] = chamfer_distance(s, symm_strokes[s_id])
    #print(chamfer_dists)
    #with open(os.path.join(folder, "batches_results_normal_correct.json"), "rb") as fp:
    with open(os.path.join(folder, "batches_results_normal.json"), "rb") as fp:
        batches_results = json.load(fp)
    points = [inter[0] for batch in batches_results for inter in batch["intersections"]]
    with open(os.path.join(folder, "batches_results_bootstrapped.json"), "rb") as fp:
        batches_results = json.load(fp)
    points += [inter[0] for batch in batches_results for inter in batch["intersections"]]

    #fixed_strokes = batches_results[-1]["fixed_strokes"]
    #proxies = batches_results[-1]["final_proxies"]
    #for p_id, p in enumerate(proxies):
    #    if p is not None and len(p) > 0:
    #        fixed_strokes[p_id] = p

    pickle_folder = os.path.join(folder, "pickle")
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    #tmp_pts = np.ones([len(points), 4])
    #tmp_pts[:, :3] = np.array(points)
    #tmp_pts = np.dot(transformation, tmp_pts.T).T
    #new_points = tmp_pts[:, :3]
    points = np.array(points)
    #ps.init()
    #utils_plot.plot_curves(wires_strokes, "wires_")
    #symm_ps = utils_plot.plot_curves(symm_strokes, "symm_", color=(1, 0, 0))
    #ps.register_point_cloud("points", points)
    #ps.show()
    folder_a = os.path.join(folder, "pilot_study_a")
    if not os.path.exists(folder_a):
        os.mkdir(folder_a)
    folder_b = os.path.join(folder, "pilot_study_b")
    if not os.path.exists(folder_b):
        os.mkdir(folder_b)

    turntable_wires_folder = os.path.join(folder, "pilot_study_wires_opengl")
    #turntable_wires_folder = os.path.join(folder, "pilot_study_wires_opengl_opacity")
    if not os.path.exists(turntable_wires_folder):
        os.mkdir(turntable_wires_folder)
    turntable_symm_folder = os.path.join(folder, "pilot_study_ours_opengl")
    #turntable_symm_folder = os.path.join(folder, "pilot_study_ours_opengl_opacity")
    if not os.path.exists(turntable_symm_folder):
        os.mkdir(turntable_symm_folder)

    wires_choice = np.random.choice([0, 1])
    assignment = {
        "wires": int(wires_choice),
        "ours": 1-int(wires_choice)
    }
    with open(os.path.join(folder, "pilot_choice.json"), "w") as fp:
        json.dump(assignment,fp)

    turntable_opengl(wires_strokes, symm_pressures, [folder_a, folder_b][wires_choice], chamfer_dists, sketch, cam, points)
    turntable_opengl(symm_strokes, symm_pressures, [folder_a, folder_b][1-wires_choice], chamfer_dists, sketch, cam, points)
    #turntable_opengl(wires_strokes, symm_pressures, turntable_wires_folder, chamfer_dists, sketch, cam, points)
    #turntable_opengl(symm_strokes, symm_pressures, turntable_symm_folder, chamfer_dists, sketch, cam, points)

def export_strokes(strokes, obj_file_name):
    obj_file_txt = ""
    p_counter = 0
    for s_id, s in enumerate(strokes):
        if len(s) == 0:
            continue
        p_counter += 1
        #obj_file_txt += "o ["+str(s_id)+"]\n"
        for p_id, p in enumerate(s):
            obj_file_txt += "v "+str(p[0])+" "+str(p[1])+" "+str(p[2])+"\n"
        for p_id, p in enumerate(s[:-1]):
            obj_file_txt += "l "+str(p_counter)+" "+str(p_counter+1)+"\n"
            p_counter += 1
    #print(obj_file_txt)
    with open(obj_file_name, "w") as fp:
        fp.write(obj_file_txt)


def diff_visu_blender(folder, wires_file_name):
    blender_folder = os.path.join(folder, "blender")
    if not os.path.exists(blender_folder):
        os.mkdir(blender_folder)
    wires_strokes = get_wires_strokes(wires_file_name)
    symm_strokes, symm_pressures = get_symm_strokes(os.path.join(folder, "batches_results_non_symmetric.json"),
                                                    os.path.join(folder, "pickle"))

    print(len(symm_pressures))
    print("len(wires_strokes)")
    print(len(wires_strokes))
    print(len(symm_strokes))
    transform_distances = []
    transformations = []
    with open(os.path.join(os.path.join(folder, "pickle"), "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    with open(os.path.join(os.path.join(folder, "pickle"), "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)

    for s_id, s in enumerate(wires_strokes):
        if len(s) == 0 or len(symm_strokes[s_id]) != len(s):
            continue
        #transformation, other_s_transformed, _ = procrustes(symm_strokes[s_id], s)
        try:
            transformation, other_s_transformed, _ = procrustes(s, symm_strokes[s_id])
        except:
            continue
        transformed_symm_strokes = []
        nb_close_strokes = 0
        for symm_s_id, symm_s in enumerate(wires_strokes):
            if len(symm_s) == 0 or len(symm_strokes[symm_s_id]) != len(symm_s):
                transformed_symm_strokes.append([])
                continue
            tmp_pts = np.ones([len(symm_s), 4])
            tmp_pts[:, :3] = np.array(symm_s)
            tmp_pts = np.dot(transformation, tmp_pts.T).T
            if len(wires_strokes[s_id]) > 0:
                chamfer_dist = chamfer_distance(tmp_pts[:, :3], symm_strokes[symm_s_id])
                if chamfer_dist < 0.1*tools_3d.line_3d_length(wires_strokes[symm_s_id]):
                    nb_close_strokes += 1
            transformed_symm_strokes.append(tmp_pts[:, :3])
        transform_distances.append(nb_close_strokes)
        transformations.append(transformation)
        #utils_plot.plot_curves(transformed_symm_strokes, "transformed_symm_")
        #ps.show()
    #print(transform_distances)
    transformation = transformations[np.argmax(transform_distances)]
    #transformation = np.identity(4)
    transformed_symm_strokes = []
    for symm_s_id, symm_s in enumerate(wires_strokes):
        if len(symm_s) == 0:
            transformed_symm_strokes.append([])
            continue
        tmp_pts = np.ones([len(symm_s), 4])
        tmp_pts[:, :3] = np.array(symm_s)
        tmp_pts = np.dot(transformation, tmp_pts.T).T
        transformed_symm_strokes.append(tmp_pts[:, :3])

    not_processed_by_either = []
    only_processed_by_wires = []
    only_processed_by_ours = []

    for s_id in range(len(wires_strokes)):
        if len(wires_strokes[s_id]) == 0 or len(symm_strokes[s_id]) == 0:
            not_processed_by_either.append(s_id)
        if len(wires_strokes[s_id]) > 0 and len(symm_strokes[s_id]) == 0:
            only_processed_by_wires.append(s_id)
        if len(wires_strokes[s_id]) == 0 and len(symm_strokes[s_id]) > 0:
            only_processed_by_ours.append(s_id)

    np.save(os.path.join(blender_folder, "not_processed_by_either"), np.array(not_processed_by_either))
    np.save(os.path.join(blender_folder, "only_processed_by_wires"), np.array(only_processed_by_wires))
    np.save(os.path.join(blender_folder, "only_processed_by_ours"), np.array(only_processed_by_ours))
    wires_strokes = transformed_symm_strokes
    # create diff map
    chamfer_dists = np.zeros(len(wires_strokes), dtype=float)
    for s_id, s in enumerate(wires_strokes):
        if len(s) > 0 and len(symm_strokes[s_id]) > 0:
            chamfer_dists[s_id] = chamfer_distance(s, symm_strokes[s_id])
    cmap = sns.dark_palette("#e34a33", as_cmap=True)
    cmap = sns.dark_palette("#e34a33", as_cmap=True)
    cmap = sns.dark_palette("#ff7f00", as_cmap=True)
    cmap = sns.dark_palette("#e41a1c", as_cmap=True)
    cmap = sns.dark_palette("#ff0000", as_cmap=True)
    cmap = sns.color_palette("flare_r", as_cmap=True)
    #cmap = sns.blend_palette([[0, 0, 0], [228, 26, 28]], as_cmap=True)
    cmap = sns.blend_palette(["#000000", "#e41a1c"], as_cmap=True, input="hex")
    max_dist = np.max(chamfer_dists)
    min_dist = np.min(chamfer_dists)
    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
    #print(angle)
    colors = np.zeros([len(symm_strokes), 3])
    for s_id, s in enumerate(symm_strokes):
        #print(s_id, s)
        if len(s) == 0:
            continue
        s = np.array(s)

        diff = (chamfer_dists[s_id]-min_dist)/(max_dist-min_dist)
        #print(cmap(diff))
        colors[s_id] = cmap(diff)[:3]
        #proj_s = np.array(cam.project_polyline(s))
        #plt.plot(proj_s[:, 0], proj_s[:, 1], c=cmap(diff))
        export_strokes([s], os.path.join(blender_folder, "stroke_diff_ours_"+str(s_id)+".obj"))
    for s_id, s in enumerate(wires_strokes):
        #print(s_id, s)
        if len(s) == 0:
            continue
        s = np.array(s)
        export_strokes([wires_strokes[s_id]], os.path.join(blender_folder, "stroke_diff_wires_"+str(s_id)+".obj"))

    np.save(os.path.join(blender_folder, "diff_colors"), colors)
    mean_pressures = np.zeros(len(symm_pressures))
    for press_id, press in enumerate(symm_pressures):
        if len(press) > 0:
            mean_pressures[press_id] = np.mean(press)
    np.save(os.path.join(blender_folder, "diff_pressures"), mean_pressures)
    fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
    axes.set_xlim(x_lim)
    axes.set_ylim(y_lim)
    axes.set_aspect("equal")
    axes.axis("off")
    #plt.show()
    #tmp_file_name = os.path.join(folder_name, str(np.char.zfill(str(angle_id), 3))+".png")
    #plt.savefig(tmp_file_name, dpi=my_dpi)
    #plt.close(fig)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    sketch_id = 7
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    #wires_file_name = os.path.join(folder, "batches_results_normal_fail.json")
    #wires_file_name = os.path.join("../data", "full_json", id_file_list[sketch_id-1])
    if sketch_id > 47:
        #wires_file_name = os.path.join(folder, "sketch.json")
        wires_file_name = os.path.join(folder, "wires_curves.json")
        if not os.path.exists(wires_file_name):
            wires_file_name = os.path.join(folder, "sketch.json")
    else:
        wires_file_name = os.path.join("../data", "full_json", id_file_list[sketch_id-1])
    diff_visu_blender(folder, wires_file_name)
