import os, sys, inspect
from sklearn.neighbors import NearestNeighbors
from trimesh.registration import procrustes
from copy import deepcopy
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
from pystrokeproc.sketch_io import SketchSerializer as skio
import sketch_wires
from matplotlib.collections import LineCollection
import tools_3d
import seaborn as sns
import json
import networkx as nx
import polyscope as ps
import numpy as np
import pickle
import utils_plot
import matplotlib.pyplot as plt

my_dpi = 100

id_file_list = [
    "student8_house_rough_full.json",
    "designer2_armchair_02_rough_full.json",
    "Prof2task2_printer_02_rough_full.json",
    "student8_hairdryer_rough_full.json",
    "Professional2_vacuum_cleaner_rough_full.json",
    "Professional6_mouse_rough_full.json",
    "designer2_bookshelves_rough_full.json",
    "designer2_trash_bin_rough_full.json",
    "Professional3_tubes_rough_full.json",
    "designer2_chair_04_rough_full.json",
    "designer2_guitar_01_rough_full.json",
    "designer2_printer_02_rough_full.json",
    "Prof2task2_guitar_01_rough_full.json",
    "Professional1_house_rough_full.json",
    "designer2_cabinet_02_rough_full.json",
    "Prof2task2_cabinet_01_rough_full.json",
    "Professional2_house_rough_full.json",
    "designer2_cabinet_01_rough_full.json",
    "student1_house_rough_full.json",
    "student1_vacuum_cleaner_rough_full.json",
    "student3_wobble_surface_rough_full.json",
    "student4_house_rough_full.json",
    "student5_house_rough_full.json",
    "student9_house_rough_full.json",
    "student8_mouse_rough_full.json",
    "designer2_lamp_rough_full.json",
    "student9_tubes_rough_full.json",
    "Professional6_house_rough_full.json",
    "Professional5_wobble_surface_rough_full.json",
    "Professional5_house_rough_full.json",
    "Professional4_tubes_rough_full.json",
    "Professional3_house_rough_full.json",
    "student9_bumps_rough_full.json",
    "student7_vacuum_cleaner_rough_full.json",
    "student6_house_rough_full.json",
    "student1_shampoo_bottle_rough_full.json",
    "Professional6_hairdryer_rough_full.json",
    "Professional4_vacuum_cleaner_rough_full.json",
    "Professional4_hairdryer_rough_full.json",
    "Professional3_mixer_rough_full.json",
    "Professional3_bumps_rough_full.json",
    "student7_wobble_surface_rough_full.json",
    "student2_house_rough_full.json",
    "Professional6_wobble_surface_rough_full.json",
    "Professional6_shampoo_bottle_rough_full.json",
    "Professional1_tubes_rough_full.json",
    "Prof5task2_car_02_rough_full.json",
    "Prof2task2_chair_01_bestScore_full.json",
    "student3_house_bestScore_full.json",
    "",
    "student5_potato_chip_bestScore_full.json",
    "student5_wobble_surface_bestScore_full.json",
    "Professional2_shampoo_bottle_bestScore_full.json",
    "",
    "Professional2_potato_chip_bestScore_full.json",
    "",
    "Professional3_vacuum_cleaner_bestScore_full.json",
    "",
    "student4_potato_chip_bestScore_full.json",
    "",
    "",
    "Professional3_hairdryer_bestScore_full.json",
    "",
    "",
    "",
    "Professional3_potato_chip_bestScore_full.json",
    "Prof2task2_lamp_bestScore_full.json",
    "student3_waffle_iron_bestScore_full.json",
    "Professional5_hairdryer_bestScore_full.json",
    "",
    "student4_wobble_surface_bestScore_full.json",
    "",
    "",
    "Professional1_vacuum_cleaner_bestScore_full.json",
    "Professional3_shampoo_bottle_bestScore_full.json",
    "",
    "",
    "",
    "",
    "student7_shampoo_bottle_bestScore_full.json",
    "Professional2_waffle_iron_bestScore_full.json",
    "",
    "",
    "",
    "",
    "Professional2_hairdryer_bestScore_full.json",
    "Professional2_mouse_bestScore_full.json",
    "student2_wobble_surface_bestScore_full.json",
    "",
    "",
    "",
    "designer2_chair_01_bestScore_full.json",
    "",
    "Professional3_waffle_iron_bestScore_full.json",
]
#id_file_list = [
#    "student8_house_rough_full.json",
#    "designer2_armchair_02_rough_full.json",
#    "Prof2task2_printer_02_rough_full.json",
#    "student8_hairdryer_rough_full.json",
#    "Professional2_vacuum_cleaner_rough_full.json",
#    "Professional6_mouse_rough_full.json",
#    "designer2_bookshelves_rough_full.json",
#    "designer2_trash_bin_rough_full.json",
#    "Professional3_tubes_rough_full.json",
#    "designer2_chair_04_rough_full.json",
#    "designer2_guitar_01_rough_full.json",
#    "designer2_printer_02_rough_full.json",
#    "Prof2task2_guitar_01_rough_full.json",
#    "Professional1_house_rough_full.json",
#    "designer2_cabinet_02_rough_full.json",
#    "Prof2task2_cabinet_01_rough_full.json",
#    "Professional2_house_rough_full.json",
#    "designer2_cabinet_01_rough_full.json",
#    "student1_house_rough_full.json",
#    "student1_vacuum_cleaner_rough_full.json",
#    "student3_wobble_surface_rough_full.json",
#    "student4_house_rough_full.json",
#    "student5_house_rough_full.json",
#    "student9_house_rough_full.json",
#    "student8_mouse_rough_full.json",
#    "designer2_lamp_rough_full.json",
#    "student9_tubes_rough_full.json",
#    "Professional6_house_rough_full.json",
#    "Professional5_wobble_surface_rough_full.json",
#    "Professional5_house_rough_full.json",
#    "Professional4_tubes_rough_full.json",
#    "Professional3_house_rough_full.json",
#    "student9_bumps_rough_full.json",
#    "student7_vacuum_cleaner_rough_full.json",
#    "student6_house_rough_full.json",
#    "student1_shampoo_bottle_rough_full.json",
#    "Professional6_hairdryer_rough_full.json",
#    "Professional4_vacuum_cleaner_rough_full.json",
#    "Professional4_hairdryer_rough_full.json",
#    "Professional3_mixer_rough_full.json",
#    "Professional3_bumps_rough_full.json",
#    "student7_wobble_surface_rough_full.json",
#    "student2_house_rough_full.json",
#    "Professional6_wobble_surface_rough_full.json",
#    "Professional6_shampoo_bottle_rough_full.json",
#    "Professional1_tubes_rough_full.json",
#    "Prof5task2_car_02_rough_full.json"]

def chamfer_distance(x, y, metric='l2', direction='bi', return_pointwise_distances=False):
    """Chamfer distance between two point clouds
    Parameters
    ----------
    x: numpy array [n_points_x, n_dims]
        first point cloud
    y: numpy array [n_points_y, n_dims]
        second point cloud
    metric: string or callable, default ‘l2’
        metric to use for distance computation. Any metric from scikit-learn or scipy.spatial.distance can be used.
    direction: str
        direction of Chamfer distance.
            'y_to_x':  computes average minimal distance from every point in y to x
            'x_to_y':  computes average minimal distance from every point in x to y
            'bi': compute both
    Returns
    -------
    chamfer_dist: float
        computed bidirectional Chamfer distance:
            sum_{x_i \in x}{\min_{y_j \in y}{||x_i-y_j||**2}} + sum_{y_j \in y}{\min_{x_i \in x}{||x_i-y_j||**2}}
    """

    if direction == 'y_to_x':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        chamfer_dist = np.mean(min_y_to_x)
        if return_pointwise_distances:
            return chamfer_dist, min_y_to_x
    elif direction == 'x_to_y':
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_x_to_y)
        if return_pointwise_distances:
            return chamfer_dist, min_x_to_y
    elif direction == 'bi':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_y_to_x) + np.mean(min_x_to_y)
        if return_pointwise_distances:
            return chamfer_dist, min_x_to_y, min_y_to_x
    else:
        raise ValueError("Invalid direction type. Supported types: \'y_x\', \'x_y\', \'bi\'")

    return chamfer_dist

def get_wires_strokes(file_name):
    original_sketch, sketch_3d, cam = sketch_wires.read_data_wires(file_name, include_dict_points=True)

    new_fixed_strokes = []
    for s_id, s in enumerate(sketch_3d.strokes):
        #if len(s.points_list) == 0:
        #    continue
        original_s = original_sketch.strokes[s_id]
        #print(original_s.id)
        original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])

        new_fixed_strokes.append([p.coords for p in s.points_list])

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes

def get_symm_strokes(file_name, pickle_folder):
    with open(file_name, "rb") as fp:
        batches_results = json.load(fp)
    if "non_symmetric" in file_name:
        fixed_strokes = batches_results[-1]["with_non_symmetric_strokes"]
    else:
        fixed_strokes = batches_results[-1]["fixed_strokes"]
        proxies = batches_results[-1]["final_proxies"]
        for p_id, p in enumerate(proxies):
            if p is not None and len(p) > 0:
                fixed_strokes[p_id] = p

    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    sketch_file_name = os.path.join(folder, "sketch.json")
    print(sketch_file_name)
    original_sketch, _, _ = sketch_wires.read_data_wires(sketch_file_name, include_dict_points=True)
    print(len(original_sketch.strokes))
    new_fixed_strokes = [[] for i in original_sketch.strokes]
    for s_id, s in enumerate(fixed_strokes):
        for orig_s_id in sketch.strokes[s_id].original_id:
            if len(s) == 0:
                new_fixed_strokes[orig_s_id] = []
                continue
            original_s = [tmp_s for tmp_s in original_sketch.strokes if tmp_s.original_id[0] == orig_s_id][0]
            original_pts = np.array(original_s.linestring.linestring)
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                line_v /= np.linalg.norm(line_v)
                lifted_points = np.array([cam.lift_point_close_to_line(
                    p, line_p, line_v) for p in original_pts])
            else:
                lifted_points = np.array([cam.lift_point_close_to_polyline(
                    p, np.array(s)) for p in original_pts])
            new_fixed_strokes[orig_s_id] = lifted_points

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes

def turntable(fixed_strokes, folder_name, colormap, sketch, cam, points):

    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2
    cmap = sns.color_palette("magma", as_cmap=True)
    cmap = sns.diverging_palette(220, 20, as_cmap=True, center="dark")
    cmap = sns.color_palette("icefire", as_cmap=True)
    max_dist = np.max(colormap)
    min_dist = np.min(colormap)
    for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(angle))
        #print(angle)
        for s_id, s in enumerate(fixed_strokes):
            #print(s_id, s)
            if len(s) == 0:
                continue
            s = np.array(s)
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)
            segs = [[proj_s[p_id], proj_s[p_id+1]] for p_id in range(len(proj_s)-1)]
            #colors = np.zeros([len(widths), 4])
            c = cmap((colormap[s_id]-min_dist)/(max_dist-min_dist))
            colors = [c for i in segs]
            #print(colors)
            #colors[:, -1] = widths
            #print(colors)
            #widths *= 1.2
            #widths *= 3.0
            lc = LineCollection(segs, linewidths=np.repeat(2, len(colors)), colors=colors)
                                #path_effects=[pe.Stroke(linewidth=3, foreground='black'), pe.Normal()])
            axes.add_collection(lc)
        axes.set_xlim(x_lim)
        axes.set_ylim(y_lim)
        axes.set_aspect("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(folder_name, str(np.char.zfill(str(angle_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)

def diff_visu(folder, wires_file_name):
    wires_strokes = get_wires_strokes(wires_file_name)
    #symm_strokes = get_symm_strokes(os.path.join(folder, "batches_results_bootstrapped.json"),
    #                                os.path.join(folder, "pickle"))
    #symm_strokes = get_symm_strokes(os.path.join(folder, "batches_results_post.json"),
    #                                os.path.join(folder, "pickle"))
    symm_strokes = get_symm_strokes(os.path.join(folder, "batches_results_non_symmetric.json"),
                                    os.path.join(folder, "pickle"))
    print("len(wires_strokes)")
    print(len(wires_strokes))
    print(len(symm_strokes))
    transform_distances = []
    transformations = []
    for s_id, s in enumerate(wires_strokes):
        if len(s) == 0 or len(symm_strokes[s_id]) != len(s):
            continue
        #print(s_id)
        #transformation, other_s_transformed, _ = procrustes(symm_strokes[s_id], s)
        try:
            transformation, other_s_transformed, _ = procrustes(s, symm_strokes[s_id])
        except:
            continue
        transformed_symm_strokes = []
        nb_close_strokes = 0
        for symm_s_id, symm_s in enumerate(wires_strokes):
            if len(symm_s) == 0 or len(symm_strokes[symm_s_id]) != len(symm_s):
                transformed_symm_strokes.append([])
                continue
            tmp_pts = np.ones([len(symm_s), 4])
            tmp_pts[:, :3] = np.array(symm_s)
            tmp_pts = np.dot(transformation, tmp_pts.T).T
            if len(wires_strokes[s_id]) > 0:
                chamfer_dist = chamfer_distance(tmp_pts[:, :3], symm_strokes[symm_s_id])
                if chamfer_dist < 0.1*tools_3d.line_3d_length(wires_strokes[symm_s_id]):
                    nb_close_strokes += 1
            transformed_symm_strokes.append(tmp_pts[:, :3])
        transform_distances.append(nb_close_strokes)
        transformations.append(transformation)
        #utils_plot.plot_curves(transformed_symm_strokes, "transformed_symm_")
        #ps.show()
    #print(transform_distances)
    transformation = transformations[np.argmax(transform_distances)]
    transformed_symm_strokes = []
    for symm_s_id, symm_s in enumerate(wires_strokes):
        if len(symm_s) == 0:
            transformed_symm_strokes.append([])
            continue
        tmp_pts = np.ones([len(symm_s), 4])
        tmp_pts[:, :3] = np.array(symm_s)
        tmp_pts = np.dot(transformation, tmp_pts.T).T
        transformed_symm_strokes.append(tmp_pts[:, :3])

    for s_id in range(len(wires_strokes)):
        if len(wires_strokes[s_id]) == 0 or len(symm_strokes[s_id]) == 0:
            wires_strokes[s_id] = []
            transformed_symm_strokes[s_id] = []
            symm_strokes[s_id] = []

    wires_strokes = transformed_symm_strokes
    # create diff map
    chamfer_dists = np.zeros(len(wires_strokes), dtype=float)
    for s_id, s in enumerate(wires_strokes):
        if len(s) > 0:
            chamfer_dists[s_id] = chamfer_distance(s, symm_strokes[s_id])
    #print(chamfer_dists)
    with open(os.path.join(folder, "batches_results_normal.json"), "rb") as fp:
        batches_results = json.load(fp)
    points = [inter[0] for batch in batches_results for inter in batch["intersections"]]
    with open(os.path.join(folder, "batches_results_bootstrapped.json"), "rb") as fp:
        batches_results = json.load(fp)
    points += [inter[0] for batch in batches_results for inter in batch["intersections"]]

    fixed_strokes = batches_results[-1]["fixed_strokes"]
    proxies = batches_results[-1]["final_proxies"]
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0:
            fixed_strokes[p_id] = p

    pickle_folder = os.path.join(folder, "pickle")
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    #tmp_pts = np.ones([len(points), 4])
    #tmp_pts[:, :3] = np.array(points)
    #tmp_pts = np.dot(transformation, tmp_pts.T).T
    #new_points = tmp_pts[:, :3]
    points = np.array(points)
    #ps.init()
    #utils_plot.plot_curves(wires_strokes, "wires_")
    #symm_ps = utils_plot.plot_curves(symm_strokes, "symm_", color=(1, 0, 0))
    #ps.register_point_cloud("points", points)
    #ps.show()
    turntable_wires_folder = os.path.join(folder, "turntable_diff_wires")
    if not os.path.exists(turntable_wires_folder):
        os.mkdir(turntable_wires_folder)
    turntable_symm_folder = os.path.join(folder, "turntable_diff_symm")
    if not os.path.exists(turntable_symm_folder):
        os.mkdir(turntable_symm_folder)
    turntable(wires_strokes, turntable_wires_folder, chamfer_dists, sketch, cam, points)
    turntable(symm_strokes, turntable_symm_folder, chamfer_dists, sketch, cam, points)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    #sketch_id = 42
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    if sketch_id > 47:
        #wires_file_name = os.path.join(folder, "sketch.json")
        wires_file_name = os.path.join(folder, "wires_curves.json")
        if not os.path.exists(wires_file_name):
            wires_file_name = os.path.join(folder, "sketch.json")
    else:
        wires_file_name = os.path.join("../data", "full_json", id_file_list[sketch_id-1])
    diff_visu(folder, wires_file_name)
