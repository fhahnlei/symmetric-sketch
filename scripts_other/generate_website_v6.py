import os
import pickle
import json
import numpy as np
#<html>
#<body>
#<table style='width:100%'>
#<tr><th>Input Sketch</th><th>Depth-map and Intersections</th><th>Turntable animation</th></tr>
#<tbody>
#<tr><td style="text-align:center"><figure><a href="results_folder/designer2_bookshelves_bestScore_full/input_sketch.svg" target="_blank"><img src = 'results_folder/designer2_bookshelves_bestScore_full/input_sketch.svg' style='align:center;max-width:512px;max-height:512px'></a><figcaption>designer2_bookshelves</figcaption></figure></td><td style="text-align: center;"><a href="results_folder/designer2_bookshelves_bestScore_full/depth_map.svg" target="_blank"><img src = 'results_folder/designer2_bookshelves_bestScore_full/depth_map.svg' style='max-width:512px;max-height:512px'></a></td><td style="text-align: center;"><a href="animations_360/designer2_bookshelves_bestScore_full.gif" target="_blank"><img src = 'animations_360/designer2_bookshelves_bestScore_full.gif' style='max-width:512px;max-height:512px'></a></td></tr>
#
#</tbody>
#</table>
#</body>
#</html>

final_sketch_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                    32, 34, 35, 36, 37, 42, 43, 44, 45]
new_sketch_ids = [48, 50, 51, 54, 59, 60, 65, 70, 71, 78,
                  79, 92, 102]
sketch_ids = final_sketch_ids + new_sketch_ids
print(len(sketch_ids))
first_half = np.random.choice(sketch_ids, int(len(sketch_ids)/2), replace=False).tolist()
snd_half = [s_id for s_id in sketch_ids if not s_id in first_half]
print(np.sort(first_half))
print(np.sort(snd_half))
print(first_half)
print(snd_half)
print(len(first_half))
print(len(snd_half))
input_sketch_folder = "../figures/input_sketches"
input_sketch_folder = "input_sketches"
registration_dir = "../registrations"
results_dir = "."
bumps_sketches = [33, 41, 68, 77, 96]
ignore_sketches = [58]
baseline_sketch_names = [
    "student8_house",
    "designer2_chair_04",
    "designer2_armchair_02",
    "designer2_bookshelves",
    "Professional1_house",
    "Professional2_house",
    "Professional3_house",
    "Professional5_house",
    "Professional6_mouse",
    "designer2_cabinet_02",
    "Prof2task2_cabinet_01",
    "Prof2task2_printer_02",
]
sketch_names = [
    "designer2_trash_bin",
    "Prof2task2_lamp",
    "Professional3_vacuum_cleaner",
    "Professional2_potato_chip",
    "Professional3_potato_chip",
    "Professional2_mouse",
    "Professional2_shampoo_bottle",
    "Professional3_shampoo_bottle",
    "Professional3_waffle_iron",
]
improve_ellipses = [
    "Professional2_hairdryer",
    "Professional2_vacuum_cleaner",
    "Professional3_hairdryer",
    "Professional3_mixer",
]

failures = [
    "Prof2task2_chair_01",
    "Prof2task2_guitar_01",
    "Professional1_vacuum_cleaner",
    "Professional2_waffle_iron",
]

id_file_list = [
    "student8_house_bestScore_full.json",
    "designer2_armchair_02_bestScore_full.json",
    "Prof2task2_printer_02_bestScore_full.json",
    "student8_hairdryer_bestScore_full.json",
    "Professional2_vacuum_cleaner_bestScore_full.json",
    "Professional6_mouse_final_full.json",
    "designer2_bookshelves_bestScore_full.json",
    "designer2_trash_bin_final_full.json",
    "Professional3_tubes_bestScore_full.json",
    "designer2_chair_04_highScore_full.json",
    "designer2_guitar_01_bestScore_full.json",
    "designer2_printer_02_bestScore_full.json",
    "Prof2task2_guitar_01_final_full.json",
    "Professional1_house_bestScore_full.json",
    "designer2_cabinet_02_bestScore_full.json",
    "Prof2task2_cabinet_01_bestScore_full.json",
    "Professional2_house_bestScore_full.json",
    "designer2_cabinet_01_bestScore_full.json",
    "student1_house_bestScore_full.json",
    "student1_vacuum_cleaner_bestScore_full.json",
    "student3_wobble_surface_bestScore_full.json",
    "student4_house_bestScore_full.json",
    "student5_house_bestScore_full.json",
    "student9_house_bestScore_full.json",
    "student8_mouse_bestScore_full.json",
    "designer2_lamp_bestScore_full.json",
    "student9_tubes_bestScore_full.json",
    "Professional6_house_bestScore_full.json",
    "Professional5_wobble_surface_bestScore_full.json",
    "Professional5_house_bestScore_full.json",
    "Professional4_tubes_bestScore_full.json",
    "Professional3_house_bestScore_full.json",
    "student9_bumps_bestScore_full.json",
    "student7_vacuum_cleaner_bestScore_full.json",
    "student6_house_bestScore_full.json",
    "student1_shampoo_bottle_bestScore_full.json",
    "Professional6_hairdryer_bestScore_full.json",
    "Professional4_vacuum_cleaner_bestScore_full.json",
    "Professional4_hairdryer_bestScore_full.json",
    "Professional3_mixer_bestScore_full.json",
    "Professional3_bumps_bestScore_full.json",
    "student7_wobble_surface_bestScore_full.json",
    "student2_house_bestScore_full.json",
    "Professional6_wobble_surface_bestScore_full.json",
    "Professional6_shampoo_bottle_bestScore_full.json",
    "Professional1_tubes_bestScore_full.json",
    "Prof5task2_car_02_bestScore_full.json",
    "../reconstructions_json_400/Prof2task2_chair_01_bestScore_full.json",
    "../reconstructions_json_400/student3_house_bestScore_full.json",
    "",
    "../reconstructions_json_400/student5_potato_chip_bestScore_full.json",
    "../reconstructions_json_400/student5_wobble_surface_bestScore_full.json",
    "../reconstructions_json_400/Professional2_shampoo_bottle_bestScore_full.json",
    "",
    "../reconstructions_json_400/Professional2_potato_chip_bestScore_full.json",
    "",
    "../reconstructions_json_400/Professional3_vacuum_cleaner_bestScore_full.json",
    "",
    "../reconstructions_json_400/student4_potato_chip_bestScore_full.json",
    "",
    "",
    "../reconstructions_json_400/Professional3_hairdryer_bestScore_full.json",
    "",
    "",
    "",
    "../reconstructions_json_400/Professional3_potato_chip_bestScore_full.json",
    "../reconstructions_json_400/Prof2task2_lamp_bestScore_full.json",
    "../reconstructions_json_400/student3_waffle_iron_bestScore_full.json",
    "../reconstructions_json_400/Professional5_hairdryer_bestScore_full.json",
    "",
    "../reconstructions_json_400/student4_wobble_surface_bestScore_full.json",
    "",
    "",
    "../reconstructions_json_400/Professional1_vacuum_cleaner_bestScore_full.json",
    "../reconstructions_json_400/Professional3_shampoo_bottle_bestScore_full.json",
    "",
    "",
    "",
    "",
    "../reconstructions_json_400/student7_shampoo_bottle_bestScore_full.json",
    "../reconstructions_json_400/Professional2_waffle_iron_bestScore_full.json",
    "",
    "",
    "",
    "",
    "../reconstructions_json_400/Professional2_hairdryer_bestScore_full.json",
    "../reconstructions_json_400/Professional2_mouse_bestScore_full.json",
    "../reconstructions_json_400/student2_wobble_surface_bestScore_full.json",
    "",
    "",
    "",
    "../reconstructions_json_400/designer2_chair_01_bestScore_full.json",
    "",
    "../reconstructions_json_400/Professional3_waffle_iron_bestScore_full.json",
]

all_sketch_names = baseline_sketch_names + sketch_names + improve_ellipses + failures

#wires_sketch_names = []
#for x in os.walk("../figures/animations/website"):
#    if "wires" in x[0]:
#        sketch_name = x[0].split("/")[-1].split("_wires")[0]
#        if not sketch_name in wires_sketch_names:
#            wires_sketch_names.append(sketch_name)
#print(wires_sketch_names)

new_sketch_names = []
folder_numbers = []
for x in os.listdir("../data"):
    if os.path.isfile(x):
        continue
    if not x in np.array(np.arange(1, 103), dtype=str):
        continue
    folder_numbers.append(int(x))
    #sketch_name = x[0].split("/")[-1].split("_line_coverage")[0]
    #if not sketch_name in wires_sketch_names and not sketch_name in new_sketch_names:
    #    new_sketch_names.append(sketch_name)

all_sketch_info = []
for d in np.unique(folder_numbers):
    print(d)
    folder = os.path.join("../data", str(d))
    with open(os.path.join(folder, "sketch.json"), "r") as fp:
        sketch_info = json.load(fp)
    designer_name = sketch_info["designer"]
    object_name = sketch_info["object_name"]
    #print(d, designer_name, object_name)
    all_sketch_info.append([d, designer_name, object_name])
    if d < 48:
        continue
    wires_sketch_name = os.path.join("../reconstructions_json_400", designer_name+"_"+object_name+"_bestScore_full.json")
    if os.path.exists(wires_sketch_name):
        print("\""+wires_sketch_name+"\",")
        os.system("cp "+wires_sketch_name+" "+os.path.join("../data/full_json/"))
    else:
        print("\"\",")
#exit()

def img_html(path, caption="", size = 400):
    #return "<td><img src = '" + path + "' style='max-width:"+str(size)+"px;max-height:"+str(size)+"px'></td>"
    return "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:"+str(size)+";max-height:"+str(size)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"
    #return "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:512px;max-height:512px'></a></figure></td>"

def img_pdf(path, caption="", size = 400):
    #return "<td><img src = '" + path + "' style='max-width:"+str(size)+"px;max-height:"+str(size)+"px'></td>"
    return "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><embed src = \'"+path+"\' style='align:center;max-width:"+str(size)+";max-height:"+str(size)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"

def img_html_left_border(path, caption="", size = 400):
    #return "<td><img src = '" + path + "' style='max-width:"+str(size)+"px;max-height:"+str(size)+"px'></td>"
    return "<td style=\"text-align:center;border-left: solid 3px #000;\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:"+str(size)+";max-height:"+str(size)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"
    #return "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:512px;max-height:512px'></a></figure></td>"

def img_html_right_border(path, caption="", size = 400):
    #return "<td><img src = '" + path + "' style='max-width:"+str(size)+"px;max-height:"+str(size)+"px'></td>"
    return "<td style=\"text-align:center;border-right: solid 3px #000;\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:"+str(size)+";max-height:"+str(size)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"
    #return "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:512px;max-height:512px'></a></figure></td>"

def createTableExample(material, folder):
    exampleFolder = os.path.join(folder, "examples")
    examples = []
    for rootDir, dirs, fileNames in os.walk(exampleFolder):
        for fileName in fileNames:
            if material in fileName:
                examples.append(fileName)
    nbExamples = len(examples) // 4

    td_str = "<td><table>\n"
    for exampleID in range(nbExamples):
        td_str = td_str + "<tr>"
        for mapType in range(4):
            td_str = td_str + img_html(os.path.join(exampleFolder, material + "_example" + str(exampleID) +"_" + str(mapType) + ".png"), size=128)
        td_str = td_str + "</tr>\n"

    td_str = td_str + "</table></td>"
    return td_str

def sketch_tr(sketch_name, folder):
    full_str = "<tr>"

    #baseline
    #full_str = full_str + img_html(os.path.join(input_sketch_folder, sketch_name + ".svg"), caption="Input Sketch")

    #every other setting
    #if os.path.exists(os.path.join(input_sketch_folder, sketch_name + ".png")):
    #    full_str = full_str + img_html(os.path.join(input_sketch_folder, sketch_name + ".png"), caption="Input Sketch")
    #else:
    #full_str = full_str + img_html(os.path.join("full_svg", id_file_list[int(folder)-1].replace(".json", ".svg")), caption="Input Sketch")
    full_str = full_str + img_html(os.path.join(folder, "input_sketch.png"), caption="Input Sketch", size=400)
    #full_str = full_str + img_html(os.path.join(sketch_name+"_wires", "anim.gif"), caption="Lifting3D")
    full_str = full_str + img_html(os.path.join(folder, "turntable_2d", "anim.gif"), caption="Ours - Global symmetry", size=400)
    full_str = full_str + img_html(os.path.join(folder, "correspondences_batches_results_normal.png"), caption="Symmetry correspondence visualization - Global symmetry",
                                   size=800)
    full_str = full_str + img_html(os.path.join(folder, "turntable_2d_bootstrapped", "anim.gif"), caption="Ours - Full", size=400)
    full_str = full_str + img_html(os.path.join(folder, "correspondences_batches_results_bootstrapped.png"), caption="Symmetry correspondence visualization - Global symmetry",
                                   size=800)
    full_str = full_str + img_html(os.path.join(folder, "turntable_diff_wires", "000.png"), caption="Difference map", size=400)
    full_str = full_str + img_html(os.path.join(folder, "turntable_diff_wires", "anim.gif"), caption="Difference - Lifting3d", size=400)
    full_str = full_str + img_html(os.path.join(folder, "turntable_diff_symm", "anim.gif"), caption="Difference - Ours", size=400)
    full_str = full_str + img_html(os.path.join(folder, "post_symmetrized_strokes.png"), caption="Postprocessed strokes - Symmetry", size=400)
    full_str = full_str + img_html(os.path.join(folder, "turntable_2d_post", "anim.gif"), caption="Postprocessed strokes - Symmetry", size=400)
    full_str = full_str + img_html(os.path.join(folder, "non_symmetric_strokes.png"), caption="Postprocessed strokes - Intersections", size=400)
    full_str = full_str + img_html(os.path.join(folder, "turntable_2d_non_symmetric", "anim.gif"), caption="Postprocessed strokes - Intersections", size=400)
    full_str = full_str + img_html(os.path.join(folder, "turntable_2d_wires", "anim.gif"), caption="Lifting3D")
    full_str = full_str + img_html(os.path.join(folder, "tan_scores.png"), caption="Tangential intersections")
    full_str = full_str + img_html(os.path.join(folder, "curves_without_clustering.png"), caption="Curves without clustering")
    full_str = full_str + img_html(os.path.join(folder, "curves_with_clustering.png"), caption="Curves with clustering")
    #full_str = full_str + img_html(os.path.join(folder, "ortho_views", "002.png"), caption="Side view 1", size=400)
    #full_str = full_str + img_html(os.path.join(folder, "ortho_views", "003.png"), caption="Side view 2", size=400)
    #full_str = full_str + img_html(os.path.join(folder, "non_symmetric_strokes.png"), caption="Missing strokes", size=400)
    #full_str = full_str + img_html(os.path.join(folder, "non_symmetric_anim.gif"), caption="Reconstructed missing strokes", size=400)
    #full_str = full_str + img_html(os.path.join(folder, "ortho_views_with_non_symmetric", "002.png"), caption="Front view", size=400)
    #full_str = full_str + img_html(os.path.join(folder, "ortho_views_with_non_symmetric", "003.png"), caption="Side view", size=400)

    #full_str = full_str + img_html(os.path.join(folder, "correspondence_visualization_result_normal.png"), caption="Correspondence visualization of reconstruction",
    #                               size=800)
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_line_coverage", "anim.gif"), caption="Ours line_coverage")
    #full_str = full_str + img_html(os.path.join(results_dir, "non_processed_lines", sketch_name+".png"), caption="Non-processed lines")

    # foreshortening and positional
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_positional", "0001.png"), caption="Left View")
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_positional", "0000.png"), caption="Input View")
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_positional", "0002.png"), caption="Right View")
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_foreshortenings", "0001.png"), caption="Foreshortening - Left View")
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_foreshortenings", "0000.png"), caption="Foreshortening - Input View")
    #full_str = full_str + img_html(os.path.join(results_dir, sketch_name+"_foreshortenings", "0002.png"), caption="Foreshortening - Right View")

    full_str = full_str + "</tr>\n\n"
    return full_str

def video_html(path, caption="", size = 400):
    #return "<td style=\"text-align:center;border-left: solid 3px #000;\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:"+str(size)+";max-height:"+str(size)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"
    return "<td style=\"text-align:center\"> <video autoplay controls loop style='align:center;max-width:"+str(2*size)+"px;max-height:"+str(size)+"px'> <source src=\""+path+"\" type=\"video/mp4\"> Sorry, your browser does not support HTML5 video. </video></td>"

def sketch_tr_video(sketch_name, folder):
    full_str = "<tr>"
    size = 400

    with open(os.path.join("../data", folder, "pilot_choice.json"), "r") as fp:
        wires_choice = json.load(fp)
    wires_folder = "pilot_study_a"
    ours_folder = "pilot_study_b"
    if wires_choice["wires"] == 1:
        wires_folder = "pilot_study_b"
        ours_folder = "pilot_study_a"

    #full_str = full_str + img_html(os.path.join(folder, "user_study_column.png"), caption=sketch_name, size=(400,))
    path = os.path.join(folder, "user_study_column.png")
    size_1 = 20
    size_2 = 400
    caption = ""
    full_str += "<td style=\"text-align:center\"><p style=\"color:#1b9e77\";>Ours</p> <p style=\"color:#7570b3\";>Both plausible</p> <p style=\"color:#d95f02\";>[Gryaditskaya et al. 2020]</p> <p style=\"color:#666666\";>Both implausible</p></td>"
    full_str += "<td style=\"text-align:center\"><figure><a href=\""+path+"\" target=\"_blank\"><img src = \'"+path+"\' style='align:center;max-width:"+str(size_2)+";max-height:"+str(size_2)+"px'></a><figcaption>"+caption+"</figcaption></figure></td>"
    full_str = full_str + img_html(os.path.join(folder, ours_folder, "000.png"), caption=sketch_name, size=size)
    full_str = full_str + video_html(os.path.join(folder, wires_folder, "out.mp4"), caption="", size=size)
    full_str = full_str + video_html(os.path.join(folder, ours_folder, "out.mp4"), caption="", size=size)


    full_str = full_str + "</tr>\n\n"
    return full_str

def sketch_tr_video_blender(sketch_name, folder):
    full_str = "<tr>"
    size = 400

    full_str = full_str + img_html(os.path.join(folder, "blender", "turntable_small", "0000.png"), caption=sketch_name, size=size)
    #full_str = full_str + img_html(os.path.join(folder, "blender", "turntable_small", "input_sketch.png"), caption=sketch_name, size=size)
    full_str = full_str + video_html(os.path.join(folder, "blender", "turntable_small", "out.mp4"), caption="", size=size)
    #full_str = full_str + img_html(os.path.join(folder, "blender", "turntable_small", "anim.gif"), caption="", size=size)
    full_str = full_str + img_html(os.path.join(folder, "batch_sketch_visualization.svg"), caption="", size=size)
    #full_str = full_str + video_html(os.path.join(folder, "blender", "turntable", "out.mp4"), caption="", size=size)
    #if int(folder) == 3:
    #    full_str = full_str + video_html(os.path.join(folder, "blender", "turntable copy", "out.mp4"), caption="", size=size)
    #full_str = full_str + video_html(os.path.join(folder, "blender", "turntable", "out_small.mp4"), caption="", size=size)

    full_str = full_str + "</tr>\n\n"
    return full_str

def sketch_tr_batches(sketch_name, folder):
    full_str = "<tr>"
    size = 400

    full_str = full_str + img_html(os.path.join(folder, "batch_sketch_seq_visualization.svg"), caption=sketch_name, size=size)
    full_str = full_str + img_html(os.path.join(folder, "batch_arc_diagram.svg"), caption="", size=size)
    full_str = full_str + img_html(os.path.join(folder, "batch_sketch_visualization.svg"), caption="", size=size)
    #full_str = full_str + img_html(os.path.join(folder, "batch_arc_diagram.svg"), caption="Correspondences + Histogram", size=size)
    #full_str = full_str + img_html(os.path.join(folder, "batch_sketch_visualization.svg"), caption="Batch coloring", size=size)


    full_str = full_str + "</tr>\n\n"
    return full_str

def pilot_sketch_tr(sketch_name, folder):
    full_str = "<tr>"
    size = 300

    full_str = full_str + img_html(os.path.join(folder, "pilot_study_ours", "000.png"), caption=folder+" "+sketch_name, size=size)
    full_str = full_str + img_html_left_border(os.path.join(folder, "pilot_study_wires", "001.png"), caption="", size=size)
    full_str = full_str + img_html(os.path.join(folder, "pilot_study_wires", "002.png"), caption="", size=size)
    full_str = full_str + img_html_left_border(os.path.join(folder, "pilot_study_ours", "001.png"), caption="", size=size)
    full_str = full_str + img_html(os.path.join(folder, "pilot_study_ours", "002.png"), caption="", size=size)
    #full_str = full_str + img_html_left_border(os.path.join(folder, "pilot_study_wires", "001.png"), caption="Lifting3d - Side View 1", size=size)
    #full_str = full_str + img_html(os.path.join(folder, "pilot_study_wires", "002.png"), caption="Lifting3d - Side View 2", size=size)
    #full_str = full_str + img_html_left_border(os.path.join(folder, "pilot_study_ours", "001.png"), caption="Ours - Side View 1", size=size)
    #full_str = full_str + img_html(os.path.join(folder, "pilot_study_ours", "002.png"), caption="Ours - Side View 2", size=size)

    full_str = full_str + "</tr>\n\n"
    return full_str

#for rootDir, folders, filenames in os.walk(results_dir):
#    for filename in filenames:
#        if "_norm" in filename and not filename.split("_norm")[0] in materials:
#            materials.append(filename.split("_norm")[0])

short_sketches = [
    23, 75, 17, 69, 21, 18, 79, 54, 53, 52,  1,  9, 30, 22, 86, 56, 14,
    59, 26, 24, 61, 42, 65, 32, 50, 43, 25, 27,  8, 87, 78,  4, 70, 15,
    10, 19, 66, 29, 73,  2, 74, 80, 36, 40, 44,  6, 34, 16, 48,  5, 28,
    11,  3, 41, 35, 37, 62, 76, 77, 71, 20, 51, 13,  7, 45, 58, 12, 55,
    64, 33, 68, 60, 89, 90, 82, 83, 88, 67, 84]
with open("../data/results.html", "w") as file:
    file.write("<html>\n<body>\n")
    file.write("<table style='width:100%'\n")
    #print(all_sketch_info)
    #for sketch_id, designer_name, object_name in all_sketch_info:
    for sketch_id in short_sketches:
        for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
            if tmp_sketch_id == sketch_id:
                designer_name = tmp_designer_namer
                object_name = tmp_object_name
                break
        #for sketch_id, sketch_name in enumerate(wires_sketch_names):
        #if not(np.argwhere(np.array(good) == sketch_id)/len(good) >= i/3 and np.argwhere(np.array(good) == sketch_id)/len(good) < i/3):
        #    continue
        print(sketch_id)
        file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
        #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
        file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    file.write("</table>\n</body>\n</html>")
#exit()

good = [[1, 2, 3, 4, 7, 8, 10, 12, 13, 14, 16, 17, 18, 19],
        [20, 22, 23, 24, 27, 28, 30, 32, 34, 35, 36, 42, 43],
        [45, 48, 50, 51, 54, 59, 60, 70, 71, 74, 78, 79, 86]]
for i in range(3):
    with open("../data/works_fine_"+str(i)+".html", "w") as file:
        file.write("<html>\n<body>\n")
        #file.write("<tr><th>Input Sketch</th><th>Processed Lines</th><th>Symmetry Plane 1</th><th>Symmetry Plane 2</th><th>Symmetry Plane 3</th><th>Right View</th><th>Lifting Right View</th><th>Left View</th><th>Lifting Left View</th><th>Top View </th><th>Lifting Top View</th></tr>\n")

        better_results = [
            [8, "designer2", "trash_bin"],
            [18, "designer2", "cabinet_01"],
            [9, "Professional3", "tubes"],
            [2, "designer2", "armchair_02"],
            [19, "student1", "house"],
            [20, "student1", "vacuum_cleaner"],
            [35, "student6", "house"],
            [36, "student1", "shampoo_bottle"],
            [15, "designer2", "cabinet_02"],
        ]

        similar_results = [
            [1, "student8", "house"],
            [3, "Prof2task2", "printer_02"],
            [4, "student8", "hairdryer"],
            [7, "designer2", "bookshelves"],
            [12, "designer2", "printer_02"],
            [13, "Prof2task2", "guitar_01"],
            [14, "Professional1", "house"],
            [16, "Prof2task2", "cabinet_01"],
            [21, "student3", "wobble_surface"],
            [22, "student4", "house"],
            [23, "student5", "house"],
            [24, "student9", "house"],
            [28, "Professional6", "house"],
            [30, "Professional5", "house"],
            [32, "Professional3", "house"],
            [42, "student7", "wobble_surface"],
            [45, "Professional6", "shampoo_bottle"],
        ]

        to_be_improved = [
            [10, "designer2", "chair_04"],
            [27, "student9", "tubes"],
            [5, "Professional2", "vacuum_cleaner"],
            [34, "student7", "vacuum_cleaner"],
            [6, "Professional6", "mouse"],
            [11, "designer2", "guitar_01"],
            [17, "Professional2", "house"],
            [25, "student8", "mouse"],
            [26, "designer2", "lamp"],
            [29, "Professional5", "wobble_surface"],
            [37, "Professional6", "hairdryer"],
            [43, "student2", "house"],
        ]

        limitations = [
            [31, "Professional4", "tubes"],
            [38, "Professional4", "vacuum_cleaner"],
            [39, "Professional4", "hairdryer"],
            [33, "student9", "bumps"],
            [41, "Professional3", "bumps"],
        ]

        failures = [
            [40, "Professional3", "mixer"],
            [44, "Professional6", "wobble_surface"],
        ]

        to_be_done = [
            [46, "Professional1", "tubes"],
            [47, "Prof5task2", "car_02"],
        ]


        worked_previously = [
            9, 11, 15, 21
        ]

        to_be_improved = [
            5, 6, 25, 26, 29, 40, 44, 65, 80, 83, 90
        ]

        #for sketch_id in range(1, 48):
        #    folder = os.path.join("../data", str(sketch_id))
        #    with open(os.path.join(folder, "sketch.json"), "r") as fp:
        #        sketch = json.load(fp)
        #    print("["+str(sketch_id)+", \""+sketch["designer"]+"\", \""+sketch["object_name"]+"\"],")
        #exit()

        #file.write("<h1>Works fine</h1>\n")
        file.write("<table style='width:100%'\n")
        #print(all_sketch_info)
        for sketch_id, designer_name, object_name in all_sketch_info:
        #for sketch_id, sketch_name in enumerate(wires_sketch_names):
            if not sketch_id in good[i]:
                continue
            #if not(np.argwhere(np.array(good) == sketch_id)/len(good) >= i/3 and np.argwhere(np.array(good) == sketch_id)/len(good) < i/3):
            #    continue
            print(sketch_id)
            file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
            #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
            file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

        file.write("</table>\n</body>\n</html>")


with open("../data/worked_previously.html", "w") as file:
    file.write("<h1>Worked previously</h1>\n")
    file.write("<table style='width:100%'\n")
    #print(all_sketch_info)
    for sketch_id, designer_name, object_name in all_sketch_info:
        if not sketch_id in worked_previously:
            continue
        print(sketch_id)
        file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
        #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
        file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    file.write("</table>\n</body>\n</html>")

with open("../data/to_be_improved.html", "w") as file:
    file.write("<h1>To be improved</h1>\n")
    file.write("<table style='width:100%'\n")
    #print(all_sketch_info)
    for sketch_id, designer_name, object_name in all_sketch_info:
        if not sketch_id in to_be_improved:
            continue
        print(sketch_id)
        file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
        #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
        file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    file.write("</table>\n</body>\n</html>")
    #file.write("<h1>Similar results</h1>\n")
    #file.write("<table style='width:100%'\n")
    ##print(all_sketch_info)
    #for sketch_id, designer_name, object_name in similar_results:
    #    #for sketch_id, sketch_name in enumerate(wires_sketch_names):
    #    print(sketch_id)
    #    file.write("<tr><th>"+designer_name+"_"+object_name+"</th></tr>\n")
    #    #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
    #    file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #file.write("</table>\n</body>\n</html>")

    #file.write("<h1>To be improved</h1>\n")
    #file.write("<table style='width:100%'\n")
    ##print(all_sketch_info)
    #for sketch_id, designer_name, object_name in to_be_improved:
    #    #for sketch_id, sketch_name in enumerate(wires_sketch_names):
    #    print(sketch_id)
    #    file.write("<tr><th>"+designer_name+"_"+object_name+"</th></tr>\n")
    #    #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
    #    file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #file.write("</table>\n</body>\n</html>")

    #file.write("<h1>Known limitations</h1>\n")
    #file.write("<table style='width:100%'\n")
    ##print(all_sketch_info)
    #for sketch_id, designer_name, object_name in limitations:
    #    #for sketch_id, sketch_name in enumerate(wires_sketch_names):
    #    print(sketch_id)
    #    file.write("<tr><th>"+designer_name+"_"+object_name+"</th></tr>\n")
    #    #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
    #    file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #file.write("</table>\n</body>\n</html>")

    #file.write("<h1>Failures</h1>\n")
    #file.write("<table style='width:100%'\n")
    ##print(all_sketch_info)
    #for sketch_id, designer_name, object_name in failures:
    #    #for sketch_id, sketch_name in enumerate(wires_sketch_names):
    #    print(sketch_id)
    #    file.write("<tr><th>"+designer_name+"_"+object_name+"</th></tr>\n")
    #    #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
    #    file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #file.write("</table>\n</body>\n</html>")

    #file.write("<h1>To be done</h1>\n")
    #file.write("<table style='width:100%'\n")
    ##print(all_sketch_info)
    #for sketch_id, designer_name, object_name in to_be_done:
    #    #for sketch_id, sketch_name in enumerate(wires_sketch_names):
    #    print(sketch_id)
    #    file.write("<tr><th>"+designer_name+"_"+object_name+"</th></tr>\n")
    #    #file.write(sketch_tr(designer_name+"_"+object_name, os.path.join("../data", str(sketch_id))))
    #    file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #file.write("</table>\n</body>\n</html>")

good = [1, 2, 3, 4, 7, 8, 10, 12, 13, 14, 16, 17, 18, 19,
        20, 22, 23, 24, 27, 28, 30, 32, 34, 35, 36, 42, 43,
        45, 48, 50, 51, 54, 59, 60]#, 70, 71, 74, 78, 79, 86]
#1 2 3 4 7 8 10 12 13 14 16 17 18 19 20 22 23 24 27 28 30 32 34 35 36 42 43 45 48 50 51 54 59 60 70 71 74 78 79 86
study_list = [
    1, 2, 3, 7, 8, 10, 12, 18, 20, 34, 36, 45, 54, 51, 60, 5, 65,
    35, 14, 17, 22, 24, 28, 30, 32, 43, 50, 16, 48, 59, 6, 25, 42, 29, 44,
    27, 4, 13, 19, 23, 11
]

print(all_sketch_info)
with open("../data/diff_study.html", "w") as file:
    file.write("<h1>Which one correspondends best to the Input sketch? Lifting3D or Ours?</h1>\n")
    file.write("<table style='width:100%'\n")
    file.write("<tr><th>Input Sketch</th><th>Lifting3D - Side View 1</th><th>Lifting3D - Side View 1</th><th>Ours - Side View 1</th><th>Ours - Side View 2</th></tr>\n")
    for sketch_id in study_list:

        for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
            if tmp_sketch_id == sketch_id:
                designer_name = tmp_designer_namer
                object_name = tmp_object_name
                break

        print(sketch_id)
        file.write("<tr><th>Input Sketch</th><th>Side-by-Side comparison</th></tr>\n")
        file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #for sketch_id in to_be_improved:

    #    for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
    #        if tmp_sketch_id == sketch_id:
    #            designer_name = tmp_designer_namer
    #            object_name = tmp_object_name
    #            break

    #    print(sketch_id)
    #    #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
    #    file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
    file.write("</table>\n</body>\n</html>")

short_list = [23, 75, 17, 69, 21, 18, 79, 54, 53, 52,  1,  9, 30, 22, 86, 56, 14, 59, 26, 24, 61, 42, 65, 32, 50, 43, 25, 27,  8, 87, 78,  4, 70, 15, 10, 19, 66, 29, 73,  2, 74, 80, 36, 40, 44,  6, 34, 16, 48,  5, 28, 11,  3, 41, 35, 37, 62, 76, 77, 71, 20, 51, 13,  7, 45, 58, 12, 55, 64, 33, 68, 60, 89, 90, 82, 83, 88, 67, 84]
short_list = [23, 17, 69, 21, 18, 79, 54, 53, 52,  1,  9, 30, 22, 86, 56, 14, 59, 26, 24, 61, 42, 65, 32, 50, 43, 25, 27,  8, 87, 78,  4, 70, 15, 10, 19, 29, 2, 36, 44,  6, 34, 16, 48,  5, 28, 11,  3, 35, 37, 71, 20, 51, 13,  7, 45, 12, 60, 89, 90, 83, 88, 67, 84, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102]
print(len(short_list))
#23 17 69 21 18 79 54 53 52  1  9 30 22 86 56 14 59 26 24 61 42 65 32 50 43 25 27  8 87 78  4 70 15 10 19 29 2 36 44  6 34 16 48  5 28 11  3 35 37 71 20 51 13  7 45 12 60 89 90 83 88 67 84 91 92 93 94 95 96 97 98 99 100 101 102
final_sketch_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 34, 35, 36, 37, 42, 43, 44, 45]

final_sketch_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                    32, 34, 35, 36, 37, 42, 43, 44, 45]
new_sketch_ids = [48, 50, 51, 54, 59, 60, 65, 70, 71, 78,
                  79, 92, 102]
#1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 32 34 35 36 37 42 43 44 45 48 50 51 54 59 60 65 70 71 78 79 92 102

good_sketches = [
    1, 2, 3, 4, 5, 7, 8, 10, 11, 12, 16, 19, 20,
    27, 28, 29, 30, 32, 35, 36, 37, 42, 45, 48, 50,
    51, 54, 59, 65, 70, 71, 78, 79, 102
]

medium_sketches = [
    9, 13, 18, 26, 34, 43, 60, 92
]

bad_sketches = [
    6, 14, 15, 17, 21, 22, 23, 24, 25, 44,
]

final_sketch_order = [
    1, 2, 3, 4, 5, 7, 8, 10, 11,
    12, 16, 19, 20, 9, 13, 18, 26, 34, 43,
    60, 92, 27, 28, 29, 30, 32, 35, 36, 37,
    42, 45, 48, 50, 14, 15, 17, 21, 22, 23,
    24, 25, 44, 6, 51, 54, 59, 65, 70, 71, 78, 79, 102
]
final_sketch_order_1 = [
    1, 2, 3, 4, 5, 7, 8, 10, 11]
final_sketch_order_2 = [
    12, 16, 19, 20, 9, 13, 18, 26, 34, 43]
final_sketch_order_3 = [
    60, 92, 27, 28, 29, 30, 32, 35, 36, 37]
final_sketch_order_4 = [
    42, 45, 48, 50, 14, 15, 17, 21, 22, 23]
final_sketch_order_5 = [
    24, 25, 44, 6, 51, 54, 59, 65, 70, 71, 78, 79, 102
]
final_sketch_order_6 = np.arange(130, 194).tolist()

print(len(final_sketch_order))
print(len(final_sketch_ids+new_sketch_ids))
for s_id in final_sketch_ids+new_sketch_ids:
    if not(s_id in final_sketch_order):
        print(s_id)
nb_strokes = []
nb_batches = []
all_designer_names = set()
designer_object_tuples = []
for sketch_id in final_sketch_order:

    for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
        if tmp_sketch_id == sketch_id:
            designer_name = tmp_designer_namer
            object_name = tmp_object_name
            print(sketch_id, designer_name, object_name)
            all_designer_names.add(designer_name)
            #if "student" in designer_name or "Professional" in designer_name:
            #    designer_object_tuples.append([designer_name, object_name])
            #break
    tmp_data_folder = os.path.join("../data", str(sketch_id))
    with open(os.path.join(tmp_data_folder, "sketch.json"), "r") as fp:
        sketch = json.load(fp)
    nb_strokes.append(len(sketch["strokes_topology"]))
    with open(os.path.join(tmp_data_folder, "pickle", "batches.pkl"), "rb") as fp:
        batches = pickle.load(fp)
    nb_batches.append(len(batches))

print(designer_object_tuples)
print(len(designer_object_tuples))
print(list(all_designer_names))
print(len(list(all_designer_names)))
print(nb_strokes)
print(nb_batches)
print(np.median(nb_strokes))
print(np.median(nb_batches))
#exit()

ok_reconstructions = [141, 152, 153, 156, 165, 167, 168, 170, 172]

rough_reconstructions = [134, 135, 138, 142, 147, 148, 149, 150, 166]

curves = [133, 137, 140, 145, 146, 151, 158, 161]

non_axis_aligned = [181, 188, 190, 191]
print(len(final_sketch_order))
print(len(ok_reconstructions))
print(len(rough_reconstructions))
print(len(curves))
print(len(non_axis_aligned))
print(len(final_sketch_order) + len(ok_reconstructions) + len(rough_reconstructions) + len(curves) + len(non_axis_aligned))
exit()
print("new reconstructions")
print(len(ok_reconstructions + rough_reconstructions + curves + non_axis_aligned))

with open("../data/new_sketches_ok_reconstructions.html", "w") as file:
    #file.write("<h1>Ok reconstructions</h1>\n")

    file.write("<table style='width:100%'\n")
    file.write("<tr><th>Input Sketch</th><th>Ours</th></tr>\n")
    for sketch_id in ok_reconstructions:
        if not os.path.exists(os.path.join("../data", str(sketch_id), "sketch.json")):
            continue
        with open(os.path.join("../data", str(sketch_id), "sketch.json"), "r") as fp:
            sketch = json.load(fp)
            designer_name = sketch["designer"]
            object_name = sketch["object_name"]

        file.write(sketch_tr_video_blender(str(sketch_id)+" "+designer_name+"_"+object_name, str(sketch_id)))

with open("../data/new_sketches_rough_reconstructions.html", "w") as file:
    #file.write("<h1>Ok reconstructions</h1>\n")

    file.write("<table style='width:100%'\n")
    file.write("<tr><th>Input Sketch</th><th>Ours</th></tr>\n")
    for sketch_id in rough_reconstructions:
        if not os.path.exists(os.path.join("../data", str(sketch_id), "sketch.json")):
            continue
        with open(os.path.join("../data", str(sketch_id), "sketch.json"), "r") as fp:
            sketch = json.load(fp)
            designer_name = sketch["designer"]
            object_name = sketch["object_name"]

        file.write(sketch_tr_video_blender(str(sketch_id)+" "+designer_name+"_"+object_name, str(sketch_id)))

with open("../data/new_sketches_non_axis_aligned.html", "w") as file:
    #file.write("<h1>Ok reconstructions</h1>\n")

    file.write("<table style='width:100%'\n")
    file.write("<tr><th>Input Sketch</th><th>Ours</th></tr>\n")
    for sketch_id in non_axis_aligned:
        if not os.path.exists(os.path.join("../data", str(sketch_id), "sketch.json")):
            continue
        with open(os.path.join("../data", str(sketch_id), "sketch.json"), "r") as fp:
            sketch = json.load(fp)
            designer_name = sketch["designer"]
            object_name = sketch["object_name"]

        file.write(sketch_tr_video_blender(str(sketch_id)+" "+designer_name+"_"+object_name, str(sketch_id)))

with open("../data/new_sketches_curves.html", "w") as file:
    #file.write("<h1>Ok reconstructions</h1>\n")

    file.write("<table style='width:100%'\n")
    file.write("<tr><th>Input Sketch</th><th>Ours</th></tr>\n")
    for sketch_id in curves:
        if not os.path.exists(os.path.join("../data", str(sketch_id), "sketch.json")):
            continue
        with open(os.path.join("../data", str(sketch_id), "sketch.json"), "r") as fp:
            sketch = json.load(fp)
            designer_name = sketch["designer"]
            object_name = sketch["object_name"]

        file.write(sketch_tr_video_blender(str(sketch_id)+" "+designer_name+"_"+object_name, str(sketch_id)))
exit()

with open("../data/study_objects.html", "w") as file:
    file.write("<table style='width:100%'\n")
    #file.write("<tr><th>Input Sketch</th><th>Lifting3D - Side View 1</th><th>Lifting3D - Side View 1</th><th>Ours - Side View 1</th><th>Ours - Side View 2</th></tr>\n")
    #for sketch_id in study_list:
    #for sketch_id in np.unique(final_sketch_ids + new_sketch_ids):
    for sketch_id in final_sketch_order:

        for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
            if tmp_sketch_id == sketch_id:
                designer_name = tmp_designer_namer
                object_name = tmp_object_name
                break

        print(sketch_id)
        file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
        #file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
        file.write(sketch_tr(designer_name+"_"+object_name, str(sketch_id)))

    #for sketch_id in to_be_improved:

    #    for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
    #        if tmp_sketch_id == sketch_id:
    #            designer_name = tmp_designer_namer
    #            object_name = tmp_object_name
    #            break

    #    print(sketch_id)
    #    #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
    #    file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
    file.write("</table>\n</body>\n</html>")

for turntable_id, sketch_ids in enumerate([final_sketch_order_1, final_sketch_order_2, final_sketch_order_3, final_sketch_order_4, final_sketch_order_5]):
    with open("../data/study_videos_"+str(turntable_id)+".html", "w") as file:
        file.write("<table style='width:100%'\n")
        #for sketch_id in study_list:
        file.write("<tr><th>Color code</th><th>Study votes  </th><th>Input Sketch</th><th>[Gryaditskaya et al. 2020]</th><th>Ours</th></tr>\n")
        #for sketch_id in np.unique(final_sketch_ids + new_sketch_ids):
        #for sketch_id in final_sketch_order:
        for sketch_id in sketch_ids:

            for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
                if tmp_sketch_id == sketch_id:
                    designer_name = tmp_designer_namer
                    object_name = tmp_object_name
                    break

            print(sketch_id)
            #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
            #file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
            file.write(sketch_tr_video(designer_name+"_"+object_name, str(sketch_id)))

    #for sketch_id in to_be_improved:

    #    for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
    #        if tmp_sketch_id == sketch_id:
    #            designer_name = tmp_designer_namer
    #            object_name = tmp_object_name
    #            break

    #    print(sketch_id)
    #    #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
    #    file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
        file.write("</table>\n</body>\n</html>")


with open("../data/batches.html", "w") as file:
    file.write("<table style='width:100%'\n")
    #for sketch_id in study_list:
    file.write("<tr><th>Sketch sequence</th><th>Symmetry correspondence density</th><th>Segmented building blocks</th></tr>\n")
    #for sketch_id in np.unique(final_sketch_ids + new_sketch_ids):
    for sketch_id in final_sketch_order:

        for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
            if tmp_sketch_id == sketch_id:
                designer_name = tmp_designer_namer
                object_name = tmp_object_name
                break

        print(sketch_id)
        #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
        #file.write(pilot_sketch_tr(designer_name+"_"+object_name, str(sketch_id)))
        file.write(sketch_tr_batches(designer_name+"_"+object_name, str(sketch_id)))

    file.write("</table>\n</body>\n</html>")


for turntable_id, sketch_ids in enumerate([final_sketch_order_1, final_sketch_order_2, final_sketch_order_3, final_sketch_order_4, final_sketch_order_5]):
    with open("../data/turntables_"+str(turntable_id)+".html", "w") as file:
        file.write("<table style='width:100%'\n")
        #for sketch_id in study_list:
        file.write("<tr><th>Input Sketch</th><th>Turntable animation</th><th>Segmented building blocks</th></tr>\n")
        #for sketch_id in np.unique(final_sketch_ids + new_sketch_ids):
        #for sketch_id in final_sketch_order:
        for sketch_id in sketch_ids:

            for tmp_sketch_id, tmp_designer_namer, tmp_object_name in all_sketch_info:
                if tmp_sketch_id == sketch_id:
                    designer_name = tmp_designer_namer
                    object_name = tmp_object_name
                    break

            print(sketch_id)
            #file.write("<tr><th>"+str(sketch_id) + " " +designer_name+"_"+object_name+"</th></tr>\n")
            file.write(sketch_tr_video_blender(designer_name+"_"+object_name, str(sketch_id)))

        file.write("</table>\n</body>\n</html>")
