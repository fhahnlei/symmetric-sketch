#!/usr/bin/env zsh
sketch_id=$1

echo $sketch_id
python diff_study.py --sketch_id=$sketch_id
#
cd ../data/$sketch_id/pilot_study_a
/usr/local/bin/./ffmpeg -y -i %03d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4
cd ..
cd pilot_study_b
/usr/local/bin/./ffmpeg -y -i %03d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4
cd ..
#
#pwd
/usr/local/bin/./ffmpeg -y -i pilot_study_a/out.mp4 -i pilot_study_b/out.mp4 -filter_complex hstack=inputs=2 out.mp4

pwd
cp out.mp4 ../../../diff_study/public/data/study/$sketch_id/
cp pilot_study_a/000.png ../../../diff_study/public/data/study/$sketch_id/
