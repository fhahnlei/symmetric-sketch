import numpy as np
import os, sys, json

answers_folder = os.path.join("../diff_study_answers")
data_folder = os.path.join("../data")

choices = {
    "wires": [],
    "ours": [],
    "both_plausible": [],
    "both_implausible": [],
}

all_sketch_wires = np.zeros(103, dtype=int)
sketch_ids_answers = []
for x in os.listdir(answers_folder):
    print(x)
    with open(os.path.join(answers_folder, x), "r") as fp:
        answers = fp.read().split(",")
    #print(answers)
    for a in answers:
        sketch_id = a.split("_")[0]
        result = a.split("_")[1]+"_"+a.split("_")[2]
        #print(sketch_id, result)
        if int(sketch_id) == 0:
            continue

        wires_choice_file_name = os.path.join(data_folder, sketch_id, "pilot_choice.json")
        with open(wires_choice_file_name, "r") as fp:
            wires_choice = json.load(fp)

        choice_inv = {}
        choice_inv[wires_choice["wires"]] = "wires"
        choice_inv[wires_choice["ours"]] = "ours"
        all_sketch_wires[int(sketch_id)] = wires_choice["wires"]
        if result == "b_plausible":
            choices[choice_inv[0]].append(x)
            sketch_ids_answers.append([int(sketch_id), result, choice_inv[0]])
        elif result == "c_plausible":
            choices[choice_inv[1]].append(x)
            sketch_ids_answers.append([int(sketch_id), result, choice_inv[1]])
        elif result == "both_plausible":
            choices["both_plausible"].append(x)
            sketch_ids_answers.append([int(sketch_id), result, "both_plausible"])
        elif result == "both_implausible":
            choices["both_implausible"].append(x)
            sketch_ids_answers.append([int(sketch_id), result, "both_implausible"])
print(sketch_ids_answers)
ttest_answers = np.zeros([len(sketch_ids_answers), 2], dtype=int)

for s_id, s in enumerate(sketch_ids_answers):
    if s[-1] == "ours":
        ttest_answers[s_id][0] = 1
    elif s[-1] == "wires":
        ttest_answers[s_id][1] = 1
    elif s[-1] == "both_plausible":
        ttest_answers[s_id][0] = 1
        ttest_answers[s_id][1] = 1
print(len(ttest_answers))
np.save("ttest_data", ttest_answers)
#exit()
print(all_sketch_wires)
#exit()
print(choices)
total_nb_answers = len(choices["wires"]) + len(choices["ours"]) + len(choices["both_plausible"]) + len(choices["both_implausible"])
print("wires", len(choices["wires"]), len(choices["wires"])/total_nb_answers)
print("ours", len(choices["ours"]), len(choices["ours"])/total_nb_answers)
print("both_plausible", len(choices["both_plausible"]), len(choices["both_plausible"])/total_nb_answers)
print("both_implausible", len(choices["both_implausible"]), len(choices["both_implausible"])/total_nb_answers)

sorted_ids = np.argsort([int(sketch_id[0]) for sketch_id in sketch_ids_answers])
#for id in sorted_ids:
#    print(sketch_ids_answers[id])

votes_per_sketch = {}
for id in sorted_ids:
    #print(sketch_ids_answers[id])
    if sketch_ids_answers[id][0] in votes_per_sketch.keys():
       votes_per_sketch[sketch_ids_answers[id][0]].append(sketch_ids_answers[id][-1])
    else:
        votes_per_sketch[sketch_ids_answers[id][0]] = [sketch_ids_answers[id][-1]]

ours_counters = []
wires_counters = []
both_plausible = []
both_implausible = []
undecided = []
for id in votes_per_sketch.keys():
    print("sketch", id)
    ours_count = np.sum(np.array(votes_per_sketch[id]) == "ours")
    wires_count = np.sum(np.array(votes_per_sketch[id]) == "wires")
    both_plausible_count = np.sum(np.array(votes_per_sketch[id]) == "both_plausible")
    both_implausible_count = np.sum(np.array(votes_per_sketch[id]) == "both_implausible")
    print(ours_count, wires_count)
    if ours_count > 2:
        print("ours")
        ours_counters.append(id)
    elif wires_count > 2:
        print( "wires")
        wires_counters.append(id)
        print(votes_per_sketch[id])
    elif both_plausible_count > 2:
        both_plausible.append(id)
    elif both_implausible_count > 2:
        both_implausible.append(id)
    else:
        #print(id, "undecided")
        print("undecided")
        print(votes_per_sketch[id])
        undecided.append(id)
    print()

#print(votes_per_sketch)
#print(votes_per_sketch[65])


print("len(ours_counters)")
print(len(ours_counters), len(ours_counters)/52)
print("len(wires_counters)")
print(len(wires_counters), len(wires_counters)/52)
print("len(both_plausible_counters)")
print(len(both_plausible), len(both_plausible)/52)
print("len(both_implausible_counters)")
print(len(both_implausible), len(both_implausible)/52)
print("len(undecided)")
print(len(undecided), len(undecided)/52)
undecided = len(list(votes_per_sketch.keys())) - len(ours_counters) - len(wires_counters)
print("undecided", undecided)

colors = {"both_implausible": "#666666",
          "both_plausible": "#7570b3",
          "wires": "#d95f02",
          "ours": "#1b9e77"}

import matplotlib.pyplot as plt
case_width = 20
case_height = 20

per_sketch_counts = []
for v_id, s_id in enumerate(votes_per_sketch.keys()):
    ours_count = np.sum(np.array(votes_per_sketch[s_id]) == "ours")
    wires_count = np.sum(np.array(votes_per_sketch[s_id]) == "wires")
    both_plausible_count = np.sum(np.array(votes_per_sketch[s_id]) == "both_plausible")
    both_implausible_count = np.sum(np.array(votes_per_sketch[s_id]) == "both_implausible")
    per_sketch_counts.append([ours_count, wires_count, both_plausible_count, both_implausible_count])

    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    rectangle = plt.Rectangle((0, 0), case_width, ours_count*case_height, fc=colors["ours"], ec="black", lw=1)
    axes.add_patch(rectangle)
    rectangle = plt.Rectangle((0, (ours_count)*20), case_width, both_plausible_count*case_height, fc=colors["both_plausible"], ec="black", lw=1)
    axes.add_patch(rectangle)
    rectangle = plt.Rectangle((0, (ours_count+both_plausible_count)*20), case_width, wires_count*case_height, fc=colors["wires"], ec="black", lw=1)
    axes.add_patch(rectangle)
    rectangle = plt.Rectangle((0, (ours_count+wires_count+both_plausible_count)*20), case_width, both_implausible_count*case_height, fc=colors["both_implausible"], ec="black", lw=1)
    axes.add_patch(rectangle)
    axes.axis("equal")
    folder = os.path.join("../data", str(s_id))
    tmp_file_name = os.path.join(folder, "user_study_column.png")
    my_dpi = 100
    #plt.show()
    axes.set_xlim(0, 20)
    axes.set_ylim(100, 0)
    #fig.set_size_inches((20 / my_dpi, 100 / my_dpi))
    plt.savefig(tmp_file_name, transparent=True)
    plt.close(fig)
exit()

per_sketch_counts = np.array(per_sketch_counts)
print(per_sketch_counts)
print(per_sketch_counts[per_sketch_counts[:, 0].argsort()])
#exit()
ind = np.lexsort((per_sketch_counts[:, 1], per_sketch_counts[:, 3], per_sketch_counts[:, 2], per_sketch_counts[:, 0]))
print(per_sketch_counts[ind])
#exit()

fig, axes = plt.subplots(nrows=1, ncols=1)
fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                    bottom=0.0,
                    top=1.0)
#for v_id, s_id in enumerate(votes_per_sketch.keys()):
for v_id, s_id in enumerate(np.array(list(votes_per_sketch.keys()))[np.flip(ind)]):
    print(s_id)

    ours_count = np.sum(np.array(votes_per_sketch[s_id]) == "ours")
    print(ours_count)
    rectangle = plt.Rectangle((v_id*case_width, 0), case_width, ours_count*case_height, fc=colors["ours"], ec="black", lw=0.5)
    #rectangle = plt.Rectangle((v_id*case_width, 0), case_width, ours_count*case_height, fc=colors["ours"])
    axes.add_patch(rectangle)

    both_plausible_count = np.sum(np.array(votes_per_sketch[s_id]) == "both_plausible")
    print(both_plausible_count)
    rectangle = plt.Rectangle((v_id*case_width, (ours_count)*20), case_width, both_plausible_count*case_height, fc=colors["both_plausible"], ec="black", lw=0.5)
    #rectangle = plt.Rectangle((v_id*case_width, (ours_count+wires_count)*20), case_width, both_plausible_count*case_height, fc=colors["both_plausible"])
    axes.add_patch(rectangle)

    wires_count = np.sum(np.array(votes_per_sketch[s_id]) == "wires")
    print(wires_count)
    rectangle = plt.Rectangle((v_id*case_width, (ours_count+both_plausible_count)*20), case_width, wires_count*case_height, fc=colors["wires"], ec="black", lw=0.5)
    #rectangle = plt.Rectangle((v_id*case_width, ours_count*20), case_width, wires_count*case_height, fc=colors["wires"])
    axes.add_patch(rectangle)

    both_implausible_count = np.sum(np.array(votes_per_sketch[s_id]) == "both_implausible")
    print(both_implausible_count)
    #rectangle = plt.Rectangle((v_id*case_width, (ours_count+wires_count+both_plausible_count)*20), case_width, both_implausible_count*case_height, fc=colors["both_implausible"])
    rectangle = plt.Rectangle((v_id*case_width, (ours_count+wires_count+both_plausible_count)*20), case_width, both_implausible_count*case_height, fc=colors["both_implausible"], ec="black", lw=0.5)
    axes.add_patch(rectangle)
    #if v_id < len(ind):
    #    plt.plot([v_id*case_width, v_id*case_width], [0, 100], lw=1, c="black")
    #if ours_count == 0:
    #    break
    #plt.plot([0, len(ind)*case_width], [0, 0], lw=0.5, c="black")
    #plt.plot([0, len(ind)*case_width], [100, 100], lw=0.5, c="black")
    #plt.plot([len(ind)*case_width, len(ind)*case_width], [0, 100], lw=0.5, c="black")
axes.axis("equal")
#axes.axis("off")
#tmp_file_name = os.path.join(folder, "batch_arc_diagram.svg")
tmp_file_name = os.path.join("user_study_figure.svg")
my_dpi = 100
fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
plt.savefig(tmp_file_name, transparent=True)
plt.close(fig)
#plt.show()

