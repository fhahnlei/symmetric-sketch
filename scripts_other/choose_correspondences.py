import os, sys, inspect
from skspatial.objects import Plane
from copy import deepcopy
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(1, os.path.join(current_dir, "../src"))
sys.path.insert(1, os.path.join(current_dir, "../src/fitCurves"))
import sketch_wires
from matplotlib.collections import LineCollection
import tools_3d
import seaborn as sns
import json
import polyscope as ps
import numpy as np
import pickle
import utils_plot
import matplotlib.pyplot as plt

my_dpi = 100

def plot_correspondences(pickle_folder):
    data_folder = "/".join(pickle_folder.split("/")[:-1])
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        camera = pickle.load(fp)
    #correspondence_graphs = []
    #with open(os.path.join(pickle_folder, "reference_correspondences.pkl"), "rb") as fp:
    #    correspondence_graphs = pickle.load(fp)
    #for batches_name in ["batches_results_ref.json", "batches_results_normal.json"]:
    for batches_name in ["batches_results_normal.json", "batches_results_bootstrapped.json"]:
        if not os.path.exists(os.path.join(data_folder, batches_name)):
            continue
        with open(os.path.join(data_folder, batches_name), "rb") as fp:
            batches = json.load(fp)
        all_correspondences = []
        for batch in batches:
            for corr in batch["final_correspondences"]:
                all_correspondences.append([corr["stroke_id_0"], corr["stroke_id_1"], corr["symmetry_plane_id"]])
        plt.rcParams["figure.figsize"] = (20, 10)
        plt.rcParams["figure.dpi"] = 500

        utils_plot.correspondence_visualization_only_reconstruction(sketch, camera, all_correspondences, VERBOSE=False)
        output_file_name = os.path.join(data_folder, "correspondence_visualization_result_normal.png")
        if "ref" in batches_name:
            output_file_name = os.path.join(data_folder, "correspondence_visualization_result_ref.png")
        if "bootstrapped" in batches_name:
            output_file_name = os.path.join(data_folder, "correspondence_visualization_result_bootstrapped.png")
        print(output_file_name)
        plt.savefig(output_file_name)

def ortho_views(folder):
    turntable_folder = os.path.join(folder, "turntable_2d")
    if not os.path.exists(turntable_folder):
        os.mkdir(turntable_folder)
    pickle_folder = os.path.join(folder, "pickle")
    result_file_name = "batches_results_normal_non_symmetric.json"
    result_file_name = "batches_results_normal.json"
    with open(os.path.join(folder, result_file_name), "rb") as fp:
        batches_results = json.load(fp)
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    batch = batches_results[-1]
    my_dpi = 100
    fixed_strokes = batch["fixed_strokes"]
    proxies = batch["final_proxies"]
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0:
            fixed_strokes[p_id] = p

    result_folder = os.path.join(folder, "ortho_views")
    if "non_symmetric" in result_file_name:
        fixed_strokes = batch["with_non_symmetric_strokes"]
        result_folder = os.path.join(folder, "ortho_views_with_non_symmetric")

    if not os.path.exists(result_folder):
        os.mkdir(result_folder)


    # reproject original strokes on 3d geometries
    sketch_file_name = os.path.join(folder, "sketch.json")
    original_sketch, _, _ = sketch_wires.read_data_wires(sketch_file_name)

    new_fixed_strokes = []
    new_pressures = []
    for s_id, s in enumerate(fixed_strokes):
        if len(s) == 0:
            continue
        print(s_id, sketch.strokes[s_id].original_id)
        for orig_s_id in sketch.strokes[s_id].original_id:
            #print(orig_s_id)
            #print(orig_s_id, len(original_sketch.strokes))
            original_s = [tmp_s for tmp_s in original_sketch.strokes if tmp_s.original_id[0] == orig_s_id][0]
            original_pts = np.array(original_s.linestring.linestring)
            original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])
            #print(original_pressures)
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                line_v /= np.linalg.norm(line_v)
                #print(original_pts)
                #lifted_points = np.array(cam.lift_polyline_close_to_line(
                #    original_pts, line_p, line_v))
                lifted_points = np.array([cam.lift_point_close_to_line(
                    p, line_p, line_v) for p in original_pts])
                #new_fixed_strokes.append(lifted_points)
                #new_pressures.append(original_pressures)
            else:
                lifted_points = np.array([cam.lift_point_close_to_polyline(
                    p, np.array(s)) for p in original_pts])
            new_fixed_strokes.append(lifted_points)
            new_pressures.append(original_pressures)

    fixed_strokes = deepcopy(new_fixed_strokes)
    #ps.init()
    #for s_id, s in enumerate(fixed_strokes):
    #    if len(s) == 0:
    #        continue
    #    ps.register_curve_network(str(s_id), np.array(s),
    #                              np.array([[i, i+1] for i in range(len(s)-1)]))
    #ps.show()

    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    points = np.array([p for s in fixed_strokes for p in s])
    points = np.array([inter[0] for batch in batches_results for inter in batch["intersections"]])
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2
    cam_vec = np.array(cam.cam_pos)-bbox_center
    cam_vec /= np.linalg.norm(cam_vec)
    print(cam_vec)
    x_axis = np.array([1, 0, 0])
    print(np.dot(cam_vec, x_axis))
    cam_angle_x = np.rad2deg(np.arccos(cam_vec[0]))
    cam_angle_y = cam_angle_x + 90.0
    cam_angle_z = np.rad2deg(np.arccos(cam_vec[2])) - 90
    print(cam_angle_x, cam_angle_y, cam_angle_z)
    rot_mats = [
        tools_3d.get_rotation_mat_z(np.deg2rad(0)),
        tools_3d.get_rotation_mat_z(np.deg2rad(90)),
        np.matmul(tools_3d.get_rotation_mat_x(np.deg2rad(-cam_angle_z)),
                  tools_3d.get_rotation_mat_z(np.deg2rad(cam_angle_x))),
        np.matmul(tools_3d.get_rotation_mat_x(np.deg2rad(-cam_angle_z)),
                  tools_3d.get_rotation_mat_z(np.deg2rad(cam_angle_y))),
    ]
    for angle_id, angle in enumerate([0, 90, cam_angle_x, cam_angle_y]):
    #for angle_id, rot_mat in enumerate(rot_mats):
    #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        #rot_mat = tools_3d.get_rotation_mat_y(np.deg2rad(cam_angle_z))
        #rot_mat_x = tools_3d.get_rotation_mat_z(np.deg2rad(-cam_angle_x))
        #rot_mat_z = tools_3d.get_rotation_mat_y(np.deg2rad(10))
        #rot_mat = np.matmul(rot_mat_x, rot_mat_z)
        #rot_mat = np.matmul(rot_mat_z, rot_mat_x)
        #rot_mat = rot_mat_x
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(-angle))
        #print(angle)
        for s_id, s in enumerate(fixed_strokes):
            #print(s_id, s)
            if len(s) == 0:
                continue
            s = np.array(s)
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)
            segs = [[proj_s[p_id], proj_s[p_id+1]] for p_id in range(len(proj_s)-1)]

            widths = np.array([(new_pressures[s_id][p_id]+new_pressures[s_id][p_id+1])/2
                               for p_id in range(len(proj_s)-1)])
            colors = [(0, 0, 0, w) for w in widths]
            #widths *= 1.2
            widths += 0.5
            lc = LineCollection(segs, linewidths=widths, colors=colors)

            #lc = LineCollection(segs, linewidths=1.0, colors="black")
            axes.add_collection(lc)
        axes.set_xlim(x_lim)
        axes.set_ylim(y_lim)
        axes.set_aspect("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(result_folder, str(np.char.zfill(str(angle_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)

def turntable_2d(folder, result_file_name="batches_results_normal.json"):
    turntable_folder = os.path.join(folder, "turntable_2d")
    pickle_folder = os.path.join(folder, "pickle")
    #result_file_name = "batches_results_normal_non_symmetric.json"
    #result_file_name = "batches_results_normal.json"
    if "non_symmetric" in result_file_name:
        turntable_folder = os.path.join(folder, "turntable_2d_non_symmetric")
    if "bootstrapped" in result_file_name:
        turntable_folder = os.path.join(folder, "turntable_2d_bootstrapped")
    if "post" in result_file_name:
        turntable_folder = os.path.join(folder, "turntable_2d_post")
    if "non_symmetric" in result_file_name:
        turntable_folder = os.path.join(folder, "turntable_2d_non_symmetric")
    if not os.path.exists(turntable_folder):
        os.mkdir(turntable_folder)
    with open(os.path.join(folder, result_file_name), "rb") as fp:
        batches_results = json.load(fp)
    replaced_strokes = []
    if "post" in result_file_name:
        replaced_strokes = batches_results[-1]["replaced_strokes"]
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    batch = batches_results[-1]
    fixed_strokes = batch["fixed_strokes"]
    proxies = batch["final_proxies"]
    my_dpi = 100
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0 and len(fixed_strokes[p_id]) == 0:
            fixed_strokes[p_id] = p
    symmetric_stroke_ids = []
    if "non_symmetric" in result_file_name:
        symmetric_stroke_ids = [s_id for s_id, s in enumerate(fixed_strokes) if len(s) > 0]
        fixed_strokes = batch["with_non_symmetric_strokes"]


    # reproject original strokes on 3d geometries
    sketch_file_name = os.path.join(folder, "sketch.json")
    original_sketch, _, _ = sketch_wires.read_data_wires(sketch_file_name)

    new_fixed_strokes = []
    new_pressures = []
    new_replaced_strokes = []
    new_symmetric_stroke_ids = []
    #ps.init()
    for s_id, s in enumerate(fixed_strokes):
        if len(s) == 0:
            continue
        for orig_s_id in sketch.strokes[s_id].original_id:
            #print(orig_s_id)
            original_s = [tmp_s for tmp_s in original_sketch.strokes if tmp_s.original_id[0] == orig_s_id][0]
            original_pts = np.array(original_s.linestring.linestring)
            #if s_id == 28:
            #    print(orig_s_id, original_pts)
            original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])
            #print(original_pressures)
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                line_v /= np.linalg.norm(line_v)
                #print(original_pts)
                #lifted_points = np.array(cam.lift_polyline_close_to_line(
                #    original_pts, line_p, line_v))
                lifted_points = np.array([cam.lift_point_close_to_line(
                    p, line_p, line_v) for p in original_pts])
                #new_fixed_strokes.append(lifted_points)
                #new_pressures.append(original_pressures)
            else:
                if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                    plane = Plane.best_fit(np.array(s))
                    lifted_points = cam.lift_polyline_to_plane(original_pts, s[0], plane.normal)
                else:
                    #print(s_id)
                    lifted_points = np.array([cam.lift_point_close_to_polyline_v3(
                        p, np.array(s), return_axis_point=True) for p in original_pts])
                    #print(lifted_points)
            if s_id in replaced_strokes:
                new_replaced_strokes.append(len(new_fixed_strokes))
            if s_id in symmetric_stroke_ids:
                new_symmetric_stroke_ids.append(len(new_fixed_strokes))
            #if s_id == 28:
            #    print(lifted_points)
            #    utils_plot.sketch_plot_simple(original_sketch, plot=False)
            #    proj_s = np.array(cam.project_polyline(lifted_points))
            #    plt.plot(proj_s[:, 0], proj_s[:, 1], c="r", lw=4)
            #    plt.plot(original_pts[:, 0], original_pts[:, 1], c="b", lw=4)
            #    plt.show()
            new_fixed_strokes.append(lifted_points)
            #ps.register_curve_network(str(orig_s_id), np.array(lifted_points),
            #                          np.array([[i, i+1] for i in range(len(lifted_points)-1)]))
            new_pressures.append(original_pressures)
    #ps.show()

    fixed_strokes = deepcopy(new_fixed_strokes)
    #ps.init()
    #for s_id, s in enumerate(fixed_strokes):
    #    if len(s) == 0:
    #        continue
    #    ps.register_curve_network(str(s_id), np.array(s),
    #                              np.array([[i, i+1] for i in range(len(s)-1)]))
    #ps.show()
    #exit()

    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    points = np.array([p for s in fixed_strokes for p in s])
    points = np.array([inter[0] for batch in batches_results for inter in batch["intersections"]])
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2
    for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
    #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(angle))
        #print(angle)
        for s_id, s in enumerate(fixed_strokes):
            #print(s_id, s)
            if len(s) == 0:
                continue
            s = np.array(s)
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)
            segs = [[proj_s[p_id], proj_s[p_id+1]] for p_id in range(len(proj_s)-1)]
            widths = np.array([(new_pressures[s_id][p_id]+new_pressures[s_id][p_id+1])/2
                               for p_id in range(len(proj_s)-1)])
            #colors = np.zeros([len(widths), 4])
            colors = [(0, 0, 0, w) for w in widths]
            #print(colors)
            #colors[:, -1] = widths
            #print(colors)
            #widths *= 1.2
            widths += 0.5
            #if ("post" in result_file_name and s_id in new_replaced_strokes) or \
            #        ("non_symmetric" in result_file_name and not s_id in new_symmetric_stroke_ids):
            #    colors = [(1, 0, 0, 1) for w in widths]
            #    widths = 2.0
            #widths *= 3.0
            lc = LineCollection(segs, linewidths=widths, colors=colors)
            axes.add_collection(lc)
        #axes.set_xlim(x_lim)
        #axes.set_ylim(y_lim)
        pp = np.array([356.35539412325687,-276.38582435039547])
        axes.set_xlim(np.mean(x_lim)-256, np.mean(x_lim)+256)
        axes.set_ylim(np.mean(y_lim)+256, np.mean(y_lim)-256)
        print(np.mean(x_lim))
        print(np.mean(y_lim))
        axes.set_aspect("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(turntable_folder, str(np.char.zfill(str(angle_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)
        #plt.show()

def turntable_2d_wires(folder, file_name):
    turntable_folder = os.path.join(folder, "turntable_2d_wires")
    pickle_folder = os.path.join(folder, "pickle")
    if not os.path.exists(turntable_folder):
        os.mkdir(turntable_folder)
    #with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
    #    sketch = pickle.load(fp)
    #with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
    #    cam = pickle.load(fp)
    my_dpi = 100


    # reproject original strokes on 3d geometries
    sketch_file_name = os.path.join(folder, "sketch.json")
    with open(file_name) as fp:
        sketch_json = json.load(fp)
    intersections_3d = []
    for inter in sketch_json["intersections"]:
        if not inter["coordinates3D"][0] is None:
            intersections_3d.append(inter["coordinates3D"])
    intersections_3d = np.array(intersections_3d)
    #print(intersections_3d)
    original_sketch, sketch_3d, cam = sketch_wires.read_data_wires(file_name)

    new_fixed_strokes = []
    new_pressures = []
    for s_id, s in enumerate(sketch_3d.strokes):
        if len(s.points_list) == 0:
            continue
        #for orig_s_id in sketch.strokes[s_id].original_id:
        #print(orig_s_id)
        #print(orig_s_id, len(original_sketch.strokes))
        pts_3d = np.array([p.coords for p in s.points_list])
        original_s = original_sketch.strokes[s_id]
        original_pts = np.array(original_s.linestring.linestring)
        original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])

        #print(s_id, len(pts_3d), len(original_pressures))
        new_fixed_strokes.append([p.coords for p in s.points_list])
        new_pressures.append(original_pressures)

    fixed_strokes = deepcopy(new_fixed_strokes)
    #ps.init()
    #for s_id, s in enumerate(fixed_strokes):
    #    if len(s) == 0:
    #        continue
    #    ps.register_curve_network(str(s_id), np.array(s),
    #                              np.array([[i, i+1] for i in range(len(s)-1)]))
    #ps.register_point_cloud("intersections", intersections_3d)
    #ps.show()

    x_lim, y_lim = utils_plot.get_sketch_limits(original_sketch)
    points = np.array([p for s in fixed_strokes for p in s])
    points = intersections_3d
    bbox = tools_3d.bbox_from_points(points)
    bbox_center = (bbox[:3]+bbox[3:])/2
    for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        #for angle_id, angle in enumerate(np.linspace(0, 360, 360)):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        rot_mat = tools_3d.get_rotation_mat_z(np.deg2rad(angle))
        #print(angle)
        for s_id, stroke in enumerate(sketch_3d.strokes):
            #print(s_id, s)
            s = np.array([p.coords for p in stroke.points_list])
            if len(s) == 0:
                continue
            s[:, 0] -= bbox_center[0]
            s[:, 1] -= bbox_center[1]
            s = np.array([np.matmul(rot_mat, p) for p in s])
            #s = tools_3d.apply_hom_transform_to_points(s, rot_mat)
            s[:, 0] += bbox_center[0]
            s[:, 1] += bbox_center[1]
            proj_s = np.array(cam.project_polyline(s))
            #axes.plot(proj_s[:, 0], proj_s[:, 1], c="black", lw=0.5*sketch.strokes[s_id].width)
            segs = [[proj_s[p_id], proj_s[p_id+1]] for p_id in range(len(proj_s)-1)]
            #print(proj_s)
            #print(new_pressures[s_id])
            #widths = np.array([(new_pressures[s_id][p_id]+new_pressures[s_id][p_id+1])/2
            #                   for p_id in range(len(proj_s)-1)])
            original_s_id = original_sketch.strokes[s_id].original_id[0]
            #print(original_s_id)
            #widths = sketch_json["strokes_topology"][original_s_id]["mean_pressure"]
            widths = original_sketch.strokes[s_id].mean_pressure
            #colors = np.zeros([len(widths), 4])
            colors = (0, 0, 0, widths)
            #print(colors)
            #colors[:, -1] = widths
            #print(colors)
            #widths *= 1.2
            widths += 0.5
            #widths *= 3.0
            lc = LineCollection(segs, linewidths=widths, colors=colors)
            axes.add_collection(lc)
        axes.set_xlim(x_lim)
        axes.set_ylim(y_lim)
        axes.set_aspect("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(turntable_folder, str(np.char.zfill(str(angle_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name, dpi=my_dpi)
        plt.close(fig)
        #plt.show()

def perceptual_study_data(data_folder):
    nb_sampled_corrs = 10
    plt.rcParams["figure.figsize"] = (10, 10)
    line_label_colors = sns.color_palette("Set1", n_colors=6)
    line_label_colors[5] = line_label_colors[4]
    perceptual_study_folder = os.path.join(data_folder, "perceptual_study")
    if not os.path.exists(perceptual_study_folder):
        os.mkdir(perceptual_study_folder)

    # example
    example_strokes = [0, 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13]
    example_sketch_id = 1
    example_stroke_pairs = [
        [0, 5, 1],
        [2, 7, 2],
        [5, 11, 0],
        [8, 8, 2],
        #negative
        [5, 8, 1],
        [2, 7, 1],
    ]
    pickle_folder = os.path.join(data_folder, str(example_sketch_id), "pickle")
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)
    x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
    for example_id, stroke_pair in enumerate(example_stroke_pairs):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0)
        #bottom=-0.1,top=1.0)
        sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#000000",
                                 display_strokes=example_strokes)
        sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#e7298a",
                                 display_strokes=stroke_pair[:2],
                                 linewidth_process=lambda s: 3)

        utils_plot.add_sketch_vp_arrow(sketch, x_lim, y_lim, cam,
                                       axes, stroke_pair[-1], line_label_colors)
        axes.set_xlim(0, sketch.width)
        axes.set_ylim(sketch.height, 0)
        axes.axis("equal")
        axes.axis("off")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        tmp_file_name = os.path.join(perceptual_study_folder, "example_"+str(example_id)+".png")
        plt.savefig(tmp_file_name)
        #plt.show()
        plt.close(fig)

    file_names = []
    for sketch_id in range(1, 2):
        print(sketch_id)
        pickle_folder = os.path.join(data_folder, str(sketch_id), "pickle")
        with open(os.path.join(pickle_folder, "global_candidate_correspondences_curves.pkl"), "rb") as fp:
            correspondences = pickle.load(fp)
        with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
            sketch = pickle.load(fp)
        with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
            cam = pickle.load(fp)
        with open(os.path.join(data_folder, str(sketch_id), "batches_results_normal.json"), "rb") as fp:
            batches_results = json.load(fp)

        batch_correspondences = []
        for batch in batches_results:
            #batch_correspondences += batch["final_correspondences"]
            for corr in batch["final_correspondences"]:
                batch_correspondences.append([corr["stroke_id_0"],
                                              corr["stroke_id_1"], 0, 0, corr["symmetry_plane_id"]])


        sampled_corrs = []
        already_sampled_corrs = set()
        sample_counter = 0
        max_iter_counter = 0
        while sample_counter < nb_sampled_corrs and max_iter_counter < 50:
            sampled_corr_id = np.random.choice(np.arange(len(batch_correspondences)), 1)[0]
            sampled_corr = batch_correspondences[sampled_corr_id]
            if (sampled_corr[0], sampled_corr[1], sampled_corr[4]) in already_sampled_corrs:
                max_iter_counter += 1
                continue
            already_sampled_corrs.add((sampled_corr[0], sampled_corr[1], sampled_corr[4]))
            sampled_corrs.append(sampled_corr)
            sample_counter += 1

        sample_counter = 0
        max_iter_counter = 0
        while sample_counter < nb_sampled_corrs and max_iter_counter < 50:
            sampled_corr_id = np.random.choice(np.arange(len(correspondences)), 1)[0]
            sampled_corr = correspondences[sampled_corr_id]
            if (sampled_corr[0], sampled_corr[1], sampled_corr[4]) in already_sampled_corrs:
                max_iter_counter += 1
                continue
            already_sampled_corrs.add((sampled_corr[0], sampled_corr[1], sampled_corr[4]))
            sampled_corrs.append(sampled_corr)
            sample_counter += 1

        #for sampled_corr_id in np.random.choice(np.arange(len(batch_correspondences)), nb_sampled_corrs):
        #    sampled_corr = batch_correspondences[sampled_corr_id]
        #    sampled_corrs.append(sampled_corr)
        #for sampled_corr_id in np.random.choice(np.arange(len(correspondences)), nb_sampled_corrs):
        #    sampled_corr = correspondences[sampled_corr_id]
        #    sampled_corrs.append(sampled_corr)

        for sampled_corr_id, sampled_corr in enumerate(sampled_corrs):
            fig, axes = plt.subplots(nrows=1, ncols=1)
            fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0)
            #bottom=-0.1,top=1.0)
            sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#000000")
            sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#e7298a",
                                     display_strokes=sampled_corr[:2],
                                     linewidth_process=lambda s: 3)

            utils_plot.add_sketch_vp_arrow(sketch, x_lim, y_lim, cam,
                                           axes, sampled_corr[4], line_label_colors)
            axes.set_xlim(0, sketch.width)
            axes.set_ylim(sketch.height, 0)
            axes.axis("equal")
            axes.axis("off")
            fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
            #tmp_file_name = os.path.join(perceptual_study_folder,
            #                             str(sketch_id)+"_"+str(np.char.zfill(str(sampled_corr_id), 3))+".png")
            tmp_file_name = os.path.join(perceptual_study_folder,
                                         str(sketch_id)+"_"+str(np.char.zfill(str(sampled_corr[0]), 3))+"_"+str(np.char.zfill(str(sampled_corr[1]), 3))+"_"+str(np.char.zfill(str(sampled_corr[4]), 3))+".png")
            file_names.append("http://www-sop.inria.fr/members/Felix.Hahnlein/symmetry_study/"+str(sketch_id)+"_"+str(np.char.zfill(str(sampled_corr[0]), 3))+"_"+str(np.char.zfill(str(sampled_corr[1]), 3))+"_"+str(np.char.zfill(str(sampled_corr[4]), 3))+".png")
            plt.savefig(tmp_file_name, metadata={"orientation value": "portrait"})
            plt.close(fig)
            #plt.show()
    import csv
    with open(os.path.join(perceptual_study_folder, "file_names.csv"), 'w') as f:
        write = csv.writer(f)
        write.writerows(np.array(file_names).transpose().reshape([len(file_names), 1]))

id_file_list = [
    "student8_house_rough_full.json",
    "designer2_armchair_02_rough_full.json",
    "Prof2task2_printer_02_rough_full.json",
    "student8_hairdryer_rough_full.json",
    "Professional2_vacuum_cleaner_rough_full.json",
    "Professional6_mouse_rough_full.json",
    "designer2_bookshelves_rough_full.json",
    "designer2_trash_bin_rough_full.json",
    "Professional3_tubes_rough_full.json",
    "designer2_chair_04_rough_full.json",
    "designer2_guitar_01_rough_full.json",
    "designer2_printer_02_rough_full.json",
    "Prof2task2_guitar_01_rough_full.json",
    "Professional1_house_rough_full.json",
    "designer2_cabinet_02_rough_full.json",
    "Prof2task2_cabinet_01_rough_full.json",
    "Professional2_house_rough_full.json",
    "designer2_cabinet_01_rough_full.json",
    "student1_house_rough_full.json",
    "student1_vacuum_cleaner_rough_full.json",
    "student3_wobble_surface_rough_full.json",
    "student4_house_rough_full.json",
    "student5_house_rough_full.json",
    "student9_house_rough_full.json",
    "student8_mouse_rough_full.json",
    "designer2_lamp_rough_full.json",
    "student9_tubes_rough_full.json",
    "Professional6_house_rough_full.json",
    "Professional5_wobble_surface_rough_full.json",
    "Professional5_house_rough_full.json",
    "Professional4_tubes_rough_full.json",
    "Professional3_house_rough_full.json",
    "student9_bumps_rough_full.json",
    "student7_vacuum_cleaner_rough_full.json",
    "student6_house_rough_full.json",
    "student1_shampoo_bottle_rough_full.json",
    "Professional6_hairdryer_rough_full.json",
    "Professional4_vacuum_cleaner_rough_full.json",
    "Professional4_hairdryer_rough_full.json",
    "Professional3_mixer_rough_full.json",
    "Professional3_bumps_rough_full.json",
    "student7_wobble_surface_rough_full.json",
    "student2_house_rough_full.json",
    "Professional6_wobble_surface_rough_full.json",
    "Professional6_shampoo_bottle_rough_full.json",
    "Professional1_tubes_rough_full.json",
    "Prof5task2_car_02_rough_full.json"]

if __name__ == "__main__":
    #perceptual_study_data(os.path.join("../data"))
    #exit()
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=0, type=int, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    #plot_correspondences(pickle_folder)
    #turntable_2d(folder)
    try:
        turntable_2d(folder, "batches_results_non_symmetric.json")
    except:
        print("not found")
    exit()
    try:
        turntable_2d(folder, "batches_results_normal.json")
    except:
        print("not found")
    try:
        turntable_2d(folder, "batches_results_bootstrapped.json")
    except:
        print("not found")
    try:
        turntable_2d(folder, "batches_results_post.json")
    except:
        print("not found")
    try:
        turntable_2d(folder, "batches_results_non_symmetric.json")
    except:
        print("not found")
    try:
        if os.path.exists(os.path.join(folder, "wires_curves.json")):
            turntable_2d_wires(folder, os.path.join(folder, "wires_curves.json"))
        elif sketch_id < 48:
            turntable_2d_wires(folder, os.path.join("../data", "full_json", id_file_list[sketch_id-1]))
        else:
            turntable_2d_wires(folder, os.path.join(folder, "sketch.json"))
    except:
        exit()
    exit()
    #turntable_2d(folder, "batches_results_bootstrapped.json")
    turntable_2d(folder, "batches_results_post.json")
    #turntable_2d_wires(folder, os.path.join(folder, "wires_curves.json"))
    #sketch_id = 25
    exit()
    for sketch_id in np.arange(1, 103):
        print(sketch_id)
        folder = os.path.join("../data", str(sketch_id))
        #try:
        #    with open(os.path.join(folder, "sketch.json"), "r") as fp:
        #        sketch = json.load(fp)
        #    print(sketch["designer"], sketch["object_name"])
        #except:
        #    continue
        #continue
        pickle_folder = os.path.join(folder, "pickle")
        #plot_correspondences(pickle_folder)
        #plot_correspondences(pickle_folder)
        try:
            plot_correspondences(pickle_folder)
            turntable_2d(folder)
            turntable_2d(folder, "batches_results_bootstrapped.json")
        except:
            continue
    exit()
    #sketch_id = 1
    #for sketch_id in range(14, 48):
    #    folder = os.path.join("../data", str(sketch_id))
    #    with open(os.path.join(folder, "sketch.json"), "r") as fp:
    #        sketch = json.load(fp)
    #    print("\""+sketch["designer"]+"_"+sketch["object_name"]+"_rough_full.json"+"\",")
    #exit()

    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    #plot_batch(folder)
    #visualize_neighboring_intersections(folder)
    ortho_views(folder)
    #print(id_file_list[sketch_id-1])
    #turntable_2d_wires(folder, os.path.join("../data", "full_json", id_file_list[sketch_id-1]))
    print(sketch_id)
    #turntable_2d(folder)
    exit()
    ##    with open(os.path.join(pickle_folder, "reference_correspondences.pkl"), "rb") as fp:
    ##        good_correspondences = pickle.load(fp)
    ##    print("nb_correspondences", np.sum([len(good_correspondences[i].edges()) for i in range(3)]))
    ##    print(good_correspondences[0].edges())
    #    #exit()

    #visualize_confidence_score("../data")
    #plot_spatial_distribution("../data")
    #train_confidence_score_classifier("../data")
    #vanishing_line_deviation("../data")
    #plot_temporal_distribution("../data")
    #sketch_id = 2
    #folder = os.path.join("../data", str(sketch_id))
    #pickle_folder = os.path.join(folder, "pickle")
    #exit()
    #for sketch_id in range(1, 14):
    exit()
    sketch_id = 13
    folder = os.path.join("../data", str(sketch_id))
    pickle_folder = os.path.join(folder, "pickle")
    #manual_annotation(pickle_folder)
    #turntable_2d(folder)
    plot_correspondences(pickle_folder)
    #edit_ref_correspondences(pickle_folder, 0)
