import numpy as np
from shapely.geometry import LineString
from more_itertools import distinct_combinations

def get_intersection_based_candidates(s_2d, axis_label, inter_3ds, camera,
                                      acc_radius):
    candidates = []
    s_2d_length = LineString(s_2d).length
    if axis_label < 5:
        # form candidates out of all tuples
        for comb in distinct_combinations(range(len(inter_3ds)), 2):
            if np.linalg.norm(np.array(camera.project_point(inter_3ds[comb[0]].inter_3d)) -
                              np.array(camera.project_point(inter_3ds[comb[1]].inter_3d))) < 0.5*s_2d_length:
                #np.array(camera.project_point(inter_3ds[comb[1]].inter_3d))) < 2*acc_radius:
                continue
            vec = inter_3ds[comb[1]].inter_3d - inter_3ds[comb[0]].inter_3d
            vec /= np.linalg.norm(vec)
            candidates.append(np.array(camera.lift_polyline_close_to_line(s_2d, inter_3ds[comb[0]].inter_3d, vec)))
    else:
        # planar curves
        for comb in distinct_combinations(range(len(inter_3ds)), 2):
            if np.linalg.norm(np.array(camera.project_point(inter_3ds[comb[0]].inter_3d)) -
                              np.array(camera.project_point(inter_3ds[comb[1]].inter_3d))) < 0.3*s_2d_length:
                              #np.array(camera.project_point(inter_3ds[comb[1]].inter_3d))) < 2*acc_radius:
                continue
            vec = np.array(inter_3ds[comb[0]].inter_3d) - np.array(inter_3ds[comb[1]].inter_3d)
            vec /= np.linalg.norm(vec)
            candidates.append(np.array(camera.lift_polyline_close_to_line(
                s_2d, inter_3ds[comb[0]].inter_3d, vec)))
    return candidates