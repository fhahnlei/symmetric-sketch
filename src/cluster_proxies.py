import numpy as np
import seaborn as sns
from math import acos
from skspatial.objects import Plane
import polyscope as ps
import matplotlib.pyplot as plt
import tools_3d

def cluster_lines_non_unique_angles(lines, VERBOSE=False):
	#	print("cluster_lines")
	lines = np.array(lines)
	lines_vec = lines[:, 1] - lines[:, 0]
	if VERBOSE:
		print(lines.shape)
		print(lines_vec.shape)
		print(np.linalg.norm(lines_vec, axis=1))
	for l_id, l in enumerate(lines_vec):
		lines_vec[l_id] /= np.linalg.norm(l)
	if VERBOSE:
		print(lines)
		print(lines_vec)
	#print(lines)
	if len(lines) == 1:
		return lines, [[0]]
	max_length = 0.1*np.max(np.linalg.norm(lines[:, 0] - lines[:, 1], axis=1))
	max_angle = 10.0 # 10 degrees
	unique_first_points = np.unique(lines[:, 0], axis=0)
	if unique_first_points.shape[0] == 1:
		#print(list(range(len(lines))))
		return lines, [list(range(len(lines)))]
	cam_ray = unique_first_points[1] - unique_first_points[0]
	cam_ray /= np.linalg.norm(cam_ray)
	projected_lines = np.dot(lines[:, 0], cam_ray)
	#	plt.hist(projected_lines, bins=20)
	#	plt.show()
	t_start = np.min(projected_lines)
	t_end = np.max(projected_lines)
	t_tmp = t_start
	cluster_lists = []
	already_existing_cluster_lists = set()
	angle_origin = np.array([1, 0, 0]).reshape(3, 1)
	angle_start = -180.0
	angle_end = 180.0
	#print(projected_lines)
	while t_tmp < t_end:
		#spatial_line_ids_tmp = np.argwhere(np.abs(projected_lines - t_tmp) < max_length).flatten().tolist()
		spatial_line_ids_tmp = np.argwhere(np.logical_and(np.abs(projected_lines - t_tmp) < max_length,
												  np.logical_not(np.isclose(np.abs(projected_lines - t_tmp) - max_length, 0.0)))).flatten().tolist()
		t_tmp += max_length
		if len(spatial_line_ids_tmp) == 0:
			continue
		if len(spatial_line_ids_tmp) == 1:
			if not tuple(spatial_line_ids_tmp) in already_existing_cluster_lists:
				cluster_lists.append(spatial_line_ids_tmp)
				already_existing_cluster_lists.add(tuple(spatial_line_ids_tmp))
			continue
		if VERBOSE:
			print(spatial_line_ids_tmp)
		angle_tmp = angle_start
		while angle_tmp < angle_end:
			if VERBOSE:
				print("angle_tmp", angle_tmp)
				print(angle_origin)
				print(lines_vec[spatial_line_ids_tmp])
				print(np.dot(lines_vec[spatial_line_ids_tmp], angle_origin))
			angles = np.rad2deg(np.dot(lines_vec[spatial_line_ids_tmp], angle_origin))
			#angles = np.rad2deg(angle_origin.dot(lines[spatial_line_ids_tmp]))
			if VERBOSE:
				print(angles)
				print(np.abs(angles - angle_tmp))
			#angle_line_ids = np.argwhere(np.abs(angles - angle_tmp).flatten() < 0.5*max_angle).flatten().tolist()
			angle_line_ids = np.argwhere(np.logical_and(np.abs(angles - angle_tmp).flatten() < 0.5*max_angle,
														np.logical_not(np.isclose(np.abs(angles - angle_tmp).flatten() - 0.5*max_angle, 0.0)))).flatten().tolist()
			#angle_line_ids = np.argwhere(angles < angle_tmp + 0.5*max_angle and angles >= angle_tmp - 0.5*max_angle).flatten().tolist()
			if VERBOSE:
				print(angle_line_ids)
			if len(angle_line_ids) > 0 and not tuple(np.array(spatial_line_ids_tmp)[angle_line_ids].tolist()) in already_existing_cluster_lists:
				cluster_lists.append(np.array(spatial_line_ids_tmp)[angle_line_ids].tolist())
				already_existing_cluster_lists.add(tuple(np.array(spatial_line_ids_tmp)[angle_line_ids].tolist()))
			angle_tmp += max_angle
	if VERBOSE:
		print("cluster_lists", cluster_lists)
	proxies = []
	for cluster in cluster_lists:
		if len(cluster) == 1:
			proxies.append(lines[cluster[0]])
			continue
		proxies.append(np.array([np.mean(lines[cluster, 0], axis=0),
								 np.mean(lines[cluster, 1], axis=0)]))

	if VERBOSE:
		ps.remove_all_structures()
		for p_id, p in enumerate(proxies):
			ps.register_curve_network(str(p_id), nodes=p.reshape(-1, 3),
									  edges=np.array([[0, 1]]))
			for l_id, l in enumerate(lines[cluster_lists[p_id]]):
				ps.register_curve_network(str(p_id)+"_"+str(l_id),
										  nodes=l.reshape(-1, 3),
										  edges=np.array([[0, 1]]))
		ps.show()
	return proxies, cluster_lists
#sys.exit()

# lines contains array of tuples of the same 2D stroke
# returns clusters and per_line_cluster_ids
# basically performs first step of mean shift clustering
def cluster_lines_non_unique(lines, VERBOSE=False):
#	print("cluster_lines")
	lines = np.array(lines)
	if len(lines) == 1:
		return lines, [[0]]
	#print(lines)
	max_length = 0.05*np.max(np.linalg.norm(lines[:, 0] - lines[:, 1], axis=1))
	max_length = 0.10*np.max(np.linalg.norm(lines[:, 0] - lines[:, 1], axis=1))
	unique_first_points = np.unique(lines[:, 0], axis=0)
	#print(unique_first_points.shape)
	if unique_first_points.shape[0] == 1:
		#print(list(range(len(lines))))
		return lines, [list(range(len(lines)))]
	if np.isclose(max_length, 0.0):
		return lines[0], [list(range(len(lines)))]
	#print(unique_first_points)
	cam_ray = unique_first_points[1] - unique_first_points[0]
	cam_ray /= np.linalg.norm(cam_ray)
	projected_lines = np.dot(lines[:, 0], cam_ray)
	#if len(lines) == 30:
	#	print(projected_lines)

	t_start = np.min(projected_lines)
	t_end = np.max(projected_lines)
	#print(unique_first_points)
	#print(lines)
	#print(cam_ray)
	#print(projected_lines)
	#print(t_start, t_end)
	#if len(lines) == 30:
	#	print("t", t_start, t_end, max_length)
	t_tmp = t_start
	cluster_lists = []
	already_existing_cluster_lists = set()
	#print("lines", lines)
	#print(unique_first_points)
	#print("max_length", max_length)
	#print(t_start, t_end)
	while t_tmp < t_end:
		#print(np.abs(projected_lines - t_tmp) < max_length)
		#print(np.logical_not(np.isclose(np.abs(projected_lines - t_tmp) - max_length, 0.0)))
		#line_ids_tmp = np.argwhere(np.abs(projected_lines - t_tmp) < max_length).flatten().tolist()
		line_ids_tmp = np.argwhere(np.logical_and(np.abs(projected_lines - t_tmp) < max_length,
												  np.logical_not(np.isclose(np.abs(projected_lines - t_tmp) - max_length, 0.0)))).flatten().tolist()
		#if len(lines) == 30:
		#	print(np.abs(projected_lines - t_tmp))
		#	print(np.abs(projected_lines - t_tmp) < max_length)
		#	print(line_ids_tmp)
		t_tmp += max_length
		if len(line_ids_tmp) > 0 and not tuple(line_ids_tmp) in already_existing_cluster_lists:
			cluster_lists.append(line_ids_tmp)
			already_existing_cluster_lists.add(tuple(line_ids_tmp))
	proxies = []
	for cluster in cluster_lists:
		if len(cluster) == 1:
			proxies.append(lines[cluster[0]])
			continue
		proxies.append(np.array([np.mean(lines[cluster, 0], axis=0),
								 np.mean(lines[cluster, 1], axis=0)]))
	#print(proxies)

	if VERBOSE:
		print("VERBOSE")
		ps.remove_all_structures()
		for p_id, p in enumerate(proxies):
			ps.register_curve_network(str(p_id), nodes=p.reshape(-1, 3),
									  edges=np.array([[0, 1]]))
			for l_id, l in enumerate(lines[cluster_lists[p_id]]):
				ps.register_curve_network(str(p_id)+"_"+str(l_id),
										  nodes=l.reshape(-1, 3),
										  edges=np.array([[0, 1]]))
		ps.show()
	return proxies, cluster_lists
	#sys.exit()

# lines contains array of tuples of the same 2D stroke
# returns clusters and per_line_cluster_ids
# basically performs first step of mean shift clustering
def cluster_lines_non_unique_general(lines, max_length_ratio=0.025, is_ellipse=False, VERBOSE=False):
	#	print("cluster_lines")
	lines = np.array(lines)
	if len(lines) == 1:
		if VERBOSE:
			print("VERBOSE")
			#ps.init()
			#ps.remove_all_structures()
			cmap = sns.color_palette("Set1", n_colors=len(lines))
			l = np.array(lines[0])
			edges_array = np.array([[i, i+1] for i in range(len(l)-1)])
			ps.register_curve_network(str(0),
									  nodes=l.reshape(-1, 3),
									  edges=edges_array, color=cmap[0])
			ps.show()
		return lines, [[0]]
	#print(lines)
	line_lengths = [tools_3d.line_3d_length(line) for line in lines]
	max_length = max_length_ratio*np.max(line_lengths)
	clusters = set()

	if is_ellipse:
		planes = [Plane.best_fit(l) for l in lines]
		angles = np.zeros([len(lines), len(lines)])
		for l1_id in range(len(lines)):
			for l2_id in range(len(lines)):
				angles[l1_id, l2_id] = np.rad2deg(acos(min(1, abs(np.dot(planes[l1_id].normal, planes[l2_id].normal)))))

	distances = np.zeros([len(lines), len(lines)])
	distances[:, :] = 10000.0
	for i in range(len(lines)):
		distances[i, i] = 0.0
	for l_1_id, l_1 in enumerate(lines):
		for l_2_id, l_2 in enumerate(lines[l_1_id+1:]):
			#dist = max(directed_hausdorff(l_1, l_2)[0], directed_hausdorff(l_2, l_1)[0])
			#print(np.array(l_1))
			#print(np.array(l_2))
			#print(np.array(l_1) - np.array(l_2))
			#print(np.linalg.norm(np.array(l_1) - np.array(l_2), axis=-1))
			#sys.exit()
			dist = np.max(np.linalg.norm(np.array(l_1) - np.array(l_2), axis=-1))
			distances[l_1_id, l_2_id+l_1_id+1] = dist
			distances[l_2_id+l_1_id+1, l_1_id] = dist

	variances = np.zeros([len(lines), len(lines)])
	variances[:, :] = 10000.0
	for i in range(len(lines)):
		variances[i, i] = 0.0
	for l_1_id, l_1 in enumerate(lines):
		for l_2_id, l_2 in enumerate(lines[l_1_id+1:]):
			point_dists = np.linalg.norm(np.array(l_1) - np.array(l_2), axis=-1)
			mean_dist = np.mean(point_dists)
			var = np.mean([(p_dist - mean_dist)**2 for p_dist in point_dists])
			variances[l_1_id, l_2_id+l_1_id+1] = var
			variances[l_2_id+l_1_id+1, l_1_id] = var

	#print(distances)
	for l_1_id, l_1 in enumerate(lines):
		#cluster = tuple(np.argwhere(distances[l_1_id] < max_length_ratio*line_lengths[l_1_id]).flatten())
		#cluster = tuple(np.argwhere(np.logical_and(distances[l_1_id] < max_length_ratio*line_lengths[l_1_id],
		#											np.logical_not(np.isclose(distances[l_1_id] - max_length_ratio*line_lengths[l_1_id], 0.0)))).flatten())
		cluster = tuple(np.argwhere(np.logical_and(
			np.logical_and(distances[l_1_id] < max_length_ratio*line_lengths[l_1_id],
												   np.logical_not(np.isclose(distances[l_1_id] - max_length_ratio*line_lengths[l_1_id], 0.0))),
			variances[l_1_id] < 1e-3
		)).flatten())
		if is_ellipse:
			cluster = tuple(np.argwhere(np.logical_and(
				np.logical_and(distances[l_1_id] < max_length_ratio*line_lengths[l_1_id],
							   np.logical_not(np.isclose(distances[l_1_id] - max_length_ratio*line_lengths[l_1_id], 0.0))),
										angles[l_1_id] < 30.0)).flatten())
		if len(cluster) > 0 and cluster in clusters:
			continue
		clusters.add(tuple(cluster))

	#print(clusters)
	cluster_lists = list(clusters)
	#print(cluster_lists)
	proxies = []
	for cluster in cluster_lists:
		if len(cluster) == 1:
			proxies.append(lines[cluster[0]])
			continue
		proxies.append(np.array([np.mean(lines[list(cluster), i], axis=0) for i in range(len(lines[0]))]))

	if VERBOSE:
		print("VERBOSE")
		print(distances)
		print(variances)
		print(cluster_lists)
		#ps.init()
		#ps.remove_all_structures()
		cmap = sns.color_palette("Set1", n_colors=len(proxies))
		for p_id, p in enumerate(proxies):
			edges_array = np.array([[i, i+1] for i in range(len(p)-1)])
			ps.register_curve_network(str(p_id), nodes=p.reshape(-1, 3),
									  edges=edges_array, color=cmap[p_id])
			for l_id, l in enumerate(lines[list(cluster_lists[p_id])]):
				edges_array = np.array([[i, i+1] for i in range(len(l)-1)])
				ps.register_curve_network(str(p_id)+"_"+str(cluster_lists[p_id][l_id]),
										  nodes=l.reshape(-1, 3),
										  edges=edges_array, color=cmap[p_id])
		ps.show()

	return proxies, cluster_lists

def cluster_proxy_strokes(global_candidate_correspondences, per_stroke_proxies, sketch,
						  EXCLUDE_CURVES=False):
	#print(len(global_candidate_correspondences))
	for s_id in range(len(sketch.strokes)):
		if EXCLUDE_CURVES and sketch.strokes[s_id].axis_label == 5:
			continue
		#print(s_id)
		all_candidates = []
		candidate_id_counter = 0
		for corr in global_candidate_correspondences:
			if corr[0] == s_id:
				#	print(corr[0], corr[1], corr[4], tools_3d.line_3d_length(corr[2]), tools_3d.line_3d_length(corr[3]))
				if tools_3d.line_3d_length(corr[2]) > 0.0:
					all_candidates.append(corr[2])
					candidate_id_counter += 1
			if corr[1] == s_id:
				#if s_id == 41:
				#	print(corr[0], corr[1])
				#if s_id == 29:
				#	print(corr[0], corr[1], corr[4], tools_3d.line_3d_length(corr[2]), tools_3d.line_3d_length(corr[3]))
				if tools_3d.line_3d_length(corr[3]) > 0.0:
					all_candidates.append(corr[3])
					candidate_id_counter += 1
		#print(len(all_candidates))
		#print(sketch.strokes[s_id].axis_label)
		#all_candidates = [corr[2] if corr[0] == s_id else  corr[2] if corr[1] == s_id
		#                  for corr in global_candidate_correspondences]
		#print("new line")
		#proxies, cand_line_ids = symmetry_tools.cluster_straight_candidate_lines(all_candidates)
		#if s_id == 29:
		#	print("len(all_candidates))", len(all_candidates))
		if len(all_candidates) == 0:
			continue
		#print("s_id", s_id, len(all_candidates))
		if sketch.strokes[s_id].axis_label < 3:
			#proxies, per_proxy_line_ids = cluster_lines_non_unique(all_candidates, VERBOSE=s_id == 18)
			proxies, per_proxy_line_ids = cluster_lines_non_unique(all_candidates)
			#if s_id == 0:
			#	print(s_id, per_proxy_line_ids)
		elif sketch.strokes[s_id].axis_label == 3: # non axis-aligned stroke
			proxies, per_proxy_line_ids = cluster_lines_non_unique_angles(all_candidates)
			#print(s_id, per_proxy_line_ids)
		if sketch.strokes[s_id].axis_label == 5:
			ratio = 0.7
			if sketch.strokes[s_id].is_ellipse():
				ratio = 0.1
			proxies, per_proxy_line_ids = cluster_lines_non_unique_general(all_candidates,
																		   max_length_ratio=ratio,
																		   is_ellipse=sketch.strokes[s_id].is_ellipse(),
																		   VERBOSE=False)
		#print(len(proxies))
		#proxies, per_proxy_line_ids = cluster_lines_non_unique_general(all_candidates, VERBOSE= s_id == 0)
		#print(len(proxies))
		#proxies, per_proxy_line_ids = cluster_lines_non_unique(all_candidates)
		#print(per_proxy_line_ids)
		#if s_id == 29:
		#	for p in proxies:
		#		print(tools_3d.line_3d_length(np.array(p)))
			#exit()

		#if s_id == 47:
		#	print("s_id: 47")
		#	print(all_candidates)
		#	print(len(all_candidates))
		#	print(proxies)
		#if s_id == 11:
		#	proxies = [proxies[1]]
		#	per_proxy_line_ids = [per_proxy_line_ids[1]]
		per_stroke_proxies[s_id] = proxies
		#cand_proxy_ids = np.zeros(candidate_id_counter, dtype=np.int)
		#for proxy_id, cand_list in enumerate(cand_line_ids):
		#	for cand_id in cand_list:
		#		cand_proxy_ids[cand_id] = proxy_id
		cand_proxy_ids = [[] for i in range(candidate_id_counter)]
		for proxy_id, cluster in enumerate(per_proxy_line_ids):
			for l_id in cluster:
				cand_proxy_ids[l_id].append(proxy_id)
		#print(s_id, cand_proxy_ids)
		#print(proxies)
#		print(len(proxies))
#		print(per_proxy_line_ids)
#		print(cand_proxy_ids)

		# reaffect candidates by proxies
		candidate_id_counter = 0
		for corr_id, corr in enumerate(global_candidate_correspondences):
			if corr[0] == s_id:
				global_candidate_correspondences[corr_id][5] = cand_proxy_ids[candidate_id_counter]
				candidate_id_counter += 1
			if corr[1] == s_id:
				global_candidate_correspondences[corr_id][6] = cand_proxy_ids[candidate_id_counter]
				candidate_id_counter += 1
