import matplotlib.pyplot as plt
import matplotlib.patheffects as pe
from skspatial.objects import Plane
from matplotlib.patches import Rectangle
import seaborn as sns
import networkx as nx
from copy import deepcopy
from shapely.geometry import MultiPoint
from matplotlib.patches import Circle
import os
import numpy as np
from descartes import PolygonPatch
import polyscope as ps
import tools_3d
from scipy.spatial.transform import Rotation

my_dpi = 96.0
patch_blue = "#B2CBE5"
INTERSECTION_COLOR = (0.307, 0.110, 0.89)
major_axis_colors = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]

def sketch_plot_simple(sketch, VERBOSE=True, title="", output_file_name="",
                       color_fun=lambda s: "black", plot=True):
    # Plot original sketch
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes, color_process=color_fun, norm_global=True)
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    #if title == "":
    #    plt.title(output_file_name)
    if output_file_name != "":
        plt.savefig(output_file_name)
    if VERBOSE:
        if plot:
            plt.show()
    else:
        plt.clf()
    if plot:
        plt.close(fig)
    else:
        return fig, axes

def plot_curves(curves, name_prefix="", color=(0, 0, 1), radius=0.005, enabled=True, type_ids=None, type_colors=None):
    for curve_id, curve_geom in enumerate(curves):
        if len(curve_geom) == 0:
            continue
        if len(curve_geom) == 1:
            edges_array = np.array([[0, 0]])
        else:
            edges_array = np.array([[i, i + 1] for i in range(len(curve_geom) - 1)])
        edge_color = color
        if type_ids is not None:
            edge_color = type_colors[type_ids[curve_id]]
        ps.register_curve_network(name_prefix + "_" + str(curve_id), nodes=np.array(curve_geom),
                                  edges=edges_array, color=edge_color, radius=radius, enabled=enabled)

def sketch_plot_highlight(sketch, highlighted_strokes, VERBOSE=True, title="", output_file_name="",
                       color_fun=lambda s: "black", return_plot=False, axes_equal=True):
    # Plot original sketch
    fig, axes = plt.subplots(nrows=1, ncols=1)
    #print(title)
    if title == "":
        fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "grey")
    sketch.display_strokes_2(fig=fig, ax=axes, color_process=color_fun,
                             linewidth_data=None,
                             linewidth_process=lambda s: [s[0]+2],
                             display_strokes=highlighted_strokes)
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    if axes_equal:
        axes.set_aspect("equal")
        #axes.axis("equal")
    axes.axis("off")
    if title != "":
        plt.title(title)
    if return_plot:
        return fig, axes
    if output_file_name != "":
        plt.savefig(output_file_name)
    if VERBOSE:
        plt.show()
    else:
        plt.clf()
    plt.close(fig)

def sketch_plot_successive(sketch, folder):

    for s_id, s in enumerate(sketch.strokes):
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
        #pts = np.array([p.coords for p in s.points_list])
        #axes.plot(pts[:, 0], pts[:, 1], lw=3.0, c="black")

        for prev_s_id, prev_s in enumerate(sketch.strokes[:s_id]):
            pts = np.array([p.coords for p in prev_s.points_list])
            axes.plot(pts[:, 0], pts[:, 1], lw=1.0, c="grey")

        #axes.set_xlim(0, sketch.width)
        #axes.set_ylim(sketch.height, 0)
        axes.set_xlim(0, max(sketch.width, sketch.height))
        axes.set_ylim(max(sketch.width, sketch.height), 0)
        #axes.axis("equal")
        axes.axis("off")
        tmp_file_name = os.path.join(folder, str(np.char.zfill(str(s_id), 3))+".png")
        fig.set_size_inches((512 / my_dpi, 512 / my_dpi))
        #plt.show()
        plt.savefig(tmp_file_name)
        plt.close(fig)

def plot_anchored_strokes(sketch, final_full_anchored_stroke_ids,
                          final_half_anchored_stroke_ids, final_triple_intersections,
                          output_file_name):
    final_half_anchored_stroke_ids = list(set(final_half_anchored_stroke_ids) - set(final_full_anchored_stroke_ids))
    #print(final_half_anchored_stroke_ids)
    #print(final_full_anchored_stroke_ids)
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes,
                             color_process=lambda s: "#0000008D",
                             norm_global=True)
    if len(final_half_anchored_stroke_ids) > 0:
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "#1b9e77",
                                 display_strokes=final_half_anchored_stroke_ids,
                                 norm_global=True, linewidth_process=lambda s: 3.0)
    if len(final_full_anchored_stroke_ids) > 0:
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "#d95f02",
                                 display_strokes=final_full_anchored_stroke_ids,
                                 norm_global=True, linewidth_process=lambda s: 3.0)
    for inter_id in final_triple_intersections:
        inter = sketch.intersection_graph.get_intersections([inter_id])[0]
        axes.add_artist(Circle(xy=inter.inter_coords, radius=0.5*inter.acc_radius, color="#7570b3",
                               zorder=5))

    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    #plt.show()
    plt.savefig(output_file_name)
    plt.close(fig)

def sketch_plot_intersections_triple(sketch, VERBOSE=True, title="", output_file_name="", color_fun=lambda s: "black"):

    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    extended_ids =[inter.inter_id for inter in sketch.intersection_graph.get_intersections()
                   if inter.is_extended]
    for inter in sketch.intersection_graph.get_intersections():
        #inter_neighbours =
        if len(list(set(inter.adjacent_inter_ids) - set(extended_ids))) > 2:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=0.5*inter.acc_radius, color="red",
                                   zorder=5))
        else:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=0.5*inter.acc_radius, color="#0000008D",
                                   zorder=5))
    sketch.display_strokes_2(fig=fig, ax=axes,
                             color_process=color_fun,
                             norm_global=True)
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    if VERBOSE:
        plt.show()
    else:
        plt.savefig(output_file_name)
        plt.close(fig)

def sketch_plot_per_stroke_triple_intersections(sketch, per_stroke_triple_intersections):

    plt.rcParams["figure.figsize"] = (10, 10)
    for s in per_stroke_triple_intersections:
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        for i_triple in s["i_triple_intersections"]:
            for inter in sketch.intersection_graph.get_intersections([i_triple["inter_id"]]):
                axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="red",
                                       zorder=5))
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "black",
                                 norm_global=True)
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "red",
                                 norm_global=True, display_strokes=[s["s_id"]])
        axes.set_xlim(0, sketch.width)
        axes.set_ylim(sketch.height, 0)
        axes.axis("equal")
        axes.axis("off")
        plt.show()

def sketch_plot_intersections(sketch, VERBOSE=True, title="", output_file_name="", color_fun=lambda s: "black"):

    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    for inter in sketch.intersection_graph.get_intersections():
        if inter.is_tangential:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="red",
                                   zorder=5))
        elif inter.is_extended:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="green",
                                   zorder=5))
        elif hasattr(inter, "is_parallel") and inter.is_parallel:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="pink",
                                   zorder=10))
        else:
            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="blue"))
    sketch.display_strokes_2(fig=fig, ax=axes,
                             color_process=color_fun,
                             norm_global=True)
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    if VERBOSE:
        plt.show()
    else:
        plt.savefig(output_file_name)
        plt.close(fig)

def sketch_plot_acc_radius(sketch, VERBOSE=True, title="", output_file_name="", color_fun=lambda s: "black"):
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    for s in sketch.strokes:
        if np.isclose(s.acc_radius, 0.0):
            continue
        patch = PolygonPatch(s.linestring.linestring.buffer(s.acc_radius), fc=patch_blue, ec=patch_blue, alpha=0.5, zorder=0)
        #PolygonPatch(dilated, fc=BLUE, ec=BLUE, alpha=0.5, zorder=2)
        axes.add_patch(patch)
    sketch.display_strokes_2(fig=fig, ax=axes,
                             color_process=color_fun,
                             norm_global=True)
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    if VERBOSE:
        plt.show()
    else:
        plt.savefig(output_file_name)
        plt.close(fig)

def plot_candidate_correspondences(sketch, correspondences,
                                   planes_scale_factors, cam,
                                   intersections, data_folder):
    sym_plane_point = np.zeros(3, dtype=np.float)
    shifted_origins = [cam.cam_pos + planes_scale_factors[i]*(np.array(sym_plane_point) - cam.cam_pos)
                       for i in range(3)]
    points = []
    ps.remove_all_structures()
    for corr_id, corr in enumerate(correspondences):
        # TODO: interpolate arc parameter
        #mid_line = np.array([np.mean(corr.stroke_3ds[0], axis=0), np.mean(corr.stroke_3ds[1], axis=0)]).reshape(-1, 3)
        #ps.register_curve_network("midline_"+str(corr_id), np.array(mid_line),
        #                          np.array([[0, 1]]), color=(0, 0, 0), radius=0.001)
        for s_3d_id, s_3d in enumerate(corr.stroke_3ds):
            ps.register_curve_network(str(corr.stroke_ids[s_3d_id])+"_"+str(corr_id),
                                      np.array(s_3d), np.array([[i, i+1] for i in range(len(s_3d)-1)]),
                                      color=sketch.strokes[corr.stroke_ids[s_3d_id]].color)
            for p in s_3d:
                points.append(p)
    points = np.array(points)
    bbox = tools_3d.bbox_from_points(points)
    x_plane_points = [[shifted_origins[0][0], bbox[1], bbox[2]],
                      [shifted_origins[0][0], bbox[4], bbox[2]],
                      [shifted_origins[0][0], bbox[4], bbox[5]],
                      [shifted_origins[0][0], bbox[1], bbox[5]]]
    y_plane_points = [[bbox[0], shifted_origins[1][1], bbox[2]],
                      [bbox[3], shifted_origins[1][1], bbox[2]],
                      [bbox[3], shifted_origins[1][1], bbox[5]],
                      [bbox[0], shifted_origins[1][1], bbox[5]]]
    z_plane_points = [[bbox[0], bbox[1], shifted_origins[2][2]],
                      [bbox[3], bbox[1], shifted_origins[2][2]],
                      [bbox[3], bbox[4], shifted_origins[2][2]],
                      [bbox[0], bbox[4], shifted_origins[2][2]]]
    plane_faces = [[0, 1, 2], [0, 2, 3]]
    ps.register_surface_mesh("x_plane", np.array(x_plane_points),
                             np.array(plane_faces), transparency=0.4,
                             color=(1, 0, 0))
    ps.register_surface_mesh("y_plane", np.array(y_plane_points),
                             np.array(plane_faces), transparency=0.4,
                             color=(0, 1, 0))
    ps.register_surface_mesh("z_plane", np.array(z_plane_points),
                             np.array(plane_faces), transparency=0.4,
                            color=(0, 0, 1))
    # plot correspondences
    projected_bbox = deepcopy(bbox)
    tools_3d.scale_bbox(projected_bbox, -0.5)
    smaller_x_plane_points = [[shifted_origins[0][0], projected_bbox[1], projected_bbox[2]],
                              [shifted_origins[0][0], projected_bbox[4], projected_bbox[2]],
                              [shifted_origins[0][0], projected_bbox[4], projected_bbox[5]],
                              [shifted_origins[0][0], projected_bbox[1], projected_bbox[5]]]
    smaller_y_plane_points = [[projected_bbox[0], shifted_origins[1][1], projected_bbox[2]],
                              [projected_bbox[3], shifted_origins[1][1], projected_bbox[2]],
                              [projected_bbox[3], shifted_origins[1][1], projected_bbox[5]],
                              [projected_bbox[0], shifted_origins[1][1], projected_bbox[5]]]
    smaller_z_plane_points = [[projected_bbox[0], projected_bbox[1], shifted_origins[2][2]],
                              [projected_bbox[3], projected_bbox[1], shifted_origins[2][2]],
                              [projected_bbox[3], projected_bbox[4], shifted_origins[2][2]],
                              [projected_bbox[0], projected_bbox[4], shifted_origins[2][2]]]
    x_plane_projected_points = cam.project_polyline(smaller_x_plane_points)
    y_plane_projected_points = cam.project_polyline(smaller_y_plane_points)
    z_plane_projected_points = cam.project_polyline(smaller_z_plane_points)
    plane_projected_points = [x_plane_projected_points, y_plane_projected_points, z_plane_projected_points]
    #for corr_id, corr in enumerate(correspondences):
    #    # TODO: interpolate arc parameter
    #    fig, axes = sketch_plot_highlight(sketch, corr.stroke_ids,
    #                                      VERBOSE=False,
    #                                      return_plot=True)
    #    patch = PolygonPatch(MultiPoint(plane_projected_points[corr.plane_id]).convex_hull,
    #                         fc=major_axis_colors[corr.plane_id], ec=major_axis_colors[corr.plane_id], alpha=0.4, zorder=0)
    #    axes.add_patch(patch)
    #    output_file_name = os.path.join(data_folder, "corr_"+str(corr.stroke_ids[0])+"_"+str(corr.stroke_ids[1])+"_axis_"+str(corr.plane_id)+".png")
    #    plt.savefig(output_file_name)
    #    plt.close(fig)
    sketch_intersections = sketch.intersection_graph.get_intersections()
    inter_id_dict = {}
    for inter in sketch_intersections:
        inter_id_dict[inter.inter_id] = inter
    inter_3ds = []
    for inter in intersections:
        for d_1 in inter.cam_depths:
            for d in d_1:
                _, ray = cam.get_camera_point_ray(inter.inter_coords)
                inter_3d = np.array(cam.cam_pos) + d*ray
                inter_3ds.append(inter_3d)
    ps.register_point_cloud("intersections", np.array(inter_3ds), radius=0.008,
                            color=INTERSECTION_COLOR)
    bbox_diag = tools_3d.bbox_diag(bbox)
    bbox_center = 0.5*(bbox[:3]+bbox[3:])

    ps.set_ground_plane_mode("shadow_only")
    ps.set_navigation_style("free")
    ps.set_up_dir("neg_z_up")
    view_counter = 0
    for phi in np.linspace(0, 360, 36):
        r = Rotation.from_euler("z", phi, degrees=True)
        rot_cam_pos = r.apply(1.0*bbox_diag*(cam.cam_pos-bbox_center)) + bbox_center
        ps.look_at_dir(camera_location=rot_cam_pos,
                       target=bbox_center, up_dir=np.array([0, 0, -1]))
        ps.show()
        ps.screenshot(os.path.join(data_folder, str(view_counter)+".png"),
                      transparent_bg=False)
        view_counter += 1

def plot_solutions(sketch, correspondences,
                   planes_scale_factors, cam,
                   intersections, proxies, fixed_strokes, data_folder):
    sym_plane_point = np.zeros(3, dtype=np.float)
    if len(planes_scale_factors) > 0:
        shifted_origins = [cam.cam_pos + planes_scale_factors[i]*(np.array(sym_plane_point) - cam.cam_pos)
                           for i in range(3)]
    points = []
    ps.remove_all_structures()
    inter_pc = []
    #print("median_dist", median_dist)
    for inter in intersections:
        #print(inter)
        lifted_inter_0 = []
        lifted_inter_1 = []
        sketch_inter = sketch.intersection_graph.get_intersections([inter[2]])[0]
        s_0_id = sketch_inter.stroke_ids[0]
        s_0 = fixed_strokes[s_0_id]
        if proxies[s_0_id] is not None:
            s_0 = proxies[s_0_id]
        if len(s_0) > 0:
            if sketch.strokes[s_0_id].axis_label < 5:
                p = s_0[0]
                vec = s_0[-1] - s_0[0]
                vec /= np.linalg.norm(vec)
                lifted_inter_0 = cam.lift_point_close_to_line(sketch_inter.inter_coords, p, vec)
            else:
                lifted_inter_0 = cam.lift_point_close_to_polyline(sketch_inter.inter_coords, s_0)
            inter_pc.append(lifted_inter_0)
        s_1_id = sketch_inter.stroke_ids[1]
        s_1 = fixed_strokes[s_1_id]
        if proxies[s_1_id] is not None:
            s_1 = proxies[s_1_id]
        if len(s_1) > 0:
            #if final_proxis[s_1_id] is not None:
            if sketch.strokes[s_1_id].axis_label < 5:
                p = s_1[0]
                vec = s_1[-1] - s_1[0]
                vec /= np.linalg.norm(vec)
                lifted_inter_1 = cam.lift_point_close_to_line(sketch_inter.inter_coords, p, vec)
            else:
                lifted_inter_1 = cam.lift_point_close_to_polyline(sketch_inter.inter_coords, s_1)
            inter_pc.append(lifted_inter_1)

    #print(len(inter_pc))
    if len(inter_pc) > 0:
        inter_pc = np.array(inter_pc).reshape(-1, 3)
        ps.register_point_cloud("intersections", inter_pc, color=INTERSECTION_COLOR, radius=0.008)
        points = np.array(inter_pc)
        bbox = tools_3d.bbox_from_points(points)
        if len(planes_scale_factors) > 0:
            x_plane_points = [[shifted_origins[0][0], bbox[1], bbox[2]],
                              [shifted_origins[0][0], bbox[4], bbox[2]],
                              [shifted_origins[0][0], bbox[4], bbox[5]],
                              [shifted_origins[0][0], bbox[1], bbox[5]]]
            y_plane_points = [[bbox[0], shifted_origins[1][1], bbox[2]],
                              [bbox[3], shifted_origins[1][1], bbox[2]],
                              [bbox[3], shifted_origins[1][1], bbox[5]],
                              [bbox[0], shifted_origins[1][1], bbox[5]]]
            z_plane_points = [[bbox[0], bbox[1], shifted_origins[2][2]],
                              [bbox[3], bbox[1], shifted_origins[2][2]],
                              [bbox[3], bbox[4], shifted_origins[2][2]],
                              [bbox[0], bbox[4], shifted_origins[2][2]]]
            plane_faces = [[0, 1, 2], [0, 2, 3]]
            ps.register_surface_mesh("x_plane", np.array(x_plane_points),
                                     np.array(plane_faces), transparency=0.4,
                                     color=(1, 0, 0))
            ps.register_surface_mesh("y_plane", np.array(y_plane_points),
                                     np.array(plane_faces), transparency=0.4,
                                     color=(0, 1, 0))
            ps.register_surface_mesh("z_plane", np.array(z_plane_points),
                                     np.array(plane_faces), transparency=0.4,
                                     color=(0, 0, 1))
            x_plane_projected_points = cam.project_polyline(x_plane_points)
            y_plane_projected_points = cam.project_polyline(y_plane_points)
            z_plane_projected_points = cam.project_polyline(z_plane_points)
            plane_projected_points = [x_plane_projected_points, y_plane_projected_points, z_plane_projected_points]
            for corr_id, corr in enumerate(correspondences):
                # TODO: interpolate arc parameter
                fig, axes = sketch_plot_highlight(sketch, [corr[0], corr[1]],
                                                  VERBOSE=False,
                                                  return_plot=True)
                patch = PolygonPatch(MultiPoint(plane_projected_points[corr[4]]).convex_hull,
                                     fc=major_axis_colors[corr[4]], ec=major_axis_colors[corr[4]], alpha=0.4, zorder=0)
                axes.add_patch(patch)
                output_file_name = os.path.join(data_folder, "corr_"+str(corr[0])+"_"+str(corr[1])+"_axis_"+str(corr[4])+".png")
                plt.savefig(output_file_name)
                plt.close(fig)

    #if len(inter_pc) == 0:
    bbox_points = []
    for s in proxies:
        #print(s)
        if s is None:
            continue
        for p in s:
            bbox_points.append(p)
    #bbox_points = [p for s in proxies for p in s if s is not None]
    bbox_points += [p for s in fixed_strokes for p in s]
    bbox = tools_3d.bbox_from_points(bbox_points)
    #for corr_id, corr in enumerate(correspondences):
    #    #print(corr)
    #    #for curve_id, curve in enumerate([corr[2], corr[3]]):
    #    for curve_id, curve in enumerate([corr["stroke_3d_0"], corr["stroke_3d_1"]]):
    #        curve = np.array(curve)
    #        ps.register_curve_network(str(corr_id)+"_"+str(curve_id),
    #                                  curve, edges=np.array([[i, i+1] for i in range(len(curve)-1)]))
    for s_id, proxy in enumerate(proxies):
        if proxy is not None and len(proxy) > 0:
            ps.register_curve_network("proxy_"+str(s_id),
                                      np.array(proxy),
                                      np.array([[i, i+1] for i in range(len(proxy)-1)]),
                                      color=sketch.strokes[s_id].color)
    for s_id, proxy in enumerate(fixed_strokes):
        if len(fixed_strokes[s_id]) > 0:
            ps.register_curve_network("proxy_"+str(s_id),
                                      fixed_strokes[s_id],
                                      np.array([[i, i+1] for i in range(len(fixed_strokes[s_id])-1)]),
                                      color=sketch.strokes[s_id].color)
    bbox_diag = tools_3d.bbox_diag(bbox)
    bbox_center = 0.5*(bbox[:3]+bbox[3:])
    ps.set_ground_plane_mode("shadow_only")
    ps.set_navigation_style("free")
    ps.set_up_dir("neg_z_up")
    view_counter = 0
    #ps.show()
    for phi in np.linspace(0, 360, 36):
        r = Rotation.from_euler("z", phi, degrees=True)
        rot_cam_pos = r.apply(1.0*bbox_diag*(cam.cam_pos-bbox_center)) + bbox_center
        ps.look_at_dir(camera_location=rot_cam_pos,
                       target=bbox_center, up_dir=np.array([0, 0, -1]))
        ps.screenshot(os.path.join(data_folder, str(view_counter)+".png"),
                      transparent_bg=True)
        view_counter += 1
        #ps.show()

def get_sketch_limits(sketch):
    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
                        bottom=0.0,
                        top=1.0)
    sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "black")
    axes.set_xlim(0, sketch.width)
    axes.set_ylim(sketch.height, 0)
    axes.axis("equal")
    axes.axis("off")
    x_lim = axes.get_xlim()
    y_lim = axes.get_ylim()
    plt.close(fig)
    return x_lim, y_lim

def add_sketch_vp_arrow(sketch, x_lim, y_lim, camera, axis, axis_id, colors):
    sketch_points = np.array([p.coords for s in sketch.strokes for p in s.points_list])
    lowest_point = sketch_points[np.argmax(sketch_points[:, 1])]

    if axis_id == 0:
        left_range = lowest_point[0] - x_lim[0]
        middle_left = np.array([lowest_point[0] - left_range/4, lowest_point[1]])
        vec = np.array(camera.vanishing_points_coords[0]) - middle_left
        vec /= np.linalg.norm(vec)
        #middle_left = lowest_point + left_range/4*vec
        axis.arrow(middle_left[0], middle_left[1], left_range/2*vec[0], left_range/2*vec[1],
                   width=5, color=colors[0], alpha=0.5)

    elif axis_id == 1:
        right_range = x_lim[1] - lowest_point[0]
        middle_right = np.array([lowest_point[0] + right_range/4, lowest_point[1]])
        vec = np.array(camera.vanishing_points_coords[1]) - middle_right
        vec /= np.linalg.norm(vec)
        axis.arrow(middle_right[0], middle_right[1], right_range/2*vec[0], right_range/2*vec[1],
                   width=5, color=colors[1], alpha=0.5)

    elif axis_id == 2:
        vec = np.array([0, 1])
        up_range = y_lim[1] - y_lim[0]
        middle_up = np.array([x_lim[1]-10, y_lim[0]+up_range/4])
        axis.arrow(middle_up[0], middle_up[1], up_range/2*vec[0], up_range/2*vec[1],
                   width=5, color=colors[2], alpha=0.5)

def correspondence_visualization_only_reconstruction(
        sketch, camera, correspondences, VERBOSE=True):

    line_label_colors = sns.color_palette("Set1", n_colors=6)
    line_label_colors[5] = line_label_colors[4]
    cmap = sns.color_palette("tab20b", n_colors=len(sketch.strokes))
    cmap = sns.color_palette(list(sns.color_palette("Set1", n_colors=8))+
                             list(sns.color_palette("Dark2", n_colors=8))+
                             list(sns.color_palette("Accent", n_colors=8)),
                             n_colors=len(sketch.strokes))
    #cmap = sns.color_palette("Paired", n_colors=len(sketch.strokes))
    x_lim, y_lim = get_sketch_limits(sketch)
    fig, axes = plt.subplots(nrows=1, ncols=3)
    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                        bottom=0.0, top=1.0)

    #fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
    #                    bottom=0.0, top=1.0)

    for j in range(3):
        sketch.display_strokes_2(fig=fig, ax=axes[j], color_process=lambda s: "#000000d8",
                                 linewidth_process=lambda s: 0.5)
        axes[j].set_xlim(x_lim)
        axes[j].set_ylim(y_lim)
        axes[j].set_aspect("equal")
        axes[j].axis("off")
    self_sym_strokes = [[] for i in range(3)]

    axes[0].text(.95, .05, "Black lines are self-symmetric",
                 horizontalalignment='center',
                 transform=axes[0].transAxes)
    for axis_id in range(3):
        stroke_colors = ["black" for i in range(len(sketch.strokes))]
        graph = nx.Graph()
        for corr in correspondences:
            if corr[-1] != axis_id:
                continue
            graph.add_edge(corr[0], corr[1])
        components = [c for c in nx.connected_components(graph)]
        for c in components:
            if len(c) == 1:
                self_sym_strokes[axis_id].append(list(c)[0])
                color = "#000000"
                s_id = list(c)[0]
                stroke_colors[s_id] = color
                pts = np.array([p.coords for p in sketch.strokes[s_id].points_list])
                axes[axis_id].plot(pts[:, 0], pts[:, 1], color=color, lw=2)
                continue
            color = cmap[np.min(list(c))]
            for s_id in c:
                stroke_colors[s_id] = color
                pts = np.array([p.coords for p in sketch.strokes[s_id].points_list])
                axes[axis_id].plot(pts[:, 0], pts[:, 1], color=color, lw=3)
        axes[axis_id].text(.5, .95, "Symmetry correspondences for axis "+str(axis_id),
                              horizontalalignment='center',
                              transform=axes[axis_id].transAxes)
        add_sketch_vp_arrow(sketch, x_lim, y_lim, camera, axes[axis_id], axis_id, line_label_colors)
    if VERBOSE:
        plt.show()

if __name__ == "__main__":
    import pickle, json
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    parser.add_argument("--batches_file", default="", type=str, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    batches_file = args.batches_file
    print(sketch_id)
    pickle_folder = os.path.join("../data", str(sketch_id), "pickle")
    data_folder = "/".join(pickle_folder.split("/")[:-1])
    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        camera = pickle.load(fp)
    with open(os.path.join(data_folder, batches_file), "r") as fp:
        batches = json.load(fp)
    all_correspondences = []
    for batch in batches:
        for corr in batch["final_correspondences"]:
            all_correspondences.append([corr["stroke_id_0"], corr["stroke_id_1"], corr["symmetry_plane_id"]])
    plt.rcParams["figure.figsize"] = (20, 10)
    plt.rcParams["figure.dpi"] = 200
    correspondence_visualization_only_reconstruction(sketch, camera, all_correspondences, VERBOSE=False)
    plt.savefig(os.path.join(data_folder, "correspondences_"+str(batches_file.split(".")[0])+".png"))