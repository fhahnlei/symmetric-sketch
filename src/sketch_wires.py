# snippets of wires reimplementation code
import os, sys
from skspatial.objects import Line

sys.path.insert(1, "src")
#import fitCurves, bezier
from src.fitCurves import bezier
from src.fitCurves import fitCurves
import networkx as nx
import numpy as np
from sklearn import linear_model
import pystrokeproc as pystroke
from pystrokeproc.sketch_core import Sketch, Stroke
import json
from camera import Camera
import matplotlib.pyplot as plt
import polyscope as ps
import tools_3d
#import sketch_dependency_graph
from more_itertools import distinct_combinations
from copy import deepcopy
from matplotlib.patches import Circle
from shapely.geometry import LineString, Point
import bezier_yu
from math import acos
from pystrokeproc.sketch_io import SketchSerializer as skio
from pystrokeproc.sketch_vanishing_points import estimate_vanishing_points
from pystrokeproc.sketch_camera import estimate_camera, get_translation_reference


class StrokePoint3D:
    def __init__(self, x=0, y=0, z=0):
        self.set_coords(x, y, z)

    def set_coords(self, x, y, z):
        self.coords = np.array([x, y, z], dtype=float)

    def get_distance(self, p):
        return np.linalg.norm(self.coords - p.coords)


class Stroke3D:
    def __init__(self, points, id=-1, is_curved=False):
        self.points_list = points
        self.id = id
        self.is_curved = is_curved

    def from_array(self, points):
        points_list = [StrokePoint3D(p[0], p[1], p[2]) for p in points]
        self.__init__(points_list)

    def set_id(self, id):
        self.id = id

    def get_id(self):
        return self.id

class Sketch3D:
    def __init__(self):
        self.sketch2D = None
        self.strokes = []

    def update_stroke_ids(self):
        for s_id, s in enumerate(self.strokes):
            s.set_id(s_id)

    # associate a pystrokeproc sketch with this sketch
    def set_sketch2D(self, sketch2D):
        self.sketch2D = sketch2D

def get_likely_intersections(sketch):
    likely_intersections = []
    for inter in sketch.intersection_graph.get_intersections():
        inter.is_triplet = False
        extended_inter = np.any([sketch.strokes[inter.stroke_ids[s_id]].linestring.linestring.distance(Point(inter.inter_coords)) >
                                 sketch.strokes[inter.stroke_ids[s_id]].acc_radius for s_id in range(2)])
        inter.is_extended = extended_inter
        if inter.inter_id in likely_intersections or len(
                inter.adjacent_inter_ids) < 2:
            continue
        inter.is_triplet = True
        # all outgoing strokes from the intersection "cluster"
        outgoing_strokes = [sketch.strokes[s.stroke_id] for s in
                            sketch.intersection_graph.get_strokes_by_inter_ids(
                                inter.adjacent_inter_ids + [inter.inter_id])]
        # look if they form at least three strokes of different orientations, i.e.
        # they differ more than 5 degree
        if len(outgoing_strokes) < 3:
            continue
        differently_angled_strokes = []
        for s in outgoing_strokes:
            if s.is_curved() or s.axis_label == 5:
                continue
            different_for_all_strokes = True
            for diff_s in differently_angled_strokes:
                #_, u1 = s.get_linear_fitting()
                #_, u2 = diff_s.get_linear_fitting()
                #print(u1, u2)
                #print(np.dot(u1, u2))
                #print(min(1.0, np.dot(u1, u2)))

                #return acos(abs(np.dot(u1, u2)))
                if s.get_line_angle(diff_s) < np.deg2rad(5.0)/np.pi:  # 5 degrees
                    different_for_all_strokes = False
                    break
            if different_for_all_strokes:
                differently_angled_strokes.append(s)
        curves_outgoing_strokes = [s.is_curved() or s.axis_label == 5
                                   for s in outgoing_strokes]
        #if s.is_curved() or s.axis_label == 5 and \
        #        len(curves_outgoing_strokes) > 1:
        #    likely_intersections.append(inter.inter_id)

        if (np.any(curves_outgoing_strokes) and len(differently_angled_strokes) > 1) or \
                len(differently_angled_strokes) > 2:
            likely_intersections.append(inter.inter_id)

    # we don't throw intersections away if there is no likely intersection at
    # the end of a stroke
    for s_id, s in enumerate(sketch.strokes):
        #if sketch.strokes[s_id].is_curved():
        #    continue
        stroke_inters = sketch.intersection_graph.get_intersections_by_stroke_id(
            s_id)
        if len(stroke_inters) == 0:
            continue
        #mid_inter_params = [inter.mid_inter_param for inter in stroke_inters]
        mid_inter_params = []
        stroke_partners = []
        for inter in stroke_inters:
            curr_s_id = \
                np.argwhere(np.array(inter.stroke_ids)[:] == s_id).flatten()[0]
            stroke_partners.append(inter.stroke_ids[np.argwhere(np.array(inter.stroke_ids)[:] != s_id).flatten()[0]])
            mid_inter_params.append(inter.mid_inter_param[curr_s_id])
        max_inter_param = np.max(mid_inter_params)
        min_inter_param = np.min(mid_inter_params)
        # get all intersections which are within 25% of the endpoints of the stroke
        beginning_inters = []
        end_inters = []
        for inter in stroke_inters:
            curr_s_id = \
                np.argwhere(np.array(inter.stroke_ids)[:] == s_id).flatten()[0]
            #if inter.stroke_ids[1 - curr_s_id] > s_id:
            #    continue
            #if inter.mid_inter_param[curr_s_id] <= 0.25:
            if inter.mid_inter_param[curr_s_id] <= min_inter_param + 0.25:
                beginning_inters.append(inter)
            #elif inter.mid_inter_param[curr_s_id] >= 0.75:
            elif inter.mid_inter_param[curr_s_id] >= max_inter_param - 0.25:
                end_inters.append(inter)

        beginning_inters_likely = [inter.inter_id in likely_intersections
                                   for inter in beginning_inters]
        #if s_id == 28:
        #    print("likely", [inter.stroke_ids for inter in beginning_inters])
#        if not np.any(beginning_inters_likely):
#        #    if s_id == 28:
#        #        print("added", [inter.stroke_ids for inter in beginning_inters])
#            if s_id == 2:
#                print([inter.inter_id for inter in beginning_inters])
        likely_intersections += [inter.inter_id for inter in
                                 beginning_inters]

        end_inters_likely = [inter.inter_id in likely_intersections
                             for inter in end_inters]
        #if s_id == 2:
        #    print("get_likely_intersections")
        #    print(mid_inter_params)
        #    print(stroke_partners)
        #    print([inter.inter_id for inter in stroke_inters])
        #    sys.exit()

#        if not np.any(end_inters_likely):
#            if s_id == 2:
#                print([inter.inter_id for inter in end_inters])
        likely_intersections += [inter.inter_id for inter in end_inters]

        if s.is_curved() or s.axis_label == 5:
            # don't throw tangential intersections away
            inter_tangents = []
            curve_inters = [inter.inter_id for inter in stroke_inters
                            if sketch.strokes[np.array(inter.stroke_ids)[
                    np.array(inter.stroke_ids) != s_id][0]].axis_label == 5]
            likely_intersections += curve_inters
            #print(curve_inters)
        #if (s.is_curved() or s.axis_label == 5) and s.is_ellipse():
        #    # conservative strategy for ellipses
        #    print(s_id)
        #    print("conservative")
        #    likely_intersections += [inter.inter_id in stroke_inters]

    # keep ellipse intersections
    for inter in sketch.intersection_graph.get_intersections():
        if (sketch.strokes[inter.stroke_ids[0]].axis_label == 5 and \
            sketch.strokes[inter.stroke_ids[0]].is_ellipse()) or \
            (sketch.strokes[inter.stroke_ids[1]].axis_label == 5 and \
            sketch.strokes[inter.stroke_ids[1]].is_ellipse()):
            likely_intersections.append(inter.inter_id)
    likely_intersections = np.unique(likely_intersections)

    return likely_intersections


def get_clusters_straight_lines(sketch):
    # build adjacency matrix
    adj_mat = np.zeros([len(sketch.strokes), len(sketch.strokes)],
                       dtype=np.bool)
    adj_mat[:, :] = False
    all_node_strokes = sketch.intersection_graph.get_strokes()
    all_node_strokes_len = len(all_node_strokes)
    for i in range(all_node_strokes_len):
        if sketch.strokes[all_node_strokes[all_node_strokes_len-i-1].stroke_id].axis_label == 5:
            del all_node_strokes[all_node_strokes_len-i-1]
    for node_stroke_id, node_stroke in enumerate(all_node_strokes):
        s_id = node_stroke.stroke_id
        inters = [inter
                  for inter in sketch.intersection_graph.get_intersections(node_stroke.inter_ids)
                  if sketch.strokes[np.array(inter.stroke_ids)[np.array(inter.stroke_ids)!=s_id][0]].axis_label != 5]
        for inter in inters:
            inter_params = inter.inter_params[0]
            other_stroke_id = inter.stroke_ids[0] if inter.stroke_ids[
                                                         0] != s_id else \
            inter.stroke_ids[1]
            if abs(inter_params[1] - inter_params[0]) >= 0.75 and \
                    sketch.strokes[s_id].axis_label == sketch.strokes[other_stroke_id].axis_label and \
                    abs(s_id - other_stroke_id) <= 5 and \
                    sketch.strokes[s_id].get_line_angle(
                        sketch.strokes[other_stroke_id]) <= np.deg2rad(
                5.0):  # 5 degrees
            #if abs(inter_params[1] - inter_params[0]) >= 0.75 and \
            #        sketch.strokes[s_id].get_line_angle(
            #            sketch.strokes[other_stroke_id]) <= np.deg2rad(
            #    5.0):  # 5 degrees
            #if abs(inter_params[1] - inter_params[0]) >= 0.75 and \
            #        abs(s_id - other_stroke_id) <= 5 and \
            #        sketch.strokes[s_id].get_line_angle(
            #            sketch.strokes[other_stroke_id]) <= np.deg2rad(
            #    5.0):  # 5 degrees
                adj_mat[s_id, other_stroke_id] = True
                adj_mat[other_stroke_id, s_id] = True
    # return connected components
    components = [list(g) for g in
                  nx.connected_components(nx.from_numpy_array(adj_mat))
                  if len(g) > 1]
    return components


# return one straight line per cluster
def get_aggregated_straight_lines(sketch, clusters):
    regr = linear_model.LinearRegression()
    aggregated_lines = []
    for cluster in clusters:
        cluster_points = []
        for c in cluster:
            cluster_points.append(sketch.strokes[c].points_list[0])
            cluster_points.append(sketch.strokes[c].points_list[-1])
        cluster_points_coords = np.array([p.coords for p in cluster_points])
        x_train = cluster_points_coords[:, 0].reshape(-1, 1)
        y_train = cluster_points_coords[:, 1].reshape(-1, 1)
        use_x_train = True
        if abs(np.max(cluster_points_coords[:, 1]) - np.min(
                cluster_points_coords[:, 1])) > \
                abs(np.max(cluster_points_coords[:, 0]) - np.min(
                    cluster_points_coords[:, 0])):
            use_x_train = False
            x_train = cluster_points_coords[:, 1].reshape(-1, 1)
            y_train = cluster_points_coords[:, 0].reshape(-1, 1)
        regr.fit(x_train, y_train)
        predicted_values = np.array(regr.predict(x_train)).flatten()
        aggr_line = np.array(
            [[cluster_points_coords[p_id, 0], predicted_values[p_id]]
             for p_id in range(len(cluster_points_coords))])
        if not use_x_train:
            aggr_line = np.array(
                [[predicted_values[p_id], cluster_points_coords[p_id, 1]]
                 for p_id in range(len(cluster_points_coords))])
        # aggr_line is unsorted. extract extremal points
        x_min = np.min(aggr_line[:, 0])
        x_max = np.max(aggr_line[:, 0])
        y_min = np.min(aggr_line[:, 1])
        y_max = np.max(aggr_line[:, 1])
        min_min = np.array([x_min, y_min])
        min_max = np.array([x_min, y_max])
        max_min = np.array([x_max, y_min])
        max_max = np.array([x_max, y_max])
        dists_min = [np.linalg.norm(p - min_min) for p in aggr_line]
        dists_max = [np.linalg.norm(p - min_max) for p in aggr_line]
        min_id = np.argmin(dists_min)
        max_id = np.argmin(dists_max)
        if dists_min[min_id] < dists_max[max_id]:
            first_point = aggr_line[min_id]
            dists_min = [np.linalg.norm(p - max_max) for p in aggr_line]
            snd_point = aggr_line[np.argmin(dists_min)]
            aggregated_lines.append(np.array([first_point, snd_point]))
        else:
            first_point = aggr_line[max_id]
            dists_min = [np.linalg.norm(p - max_min) for p in aggr_line]
            snd_point = aggr_line[np.argmin(dists_min)]
            aggregated_lines.append(np.array([first_point, snd_point]))
    return aggregated_lines


def replace_stroke_clusters(sketch, clusters, cluster_lines):
    # replace lines in sketch.strokes
    first_cluster_lines = [cluster[0] for cluster in clusters]
    for cluster_id, s_id in enumerate(first_cluster_lines):
        max_acc_radius = np.max([sketch.strokes[c_i].acc_radius
                                 for c_i in clusters[cluster_id]])
        axis_label = sketch.strokes[s_id].axis_label
        stroke_width = sketch.strokes[s_id].width
        sketch.strokes[s_id].from_array(cluster_lines[cluster_id].tolist())
        sketch.strokes[s_id].from_array(cluster_lines[cluster_id].tolist())
        sketch.strokes[s_id].axis_label = axis_label
        sketch.strokes[s_id].acc_radius = max_acc_radius
        sketch.strokes[s_id].width = stroke_width
        for c_i in clusters[cluster_id][1:]:
            sketch.strokes[s_id].original_id += sketch.strokes[c_i].original_id
    remove_strokes = [c for cluster in clusters for c in cluster[1:]]
    #print(sorted(remove_strokes, reverse=True))
    for s_id in sorted(remove_strokes, reverse=True):
    #for s_id in reversed(remove_strokes):
        del sketch.strokes[s_id]

def cluster_straight_lines(sketch):
    clusters = get_clusters_straight_lines(sketch)
    cluster_lines = get_aggregated_straight_lines(sketch, clusters)
    replace_stroke_clusters(sketch, clusters, cluster_lines)

def cluster_curves(sketch, VERBOSE=False):
    absorbed_stroke_ids = []
    for c_1_id, c_1 in enumerate(sketch.strokes):
        #if c_1.axis_label != 5:
        #    continue
        c_1_linestring = LineString([p.coords for p in c_1.points_list])
        c_1_neighbours = [s_id for s_id in sketch.intersection_graph.get_stroke_neighbours(c_1_id)]
        #c_1_neighbours = [s_id for s_id in sketch.intersection_graph.get_stroke_neighbours(c_1_id)
        #                  if sketch.strokes[s_id].axis_label == 5]
        for c_2_id in c_1_neighbours:
            c_2 = sketch.strokes[c_2_id]
            if c_1.axis_label != c_2.axis_label:
                continue
            if c_2_id == c_1_id:
                continue
            c_2_linestring = LineString([p.coords for p in c_2.points_list])
            max_acc_radius = max(c_1.acc_radius, c_2.acc_radius)
            curve_inters = sketch.intersection_graph.get_intersections_by_stroke_ids(c_1_id, c_2_id)
            #print(c_1_id, c_2_id, len(curve_inters))
            line_inter = c_1_linestring.buffer(max_acc_radius).intersection(c_2_linestring)
            #print(line_inter.length)
            c_1_inter_ratio = line_inter.length/c_1_linestring.length
            c_2_inter_ratio = line_inter.length/c_2_linestring.length
            #print(c_1_id, c_2_id, c_1_inter_ratio, c_2_inter_ratio)
            inter_ratios = [c_1_inter_ratio, c_2_inter_ratio]
            stroke_ids = [c_1_id, c_2_id]
            linestrings = [c_1_linestring, c_2_linestring]
            if np.max(inter_ratios) >= 1.0 and \
                    linestrings[np.argmax(inter_ratios)].length < linestrings[np.argmin(inter_ratios)].length:
                absorbed_stroke_ids.append(stroke_ids[np.argmax(inter_ratios)])
                sketch.strokes[stroke_ids[np.argmin(inter_ratios)]].original_id += sketch.strokes[stroke_ids[np.argmax(inter_ratios)]].original_id
                #print(stroke_ids[np.argmax(inter_ratios)])
            #elif np.min(inter_ratios) >= 0.75:
            #    absorbed_stroke_ids.append(np.argmin(inter_ratios))
            #    print(np.argmin(inter_ratios))
    absorbed_stroke_ids = np.unique(absorbed_stroke_ids)
    new_stroke_ids = list(range(len(sketch.strokes)))
    for del_id in sorted(absorbed_stroke_ids, reverse=True):
        del new_stroke_ids[del_id]
        #del sketch.strokes[del_id]

    if VERBOSE:
        fig, axes = plt.subplots(nrows=1, ncols=2)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        #sketch.display_strokes_2(fig=fig, ax=axes[0],
        #                         color_process=lambda s: "green" if s.axis_label == 5 else "black",
        #                         linewidth_data=lambda s: 3.0, norm_global=True)
        sketch.display_strokes_2(fig=fig, ax=axes[0],
                                 color_process=lambda s: "green" if s.axis_label == 5 else "black",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=new_stroke_ids)
        sketch.display_strokes_2(fig=fig, ax=axes[0],
                                 color_process=lambda s: "red",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=absorbed_stroke_ids)
        sketch.display_strokes_2(fig=fig, ax=axes[1],
                                 color_process=lambda s: "green" if s.axis_label == 5 else "black",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=new_stroke_ids)
        for ax in axes:
            ax.set_xlim(0, sketch.width)
            ax.set_ylim(sketch.height, 0)
            ax.axis("equal")
            ax.axis("off")
        plt.show()
    #sys.exit()
    for del_id in sorted(absorbed_stroke_ids, reverse=True):
        del sketch.strokes[del_id]
    for s in sketch.strokes:
        s.original_id = np.unique(s.original_id).tolist()

def process_raw_sketch(file_name):
    # filter out ellipses beforehand
    sketch = skio.load(file_name)

    for s_id, s in sketch.strokes:
        s.original_id = [s_id]
    sketch.resample_rdp(3.0)
    sketch.shave_empty_strokes(10.0)
    triplet = estimate_vanishing_points(sketch)
    tmp_triplet = deepcopy(triplet)
    print(triplet)
    sorting = [1, 0, 2]
    triplet["vanishing_points"] = [triplet["vanishing_points"][sorting[0]],
                                   triplet["vanishing_points"][sorting[1]],
                                   triplet["vanishing_points"][sorting[2]]]
    for s_id in range(len(triplet["topology"]["strokes"])):
        triplet["topology"]["strokes"][s_id]["vp_votes"] = [triplet["topology"]["strokes"][s_id]["vp_votes"][sorting[0]],
                                                            triplet["topology"]["strokes"][s_id]["vp_votes"][sorting[1]],
                                                            triplet["topology"]["strokes"][s_id]["vp_votes"][sorting[2]]]
    for s_id, s in enumerate(sketch.strokes):
        sketch.strokes[s_id].acc_radius = 10.0
        if s.is_curved(method="pca", threshold=0.05):
            sketch.strokes[s_id].axis_label = 5
    for s in triplet["topology"]["strokes"]:
        grp = np.argmax(s["vp_votes"][:3])
        if np.max(s["vp_votes"][:3]) < 0.0025:
            grp = 3
        sketch.strokes[s["stroke_id"]].axis_label = grp

    pp = np.array(triplet["principal_point"])
    vps = [np.array(p) for p in triplet["vanishing_points"]]

    t_ref = get_translation_reference(triplet['topology']['strokes'])
    K, R, T, nbfin = estimate_camera(sketch.width, sketch.height, vps[0], vps[1], vps[2], pp, t_ref)
    print(K)
    print(R)
    print(T)
    print(vps)
    #exit()
    #tmp_vps = deepcopy(vps)
    #vps = [tmp_vps[2],
    #       tmp_vps[1],
    #       tmp_vps[0]]

    #tmp = vps[1]
    #vps[1] = vps[0]
    #vps[0] = tmp

    cam_pos = np.matmul(-R, T)
    #cam_pos = T
    print("cam_pos", cam_pos)
    r_t = np.zeros(shape=(3, 4))
    r_t[:3, :3] = R
    r_t[:3, 3] = T
    proj_mat = np.matmul(K, r_t)
    view_dir = proj_mat[2, :3]
    cam = Camera(proj_mat=proj_mat,
                 focal_dist=K[0, 0],
                 t=T,
                 view_dir=view_dir,
                 principal_point=pp,
                 rot_mat=R,
                 K=K,
                 cam_pos=cam_pos,
                 vanishing_points_coords=np.array(vps))
    cam.compute_inverse_matrices()

    ps.register_point_cloud("cam_pos", np.array([cam_pos]))
    pts = np.array([cam.lift_point(np.array([10, 10]), t) for t in np.linspace(-1.0, 20.0, 10)])
    pts_1 = np.array([cam.lift_point(np.array([100, 200]), t) for t in np.linspace(-1.0, 20.0, 10)])
    pts_2 = np.array([cam.lift_point(np.array([400, 200]), t) for t in np.linspace(-1.0, 20.0, 10)])
    dir_0 = pts[-1] - pts[0]
    dir_0 /= np.linalg.norm(dir_0)
    line_0 = Line(pts[0], dir_0)
    dir_1 = pts_1[-1] - pts_1[0]
    dir_1 /= np.linalg.norm(dir_1)
    line_1 = Line(pts_1[0], dir_1)
    cam_pos = line_0.intersect_line(line_1)
    cam.cam_pos = cam_pos

    #ps.register_point_cloud("cam_pos_corrected", np.array([cam_pos]))
    #ps.register_point_cloud("lifted_pts", pts)
    #ps.register_point_cloud("lifted_pts_1", pts_1)
    #ps.register_point_cloud("lifted_pts_2", pts_2)
    #ps.register_curve_network("aligned_pts", np.array([pts[0], pts[-1]]), np.array([[0, 1]]))
    #ps.register_curve_network("aligned_pts_1", np.array([pts_1[0], pts_1[-1]]), np.array([[0, 1]]))
    #ps.register_curve_network("aligned_pts_2", np.array([pts_2[0], pts_2[-1]]), np.array([[0, 1]]))
    #ps.show()

    return sketch, cam

# return sketch_wires (contains pystroke.sketch), camera object
def read_data_wires(file_name, include_dict_points=False):
    fh = open(file_name)
    file_content = json.load(fh)
    fh.close()
    file_content["strokes_topology"] = file_content[
        "strokes_topology"]  # [:200]
    cam = Camera(proj_mat=file_content["cam_param"]["P"],
                 focal_dist=file_content["cam_param"]["f"],
                 fov=file_content["cam_param"]["fov"],
                 t=file_content["cam_param"]["t"],
                 view_dir=file_content["cam_param"]["view_dir"],
                 principal_point=file_content["cam_param"]["principal_point"],
                 rot_mat=file_content["cam_param"]["R"],
                 K=file_content["cam_param"]["K"],
                 cam_pos=file_content["cam_param"]["C"],
                 vanishing_points_coords=file_content["cam_param"]["vp_coord"])
    cam.compute_inverse_matrices()

    # read 2D data
    sketch = Sketch()
    width = 0.0
    height = 0.0
    for s_id, s in enumerate(file_content["strokes_topology"]):
        if isinstance(s["points2D"], dict):
            if include_dict_points:
                s["points2D"] = [s["points2D"]]
            else:
                continue
        #if s["primitive_type"] != 0:
        #    continue
        # stroke = Stroke([], acc_radius=5.0)
        acc_radius = 0.0
        if "accuracy_radius" in s and not s["accuracy_radius"] is None:
            #acc_radius = 2.0*s["accuracy_radius"]
            acc_radius = s["accuracy_radius"]
        else:
            acc_radius = 0.0
        #print(s.keys())
        #print(s["accuracy_radius"])
        stroke = Stroke([], acc_radius=acc_radius)
        if not isinstance(s["points2D"], dict):
            #if len(s["points2D"]) == 0 and s["depth_assigned"] and len(s["points3D"]) > 0:
            #    s["points2D"] = [{"x": p[0], "y": p[1]} for p in cam.project_polyline(s["points3D"])]
            coords = np.array([[p["x"], p["y"]] for p in s["points2D"]])
            if len(coords) == 1:
                coords = []
            if len(coords) > 0:
                width = max(width, np.max(coords[:, 0]))
                height = max(height, np.max(coords[:, 1]))
            stroke.from_array(coords)
            stroke.id = s_id
            stroke.original_id = [s_id]
            stroke.axis_label = s["line_group"] - 1
            stroke.mean_pressure = s["mean_pressure"]
            stroke.width = s["mean_pressure"]
            for p_id in range(len(stroke.points_list)):
                if "p" in s["points2D"][p_id].keys():
                    stroke.points_list[p_id].data["pressure"] = s["points2D"][p_id]["p"]
                else:
                    stroke.points_list[p_id].data["pressure"] = s["mean_pressure"]
        sketch.strokes.append(stroke)
    print("num_strokes:", len(sketch.strokes))
    print("sketch_dimensions:", int(width), int(height))
    sketch.width = width
    sketch.height = height

    sketch3d = Sketch3D()
    sketch3d.sketch2D = sketch
    for s in file_content["strokes_topology"]:
        if isinstance(s["points2D"], dict):
            continue
        #if s["primitive_type"] != 0:
        #    continue
        stroke = Stroke3D([])
        if s["depth_assigned"]:
            stroke.from_array(s["points3D"])
        sketch3d.strokes.append(stroke)
    sketch3d.update_stroke_ids()

    return sketch, sketch3d, cam

def cluster_straight_candidate_lines(candidate_lines, intersection_sets):
    clustered_candidate_lines = []
    clustered_intersection_sets = []
    for inter_set_id, inter_set in enumerate(intersection_sets):
        found_cluster = False
        for inter_cluster_id, inter_cluster in enumerate(
                clustered_intersection_sets):
            if np.sum(np.in1d(inter_set, inter_cluster)) == len(inter_set):
                if tools_3d.angle_line_line(
                        clustered_candidate_lines[inter_cluster_id][0],
                        candidate_lines[inter_set_id]) < np.deg2rad(
                    5.0):  # 5 degrees
                    found_cluster = True
                    clustered_candidate_lines[inter_cluster_id].append(
                        candidate_lines[inter_set_id])
                    break
        if found_cluster:
            continue
        clustered_candidate_lines.append([candidate_lines[inter_set_id]])
        clustered_intersection_sets.append(inter_set)

    for cluster_id, clustered_lines in enumerate(clustered_candidate_lines):
        clustered_candidate_lines[cluster_id] = tools_3d.merge_n_line_segments(
            clustered_lines)

    return clustered_candidate_lines, clustered_intersection_sets

def compute_tangential_intersections(sketch, relax_axis_cstrt=False, VERBOSE=False):

    beziers = [[] for i in range(len(sketch.strokes))]
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5 or s.is_ellipse():
            continue
        points = np.array([p.coords for p in s.points_list])
        beziers[s_id] = np.array(fitCurves.generate_bezier_without_tangents(points))
    for inter in sketch.intersection_graph.get_intersections():
        inter.is_tangential = False
        s_id_0, s_id_1 = inter.stroke_ids
        accept_inter = (sketch.strokes[s_id_0].axis_label < 4 and\
                sketch.strokes[s_id_0].axis_label == sketch.strokes[s_id_1].axis_label)
        if relax_axis_cstrt:
            accept_inter = (sketch.strokes[s_id_0].axis_label < 4 and sketch.strokes[s_id_1].axis_label < 4)
        if accept_inter:
            intersection_length_0 = inter.inter_params[0][1] - inter.inter_params[0][0]
            intersection_length_1 = inter.inter_params[1][1] - inter.inter_params[1][0]
            #if 21 in inter.stroke_ids or 27 in inter.stroke_ids:
            ##if 18 in inter.stroke_ids:
            #    continue
            if min(intersection_length_0, intersection_length_1) > 0.33:
                inter.is_tangential = True
                #print(inter.stroke_ids)
            continue

        if not (sketch.strokes[s_id_0].axis_label == 5 or sketch.strokes[s_id_1].axis_label == 5):
            continue
        if sketch.strokes[s_id_0].is_ellipse() or sketch.strokes[s_id_1].is_ellipse():
            continue
        first_inter_seg = np.array([sketch.strokes[s_id_0].linestring.eval(t)
                                    for t in np.linspace(inter.inter_params[0][0],
                                                         inter.inter_params[0][1], 10)])
        snd_inter_seg = np.array([sketch.strokes[s_id_1].linestring.eval(t)
                                  for t in np.linspace(inter.inter_params[1][0],
                                                       inter.inter_params[1][1], 10)])
        if sketch.strokes[s_id_0].axis_label == 5:
            closest_t_0 = [bezier_yu.get_closest_t(beziers[s_id_0], inter_p)
                           for inter_p in first_inter_seg]
            closest_point_0 = np.array([bezier.q(beziers[s_id_0], t)
                                        for t in closest_t_0])
            tan_0 = np.array([bezier.qprime(beziers[s_id_0], t)
                              for t in closest_t_0])
        else:
            tan_0 = np.repeat(np.array([sketch.strokes[s_id_0].points_list[0].coords - \
                              sketch.strokes[s_id_0].points_list[-1].coords]), 10,
                              axis=0)
        norm = np.linalg.norm(tan_0, axis=1)
        tan_0[:, 0] /= norm
        tan_0[:, 1] /= norm
        if sketch.strokes[s_id_1].axis_label == 5:
            closest_t_1 = [bezier_yu.get_closest_t(beziers[s_id_1], inter_p)
                           for inter_p in first_inter_seg]
            closest_point_1 = np.array([bezier.q(beziers[s_id_1], t)
                                        for t in closest_t_1])
            tan_1 = np.array([bezier.qprime(beziers[s_id_1], t)
                              for t in closest_t_1])
        else:
            tan_1 = np.repeat(np.array([sketch.strokes[s_id_1].points_list[0].coords - \
                                        sketch.strokes[s_id_1].points_list[-1].coords]), 10,
                              axis=0)
        norm = np.linalg.norm(tan_1, axis=1)
        tan_1[:, 0] /= norm
        tan_1[:, 1] /= norm

        tan_angles = [np.rad2deg(acos(min(1.0, np.abs(np.dot(tan_0[i], tan_1[i]))))) for i in range(len(tan_0))]
        median_angle = np.median(tan_angles)

        inter.is_tangential = median_angle < 20.0

        if VERBOSE:
            print("tan_angles", tan_angles)
            print("median_angle", median_angle)
            fig, axes = plt.subplots(nrows=1, ncols=1)
            fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                                bottom=0.0,
                                top=1.0)
            sketch.display_strokes_2(fig=fig, ax=axes,
                                              #color_process=lambda s: "#0000007D" if not s.id in [6, 8, 11] else "#00000000",
                                              color_process=lambda s: "#0000007D",
                                              linewidth_data=lambda s: 1.0, norm_global=True)
            #sketch.display_strokes_2(fig=fig, ax=axes,
            #                         color_process=lambda s: "red",
            #                         linewidth_data=lambda s: 4.0, norm_global=True,
            #                         display_strokes=[s_id_0, s_id_1])
            axes.plot(first_inter_seg[:, 0], first_inter_seg[:, 1], lw=3)
            axes.plot(snd_inter_seg[:, 0], snd_inter_seg[:, 1], lw=3)

            if sketch.strokes[s_id_0].axis_label == 5:
                c_points = []
                for t in np.linspace(0.0, 1.0, num=20):
                    c_points.append(bezier.q(beziers[s_id_0], t))
                c_points = np.array(c_points)
                axes.plot(c_points[:, 0], c_points[:, 1])

                for closest_point in closest_point_0:
                    axes.add_artist(Circle(xy=closest_point, radius=inter.acc_radius, color="blue"))
                for tan_id, tan in enumerate(tan_0):
                    tan_vec = np.array([closest_point_0[tan_id], closest_point_0[tan_id]+30.0*tan])
                    axes.plot(tan_vec[:, 0], tan_vec[:, 1])
            if sketch.strokes[s_id_1].axis_label == 5:
                c_points = []
                for t in np.linspace(0.0, 1.0, num=20):
                    c_points.append(bezier.q(beziers[s_id_1], t))
                c_points = np.array(c_points)
                axes.plot(c_points[:, 0], c_points[:, 1])

                for closest_point in closest_point_1:
                    axes.add_artist(Circle(xy=closest_point, radius=inter.acc_radius, color="blue"))
                for tan_id, tan in enumerate(tan_1):
                    tan_vec = np.array([closest_point_1[tan_id], closest_point_1[tan_id]+30.0*tan])
                    axes.plot(tan_vec[:, 0], tan_vec[:, 1])
            axes.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius, color="green"))

            axes.set_xlim(0, sketch.width)
            axes.set_ylim(sketch.height, 0)
            axes.axis("equal")
            axes.axis("off")
            plt.show()