import sys, os
import math
import cv2
from scipy import stats
from scipy import interpolate
import seaborn as sns
import matplotlib.patheffects as pe
from skimage.transform import rotate
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse, Arc, Wedge
import utils_plot
import open3d as o3d
import numpy as np
from shapely.geometry import MultiPoint, Point, LineString, Polygon, MultiLineString
import tools_3d
from copy import deepcopy
from descartes import PolygonPatch
import matplotlib.pyplot as plt
from pystrokeproc.sketch_core import Sketch, Stroke
import polyscope as ps
from skspatial.objects import Plane, Points
from matplotlib.patches import Circle
from matplotlib.patches import Polygon as plt_polygon
from shapely.strtree import STRtree
from sklearn.cluster import MeanShift, estimate_bandwidth
from scipy.spatial import Delaunay
from scipy.linalg import lstsq
from scipy.optimize import minimize, Bounds
from scipy.spatial import distance
from scipy.spatial.distance import directed_hausdorff
from hausdorff import hausdorff_distance
from time import process_time, time
from itertools import permutations
from more_itertools import distinct_combinations
from rdp import rdp
from src.fitCurves import bezier
from src.fitCurves import fitCurves
#import fitCurves, bezier
from tqdm import trange
from chamferdist import ChamferDistance
import networkx as nx
from sklearn.cluster import KMeans

patch_blue = "#B2CBE5"
patch_red = "#e41a1c"
import numpy as np
from sklearn.neighbors import NearestNeighbors
CURVE_SAMPLE_SIZE = 30


def chamfer_distance(x, y, metric='l2', direction='bi', return_pointwise_distances=False):
    """Chamfer distance between two point clouds
    Parameters
    ----------
    x: numpy array [n_points_x, n_dims]
        first point cloud
    y: numpy array [n_points_y, n_dims]
        second point cloud
    metric: string or callable, default ‘l2’
        metric to use for distance computation. Any metric from scikit-learn or scipy.spatial.distance can be used.
    direction: str
        direction of Chamfer distance.
            'y_to_x':  computes average minimal distance from every point in y to x
            'x_to_y':  computes average minimal distance from every point in x to y
            'bi': compute both
    Returns
    -------
    chamfer_dist: float
        computed bidirectional Chamfer distance:
            sum_{x_i \in x}{\min_{y_j \in y}{||x_i-y_j||**2}} + sum_{y_j \in y}{\min_{x_i \in x}{||x_i-y_j||**2}}
    """

    pointwise_dists = []
    if direction=='y_to_x':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        chamfer_dist = np.mean(min_y_to_x)
        pointwise_dists = min_y_to_x
    elif direction=='x_to_y':
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_x_to_y)
        pointwise_dists = min_x_to_y
    elif direction=='bi':
        x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric=metric).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_y_to_x) + np.mean(min_x_to_y)
        pointwise_dists = [min_x_to_y, min_y_to_x]
    else:
        raise ValueError("Invalid direction type. Supported types: \'y_x\', \'x_y\', \'bi\'")

    if return_pointwise_distances:
        return chamfer_dist, pointwise_dists
    return chamfer_dist

# reconstruction contains the reconstructed 3d data: shape: (2, 3)
# stroke_ids contains the two stroke ids which were used for this reconstruction
# vp: index of vanishing point used reconstruction, i.e., which symmetry plane
# has been used
class SymmetryCandidate:
    def __init__(self, reconstruction=[], stroke_ids=[], vp=-1):
        self.reconstruction = reconstruction
        self.stroke_ids = stroke_ids
        self. vp = vp

class Intersection3d:
    def __init__(self, inter_id=-1, inter_3d=[], stroke_ids=[],
                 stroke_candidates=[],
                 candidate_correspondence_ids=[]):
        self.inter_id = inter_id
        self.inter_3d = inter_3d
        self.stroke_ids = stroke_ids
        self.stroke_candidates = stroke_candidates
        self.candidate_correspondence_ids = candidate_correspondence_ids

class Intersection3dProxy:
    def __init__(self, inter_id=-1, inter_3ds=[], stroke_ids=[],
                 proxy_ids=[], acc_radius=-1):
        self.inter_id = inter_id
        self.inter_3ds = inter_3ds
        self.stroke_ids = stroke_ids
        self.proxy_ids = proxy_ids
        self.acc_radius = acc_radius

    def __str__(self):
        return "inter_id: "+str(self.inter_id)+", stroke_ids: " + str(self.stroke_ids) \
               + ", proxy_ids: " + str(self.proxy_ids) + ", inter_3ds: " + str(self.inter_3ds)

class IntersectionSimple:
    def __init__(self, inter_id=-1, stroke_ids=[],
                 cam_depths=[], acc_radius=-1, epsilon=0.0, is_tangential=False,
                 is_fixed=False, fix_depth=-1, is_triplet=False, is_extended=False,
                 is_parallel=False):
        self.inter_id = inter_id
        self.stroke_ids = stroke_ids
        self.cam_depths = cam_depths
        self.acc_radius = acc_radius
        self.epsilon = epsilon
        self.is_tangential = is_tangential
        self.is_fixed = is_fixed
        self.fix_depth = fix_depth
        self.is_triplet = is_triplet
        self.is_extended = is_extended
        self.is_parallel = is_parallel

    def __str__(self):
        return "inter_id: "+str(self.inter_id)+", stroke_ids: " + str(self.stroke_ids) \
               + ", cam_depths: " + str(self.cam_depths)

class LineCoverage:

    def __init__(self, weight, inter_id=-1, stroke_proxy_ids=[], stroke_ids=[]):
        self.weight = weight
        self.inter_id = inter_id
        self.stroke_proxy_ids = stroke_proxy_ids
        self.stroke_ids = stroke_ids

    def __str__(self):
        return "inter_id: "+str(self.inter_id) + ", weight: " + str(self.weight) + ", stroke_ids: " + str(self.stroke_ids)

class Correspondence:

    def __init__(self, stroke_3ds, stroke_ids, candidate_ids, plane_id,
                 first_inter_stroke_id=-1, snd_inter_stroke_id=-1,
                 masked_correspondences=[]):
        self.stroke_3ds = stroke_3ds
        self.stroke_ids = stroke_ids
        self.candidate_ids = candidate_ids
        self.plane_id = plane_id
        # if self-symmetric, note first and second inter-stroke-ids
        self.first_inter_stroke_id = first_inter_stroke_id
        self.snd_inter_stroke_id = snd_inter_stroke_id
        self.masked_correspondences = masked_correspondences

class Candidate:
    def __init__(self, stroke_3d, stroke_id, plane_id, correspondence_id,
                 candidate_id, first_inter_stroke_id=-1, snd_inter_stroke_id=-1):
        self.stroke_3d = stroke_3d
        self.stroke_id = stroke_id
        self.candidate_id = candidate_id
        self.plane_id = plane_id
        self.correspondence_id = correspondence_id
        # if self-symmetric, note first and second inter-stroke-ids
        self.first_inter_stroke_id = first_inter_stroke_id
        self.snd_inter_stroke_id = snd_inter_stroke_id

def gather_symmetry_correspondences_2d_curves_v2(sketch, camera, close_thresh=20.0,
                                                 overlap_thresh=0.1, focus_vp=0,
                                                 focus_axis_label=1, VERBOSE=False):
    print("gather_symmetry_correspondences_2d_curves_v2")
    intersected_stroke_ids = []
    for s_id, s in enumerate(sketch.strokes):
        #print("s_id", s_id)
        #if s.axis_label > 2:
        #    continue
        if s.axis_label != 5:
            continue
        if s.axis_label != focus_axis_label:
            continue
        if s.is_ellipse() and s.closest_major_axis_deviation < 15.0 and \
                s.closest_major_axis_id != focus_vp:
            continue

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue
            elif s.axis_label == 5 and s.is_ellipse() and other_s.is_ellipse():
                if np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(s.minor_axis, other_s.minor_axis))))) > 30.0:
                    continue

            if (s.linestring.linestring.buffer(s.acc_radius).intersection(
                    other_s.linestring.linestring).length/s.length() > 0.5 or
                    s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                continue

            # check what axis suits this stroke pair best
            axis_check = []
            axis_ids = []
            axis_overlaps = []
            for axis_id in range(3):
                vanishing_triangle = get_vanishing_triangle(
                    s, np.array(camera.vanishing_points_coords[axis_id]),
                    #s, np.array(camera.vanishing_points_coords[focus_vp]),
                    epsilon=1.5)
                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue

                if s.is_ellipse() and other_s.is_ellipse():
                    if not s.linestring.linestring.convex_hull.intersects(other_s.linestring.linestring.convex_hull):
                        intersected_stroke_ids.append([s_id, other_s_id])
                        # can only arrive here if focus_vp == closest_minor_axis_id
                        break
                        axis_check.append(True)
                        axis_ids.append(axis_id)
                        axis_overlaps.append(100.0)
                else:
                    intersect = curve_symmetry_candidate(s, other_s, vanishing_triangle,
                                                         focus_vp, sketch, camera,
                                                         dist_threshold=0.15, VERBOSE=False)
                    if intersect:
                        #intersected_stroke_ids.append([s_id, other_s_id])
                        #break
                        axis_check.append(True)
                        axis_ids.append(axis_id)
                        tight_vanishing_triangle = get_vanishing_triangle(
                            s, np.array(camera.vanishing_points_coords[axis_id]),
                            epsilon=0.0)
                        axis_overlaps.append(tight_vanishing_triangle.intersection(
                            other_s.linestring.linestring).length)
            if len(axis_overlaps) > 0 and axis_ids[np.argmax(axis_overlaps)] == focus_vp \
                    and axis_check[np.argmax(axis_overlaps)]:
                intersected_stroke_ids.append([s_id, other_s_id])
    return intersected_stroke_ids

def gather_symmetry_correspondences_2d_curves(sketch, camera, close_thresh=20.0,
                                              overlap_thresh=0.1, focus_vp=0,
                                              focus_axis_label=1, VERBOSE=False):
    intersected_stroke_ids = []
    sketch_inter_bbox = MultiPoint([inter.inter_coords
                                    for inter in sketch.intersection_graph.get_intersections()
                                    if not inter.is_extended]).bounds
    sketch_inter_diag = np.linalg.norm(np.array(sketch_inter_bbox[:2]) -
                                       np.array(sketch_inter_bbox[2:]))
    for s_id, s in enumerate(sketch.strokes):
        #if s.axis_label > 2:
        #    continue
        if s.axis_label != 5:
            continue
        if s.axis_label != focus_axis_label:
            continue

        s_coords = [p.coords for p in s.points_list]

        vanishing_triangles = \
            [MultiPoint(s_coords + [vp_i]).convex_hull
             for i, vp_i in enumerate(camera.vanishing_points_coords)]
        # extract vanishing_triangle for each convex poly
        for vanishing_triangle_id, vanishing_triangle in enumerate(vanishing_triangles):
            vp_id = -1
            for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
                found_vp = False
                for vp in camera.vanishing_points_coords:
                    if np.isclose(np.linalg.norm(p - vp), 0.0):
                        vp_id = p_id
                        found_vp = True
                        break
                if found_vp:
                    break
            triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]), vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
            # extend vanishing triangle to "infinity"
            triangle[0] += (triangle[0] - triangle[1])
            triangle[-1] += (triangle[-1] - triangle[1])
            vanishing_triangles[vanishing_triangle_id] = Polygon(triangle)

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue
            elif s.axis_label == 5 and s.is_ellipse() and other_s.is_ellipse():
                if np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(s.minor_axis, other_s.minor_axis))))) > 30.0:
                    continue

            #if (s_id == 24 and other_s_id == 23) or (s_id == 28 and other_s_id == 27) or \
            #        (s_id == 36 and other_s_id == 35):
            #    VERBOSE = True
            #else:
            #    VERBOSE = False

            #if VERBOSE:
            #    print(s_id, other_s_id, s.linestring.linestring.buffer(s.acc_radius).intersection(
            #        other_s.linestring.linestring).length/s.length())
            #    print(s_id, other_s_id, s.linestring.linestring.buffer(s.acc_radius).intersection(
            #        other_s.linestring.linestring).length/other_s.length())

            if (s.linestring.linestring.buffer(s.acc_radius).intersection(
                    other_s.linestring.linestring).length/s.length() > 0.5 or
                    s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                continue

            overlaps = []
            axis_ids = []
            hausdorff_distances = []
            #VERBOSE = s_id == 36 and other_s_id == 23
            for i, vanishing_triangle in enumerate(vanishing_triangles):

                if s.is_ellipse() and s.closest_major_axis_deviation < 15.0 and \
                        s.closest_major_axis_id != i:
                    continue
                #if min(s_vanishing_intersection.length, other_s_vanishing_intersection.length)/\
                #    max(s_vanishing_intersection.length, other_s_vanishing_intersection.length) < 0.5:
                #    continue
                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue
                if type(other_intersecting_segment) == MultiLineString:
                    pts = [p for l in other_intersecting_segment.geoms for p in l.coords]
                else:
                    pts = [p for p in other_intersecting_segment.coords]
                hd = hausdorff_distance(np.array(pts),
                                        np.array(sketch.strokes[s_id].linestring.linestring))
                if s.is_ellipse() and other_s.is_ellipse():
                    if s.linestring.linestring.convex_hull.intersects(other_s.linestring.linestring.convex_hull):
                        continue
                    overlaps.append(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                    hausdorff_distances.append(hd)
                    axis_ids.append(i)
                    continue
                if VERBOSE:
                    print("hd", hd, 0.05*sketch.strokes[s_id].length())
                #if VERBOSE and s.axis_label == 5:
                #    #if True:
                #    #print(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                #    #print(overlap_thresh)
                #    #print(sketch.strokes[s_id].linestring.linestring.hausdorff_distance(
                #    #    sketch.strokes[other_s_id].linestring.linestring))
                #    #print(sketch_inter_diag)
                #    fig, ax = plt.subplots(nrows=1, ncols=1)
                #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                #                        bottom=0.0,
                #                        top=1.0)

                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "#0000007D",
                #                             linewidth_data=lambda s: 2.0)
                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "red",
                #                             display_strokes=[s_id, other_s_id],
                #                             linewidth_data=lambda s: 3.0)
                #    poly_patch = PolygonPatch(vanishing_triangle, fc='#6699cc',
                #                              alpha=0.5)
                #    ax.add_patch(poly_patch)

                #    ax.set_xlim(0, sketch.width)
                #    ax.set_ylim(sketch.height, 0)
                #    ax.axis("equal")
                #    ax.axis("off")
                #    plt.show()
                s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                other_s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if type(s_vanishing_intersection) is MultiLineString:
                    s_vanishing_pts = np.concatenate([np.array(l) for l in s_vanishing_intersection])
                elif type(s_vanishing_intersection) is LineString:
                    s_vanishing_pts = np.array(s_vanishing_intersection)
                else:
                    continue
                #else:
                #    s_vanishing_pts = np.array(s_vanishing_intersection)
                if type(other_s_vanishing_intersection) is MultiLineString:
                    other_s_vanishing_pts = np.concatenate([np.array(l) for l in other_s_vanishing_intersection])
                elif type(other_s_vanishing_intersection) is LineString:
                    other_s_vanishing_pts = np.array(other_s_vanishing_intersection)
                else:
                    continue
                #else:
                #    other_s_vanishing_pts = np.array(other_s_vanishing_intersection)
#                initial_T = np.identity(4) # Initial transformation for ICP
#                source = np.zeros([len(s_vanishing_pts), 3])
#                source[:, :2] = s_vanishing_pts
#                target = np.zeros([len(other_s_vanishing_pts), 3])
#                target[:, :2] = other_s_vanishing_pts
#                source_pc = o3d.geometry.PointCloud()
#                source_pc.points = o3d.utility.Vector3dVector(source)
#                target_pc = o3d.geometry.PointCloud()
#                target_pc.points = o3d.utility.Vector3dVector(target)
#
#                distance = 1000.0 # The threshold distance used for searching correspondences
#                icp_type = o3d.pipelines.registration.TransformationEstimationPointToPoint(with_scaling=False)
#                iterations = o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration = 10000)
#                result = o3d.pipelines.registration.registration_icp(source_pc, target_pc, distance, initial_T, icp_type, iterations)
#                trans = np.asarray(deepcopy(result.transformation))
#                #if trans[0, 0]*trans[1, 1] < 0:
#                #    trans[:, 0] *= -1
#                source_pc.transform(trans)
#                scale_x = np.linalg.norm(trans[:, 0])
#                scale_y = np.linalg.norm(trans[:, 1])
#                scale_z = np.linalg.norm(trans[:, 2])
#                scale_z = trans[2, 2]
#                reflection = scale_z < 0
#                rot_mat = deepcopy(trans)
#                rot_mat[:, 0] /= scale_z
#                rot_mat[:, 1] /= scale_z
#                rot_mat[:, 2] /= scale_z
#                angle = np.rad2deg(np.arccos(rot_mat[0, 0]))
#
#                reject = True
#                tmp_vp = np.array(camera.vanishing_points_coords[i])
#                center = np.mean(np.array(s_vanishing_pts), axis=0)
#                vanishing_line = tmp_vp - center
#                vanishing_line /= np.linalg.norm(vanishing_line)
#                ideal_vec = np.array([1, 0])
#                vanishing_line_angle = np.rad2deg(np.arccos(min(1.0, abs(np.dot(ideal_vec, vanishing_line)))))
#
#                corrected_angle = angle - vanishing_line_angle
#                rot_angle = 270*int(reflection) + angle# - vanishing_line_angle
#                if VERBOSE:
#                    print("vanishing_line_angle", vanishing_line_angle)
#                    print(reflection)
#                    print(rot_angle)
#                    print(angle)
#                    print(rot_mat)
#                    print(scale_z)
#                #if abs(rot_angle) < 20.0 or abs(abs(rot_angle) - 360) < 20.0 or abs(abs(rot_angle) - 180.0) < 20.0 or abs(corrected_angle) < 20.0:
#                if abs(rot_angle) < 20.0 or abs(abs(rot_angle) - 360) < 20.0 or abs(abs(rot_angle) - 180.0) < 20.0:# or abs(corrected_angle) < 20.0:
#                    reject = False
#                if abs(scale_z) < 0.2:
#                    reject = True
#
#                if hausdorff_distance(np.array(pts),
#                                      np.array(sketch.strokes[s_id].linestring.linestring)) < 0.3*sketch.strokes[s_id].length():
#
#                    reject = True
#                if VERBOSE:
#                    print("Rejected", reject)
#                if reject:
#                    continue
                #if hd < 0.05*sketch.strokes[s_id].length():
                #    continue

                overlaps.append(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                hausdorff_distances.append(hd)
                axis_ids.append(i)

                if VERBOSE:# and s_id == 65 and s.axis_label == 5:
                    s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                    other_s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                    if type(s_vanishing_intersection) is MultiLineString:
                        s_vanishing_pts = np.concatenate([np.array(l) for l in s_vanishing_intersection])
                    else:
                        s_vanishing_pts = np.array(s_vanishing_intersection)
                    if type(other_s_vanishing_intersection) is MultiLineString:
                        other_s_vanishing_pts = np.concatenate([np.array(l) for l in other_s_vanishing_intersection])
                    else:
                        other_s_vanishing_pts = np.array(other_s_vanishing_intersection)
                    print(s_vanishing_pts, other_s_vanishing_pts)
                    initial_T = np.identity(4) # Initial transformation for ICP
                    source = np.zeros([len(s_vanishing_pts), 3])
                    source[:, :2] = s_vanishing_pts
                    print(source.shape)
                    target = np.zeros([len(other_s_vanishing_pts), 3])
                    target[:, :2] = other_s_vanishing_pts
                    source_pc = o3d.geometry.PointCloud()
                    source_pc.points = o3d.utility.Vector3dVector(source)
                    target_pc = o3d.geometry.PointCloud()
                    target_pc.points = o3d.utility.Vector3dVector(target)

                    distance = 1000.0 # The threshold distance used for searching correspondences
                    icp_type = o3d.pipelines.registration.TransformationEstimationPointToPoint(with_scaling=False)
                    iterations = o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration = 10000)
                    result = o3d.pipelines.registration.registration_icp(source_pc, target_pc, distance, initial_T, icp_type, iterations)
                    print(result)
                    print(result.transformation)
                    trans = np.asarray(deepcopy(result.transformation))
                    #if trans[0, 0]*trans[1, 1] < 0:
                    #    trans[:, 0] *= -1
                    source_pc.transform(trans)
                    scale_x = np.linalg.norm(trans[:, 0])
                    scale_y = np.linalg.norm(trans[:, 1])
                    scale_z = np.linalg.norm(trans[:, 2])
                    scale_z = trans[2, 2]
                    reflection = scale_z < 0
                    rot_mat = deepcopy(trans)
                    print(scale_x, scale_y, scale_z)
                    rot_mat[:, 0] /= scale_z
                    rot_mat[:, 1] /= scale_z
                    rot_mat[:, 2] /= scale_z
                    print("rot_mat", rot_mat)
                    print(np.rad2deg(np.arccos(rot_mat[0, 0])))
                    print(np.rad2deg(np.arcsin(rot_mat[1, 0])))
                    angle = np.rad2deg(np.arccos(rot_mat[0, 0]))
                    print("reflection", reflection)

                    rot_angle = 270*int(reflection) + angle
                    print("rot_angle", rot_angle)
                    reject = True
                    if abs(rot_angle) < 20.0 or abs(abs(rot_angle) - 360) < 20.0 or abs(abs(rot_angle) - 180.0) < 20.0:
                        reject = False
                    if abs(scale_z) < 0.2:
                        reject = True

                    print("overlaps", overlaps)
                    print("hausdorff_distances", hausdorff_distance(s_vanishing_pts,
                                          other_s_vanishing_pts), 0.30*sketch.strokes[s_id].length(), s.acc_radius)
                    if hausdorff_distance(np.array(pts),
                                          np.array(sketch.strokes[s_id].linestring.linestring)) < 0.3*sketch.strokes[s_id].length():

                        reject = True
                    print("REJECT", reject)
                    print(s_id, other_s_id)
                    #if reject:
                    #    continue

                    #if True:
                    #print(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                    #print(overlap_thresh)
                    #print(sketch.strokes[s_id].linestring.linestring.hausdorff_distance(
                    #    sketch.strokes[other_s_id].linestring.linestring))
                    #print(sketch_inter_diag)
                    plt.rcParams["figure.figsize"] = (10, 10)
                    fig, ax = plt.subplots(nrows=1, ncols=1)
                    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                                        bottom=0.0,
                                        top=1.0)

                    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                                             color_process=lambda s: "#0000007D",
                                             linewidth_data=lambda s: 2.0)
                    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                                             color_process=lambda s: "red",
                                             display_strokes=[s_id, other_s_id],
                                             linewidth_data=lambda s: 3.0)
                    poly_patch = PolygonPatch(vanishing_triangle, fc='#6699cc',
                                              alpha=0.5)
                    transformed_source_pts = np.asarray(source_pc.points)
                    #print(transformed_source_pts)
                    plt.plot(transformed_source_pts[:, 0], transformed_source_pts[:, 1], c="green", lw=3)
                    s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                    if type(s_vanishing_intersection) is MultiLineString:
                        s_vanishing_pts = np.concatenate([np.array(l) for l in s_vanishing_intersection])
                    else:
                        s_vanishing_pts = np.array(s_vanishing_intersection)
                    #print(s_vanishing_intersection)
                    #plt.plot(s_vanishing_pts[:, 0],
                    #         s_vanishing_pts[:, 1])
                    ax.add_patch(poly_patch)

                    ax.set_xlim(0, sketch.width)
                    ax.set_ylim(sketch.height, 0)
                    ax.axis("equal")
                    ax.axis("off")
                    plt.show()
            if VERBOSE:
                print("overlaps", overlaps)
                print("hausdorff_distances", hausdorff_distances)
                print("axis_ids", axis_ids)
            if VERBOSE:
                exit()
            if len(overlaps) == 0:
                continue
            if len(overlaps) == 1 and axis_ids[0] != focus_vp:
                continue
            if len(overlaps) == 1 and axis_ids[0] == focus_vp and overlaps[0] > overlap_thresh:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
                continue
            sorted_overlap_ids = np.argsort(overlaps)
            #if axis_ids[sorted_overlap_ids[-1]] != focus_vp:
            #    continue
            if VERBOSE:
                print(overlaps[sorted_overlap_ids[-1]])
            if overlaps[sorted_overlap_ids[-1]] < overlap_thresh:
                continue
            if np.isclose(overlaps[sorted_overlap_ids[-2]], 0.0) and \
                    axis_ids[sorted_overlap_ids[-1]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
                continue
            confidence = overlaps[sorted_overlap_ids[-1]]/overlaps[sorted_overlap_ids[-2]]
            if VERBOSE:
                print("confidence", confidence)
            if confidence < 1.33 and overlaps[sorted_overlap_ids[-2]] > overlap_thresh and \
                    axis_ids[sorted_overlap_ids[-2]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
            if overlaps[sorted_overlap_ids[-1]] > overlap_thresh and \
                    axis_ids[sorted_overlap_ids[-1]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)

    return intersected_stroke_ids

def gather_symmetry_correspondences_2d(sketch, camera, selected_planes,
                                       close_thresh=20.0, overlap_thresh=0.7, focus_vp=0, focus_axis_label=1):
    print("gather_symmetry_correspondences_2d")
    intersected_stroke_ids = []
    straight_line_overlap_thresh = overlap_thresh
    curve_overlap_thresh = 0.1
    sketch_inter_bbox = MultiPoint([inter.inter_coords
                                      for inter in sketch.intersection_graph.get_intersections()
                                      if not inter.is_extended]).bounds
    sketch_inter_diag = np.linalg.norm(np.array(sketch_inter_bbox[:2]) -
                                       np.array(sketch_inter_bbox[2:]))
    for s_id, s in enumerate(sketch.strokes):
        #print(s_id)
        #if s.axis_label > 2:
        #    continue
        if s.axis_label == 5:
            continue
            overlap_thresh = curve_overlap_thresh
        else:
            overlap_thresh = straight_line_overlap_thresh
        if s.axis_label != focus_axis_label:
            continue
        #if s.axis_label == 5:
        #    print("curve", s_id)

        start_point = np.array(s.points_list[0].coords)
        end_point = np.array(s.points_list[-1].coords)
        s_coords = [p.coords for p in s.points_list]

        #vanishing_triangles = \
        #    [MultiPoint([start_point + 0.5*(start_point - vp_i),
        #                 end_point + 0.5*(end_point - vp_i),
        #                 vp_i]).convex_hull
        #     for i, vp_i in enumerate(camera.vanishing_points_coords) if not i == s.axis_label and i == focus_vp]
        vanishing_triangles = \
            [MultiPoint(s_coords + [vp_i]).convex_hull
             for i, vp_i in enumerate(camera.vanishing_points_coords) if not i == s.axis_label and i == focus_vp]
        # extract vanishing_triangle for each convex poly
        for vanishing_triangle_id, vanishing_triangle in enumerate(vanishing_triangles):
            vp_id = -1
            for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
                found_vp = False
                for vp in camera.vanishing_points_coords:
                    if np.isclose(np.linalg.norm(p - vp), 0.0):
                        vp_id = p_id
                        found_vp = True
                        break
                if found_vp:
                    break
#            print(vanishing_triangle.exterior.coords)
#            print(vp_id)
            triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]), vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
            # extend vanishing triangle to "infinity"
            triangle[0] += (triangle[0] - triangle[1])
            triangle[-1] += (triangle[-1] - triangle[1])
            vanishing_triangles[vanishing_triangle_id] = Polygon(triangle)
        #sys.exit()

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue
            #if s.axis_label == 5 and abs(s_id - other_s_id) < 5:
            #    continue

            #if s_id == 38 and other_s_id == 21:
            #    print("Here")
            #    print(s_id, other_s_id)


            if s.axis_label < 3 and \
                    other_s.linestring.linestring.distance(s.linestring.linestring) < min(other_s.acc_radius, s.acc_radius):
                    #other_s.linestring.linestring.distance(s.linestring.linestring) < close_thresh:
                #if s_id == 56 and other_s_id == 55:
                #    print("distance", other_s.linestring.linestring.distance(s.linestring.linestring))
                #    print(s_id, other_s_id)
                #    print(other_s.linestring.linestring.length)
                #    print(s.acc_radius, other_s.acc_radius)
                #    exit()
                continue
            if s.axis_label > 3 and \
                    (s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/s.length() > 0.5 or
                      s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                #if s_id == 38 and other_s_id == 21:
                #    print("intersection")
                #    print(s_id, other_s_id)
                #    exit()
                continue

            #if s_id == 38 and other_s_id == 21:
            #    print("Here")
            #    print(s_id, other_s_id)


            for i, vanishing_triangle in enumerate(vanishing_triangles):

                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    #if s_id == 38 and other_s_id == 21:
                    #    print("no vanishing line intersection")
                    #    print(s_id, other_s_id)
                    #    exit()
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue
                #if s.axis_label == 5:
                #if s_id == 38 and other_s_id == 35:
                #print("overlap ", other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                if other_intersecting_segment.length/sketch.strokes[other_s_id].length() > overlap_thresh:
                    intersected_stroke_ids.append([s_id, other_s_id])
                    #print("success")
                    #if s_id == 56 and other_s_id == 55:

                    #    #if s.axis_label == 5:
                    #    #if True:
                    #    print(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                    #    print(overlap_thresh)
                    #    print(sketch.strokes[s_id].linestring.linestring.hausdorff_distance(
                    #        sketch.strokes[other_s_id].linestring.linestring))
                    #    print(sketch_inter_diag)
                    #    fig, ax = plt.subplots(nrows=1, ncols=1)
                    #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                    #                        bottom=0.0,
                    #                        top=1.0)

                    #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                    #                             color_process=lambda s: "#0000007D",
                    #                             linewidth_data=lambda s: 2.0)
                    #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                    #                             color_process=lambda s: "red",
                    #                             display_strokes=[s_id, other_s_id],
                    #                             linewidth_data=lambda s: 3.0)
                    #    poly_patch = PolygonPatch(vanishing_triangle, fc='#6699cc',
                    #                              alpha=0.5)
                    #    ax.add_patch(poly_patch)

                    #    ax.set_xlim(0, sketch.width)
                    #    ax.set_ylim(sketch.height, 0)
                    #    ax.axis("equal")
                    #    ax.axis("off")
                    #    plt.show()

    #if s.axis_label == 5:
    #    print("curve", intersected_stroke_ids)
    return intersected_stroke_ids

# returns array of shape (m, 2) where m is the number of candidate symmetry correspondences
def gather_symmetry_correspondences_2d_midlines(sketch, camera, close_thresh=20.0, overlap_thresh=0.7, focus_vp=0, focus_axis_label=1):
    intersected_stroke_ids = []
    midlines = []
    midpoints = []
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label > 2:
            continue
        if s.axis_label != focus_axis_label:
            continue

        start_point = np.array(s.points_list[0].coords)
        end_point = np.array(s.points_list[-1].coords)

        vanishing_triangles = \
            [MultiPoint([start_point + 0.5*(start_point - vp_i),
                         end_point + 0.5*(end_point - vp_i),
                         vp_i]).convex_hull
             for i, vp_i in enumerate(camera.vanishing_points_coords) if not i == s.axis_label and i == focus_vp]
        vanishing_points = [vp_i for i, vp_i in enumerate(camera.vanishing_points_coords) if not i == s.axis_label and i == focus_vp]

        #print(s_id)
        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if other_s.linestring.linestring.distance(s.linestring.linestring) < close_thresh:
                continue
            #if (other_s.linestring.linestring.length/s.linestring.linestring.length < 0.5 or
            #        other_s.linestring.linestring.length / s.linestring.linestring.length > 1.5):
            #    continue
            # get line between midpoints of both strokes to see which
            # vanishing_triangle intersection is best
            other_start_point = np.array(other_s.points_list[0].coords)
            other_end_point = np.array(other_s.points_list[-1].coords)
            vertical_midline = LineString([(start_point+end_point)/2.0, (other_start_point+other_end_point)/2.0])
            tmp_midlines = []
            tmp_midpoints = []
            tmp_vanishing_triangles = []

            for i, vanishing_triangle in enumerate(vanishing_triangles):
                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if type(other_intersecting_segment) != LineString:
                    continue
                if other_intersecting_segment.length/sketch.strokes[other_s_id].length() > overlap_thresh:
                    other_intersecting_segment = np.array(other_intersecting_segment)
                    # construct mid-line
                    other_s_start_vanishing_inter = tools_3d.line_line_intersection_2d(
                        start_point, vanishing_points[i],
                        other_intersecting_segment[0], other_intersecting_segment[-1])
                    other_s_end_vanishing_inter = tools_3d.line_line_intersection_2d(
                        end_point, vanishing_points[i],
                        other_intersecting_segment[0], other_intersecting_segment[-1])
                    mid_line_intersection = tools_3d.line_line_intersection_2d(
                        start_point, other_s_end_vanishing_inter,
                        end_point, other_s_start_vanishing_inter)
                    mid_line_start = tools_3d.line_line_intersection_2d(
                        start_point, vanishing_points[i],
                        mid_line_intersection, camera.vanishing_points_coords[s.axis_label])
                    mid_line_end = tools_3d.line_line_intersection_2d(
                        end_point, vanishing_points[i],
                        mid_line_intersection, camera.vanishing_points_coords[s.axis_label])
                    mid_line = np.array([mid_line_start, mid_line_end])
                    tmp_midlines.append(mid_line)
                    midpoint = (mid_line[0]+mid_line[1])/2.0
                    tmp_midpoints.append(midpoint)
                    tmp_vanishing_triangles.append(vanishing_triangle)

            #if len(tmp_midlines) == 1:
            #    midpoint = tmp_midpoints[0]
            #    mid_line = tmp_midlines[0]
            #    vanishing_triangle = tmp_vanishing_triangles[0]
            #elif len(tmp_midlines) == 2:
            #    dist_0 = vertical_midline.distance(Point(tmp_midpoints[0]))
            #    dist_1 = vertical_midline.distance(Point(tmp_midpoints[1]))
            #    min_id = np.argmin([dist_0, dist_1]).flatten()[0]
            #    midpoint = tmp_midpoints[min_id]
            #    mid_line = tmp_midlines[min_id]
            #    vanishing_triangle = tmp_vanishing_triangles[min_id]

            if len(tmp_midlines) > 0:
                #midlines.append(mid_line)
                #midpoints.append(midpoint)
                intersected_stroke_ids.append([s_id, other_s_id])

    return np.array(intersected_stroke_ids)

def reconstruct_two_points(p1, p2, refl_mat, camera):

    p1_lifted = camera.lift_point(p1, 1.0)
    p2_lifted = camera.lift_point(p2, 1.0)
    p1_projective = np.array([camera.cam_pos, p1_lifted])
    p2_projective = np.array([camera.cam_pos, p2_lifted])
    p2_projective /= np.linalg.norm(p2_projective)
    reflected_p1_projective = tools_3d.apply_hom_transform_to_points(p1_projective, refl_mat)
    reflected_p1_projective_dir_vec = reflected_p1_projective[-1] - reflected_p1_projective[0]
    reflected_p1_projective_dir_vec /= np.linalg.norm(reflected_p1_projective_dir_vec)
    p2_projective_dir_vec = p2_projective[-1] - p2_projective[0]
    p2_projective_dir_vec /= np.linalg.norm(p2_projective_dir_vec)
#    print("p1_lifted")
#    print(p1_lifted)
#    print(reflected_p1_projective_dir_vec)
#    print(p2_lifted)
#    print(p2_projective_dir_vec)
    p2_reconstructed, _ = tools_3d.line_line_collision(reflected_p1_projective[-1], reflected_p1_projective_dir_vec,
                                                       p2_lifted, p2_projective_dir_vec)
    p2_reconstructed = np.array(p2_reconstructed)
#    print("p2_reconstructed")
#    print(p2_reconstructed)
    p1_reconstructed = tools_3d.apply_hom_transform_to_points([p2_reconstructed], refl_mat)[0]
    return [p1_reconstructed, p2_reconstructed]

# returns 2 arrays of shape (N, 3), i.e., two 3d polylines
def reconstruct_symmetric_strokes_curved(s_1, s_2, focus_vp, sym_plane_point, sym_plane_normal, camera, sketch):

    # get all point correspondences between the two curves
    #print("reconstruct_symmetric_strokes_curved")
    print(s_1.id)
    print(s_2.id)
#    if not(s_1.id == 51 and s_2.id == 50):
#        return [[],[]]
    point_correspondences = []
    vp = camera.vanishing_points_coords[focus_vp]

    # get arc_params on second curve
    s_2_arc_params = []
    step_size = 0.1
    for t in np.arange(0.0, 1+step_size, step_size):
        #print(t)
#        bez = s_1.get_bezier()
#        print(bez)
#        p_1 = np.array(eval(bez,))
        p_1 = np.array(s_1.eval_point(t).coords)
        p_1_vp = 2*p_1 - vp
        vanishing_line = Stroke([])
        vanishing_line.from_array([p_1_vp, vp])

#        fig, ax = plt.subplots(nrows=1, ncols=1)
#        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
#                            bottom=0.0,
#                            top=1.0)
#
#        sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
#                                 color_process=lambda s: "#0000007D",
#                                 linewidth_data=lambda s: 2.0)
#        sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
#                                 color_process=lambda s: "red",
#                                 display_strokes=[s_1.id, s_2.id],
#                                 linewidth_data=lambda s: 3.0)
#        vanishing_line_plot = np.array([p_1_vp, vp])
#        ax.plot(vanishing_line_plot[:, 0],
#                vanishing_line_plot[:, 1], c="g")
#
#        ax.set_xlim(0, sketch.width)
#        ax.set_ylim(sketch.height, 0)
#        ax.axis("equal")
#        ax.axis("off")
#        plt.show()

        if vanishing_line.intersects_buffer(s_2, s_1.acc_radius):
            intersections = vanishing_line.get_intersection_fuzzy(s_2, s_1.acc_radius)
            #print(intersections)
            intersections = np.array([inter for line in intersections for inter in line])
            #print(intersections)
            # get intersections with the closest arc parameter
            best_arc_param = 2.0
            best_inter_id = -1
            for inter_id, inter in enumerate(intersections):
                arc_param = s_2.linestring.project(inter)
                #if abs(t - arc_param) < abs(t - best_arc_param):
                if ( abs(t - arc_param) < abs(t - best_arc_param) or
                        abs(1.0 - t - arc_param) < abs(1.0 - t - best_arc_param) )  :
                    best_arc_param = arc_param
                    best_inter_id = inter_id
            s_2_arc_params.append(best_arc_param)
            p_2 = intersections[best_inter_id]
            #print(p_2)
            point_correspondences.append([p_1, p_2])
    #print(len(point_correspondences))
    if len(point_correspondences) < 3:
        return [[], []]
    point_correspondences = np.array(point_correspondences)
    # resample_second_curve
    s_2_point_samples = []
    #for t in np.arange(np.min(s_2_arc_params), np.max(s_2_arc_params)+step_size, step_size):
    s_2_min_arc_param = np.min(s_2_arc_params)
    s_2_max_arc_param = np.max(s_2_arc_params)
#    print(len(point_correspondences))
#    s_2_step_size = (s_2_max_arc_param - s_2_min_arc_param)/(len(point_correspondences)+1)
#    print(s_2_min_arc_param)
#    print(s_2_max_arc_param)
#    print(s_2_step_size)
#    print(np.arange(s_2_min_arc_param, s_2_max_arc_param, s_2_step_size))
    #for t in np.arange(s_2_min_arc_param, s_2_max_arc_param, s_2_step_size):
    for t in np.linspace(s_2_min_arc_param, s_2_max_arc_param, len(point_correspondences)):
        p_2 = np.array(s_2.eval_point(t).coords)
        s_2_point_samples.append(p_2)
    s_2_point_samples = np.array(s_2_point_samples)
    first_corr_line = Stroke([])
    first_corr_line.from_array([point_correspondences[0][0], s_2_point_samples[0]])
    snd_corr_line = Stroke([])
    snd_corr_line.from_array([point_correspondences[-1][0], s_2_point_samples[0]])
    vanishing_line = Stroke([])
    vanishing_line.from_array([point_correspondences[0][0], vp])
    if first_corr_line.get_line_angle(vanishing_line) < snd_corr_line.get_line_angle(vanishing_line):
        point_correspondences[:, 1] = s_2_point_samples
    else:
        point_correspondences[:, 1] = np.flip(s_2_point_samples, axis=0)


    if s_1.id == 48 and s_2.id == 36:

        fig, ax = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)

        sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                                 color_process=lambda s: "#0000007D",
                                 linewidth_data=lambda s: 2.0)
        sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                                 color_process=lambda s: "red",
                                 display_strokes=[s_1.id, s_2.id],
                                 linewidth_data=lambda s: 3.0)
        ax.scatter(point_correspondences.reshape(-1, 2)[:, 0],
                   point_correspondences.reshape(-1, 2)[:, 1], c="b")
        for l in point_correspondences:
            ax.plot(l[:, 0], l[:, 1], c="g")

        ax.set_xlim(0, sketch.width)
        ax.set_ylim(sketch.height, 0)
        ax.axis("equal")
        ax.axis("off")
        plt.show()

    # 3d reconstruction of point correspondences

    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
    #for p1, p2 in point_correspondences:
    #    p1_rec, p2_rec = reconstruct_two_points(p1, p2, refl_mat, camera)
    #    print(p1_rec)
    #    print(p2_rec)
    point_correspondences_reconstructed = np.array([reconstruct_two_points(p1, p2, refl_mat, camera)
                                                    for p1, p2 in point_correspondences])
    s_1_reconstructed = point_correspondences_reconstructed[:, 0]
    s_2_reconstructed = point_correspondences_reconstructed[:, 1]
    s_2_plane = Plane.best_fit(s_2_reconstructed)
    s_2_projected = np.array([s_2_plane.project_point(p) for p in s_2_reconstructed])
    s_1_projected = np.array(tools_3d.apply_hom_transform_to_points(s_2_projected, refl_mat))
    #ps.init()
#    ps.register_point_cloud("point_correspondences_reconstructed",
#                            points=point_correspondences_reconstructed.reshape(-1, 3))
#    ps.register_curve_network("s_1_reconstructed", nodes=s_1_reconstructed,
#                              edges=np.array([[i, i+1] for i in range(len(s_1_reconstructed)-1)]))
#    ps.register_curve_network("s_1_projected", nodes=s_1_projected,
#                              edges=np.array([[i, i+1] for i in range(len(s_1_projected)-1)]))
#    ps.register_curve_network("s_2_reconstructed", nodes=s_2_reconstructed,
#                              edges=np.array([[i, i+1] for i in range(len(s_2_reconstructed)-1)]))
#    ps.register_curve_network("s_2_projected", nodes=s_2_projected,
#                              edges=np.array([[i, i+1] for i in range(len(s_2_projected)-1)]))
#    ps.show()

    return [s_1_projected, s_2_projected]

# returns 2 arrays of shape (2, 3), i.e., two 3d lines
def reconstruct_self_symmetric_stroke(s_1, sym_plane_point, sym_plane_normal, camera):

    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
    reconstructed_s_1 = reconstruct_two_points(s_1.points_list[0].coords, s_1.points_list[-1].coords, refl_mat, camera)
    return [reconstructed_s_1, reconstructed_s_1]

# returns 2 arrays of shape (2, 3), i.e., two 3d lines
def reconstruct_symmetric_strokes_straight(s_1, s_2, sym_plane_point, sym_plane_normal, camera):

    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
    #print(refl_mat)
    stroke_triangle = np.array([camera.cam_pos,
                                np.array(camera.lift_point(s_1.points_list[0].coords, 20.0)),
                                np.array(camera.lift_point(s_1.points_list[-1].coords, 20.0))])
    #stroke_triangle = np.array([camera.cam_pos,
    #                            np.array(camera.lift_point(s_1.points_list[0].coords, 1.0)),
    #                            np.array(camera.lift_point(s_1.points_list[-1].coords, 1.0))])
    other_stroke_triangle = np.array([camera.cam_pos,
                                      np.array(camera.lift_point(s_2.points_list[0].coords, 20.0)),
                                      np.array(camera.lift_point(s_2.points_list[-1].coords, 20.0))])
    #refl_mat = tools_3d.get_reflection_mat(np.array([0, 0, 0], dtype=float), np.array([0, 0, 1], dtype=float))
    #print(refl_mat)
    reflected_stroke_triangle = tools_3d.apply_hom_transform_to_points(stroke_triangle, refl_mat)
    #print(reflected_stroke_triangle)
    # intersection between both triangles
    first_triangle_point = reflected_stroke_triangle[0]
    first_triangle_normal = np.cross(reflected_stroke_triangle[1] - reflected_stroke_triangle[0],
                                     reflected_stroke_triangle[2] - reflected_stroke_triangle[0])
    first_triangle_normal /= np.linalg.norm(first_triangle_normal)
    first_triangle_plane_equation = tools_3d.plane_point_normal_to_equation(
        first_triangle_point, first_triangle_normal)
    snd_triangle_point = stroke_triangle[0]
    snd_triangle_normal = np.cross(other_stroke_triangle[1] - other_stroke_triangle[0],
                                   other_stroke_triangle[2] - other_stroke_triangle[0])
    snd_triangle_normal /= np.linalg.norm(snd_triangle_normal)
    snd_triangle_plane_equation = tools_3d.plane_point_normal_to_equation(
        snd_triangle_point, snd_triangle_normal)

    plane_inter_point, plane_inter_dir = tools_3d.plane_plane_intersection(
        first_triangle_plane_equation, snd_triangle_plane_equation)
    if s_1.id == 31 and s_2.id == 30:
        print(plane_inter_dir)
    inter_line = Plane(first_triangle_point, first_triangle_normal).intersect_plane(
        Plane(snd_triangle_point, snd_triangle_normal))
    plane_inter_point = inter_line.point
    plane_inter_dir = inter_line.vector
    if s_1.id == 31 and s_2.id == 30:
        print(plane_inter_dir)
    line_a = plane_inter_point
    line_b = plane_inter_point + 0.1*plane_inter_dir
    reflected_line = tools_3d.apply_hom_transform_to_points(np.array([line_a, line_b]), refl_mat)
    reflected_line_dir = reflected_line[-1] - reflected_line[0]
    reflected_line_dir /= np.linalg.norm(reflected_line_dir)
    inter_line = np.array(camera.lift_polyline_close_to_line([s_2.points_list[0].coords,
                                                              s_2.points_list[-1].coords],
                                                             line_a, plane_inter_dir))
    final_line = np.array(camera.lift_polyline_close_to_line([s_1.points_list[0].coords,
                                                              s_1.points_list[-1].coords],
                                                             reflected_line[0], reflected_line_dir))
    return final_line, inter_line

# s is a pystroke.Stroke
# intersections is a list of pystroke.EdgeIntersection
def get_self_symmetry_candidates(s, intersections, sketch, VERBOSE=False):

    #print([inter.inter_id for inter in intersections])
    # only do combinations of intersections between intersections at endpoints
    # of stroke
    inter_params = [np.array(inter.mid_inter_param)[np.array(inter.stroke_ids) == s.id][0]
                    for inter in intersections]
    inter_other_stroke_ids = [np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s.id][0]
                              for inter in intersections]
    if VERBOSE:
        print(inter_params)
    inter_params_middle = 0.5
    if len(inter_params) > 1:
        inter_params_middle = (np.max(inter_params) + np.min(inter_params))/2.0
    if VERBOSE:
        print(inter_params_middle)
    #start_points = [s.points_list[0].coords]
    #start_points = []
    start_points = [inter.inter_coords
                    for inter_id, inter in enumerate(intersections)
                    if inter_params[inter_id] < inter_params_middle - 0.001]
    start_points_other_stroke_ids = [inter_other_stroke_ids[inter_id]
                                     for inter_id, inter in enumerate(intersections)
                                     if inter_params[inter_id] < inter_params_middle - 0.001]
    #end_points = [s.points_list[-1].coords]
    #end_points = []
    end_points = [inter.inter_coords
                  for inter_id, inter in enumerate(intersections)
                  if inter_params[inter_id] > inter_params_middle + 0.001]
    end_points_other_stroke_ids = [inter_other_stroke_ids[inter_id]
                                   for inter_id, inter in enumerate(intersections)
                                   if inter_params[inter_id] > inter_params_middle + 0.001]
#    if len(start_points) == 0:
#        start_points = [s.points_list[0].coords]
#        start_points_other_stroke_ids = [-1]
#    if len(end_points) == 0:
#        end_points = [s.points_list[-1].coords]
#        end_points_other_stroke_ids = [-1]
    if VERBOSE:
        print("stroke_ids")
        print(start_points_other_stroke_ids)
        print(end_points_other_stroke_ids)
    candidate_tuples = [[p_start, p_end]
                        for p_start_id, p_start in enumerate(start_points)
                        for p_end_id, p_end in enumerate(end_points)
                        if (start_points_other_stroke_ids[p_start_id] == -1 or \
                        end_points_other_stroke_ids[p_end_id] == -1 or \
                        sketch.strokes[start_points_other_stroke_ids[p_start_id]].axis_label == sketch.strokes[end_points_other_stroke_ids[p_end_id]].axis_label)
                        and np.linalg.norm(np.array(p_start) - np.array(p_end)) > s.acc_radius]
    candidate_tuple_s_ids = [[start_points_other_stroke_ids[p_start_id], end_points_other_stroke_ids[p_end_id]]
                             for p_start_id, p_start in enumerate(start_points)
                             for p_end_id, p_end in enumerate(end_points)
                             if (start_points_other_stroke_ids[p_start_id] == -1 or \
                                 end_points_other_stroke_ids[p_end_id] == -1 or \
                                 sketch.strokes[start_points_other_stroke_ids[p_start_id]].axis_label == sketch.strokes[end_points_other_stroke_ids[p_end_id]].axis_label)
                             and np.linalg.norm(np.array(p_start) - np.array(p_end)) > s.acc_radius]
    if VERBOSE:
        print(candidate_tuple_s_ids)
        #exit()

    return candidate_tuples, candidate_tuple_s_ids

# candidate lines is a list of ordered tuples of points
def get_line_coverages_simple(intersections_3d, sketch, extreme_distances):
    line_coverages = [[] for i in range(len(sketch.strokes))]

    for vec_id, inter in enumerate(intersections_3d):
        if inter.is_parallel:
            continue
        sketch_inter = sketch.intersection_graph.get_intersections([inter.inter_id])[0]
        weight_0 = sketch_inter.mid_inter_param[0]
        weight_1 = sketch_inter.mid_inter_param[1]
        dist_0 = extreme_distances[inter.stroke_ids[0]][1] - extreme_distances[inter.stroke_ids[0]][0]
        dist_1 = extreme_distances[inter.stroke_ids[1]][1] - extreme_distances[inter.stroke_ids[1]][0]
        weight_0 = np.clip(weight_0, extreme_distances[inter.stroke_ids[0]][0], extreme_distances[inter.stroke_ids[0]][1])
        weight_1 = np.clip(weight_1, extreme_distances[inter.stroke_ids[1]][0], extreme_distances[inter.stroke_ids[1]][1])
        weight_0 -= extreme_distances[inter.stroke_ids[0]][0]
        weight_1 -= extreme_distances[inter.stroke_ids[1]][0]

        if dist_0 > 0.0:
            weight_0 /= dist_0
        if dist_1 > 0.0:
            weight_1 /= dist_1

        line_coverages[inter.stroke_ids[0]].append(LineCoverage(
            inter_id=inter.inter_id,
            stroke_ids=inter.stroke_ids,
            weight=weight_0))
        line_coverages[inter.stroke_ids[1]].append(LineCoverage(
            inter_id=inter.inter_id,
            stroke_ids=inter.stroke_ids,
            weight=weight_1))
        #if 5 in inter.stroke_ids:
        #    print(inter.stroke_ids, weight_0, weight_1, inter.inter_id)
    return line_coverages

def fetch_intersections_from_proxies(s_id, sketch, per_stroke_proxies, camera):
    inters = [inter for inter in sketch.intersection_graph.get_intersections_by_stroke_id(s_id)
              if sketch.strokes[inter.stroke_ids[np.argwhere(np.array(inter.stroke_ids) != s_id).flatten()[0]]].axis_label != 5]
    inters_straight_line_ids = [inter.stroke_ids[np.argwhere(np.array(inter.stroke_ids) != s_id).flatten()[0]]
                                for inter in inters]
    inters_3d = []
    for inter_id, inter in enumerate(inters):
        straight_line_id = inters_straight_line_ids[inter_id]
        for proxy in per_stroke_proxies[straight_line_id]:
            proxy_vec = proxy[-1] - proxy[0]
            proxy_vec /= np.linalg.norm(proxy_vec)
            inter_3d = camera.lift_point_close_to_line(inter.inter_coords, proxy[0], proxy_vec, return_axis_point=True)[1]
            inters_3d.append(inter_3d)
    return np.array(inters_3d)

def filter_non_reflective_points(curve, points, refl_mat, camera, IS_ELLIPSE=False):
    reflected_points = tools_3d.apply_hom_transform_to_points(points, refl_mat)
    projected_points = camera.project_polyline(reflected_points)
    curve_linestring = LineString(curve)
    acc_radius = 0.1*curve_linestring.length
    if IS_ELLIPSE:
        acc_radius = 0.05*curve_linestring.length
    close_point_ids = [True if curve_linestring.distance(Point(p)) < acc_radius else False
                       for p in projected_points]
    return points[close_point_ids]

def reconstruct_symmetric_curves_planar(s_id_1, s_id_2, axis_id,
                                        sketch, camera,
                                        per_stroke_proxies,
                                        minimal_foreshortening=True,
                                        VERBOSE=False):
    s_1_pts = np.array([p.coords for p in sketch.strokes[s_id_1].points_list])
    s_2_pts = np.array([p.coords for p in sketch.strokes[s_id_2].points_list])
    dist_vec = np.array(camera.get_camera_point_ray(s_1_pts[0])[1])
    cam_pos = np.array(camera.cam_pos)
    # opt_vars: plane_dist plane_n
    #opt_vars = np.ones(4, dtype=float)
    opt_vars = np.ones(1, dtype=float)
    #opt_vars[0] = 1.0
    #if s_id_1 == s_id_2:
    #    if axis_id ==
    #    opt_vars[]

    if VERBOSE:
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        sketch.display_strokes_2(fig=fig, ax=axes,
                                          color_process=lambda s: "#0000007D",
                                          linewidth_data=lambda s: 1.0, norm_global=True)
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "red",
                                 display_strokes=[s_id_1, s_id_2],
                                 linewidth_data=lambda s: 2.0, norm_global=True)
        axes.set_xlim(0, sketch.width)
        axes.set_ylim(sketch.height, 0)
        axes.axis("equal")
        axes.axis("off")
        plt.show()


    # x: [plane_dist, plane_n(3)]
    def opt_func(x, tmp_plane_n):

        # FULL CURVE
        plane_p = cam_pos + x[0]*dist_vec
        if np.isclose(np.linalg.norm(x[1:]), 0.0):
            return 100.0
        #x[1:] /= np.linalg.norm(x[1:])
        #c_1_full_curve = np.array(camera.lift_polyline_to_plane(s_1_pts, plane_p, x[1:]))
        c_1_full_curve = np.array(camera.lift_polyline_to_plane(s_1_pts, plane_p, tmp_plane_n))
        pts = Points(c_1_full_curve)
        if pts.are_collinear():
            return 100.0
        u, v = tools_3d.get_basis_for_planar_point_cloud(c_1_full_curve)
        ecc_1 = tools_3d.get_ellipse_eccentricity(c_1_full_curve, u, v)
        # reflect c_1
        #c_1_full_curve[:, axis_id] *= -1
        #c_1_refl_proj = np.array(camera.project_polyline(c_1_full_curve))
        c_1_refl = c_1_full_curve.copy()
        c_1_refl[:, axis_id] *= -1
        plane_2 = Plane.best_fit(c_1_refl)
        c_2_full_curve = np.array(camera.lift_polyline_to_plane(s_2_pts, plane_2.point, plane_2.normal))
        u, v = tools_3d.get_basis_for_planar_point_cloud(c_2_full_curve)
        ecc_2 = tools_3d.get_ellipse_eccentricity(c_2_full_curve, u, v)

        # OBJECTIVE FUNCTION
        dist = hausdorff_distance(c_1_refl, c_2_full_curve)
        refl_score = 1.0 - np.exp(-dist)
        #ecc_score = 1.0 - 0.5*(np.exp(-ecc_1)+np.exp(-ecc_2))
        #ecc_score = 1.0 - np.exp(-ecc_1)
        ecc_score = 1.0 - np.exp(-max(ecc_1, ecc_2))
        weight = 0.5
        obj = weight*refl_score + (1-weight)*ecc_score

        return obj

    bounds = Bounds([0, 0, 0, 0],
                    [10, 1, 1, 1])
    solutions = []
    solutions_fun = []
    for i in range(3):
        #opt_vars[1:] = 0
        #opt_vars[1+i] = 1.0
        plane_n = np.zeros(3)
        plane_n[i] = 1.0
        res = minimize(opt_func, opt_vars, method="SLSQP", bounds=bounds, args=plane_n)
        solutions.append(res.x)
        solutions_fun.append(res.fun)
    print(solutions_fun)
    res = solutions[np.argmin(solutions_fun)]
    plane_p = cam_pos + res[0]*dist_vec
    #plane_n = res[1:]
    plane_n = np.zeros(3)
    plane_n[np.argmin(solutions_fun)] = 1.0
    if np.isclose(np.linalg.norm(plane_n), 0.0):
        return [], []
    plane_n /= np.linalg.norm(plane_n)
    c_1 = np.array(camera.lift_polyline_to_plane(s_1_pts, plane_p, plane_n))
    c_1_refl = c_1.copy()
    c_1_refl[:, axis_id] *= -1
    plane_2 = Plane.best_fit(c_1_refl)
    c_2 = np.array(camera.lift_polyline_to_plane(s_2_pts, plane_2.point, plane_2.normal))
    if VERBOSE:
        ps.remove_all_structures()
        for s_id, s in enumerate(per_stroke_proxies):
            for p_id, p in enumerate(s):
                if len(p) > 0:
                    ps.register_curve_network(str(s_id)+"_"+str(p_id),
                                              nodes=p,
                                              edges=np.array([[i, i+1] for i in range(len(p)-1)]))
        ps.register_curve_network("c_1", nodes=c_1,
                                  edges=np.array([[i, i+1] for i in range(len(c_1)-1)]))
        ps.register_curve_network("c_2", nodes=c_2,
                                  edges=np.array([[i, i+1] for i in range(len(c_2)-1)]))
        ps.show()
    return c_1, c_2

def reconstruct_symmetric_curves_bezier_partial(s_id_1, s_id_2, axis_id, per_stroke_proxies,
                                                sketch, camera, device=None, minimal_foreshortening=True,
                                                VERBOSE=False):
    c_1_bezier = sketch.strokes[s_id_1].bezier
    c_2_bezier = sketch.strokes[s_id_2].bezier
    nb_c_1_bezier_ctrl_pts = len(c_1_bezier)
    nb_c_2_bezier_ctrl_pts = len(c_2_bezier)
    c_1_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_1_bezier])
    for i in range(nb_c_1_bezier_ctrl_pts):
        c_1_vecs[i] /= np.linalg.norm(c_1_vecs[i])

    c_2_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_2_bezier])
    for i in range(nb_c_2_bezier_ctrl_pts):
        c_2_vecs[i] /= np.linalg.norm(c_2_vecs[i])
    cam_pos = np.array(camera.cam_pos)
    opt_vars = np.ones(nb_c_1_bezier_ctrl_pts+nb_c_2_bezier_ctrl_pts+4, dtype=float)
    opt_vars[8] = 0.0
    opt_vars[10] = 0.0

    #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
    #    print(c_1_vecs)
    #    print(c_2_vecs)
    #    print(cam_pos)
    #    exit()

    # x: [c_1_bezier_depths, c_2_bezier_depths, a_1, b_1, a_2, b_2]
    def opt_func(x, c_2_vecs_local):
        c_1_ctrl_pts = cam_pos + x[:4].reshape(-1, 1)*c_1_vecs
        c_2_ctrl_pts = cam_pos + x[4:8].reshape(-1, 1)*c_2_vecs_local
        # FULL CURVE
        c_1_full_curve = cubic_bezier_eval_paralllel(c_1_ctrl_pts, 0.0, 1.0, CURVE_SAMPLE_SIZE)
        c_2_full_curve = cubic_bezier_eval_paralllel(c_2_ctrl_pts, 0.0, 1.0, CURVE_SAMPLE_SIZE)
        #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
        #    print(c_1_full_curve)
        #    print(c_2_full_curve)
        #    exit()
        c_1_len = np.sum(np.linalg.norm(c_1_full_curve[1:] - c_1_full_curve[:len(c_1_full_curve)-1], axis=1))
        c_2_len = np.sum(np.linalg.norm(c_2_full_curve[1:] - c_2_full_curve[:len(c_2_full_curve)-1], axis=1))
        # PARTIAL CURVE
        c_1_partial_curve = equi_resample_parallel(c_1_ctrl_pts, x[8], x[9], CURVE_SAMPLE_SIZE)
        if len(c_1_partial_curve) == 0:
            c_1_partial_curve = np.array([bezier.q(c_1_ctrl_pts, x[8])])
        c_1_partial_curve[:, axis_id] *= -1
        c_2_partial_curve = equi_resample_parallel(c_2_ctrl_pts, x[10], x[11], CURVE_SAMPLE_SIZE)
        if len(c_2_partial_curve) == 0:
            c_2_partial_curve = np.array([bezier.q(c_2_ctrl_pts, x[10])])
        c_1_partial_len = np.sum(np.linalg.norm(c_1_partial_curve[1:] - c_1_partial_curve[:(len(c_1_partial_curve)-1)], axis=1))
        c_2_partial_len = np.sum(np.linalg.norm(c_2_partial_curve[1:] - c_2_partial_curve[:(len(c_2_partial_curve)-1)], axis=1))
        # OBJECTIVE FUNCTION
        length = 1.0 - np.exp(-min(c_1_len - c_1_partial_len, c_2_len - c_2_partial_len))
        #dist = 1.0 - np.exp(-np.max(np.linalg.norm(c_1_partial_curve - c_2_partial_curve, axis=-1)))
        dist = 1.0 - np.exp(-np.mean(np.linalg.norm(c_1_partial_curve - c_2_partial_curve, axis=-1)))
        weight = 0.85
        obj_func = weight*dist + (1-weight)*length
        #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
        #    print("%.30f"%obj_func)
        #    exit()

        return obj_func

    bounds = Bounds([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [10, 10, 10, 10, 10, 10, 10, 10, 1, 1, 1, 1])
    solutions = []
    solutions_fun = []
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     (cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     -(cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
    #eq_cons = {"type": "eq",
    #             "fun": lambda x: np.array([
    #                 (x[1]*c_1_vecs[1] - x[0]*c_1_vecs[0])*(x[1]*c_1_vecs[1] - x[0]*c_1_vecs[0]) +
    #                 (x[2]*c_1_vecs[2] - x[1]*c_1_vecs[1])*(x[2]*c_1_vecs[2] - x[1]*c_1_vecs[1]) +
    #                 (x[3]*c_1_vecs[3] - x[2]*c_1_vecs[2])*(x[3]*c_1_vecs[3] - x[2]*c_1_vecs[2]) -
    #                 (x[5]*c_1_vecs[5] - x[4]*c_1_vecs[4])*(x[5]*c_1_vecs[5] - x[4]*c_1_vecs[4]) +
    #                 (x[6]*c_1_vecs[6] - x[5]*c_1_vecs[5])*(x[6]*c_1_vecs[6] - x[5]*c_1_vecs[5]) +
    #                 (x[7]*c_1_vecs[7] - x[6]*c_1_vecs[6])*(x[7]*c_1_vecs[7] - x[6]*c_1_vecs[6])
    #           ])
    #           }
#    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
#                   constraints=[ineq_cons], options={"ftol":1e-30, "maxiter":1000}, tol=1e-30)
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
    #    print(res.x)
    #    print(res.fun)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     -(cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     (cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
#    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
#                   constraints=[ineq_cons], options={"ftol":1e-30, "maxiter":1000}, tol=1e-30)
    #print(res)
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    #print(res)
    #exit()
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
    #    print(res)
    #    print(res.x)
    #    print(res.fun)
    #    exit()

    c_2_vecs = np.flip(c_2_vecs, axis=0)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     (cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     -(cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
#    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
#                   constraints=[ineq_cons], options={"ftol":1e-30, "maxiter":1000}, tol=1e-30)
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
    #    print(res.x)
    #    print(res.fun)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     -(cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     (cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
#    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
#                   constraints=[ineq_cons], options={"ftol":1e-30, "maxiter":1000}, tol=1e-30)
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    #if s_id_1 == 25 and s_id_2 == 8 and axis_id == 0:
    #    print(res.x)
    #    print(res.fun)
    #    exit()
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    min_solution = np.argmin(solutions_fun)
    #print(solutions_fun)
    #exit()
    res = solutions[min_solution]
    a_1 = res[8]
    b_1 = res[9]
    a_2 = res[10]
    b_2 = res[11]
    #if np.isclose(a_1, b_1) or np.isclose(a_2, b_2):
    if np.abs(a_1 - b_1) <= 0.1 or np.abs(a_2 - b_2) <= 0.1:
        print("REJECTED: intervals too close")
        return [], [], [], []
    c_2_vecs = np.flip(c_2_vecs, axis=0)
    if min_solution > 1:
        res[4:8] = np.flip(res[4:8])
        tmp = 1 - b_2
        b_2 = 1 - a_2
        a_2 = tmp
    assert a_2 <= b_2, "incorrect interval: "+str(a_2)+", "+str(b_2)+" min_solution: "+str(min_solution)+" res:"+str(res)
    # sanity check
    c_1_ctrl_pts = cam_pos + res[:4].reshape(-1, 1)*c_1_vecs
    c_2_ctrl_pts = cam_pos + res[4:8].reshape(-1, 1)*c_2_vecs
    invalid_c_1_ctrl_pts = np.all(np.isclose(c_1_ctrl_pts[0], c_1_ctrl_pts[1])) or \
                           np.all(np.isclose(c_1_ctrl_pts[1], c_1_ctrl_pts[2])) or \
                           np.all(np.isclose(c_1_ctrl_pts[2], c_1_ctrl_pts[3]))
    invalid_c_2_ctrl_pts = np.all(np.isclose(c_2_ctrl_pts[0], c_2_ctrl_pts[1])) or \
                           np.all(np.isclose(c_2_ctrl_pts[1], c_2_ctrl_pts[2])) or \
                           np.all(np.isclose(c_2_ctrl_pts[2], c_2_ctrl_pts[3]))
    if invalid_c_1_ctrl_pts or invalid_c_2_ctrl_pts:
        print("REJECTED: invalid ctrl_pts")
        return [], [], [], []
    c_1_sampled_pts = equi_resample_parallel(c_1_ctrl_pts, a_1, b_1, CURVE_SAMPLE_SIZE)
    c_2_sampled_pts = equi_resample_parallel(c_2_ctrl_pts, a_2, b_2, CURVE_SAMPLE_SIZE)
    # reflect c_1
    c_1_sampled_pts_reflected = c_1_sampled_pts.copy()
    c_1_sampled_pts_reflected[:, axis_id] *= -1
    c_1_len = tools_3d.line_3d_length(c_1_sampled_pts)
    c_2_len = tools_3d.line_3d_length(c_2_sampled_pts)
    c_1_acc_radius = 0.2*c_1_len
    c_2_acc_radius = 0.2*c_2_len
    c_1_interval_length = np.abs(res[9] - res[8])
    c_2_interval_length = np.abs(res[11] - res[10])
    #dist = np.max(np.linalg.norm(c_1_sampled_pts_reflected - c_2_sampled_pts, axis=-1))
    dist = np.mean(np.linalg.norm(c_1_sampled_pts_reflected - c_2_sampled_pts, axis=-1))
    #dist = chamfer_distance(c_1_sampled_pts_reflected, c_2_sampled_pts, direction="bi")
    if min_solution > 1:
        dist = np.max(np.linalg.norm(c_1_sampled_pts_reflected - np.flip(c_2_sampled_pts, axis=0), axis=-1))

    if VERBOSE:
        if dist > np.max([c_1_acc_radius, c_2_acc_radius]) or c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
            print("REJECTED")
            if dist > np.max([c_1_acc_radius, c_2_acc_radius]):
                print("acc_radii: ", c_1_acc_radius, c_2_acc_radius)
                print("dist: ", dist)
            if c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
                print("interval lengths: ", c_1_interval_length, c_2_interval_length)
        print("intervals", s_id_1, a_1, b_1, s_id_2, a_2, b_2)

        c_1_inters_3d = fetch_intersections_from_proxies(s_id_1, sketch,
                                                         per_stroke_proxies, camera)
        c_1_inters_3d_old = c_1_inters_3d.copy()
        c_2_inters_3d = fetch_intersections_from_proxies(s_id_2, sketch,
                                                         per_stroke_proxies, camera)
        c_2_inters_3d_old = c_2_inters_3d.copy()
        reflected_points = c_1_inters_3d.copy()
        reflected_points[:, axis_id] *= -1
        c_1_inters_old = np.array(camera.project_polyline(c_1_inters_3d))
        c_2_inters_old = np.array(camera.project_polyline(c_2_inters_3d))
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[axis_id] = 1.0
        refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
        c_1_points = np.array([p.coords for p in sketch.strokes[s_id_1].points_list])
        c_2_points = np.array([p.coords for p in sketch.strokes[s_id_2].points_list])
        c_1_inters_3d = filter_non_reflective_points(c_2_points, c_1_inters_3d,
                                                     refl_mat, camera, sketch.strokes[s_id_1].is_ellipse())
        c_1_inters_3d_new = c_1_inters_3d.copy()
        c_2_inters_3d = filter_non_reflective_points(c_1_points, c_2_inters_3d,
                                                     refl_mat, camera, sketch.strokes[s_id_2].is_ellipse())
        c_2_inters_3d_new = c_2_inters_3d.copy()
        c_1_inters_new = np.array(camera.project_polyline(c_1_inters_3d))
        c_2_inters_new = np.array(camera.project_polyline(c_2_inters_3d))

        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 color_process=lambda s: "#0000007D",
                                 linewidth_data=lambda s: 1.0, norm_global=True)
        sketch.display_strokes_2(fig=fig, ax=axes,
                                 #color_process=lambda s: "black" if s.axis_label != 5 else "red",
                                 color_process=lambda s: "red" if s.axis_label == 5 else "#00000000",
                                 linewidth_data=lambda s: 4.0, norm_global=True,
                                 display_strokes=[s_id_1, s_id_2])
        #axes.scatter(c_1_inters_old[:, 0], c_1_inters_old[:, 1], c="blue")
        #axes.scatter(c_1_inters_new[:, 0], c_1_inters_new[:, 1], c="red", zorder=2)
        #axes.scatter(c_2_inters_old[:, 0], c_2_inters_old[:, 1], c="blue")
        #axes.scatter(c_2_inters_new[:, 0], c_2_inters_new[:, 1], c="red", zorder=2)
        axes.set_xlim(0, sketch.width)
        axes.set_ylim(sketch.height, 0)
        axes.axis("equal")
        axes.axis("off")
        plt.show()


        c_1_ctrl_pts = cam_pos + res[:4].reshape(-1, 1)*c_1_vecs
        c_2_ctrl_pts = cam_pos + res[4:8].reshape(-1, 1)*c_2_vecs
        c_1_sampled_pts = []
        c_2_sampled_pts = []
        for i in range(CURVE_SAMPLE_SIZE):
            t = a_1 + i*(b_1 - a_1)/9
            c_1_sampled_pts.append(bezier.q(c_1_ctrl_pts, t))

            t = a_2 + i*(b_2 - a_2)/9
            c_2_sampled_pts.append(bezier.q(c_2_ctrl_pts, t))
        c_1_sampled_pts = np.array(c_1_sampled_pts)
        c_2_sampled_pts = np.array(c_2_sampled_pts)
        # reflect c_1
        c_1_sampled_pts_reflected = c_1_sampled_pts.copy()
        c_1_sampled_pts_reflected[:, axis_id] *= -1

        c_1_full_curve = []
        c_2_full_curve = []
        for t in np.linspace(0.0, 1.0, CURVE_SAMPLE_SIZE):
            c_1_full_curve.append(bezier.q(c_1_ctrl_pts, t))
            c_2_full_curve.append(bezier.q(c_2_ctrl_pts, t))
        c_1_full_len = tools_3d.line_3d_length(c_1_full_curve)
        c_2_full_len = tools_3d.line_3d_length(c_2_full_curve)

        resample_time = process_time()
        c_1_equi_resampled_pts = equi_resample(c_1_ctrl_pts, a_1, b_1, CURVE_SAMPLE_SIZE)
        print("resample_time", process_time()-resample_time)
        print(c_1_equi_resampled_pts)
        c_2_equi_resampled_pts = equi_resample(c_2_ctrl_pts, a_2, b_2, CURVE_SAMPLE_SIZE)
        resample_time = process_time()
        c_1_equi_resampled_pts = equi_resample_parallel(c_1_ctrl_pts, a_1, b_1, CURVE_SAMPLE_SIZE)
        print("resample_time_parallel", process_time()-resample_time)
        print(c_1_equi_resampled_pts)
        c_2_equi_resampled_pts = equi_resample_parallel(c_2_ctrl_pts, a_2, b_2, CURVE_SAMPLE_SIZE)

        equidist = min(c_1_len, c_2_len)/float(CURVE_SAMPLE_SIZE)
        c_1_equi_pts = []
        c_2_equi_pts = []
        for i in range(CURVE_SAMPLE_SIZE):
            t_1 = a_1 + i*equidist/c_1_full_len
            c_1_equi_pts.append(bezier.q(c_1_ctrl_pts, t_1))
            t_2 = a_2 + i*equidist/c_2_full_len
            c_2_equi_pts.append(bezier.q(c_2_ctrl_pts, t_2))
        if c_1_len > c_2_len:
            c_1_endpoint = np.array(bezier.q(c_1_ctrl_pts, b_1))
            c_1_equi_pts.append(c_1_endpoint)
        else:
            c_2_endpoint = np.array(bezier.q(c_2_ctrl_pts, b_2))
            c_2_equi_pts.append(c_2_endpoint)
        c_1_equi_pts = np.array(c_1_equi_pts)
        c_2_equi_pts = np.array(c_2_equi_pts)

        ps.remove_all_structures()
        proxies = []
        for s_id, s in enumerate(per_stroke_proxies):
            for p in s:
                proxies.append(p)
        ps.register_curve_network("proxies", nodes=np.array(proxies).reshape(-1, 3),
                                  edges=np.array([[2*i, 2*i+1] for i in range(int(len(proxies)))], dtype=np.int))
        ps.register_curve_network("c_1", nodes=c_1_sampled_pts,
                                  edges=np.array([[i, i+1] for i in range(int(len(c_1_sampled_pts))-1)], dtype=np.int))
        ps.register_curve_network("c_1_reflected", nodes=c_1_sampled_pts_reflected,
                                  edges=np.array([[i, i+1] for i in range(int(len(c_1_sampled_pts_reflected))-1)], dtype=np.int))
        #ps.register_point_cloud("c_1_inters_old", points=c_1_inters_3d_old)
        #ps.register_point_cloud("c_1_inters_new", points=c_1_inters_3d_new)
        #ps.register_point_cloud("c_2_inters_old", points=c_2_inters_3d_old)
        #ps.register_point_cloud("c_2_inters_new", points=c_2_inters_3d_new)
        ps.register_point_cloud("c_1_equi_pts", points=c_1_equi_resampled_pts)
        ps.register_point_cloud("c_2_equi_pts", points=c_2_equi_resampled_pts)
        ps.register_curve_network("c_2", nodes=c_2_sampled_pts,
                                  edges=np.array([[i, i+1] for i in range(int(len(c_2_sampled_pts))-1)], dtype=np.int))
        ps.show()

    if c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
    #if dist > np.max([c_1_acc_radius, c_2_acc_radius]) or c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
        print("REJECTED")
        if dist > np.max([c_1_acc_radius, c_2_acc_radius]):
            print("acc_radii: ", c_1_acc_radius, c_2_acc_radius)
            print("dist: ", dist)
        if c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
            print("interval lengths: ", c_1_interval_length, c_2_interval_length)
        return [], [], [], []

    assert a_1 <= b_1, "incorrect interval: "+str(a_1)+", "+str(b_1)
    assert a_2 <= b_2, "incorrect interval: "+str(a_2)+", "+str(b_2)
    return c_1_sampled_pts, c_2_sampled_pts, [a_1, b_1], [a_2, b_2]
    #sys.exit()

def get_midline_stroke_ids(selected_axes, global_candidate_correspondences,
                           per_axis_per_stroke_candidate_reconstructions,
                           sym_planes, same_axis_stroke_ids, sketch, camera,
                           accuracy_multiplier=2.0,
                           VERBOSE=False,
                           curve_epsilons=[]):
    all_midline_stroke_ids = []
    for corr in global_candidate_correspondences:
        if corr[0] == corr[1] or not sketch.strokes[corr[0]].axis_label in selected_axes:
            continue
        # project both 3D strokes on sym_plane
        s_1_projected = np.array([sym_planes[corr[4]].project_point(p) for p in corr[2]])
        s_2_projected = np.array([sym_planes[corr[4]].project_point(p) for p in corr[3]])
        s_1_projected_2d = np.array(camera.project_polyline(s_1_projected))
        s_2_projected_2d = np.array(camera.project_polyline(s_2_projected))
        mulitline = MultiLineString([s_1_projected_2d, s_2_projected_2d])
        tmp_max_acc_radius = accuracy_multiplier*max(sketch.strokes[corr[0]].acc_radius, sketch.strokes[corr[1]].acc_radius)
        # check for possible middle lines
        midline_stroke_ids = []
        #print(same_axis_stroke_ids[sketch.strokes[corr[0]].axis_label])
        for s_id in same_axis_stroke_ids[sketch.strokes[corr[0]].axis_label]:
            #print(s_id)
            if sketch.strokes[corr[0]].axis_label == 5 and \
                (sketch.strokes[s_id].is_ellipse() != sketch.strokes[corr[0]].is_ellipse()):
                continue

            s = sketch.strokes[s_id]
            inter_length = mulitline.buffer(tmp_max_acc_radius).intersection(s.linestring.linestring).length
            if inter_length/s.linestring.linestring.length > 0.75 or \
                    inter_length/mulitline.length > 0.75:
                if s.axis_label == 5 and s.is_ellipse() and sketch.strokes[s_id].closest_major_axis_deviation < 15.0:
                    if corr[4] != sketch.strokes[s_id].closest_major_axis_id:
                        continue
                midline_stroke_ids.append(s_id)

        if VERBOSE:
            fig, axes = plt.subplots(nrows=1, ncols=1)
            fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                                bottom=0.0,
                                top=1.0)
            sketch.display_strokes_2(fig=fig, ax=axes,
                                     color_process=lambda s: "#0000007D",
                                     linewidth_data=lambda s: 1.0, norm_global=True)
            sketch.display_strokes_2(fig=fig, ax=axes,
                                     color_process=lambda s: "blue",
                                     linewidth_data=lambda s: 4.0, norm_global=True,
                                     display_strokes=[corr[0], corr[1]])
            axes.plot(s_1_projected_2d[:, 0], s_1_projected_2d[:, 1], lw=tmp_max_acc_radius, zorder=0)
            axes.plot(s_2_projected_2d[:, 0], s_2_projected_2d[:, 1], lw=tmp_max_acc_radius, zorder=0)
            if len(midline_stroke_ids) > 0:
                sketch.display_strokes_2(fig=fig, ax=axes,
                                         color_process=lambda s: "green",
                                         linewidth_data=lambda s: 4.0, norm_global=True,
                                         display_strokes=midline_stroke_ids)
            axes.set_xlim(0, sketch.width)
            axes.set_ylim(sketch.height, 0)
            axes.axis("equal")
            axes.axis("off")
            plt.show()

        if len(midline_stroke_ids) == 0:
            continue
        if corr[0] in midline_stroke_ids or corr[1] in midline_stroke_ids:
            continue

        #if PLOT_DEBUG:
        #    fig, axes = plt.subplots(nrows=1, ncols=1)
        #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
        #                        bottom=0.0,
        #                        top=1.0)
        #    sketch.display_strokes_2(fig=fig, ax=axes,
        #                             color_process=lambda s: "#0000007D",
        #                             linewidth_data=lambda s: 1.0, norm_global=True)
        #    sketch.display_strokes_2(fig=fig, ax=axes,
        #                             color_process=lambda s: "blue",
        #                             linewidth_data=lambda s: 4.0, norm_global=True,
        #                             display_strokes=[corr[0], corr[1]])
        #    sketch.display_strokes_2(fig=fig, ax=axes,
        #                             color_process=lambda s: "green",
        #                             linewidth_data=lambda s: 4.0, norm_global=True,
        #                             display_strokes=midline_stroke_ids)
        #    axes.set_xlim(0, sketch.width)
        #    axes.set_ylim(sketch.height, 0)
        #    axes.axis("equal")
        #    axes.axis("off")
        #    plt.show()

        #print(len(midline_stroke_ids))
        plane_point = np.zeros(3)
        plane_normal = np.zeros(3)
        plane_normal[corr[4]] = 1.0
        #if len(midline_stroke_ids) > 1:
        #    continue
        all_midline_stroke_ids += midline_stroke_ids
        # create a self-symmetric correspondence for each midline stroke
        for midline_stroke_id in midline_stroke_ids:
            if sketch.strokes[midline_stroke_id].axis_label != 5:
                s_proj = np.array(camera.lift_polyline_to_plane([sketch.strokes[midline_stroke_id].points_list[0].coords,
                                                                 sketch.strokes[midline_stroke_id].points_list[-1].coords],
                                                                plane_point, plane_normal))
            else:
                if sketch.strokes[midline_stroke_id].is_ellipse():
                    s_proj = np.array(camera.lift_polyline_to_plane([p.coords
                                                                     for p in sketch.strokes[midline_stroke_id].points_list],
                                                                    plane_point, plane_normal))
                else:
                    c_bez = sketch.strokes[midline_stroke_id].bezier
                    c_points = []
                    for c_i in range(int(len(c_bez)/4)):
                        for t in np.linspace(0.0, 1.0, num=CURVE_SAMPLE_SIZE):
                        #for t in np.arange(0.0, 1.0, 0.1):
                            c_points.append(bezier.q(c_bez[4*c_i:4*(c_i+1)+1], t))
                    s_proj = np.array(camera.lift_polyline_to_plane(c_points, plane_point, plane_normal))

                    all_points = []
                    point_stroke_ids = []
                    #first_seg = np.array([p for p in corr[2]] + [p for p in corr[3]])
                    first_seg = np.array([p for p in corr[2]])
                    both_segs = [s_proj, first_seg]
                    for i, equi_seg in enumerate(both_segs):
                        for p in equi_seg:
                            all_points.append(p)
                            point_stroke_ids.append(i)
                    all_points = np.array(all_points)
                    sym_plane_point = np.zeros(3, dtype=np.float)
                    sym_plane_normal = np.zeros(3, dtype=np.float)
                    sym_plane_normal[corr[4]] = 1.0
                    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
                    #min_length = np.min([tools_3d.line_3d_length(corr[2+i]) for i in range(2)])
                    min_length = np.min([tools_3d.line_3d_length(both_segs[i]) for i in range(2)])

                    #symmetric_points = symmetry_tools.symmetric_point_cloud(all_points, refl_mat,
                    #                                                        threshold=0.05*bbox_diags[corr[4]],
                    #                                                        VERBOSE=True)
                    symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                             threshold=0.1*min_length,
                                                             VERBOSE=False)
                    sym_scores = [[] for l in range(2)]
                    for pt_id, sym_score in enumerate(symmetric_points):
                        s_id = point_stroke_ids[pt_id]
                        sym_scores[s_id].append(min(sym_score, 1.0))
                    for s_id, scores in enumerate(sym_scores):
                        sym_scores[s_id] = np.mean(scores)
                    #if midline_stroke_id == 27:
                    first_score = sym_scores[0]

                    all_points = []
                    point_stroke_ids = []
                    first_seg = np.array([p for p in corr[3]])
                    both_segs = [s_proj, first_seg]
                    for i, equi_seg in enumerate(both_segs):
                        for p in equi_seg:
                            all_points.append(p)
                            point_stroke_ids.append(i)
                    all_points = np.array(all_points)
                    sym_plane_point = np.zeros(3, dtype=np.float)
                    sym_plane_normal = np.zeros(3, dtype=np.float)
                    sym_plane_normal[corr[4]] = 1.0
                    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
                    #min_length = np.min([tools_3d.line_3d_length(corr[2+i]) for i in range(2)])
                    min_length = np.min([tools_3d.line_3d_length(both_segs[i]) for i in range(2)])

                    #symmetric_points = symmetry_tools.symmetric_point_cloud(all_points, refl_mat,
                    #                                                        threshold=0.05*bbox_diags[corr[4]],
                    #                                                        VERBOSE=True)
                    symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                             threshold=0.1*min_length,
                                                             VERBOSE=False)
                    sym_scores = [[] for l in range(2)]
                    for pt_id, sym_score in enumerate(symmetric_points):
                        s_id = point_stroke_ids[pt_id]
                        sym_scores[s_id].append(min(sym_score, 1.0))
                    for s_id, scores in enumerate(sym_scores):
                        sym_scores[s_id] = np.mean(scores)
                    snd_score = sym_scores[0]
                    #print(corr[0], corr[1], midline_stroke_id)
                    #print(first_score, snd_score)
                    #if midline_stroke_id == 26 or midline_stroke_id == 40:
                    #    print(corr[0], corr[1], midline_stroke_id)
                    #    print(first_score, snd_score)
                    if first_score < 0.8 or snd_score < 0.8:
                        continue

            new_corr = [midline_stroke_id, midline_stroke_id, s_proj, s_proj,
                        corr[4], -1, -1, corr[0], corr[1]]

            global_candidate_correspondences.append(new_corr)
            per_axis_per_stroke_candidate_reconstructions[corr[4]][midline_stroke_id].append(s_proj)
    return np.unique(all_midline_stroke_ids).tolist()

def get_min_var_coeffs(ctrl_pts):
    a_mat = np.array([[ctrl_pts[0][0], ctrl_pts[1][0], ctrl_pts[2][0]],
                      [ctrl_pts[0][1], ctrl_pts[1][1], ctrl_pts[2][1]],
                      [ctrl_pts[0][2], ctrl_pts[1][2], ctrl_pts[2][2]],
                      [1, 1, 1]])
    b_mat = np.array([ctrl_pts[3][0], ctrl_pts[3][1], ctrl_pts[3][2], 1])
    min_var_coeffs, _, _, _ = lstsq(a_mat, b_mat)
    return min_var_coeffs

def fit_full_bezier_lstsq(comb_t_interval_id_pt, b_vecs, cam_pos):

    # compute minimal variation coefficients
    ctrl_pts = cam_pos + b_vecs
    min_var_coeffs = get_min_var_coeffs(ctrl_pts)

    a_mat = np.zeros([3*len(comb_t_interval_id_pt)+3, 4])
    b_mat = np.zeros(3*len(comb_t_interval_id_pt)+3)
    # close to sampled pts
    for id, triplet in enumerate(comb_t_interval_id_pt):
        t, interval_id, pt = triplet
        b_coeff_0 = (1-t)*(1-t)*(1-t)
        b_coeff_1 = 3*t*(1-t)*(1-t)
        b_coeff_2 = 3*t*t*(1-t)
        b_coeff_3 = t*t*t
        for i in range(3):
            a_mat[3*id+i][0] = b_coeff_0*b_vecs[0][i]
            a_mat[3*id+i][1] = b_coeff_1*b_vecs[1][i]
            a_mat[3*id+i][2] = b_coeff_2*b_vecs[2][i]
            a_mat[3*id+i][3] = b_coeff_3*b_vecs[3][i]
            b_mat[3*id+i] = pt[i] - (b_coeff_0+b_coeff_1+b_coeff_2+b_coeff_3)*cam_pos[i]
    # minimal variation
    for i in range(3):
        a_mat[-i-1][0] = 0.05*min_var_coeffs[0]*b_vecs[0][i]
        a_mat[-i-1][1] = 0.05*min_var_coeffs[1]*b_vecs[1][i]
        a_mat[-i-1][2] = 0.05*min_var_coeffs[2]*b_vecs[2][i]
        a_mat[-i-1][3] = -0.05*b_vecs[3][i]
        b_mat[-i-1] = -0.05*cam_pos[i]*(min_var_coeffs[0]+min_var_coeffs[1]+min_var_coeffs[2]) + 0.05*cam_pos[i]

    bezier_depths, _, _, _ = lstsq(a_mat, b_mat)
    return bezier_depths

def stitch_partial_curves(per_axis_per_stroke_segments,
                          per_axis_per_stroke_intervals,
                          per_axis_per_stroke_per_interval_other_stroke_ids, sketch, camera,
                          VERBOSE=False):
    per_axis_per_stroke_curves = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    per_axis_per_stroke_interval_curve_ids = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    per_axis_per_stroke_rejected_curves = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    curve_connectivity_adj_mat = np.zeros([len(sketch.strokes), len(sketch.strokes)], dtype=int)
    for s_id, s in enumerate(sketch.strokes):
        #VERBOSE = s_id == 9 and axis_id == 2
        if s.axis_label != 5 or s.is_ellipse():
            continue
        neighbour_intersections = sketch.intersection_graph.get_intersections_by_stroke_id(s_id)
        for inter in neighbour_intersections:
            other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
            if sketch.strokes[other_s_id].axis_label == 5 and not sketch.strokes[other_s_id].is_ellipse():
                curve_connectivity_adj_mat[s_id, other_s_id] = 1
                curve_connectivity_adj_mat[other_s_id, s_id] = 1
    curve_connected_components = list(nx.connected_components(nx.Graph(curve_connectivity_adj_mat)))
    per_curve_component = np.zeros(len(sketch.strokes))
    for comp_id, comp in enumerate(curve_connected_components):
        for s_id in comp:
            per_curve_component[s_id] = comp_id

    for axis_id in range(3):
        for s_id, s in enumerate(sketch.strokes):
            #VERBOSE = s_id == 32 and axis_id == 1
            if s.axis_label != 5 or s.is_ellipse():
                continue
            if len(per_axis_per_stroke_intervals[axis_id][s_id]) == 0:
                continue
            #VERBOSE = axis_id == 2 and s_id == 9
            if VERBOSE:
                ps.remove_all_structures()
                #print(curve_connected_components)
                #print(per_curve_component[s_id])
            segments = per_axis_per_stroke_segments[axis_id][s_id]
            seg_pts = np.array([p for seg in segments for p in seg])
            bbox = tools_3d.bbox_from_points(seg_pts)
            intervals = per_axis_per_stroke_intervals[axis_id][s_id]
            # go through different interval priority queues
            all_combinations_t_interval_id_pt = []
            # find connected components of interval graph
            nb_intervals = len(intervals)
            intervals_adj_mat = np.ones([nb_intervals, nb_intervals], dtype=np.int)
            for i in range(nb_intervals):
                intervals_adj_mat[i][i] = 0
                int_i_components = [per_curve_component[other_s_id]
                                    for other_s_id in per_axis_per_stroke_per_interval_other_stroke_ids[axis_id][s_id][i]]
                for j in range(nb_intervals):
                    if j <= i:
                        continue
                    #dist = hausdorff_distance(segments[i], segments[j])
                    #if dist > 0.1*max(segments_len[i], segments_len[j]):
                    #    intervals_adj_mat[i, j] = 0
                    #    intervals_adj_mat[j, i] = 0
                    #    continue
                    int_j_components = [per_curve_component[other_s_id]
                                        for other_s_id in per_axis_per_stroke_per_interval_other_stroke_ids[axis_id][s_id][j]]
                    if VERBOSE:
                        print("components", i, int_i_components, j, int_j_components)
                    if len(np.intersect1d(int_i_components, int_j_components)) == 0:
                        intervals_adj_mat[i, j] = 0
                        intervals_adj_mat[j, i] = 0
                        continue
                    overlapping_interval = [max(intervals[i][0], intervals[j][0]),
                                            min(intervals[i][1], intervals[j][1])]
                    if not ((intervals[i][0] < intervals[j][0] and intervals[i][1] < intervals[j][1])
                            or (intervals[j][0] < intervals[i][0] and intervals[j][1] < intervals[i][1])):
                        intervals_adj_mat[i][j] = 0
                        intervals_adj_mat[j][i] = 0
                        continue
                    #if VERBOSE:
                    #    print("new_interval_comp")
                    #    print(intervals[i])
                    #    print(intervals[j])
                    #    print(overlapping_interval)
                    #    print((overlapping_interval[1] - overlapping_interval[0])/(intervals[i][1] - intervals[i][0]))
                    #    print((overlapping_interval[1] - overlapping_interval[0])/(intervals[j][1] - intervals[j][0]))
                    if min((overlapping_interval[1] - overlapping_interval[0])/(intervals[i][1] - intervals[i][0]), \
                           (overlapping_interval[1] - overlapping_interval[0])/(intervals[j][1] - intervals[j][0])) > 0.2:
                        intervals_adj_mat[i][j] = 0
                        intervals_adj_mat[j][i] = 0
            #intervals_cliques = list(nx.find_cliques(nx.Graph(intervals_adj_mat)))
            intervals_cliques = list(nx.enumerate_all_cliques(nx.Graph(intervals_adj_mat)))
            for comb in intervals_cliques:
            #for nb_intervals in range(1, len(intervals)+1):

            #    combinations_t_interval_id_pt = []
            #    if s_id == 9:
            #        print(len(list(distinct_combinations(range(len(intervals)), nb_intervals))))
            #    # TODO: replace distinct_combinations by find_cliques
            #    for comb in distinct_combinations(range(len(intervals)), nb_intervals):
                comb_t_interval_id_pt = []
                for t in np.linspace(0.0, 1.0, CURVE_SAMPLE_SIZE):
                    for interval_id in comb:
                        if (t > intervals[interval_id][0] or np.isclose(t, intervals[interval_id][0])) \
                                and (t < intervals[interval_id][1] or np.isclose(t, intervals[interval_id][1])):
                            comb_t_interval_id_pt.append([t, interval_id, []])
                            break
                if len(comb_t_interval_id_pt) == 0:
                    continue
            #    combinations_t_interval_id_pt.append(comb_t_interval_id_pt)
                all_combinations_t_interval_id_pt.append(comb_t_interval_id_pt)
                # TODO: remove duplicates
            # find 3d points from segments
            #print(all_combinations_t_interval_id_pt)
            for comb_id, comb_t_interval_id_pt in enumerate(all_combinations_t_interval_id_pt):
                pc = []
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, _ = triplet
                    #if np.isclose(t, intervals[interval_id][1]):
                    #    all_combinations_t_interval_id_pt[comb_id][id][-1] = segments[interval_id][-1]
                    #    pc.append(pt)
                    #    continue

                    pt, _ = tools_3d.interpolate_segment(segments[interval_id],
                                                      intervals[interval_id], t)
                    if len(pt) > 0:
                        all_combinations_t_interval_id_pt[comb_id][id][-1] = pt
                        pc.append(pt)

                    #for i in range(9):
                    #    t_i = intervals[interval_id][0] + \
                    #          i*(intervals[interval_id][1] - intervals[interval_id][0])/9
                    #    t_i_plus_1 = intervals[interval_id][0] + \
                    #                 (i+1)*(intervals[interval_id][1] - intervals[interval_id][0])/9
                    #    if t >= t_i and t <= t_i_plus_1:
                    #        pt = segments[interval_id][i] + (t - t_i)/(t_i_plus_1 - t_i)*\
                    #             (segments[interval_id][i+1] - segments[interval_id][i])
                    #        all_combinations_t_interval_id_pt[comb_id][id][-1] = pt
                    #        pc.append(pt)
                    #        break
                if VERBOSE:
                    if len(pc) > 0:
                        ps.register_point_cloud(str(comb_id), points=np.array(pc))

            # fit full bezier curves to sampled pts
            curves = []
            c_bezier = sketch.strokes[s_id].bezier
            b_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_bezier])
            for i in range(4):
                b_vecs[i] /= np.linalg.norm(b_vecs[i])
            cam_pos = np.array(camera.cam_pos)

            interval_curve_ids = [[] for l in range(len(intervals))]
            for comb_id, comb_t_interval_id_pt in enumerate(all_combinations_t_interval_id_pt):
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, pt = triplet
                    #if len(pt) == 0:
                    #    continue
                    interval_curve_ids[interval_id].append(len(curves))
                curve_sampled = []
                bezier_depths = fit_full_bezier_lstsq(comb_t_interval_id_pt, b_vecs, cam_pos)
                ctrl_pts_3d = cam_pos + np.array(bezier_depths).reshape(-1, 1)*b_vecs
                # sanity check
                dists = []
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, pt = triplet
                    dists.append(np.linalg.norm(np.array(bezier.q(ctrl_pts_3d, t)) - np.array(pt)))
                for t in np.linspace(0.0, 1.0, CURVE_SAMPLE_SIZE):
                    curve_sampled.append(bezier.q(ctrl_pts_3d, t))
                length = np.sum([np.linalg.norm(np.array(curve_sampled[i+1]) - np.array(curve_sampled[i]))
                          for i in range(9)])
                acc_radius = 0.2*length
                assert len(dists) > 0, "len(dists) == 0: no points sampled"
                if np.max(dists) > acc_radius:
                    per_axis_per_stroke_rejected_curves[axis_id][s_id].append(len(curves))
                curves.append(np.array(curve_sampled))
                if VERBOSE:
                    #if np.max(dists) <= acc_radius:
                    ps.register_curve_network(str(s_id)+"_comb"+str(comb_id), nodes=np.array(curve_sampled),
                                              edges=np.array([[i, i+1] for i in range(int(len(curve_sampled))-1)], dtype=np.int))

            for l in range(len(intervals)):
                interval_curve_ids[l] = np.unique(interval_curve_ids[l]).tolist()
            per_axis_per_stroke_curves[axis_id][s_id] = curves
            per_axis_per_stroke_interval_curve_ids[axis_id][s_id] = interval_curve_ids
            # sanity check
            # reject overly foreshortened curves
            ##non_rejected_curves = [i for i in range(len(per_axis_per_stroke_curves[axis_id][s_id]))]
            #curve_foreshortenings = [tools_3d.get_foreshortening(per_axis_per_stroke_curves[axis_id][s_id][curve_i], cam_pos)#/len(per_axis_per_stroke_curves[axis_id][s_id][curve_i])
            #                         for curve_i in range(len(per_axis_per_stroke_curves[axis_id][s_id]))
            #                         if not i in per_axis_per_stroke_rejected_curves[axis_id][s_id]]
            #median_foreshortening = np.median(curve_foreshortenings)
            #overly_foreshortened_curves = [i for i in range(len(curve_foreshortenings))
            #                               if tools_3d.get_foreshortening(
            #        per_axis_per_stroke_curves[axis_id][s_id][i], cam_pos) > 1.1*median_foreshortening]
            #if s_id == 9:
            #    print(curve_foreshortenings)
            #    print(median_foreshortening)
            #per_axis_per_stroke_rejected_curves[axis_id][s_id] += overly_foreshortened_curves
            #per_axis_per_stroke_rejected_curves[axis_id][s_id] = np.unique(per_axis_per_stroke_rejected_curves[axis_id][s_id]).tolist()

            if VERBOSE:
                for p_id, p in enumerate(per_axis_per_stroke_segments[axis_id][s_id]):
                    ps.register_curve_network(str(s_id)+"_"+str(p_id), nodes=p,
                                              edges=np.array([[i, i+1] for i in range(int(len(p))-1)], dtype=np.int))
                ps.show()
    return per_axis_per_stroke_curves, per_axis_per_stroke_interval_curve_ids, per_axis_per_stroke_rejected_curves

def merge_segments_intervals(segments, intervals, s_id, sketch, camera):

    merged_segments = []
    merged_intervals = []
    merged_intervals_ids = []
    c_bezier = sketch.strokes[s_id].bezier
    b_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_bezier])
    for i in range(4):
        b_vecs[i] /= np.linalg.norm(b_vecs[i])
    cam_pos = np.array(camera.cam_pos)

    # find connected components of interval graph
    nb_intervals = len(intervals)
    intervals_adj_mat = np.zeros([nb_intervals, nb_intervals], dtype=np.int)
    for i in range(nb_intervals):
        for j in range(nb_intervals):
            if j <= i:
                continue
            if intervals[i][0] <= intervals[j][0] and intervals[i][1] >= intervals[j][0] or \
                intervals[j][0] <= intervals[i][0] and intervals[j][1] >= intervals[i][0]:
                intervals_adj_mat[i][j] = 1
                intervals_adj_mat[j][i] = 1
    intervals_components = list(nx.connected_components(nx.Graph(intervals_adj_mat)))
    #print(intervals)
    #print(intervals_adj_mat)
    #print(intervals_components)

    # for each interval component find connected components of segments graph
    for interval_component in intervals_components:
        local_segments = np.array(segments)[list(interval_component)]
        local_intervals = np.array(intervals)[list(interval_component)]
        nb_local_segments = len(local_segments)
        segments_adj_mat = np.zeros([nb_local_segments, nb_local_segments], dtype=np.int)
        for i in range(nb_local_segments):
            for j in range(nb_local_segments):
                if j <= i:
                    continue
                if not(local_intervals[i][0] <= local_intervals[j][0] and local_intervals[i][1] >= local_intervals[j][0] or \
                        local_intervals[j][0] <= local_intervals[i][0] and local_intervals[j][1] >= local_intervals[i][0]):
                    continue
                overlapping_interval = [max(local_intervals[i][0], local_intervals[j][0]),
                                        min(local_intervals[i][1], local_intervals[j][1])]
                assert overlapping_interval[0] <= overlapping_interval[1], "overlapping interval incorrect: "+str(overlapping_interval)
                pt_i_a, interval_i_a = tools_3d.interpolate_segment(
                    local_segments[i], local_intervals[i], overlapping_interval[0])
                pt_i_b, interval_i_b = tools_3d.interpolate_segment(
                    local_segments[i], local_intervals[i], overlapping_interval[1])
                seg_i = [pt_i_a] + local_segments[i][interval_i_a+1:interval_i_b].tolist() + [pt_i_b]
                #print(local_intervals[i])
                #print(overlapping_interval)
                #print(seg_i)
                seg_i = np.array(seg_i)
                len_seg_i = tools_3d.line_3d_length(seg_i)
                pt_j_a, interval_j_a = tools_3d.interpolate_segment(
                    local_segments[j], local_intervals[j], overlapping_interval[0])
                pt_j_b, interval_j_b = tools_3d.interpolate_segment(
                    local_segments[j], local_intervals[j], overlapping_interval[1])
                seg_j = [pt_j_a] + local_segments[j][interval_j_a+1:interval_j_b].tolist() + [pt_j_b]
                seg_j = np.array(seg_j)
                len_seg_j = tools_3d.line_3d_length(seg_j)
                dist = max(directed_hausdorff(seg_i, seg_j)[0],
                           directed_hausdorff(seg_j, seg_i)[0])
                if dist < max(0.2*len_seg_i, 0.2*len_seg_j):
                    segments_adj_mat[i][j] = 1
                    segments_adj_mat[j][i] = 1
        #print(segments_adj_mat)

        local_seg_graph = nx.Graph(segments_adj_mat)
        local_segments_components = list(nx.connected_components(local_seg_graph))
        # merge clique segment components
        for local_seg_comp in list(local_segments_components):
            # check if local_seg_comp is clique
            local_seg_comp = list(local_seg_comp)
            n = len(local_seg_comp)
            local_seg_comp_graph = local_seg_graph.subgraph(local_seg_comp)
            if local_seg_comp_graph.size() != n*(n-1)/2:
                continue
            # "resample" points from segments
            resampled_pts = []
            for i in local_seg_comp:
                for t_id, t in enumerate(np.linspace(local_intervals[i][0], local_intervals[i][1], CURVE_SAMPLE_SIZE)):
                    resampled_pts.append([t, -1, local_segments[i][t_id]])
            # fit bezier to sampled points
            bezier_depths = fit_full_bezier_lstsq(resampled_pts, b_vecs, cam_pos)
            ctrl_pts_3d = cam_pos + np.array(bezier_depths).reshape(-1, 1)*b_vecs
            # resample max_interval from full bezier
            max_interval = [np.min(local_intervals[local_seg_comp][:, 0]),
                            np.max(local_intervals[local_seg_comp][:, 1])]
            new_pts = [bezier.q(ctrl_pts_3d, t)
                       for t in np.linspace(max_interval[0], max_interval[1], CURVE_SAMPLE_SIZE)]
            #print(np.array(list(interval_component)))
            #print(local_seg_comp)
            new_interval_ids = np.array(list(interval_component))[local_seg_comp]
            merged_segments.append(np.array(new_pts))
            merged_intervals.append(max_interval)
            merged_intervals_ids.append(new_interval_ids)

    # append all unmerged intervals as autonomous components
    used_interval_ids = [i for interval in merged_intervals_ids for i in interval]
    unused_interval_ids = [i for i in range(len(intervals)) if not i in used_interval_ids]
    for i in unused_interval_ids:
        merged_segments.append(segments[i])
        merged_intervals.append(intervals[i])
        merged_intervals_ids.append([i])

    return merged_segments, merged_intervals, merged_intervals_ids

def get_intersections_simple_batch(per_stroke_proxies, sketch, camera,
                                   batch, fixed_strokes):
    total_time = 0.0
    is_ellipse_time = 0.0
    total_intersections = 0

    simple_intersections = []
    intersections = sketch.intersection_graph.get_intersections()
    #print(len(intersections))
    for inter in intersections:
        # attribute telling us if one of the strokes is fixed
        inter.is_fixed = False
        inter.fix_depth = -1
        if len(fixed_strokes[inter.stroke_ids[0]]) > 0 and len(fixed_strokes[inter.stroke_ids[1]]) > 0:
            continue
        if inter.stroke_ids[0] > batch[1] or inter.stroke_ids[1] > batch[1]:
            continue
        if (sketch.strokes[inter.stroke_ids[0]].axis_label == 5 and sketch.strokes[inter.stroke_ids[0]].is_ellipse()) and \
                (sketch.strokes[inter.stroke_ids[1]].axis_label == 5 and sketch.strokes[inter.stroke_ids[1]].is_ellipse()):
            continue
        total_intersections += 1
        cam_depths = [[], []]
        inter_3ds = []
        lengths = [[], []]
        for vec_id, s_id in enumerate(inter.stroke_ids):
            #if s_id == 27:
            #    print("proxies")
            #    print(per_stroke_proxies[s_id])
            if len(fixed_strokes[s_id]) > 0:
                inter.is_fixed = True
                lifted_inter = None
                p = np.array(fixed_strokes[s_id])
                if sketch.strokes[s_id].axis_label < 5:
                    line_p = p[0]
                    line_v = p[-1] - p[0]
                    line_v /= np.linalg.norm(line_v)
                    tmp_time = time()
                    lifted_inter = camera.lift_point_close_to_line(inter.inter_coords, line_p, line_v)
                    total_time += time() - tmp_time
                else:
                    tmp_time = time()
                    if sketch.strokes[s_id].is_ellipse():
                        lifted_inter = camera.lift_point_close_to_polyline_v2(inter.inter_coords, p)
                    else:
                        lifted_inter = camera.lift_point_close_to_polyline_v3(inter.inter_coords, p)
                    total_time += time() - tmp_time
                if lifted_inter is not None:
                    cam_depth = np.linalg.norm(np.array(lifted_inter) - np.array(camera.cam_pos))
                    inter.fix_depth = cam_depth
                    inter_3ds.append(lifted_inter)
                if sketch.strokes[s_id].is_ellipse():
                    lengths[vec_id].append(tools_3d.bbox_diag(tools_3d.bbox_from_points(p)))
                    #u, v = tools_3d.get_basis_for_planar_point_cloud(p)
                    #ellipse_area = tools_3d.get_ellipse_area(p, u, v)
                    #lengths[vec_id].append(ellipse_area)
                else:
                    lengths[vec_id].append(tools_3d.line_3d_length(p))
                continue
            for p in per_stroke_proxies[s_id]:
                p = np.array(p)
                #if s_id == 29:
                #    print(p, tools_3d.line_3d_length(p), sketch.strokes[s_id].is_ellipse())
                #lengths.append(tools_3d.line_3d_length(p))
                if sketch.strokes[s_id].is_ellipse():
                    #u, v = tools_3d.get_basis_for_planar_point_cloud(p)
                    #ellipse_area = tools_3d.get_ellipse_area(p, u, v)
                    #lengths[vec_id].append(ellipse_area)
                    lengths[vec_id].append(tools_3d.bbox_diag(tools_3d.bbox_from_points(p)))
                else:
                    lengths[vec_id].append(tools_3d.line_3d_length(p))
                line_p = p[0]
                line_v = p[-1] - p[0]
                tmp_time = time()
                if not sketch.strokes[s_id].is_ellipse() and np.isclose(np.linalg.norm(line_v), 0.0):
                    continue
                is_ellipse_time += time() - tmp_time
                line_v /= np.linalg.norm(line_v)
                lifted_inter = None
                try:
                    if sketch.strokes[s_id].axis_label < 5:
                        tmp_time = time()
                        lifted_inter = camera.lift_point_close_to_line(inter.inter_coords, line_p, line_v)
                        total_time += time() - tmp_time
                    else:
                        tmp_time = time()
                        if sketch.strokes[s_id].is_ellipse():
                            lifted_inter = camera.lift_point_close_to_polyline_v2(inter.inter_coords, p)
                        else:
                            lifted_inter = camera.lift_point_close_to_polyline_v3(inter.inter_coords, p)
                        total_time += time() - tmp_time
                except:
                    continue
                if lifted_inter is not None:
                    cam_depth = np.linalg.norm(np.array(lifted_inter) - np.array(camera.cam_pos))
                    cam_depths[vec_id].append(cam_depth)
                    inter_3ds.append(lifted_inter)

        if len(cam_depths[0]) == 0 and len(cam_depths[1]) == 0:
            continue
        if len(lengths[0]) == 0 or len(lengths[1]) == 0:
            continue

        #median_length = max(np.median(lengths[0]), np.median(lengths[1]))
        median_length = max(np.min(lengths[0]), np.min(lengths[1]))
        max_epsilon = 0.1
        #if sketch.strokes[inter.stroke_ids[0]].is_ellipse() or \
        #    sketch.strokes[inter.stroke_ids[1]].is_ellipse():
        #    max_epsilon = 0.01
        #if 29 in inter.stroke_ids and 30 in inter.stroke_ids:
        #    print("inter ", inter.inter_id, inter.stroke_ids, max_epsilon*median_length, median_length, lengths[0], lengths[1])
        #    max_epsilon = 1.0
        #    print("cam_depths", cam_depths, inter.fix_depth)
        is_parallel = hasattr(inter, "is_parallel") and inter.is_parallel
        #if 36 in inter.stroke_ids:
        #    print(inter)
        if is_parallel:
            median_length = min(np.min(lengths[0]), np.min(lengths[1]))
            #print(median_length)
            #print(lengths)
            #print(np.median(lengths[0]), np.median(lengths[1]))
            #exit()
        #if is_parallel and 38 in inter.stroke_ids:
        #    print(median_length)
        #    exit()
        inter_simple = IntersectionSimple(inter_id=inter.inter_id,
                                          stroke_ids=inter.stroke_ids,
                                          acc_radius=inter.acc_radius,
                                          cam_depths=cam_depths,
                                          epsilon=max_epsilon*median_length,
                                          is_tangential=inter.is_tangential,
                                          is_fixed=inter.is_fixed,
                                          fix_depth=inter.fix_depth,
                                          is_triplet=inter.is_triplet,
                                          is_extended=inter.is_extended,
                                          is_parallel=is_parallel)
        inter_simple.inter_3ds = deepcopy(inter_3ds)
        inter_simple.inter_coords = inter.inter_coords
        simple_intersections.append(inter_simple)
    #print("total projection time ", total_time)
    #print("is_ellipse_time ", is_ellipse_time)
    #print("total_intersections ", total_intersections)
    return simple_intersections

def extract_correspondence_information(input_correspondences, sketch,
                                        acc_radius=0.05,
                                        VERBOSE=False):
    correspondences = []
    per_stroke_candidates = [[] for i in range(len(sketch.strokes))]

    for corr_id, corr in enumerate(input_correspondences):
        stroke_ids = [corr[0], corr[1]]
        plane_id = corr[4]
        candidate_id_0 = len(per_stroke_candidates[stroke_ids[0]])
        candidate_id_1 = len(per_stroke_candidates[stroke_ids[1]])
        strokes_3d = [corr[2], corr[3]]
        masked_correspondences = [[], []]
        inter_stroke_id_0 = corr[7]
        inter_stroke_id_1 = corr[8]
        per_stroke_candidates[stroke_ids[0]].append(
            Candidate(stroke_3d=strokes_3d[0], stroke_id=stroke_ids[0],
                      plane_id=plane_id, correspondence_id=corr_id,
                      first_inter_stroke_id=inter_stroke_id_0,
                      snd_inter_stroke_id=inter_stroke_id_1,
                      candidate_id=candidate_id_0))
        if stroke_ids[0] != stroke_ids[1]:
            per_stroke_candidates[stroke_ids[1]].append(
                Candidate(stroke_3d=strokes_3d[1], stroke_id=stroke_ids[1],
                          plane_id=plane_id, correspondence_id=corr_id,
                          first_inter_stroke_id=inter_stroke_id_0,
                          snd_inter_stroke_id=inter_stroke_id_1,
                          candidate_id=candidate_id_1))
        correspondences.append(
            Correspondence(stroke_3ds=strokes_3d, stroke_ids=stroke_ids,
                           candidate_ids=[candidate_id_0, candidate_id_1],
                           plane_id=plane_id,
                           first_inter_stroke_id=inter_stroke_id_0,
                           snd_inter_stroke_id=inter_stroke_id_1,
                           masked_correspondences=masked_correspondences))

    #median_lengths = []
    #for s_id in range(len(sketch.strokes)):
    #    median_lengths.append(np.median([tools_3d.line_3d_length(s.stroke_3d)
    #                                     for s in per_stroke_candidates[s_id]]))
    #for corr_id, corr in enumerate(correspondences):
    #    cand_0 = per_stroke_candidates[corr.stroke_ids[0]][corr.candidate_ids[0]]
    #    masked_candidates_0 = []
    #    for i in range(len(per_stroke_candidates[corr.stroke_ids[0]])):
    #        dist = max(directed_hausdorff(cand_0.stroke_3d, per_stroke_candidates[corr.stroke_ids[0]][i].stroke_3d)[0],
    #                   directed_hausdorff(per_stroke_candidates[corr.stroke_ids[0]][i].stroke_3d, cand_0.stroke_3d)[0])
    #        if dist > acc_radius*median_lengths[corr.stroke_ids[0]]:
    #            masked_candidates_0.append(i)
    #    cand_1 = per_stroke_candidates[corr.stroke_ids[1]][corr.candidate_ids[1]]
    #    masked_candidates_1 = []
    #    for i in range(len(per_stroke_candidates[corr.stroke_ids[1]])):
    #        dist = max(directed_hausdorff(cand_1.stroke_3d, per_stroke_candidates[corr.stroke_ids[1]][i].stroke_3d)[0],
    #                   directed_hausdorff(per_stroke_candidates[corr.stroke_ids[1]][i].stroke_3d, cand_1.stroke_3d)[0])
    #        if dist > acc_radius*median_lengths[corr.stroke_ids[1]]:
    #            masked_candidates_1.append(i)
    #    masked_correspondences = [[per_stroke_candidates[corr.stroke_ids[0]][i].correspondence_id
    #                               for i in masked_candidates_0],
    #                              [per_stroke_candidates[corr.stroke_ids[1]][i].correspondence_id
    #                               for i in masked_candidates_1]]
    #    correspondences[corr_id].masked_correspondences = masked_correspondences

        if VERBOSE:
            ps.remove_all_structures()
            edges_array = np.array([[i, i+1] for i in range(len(cand_0.stroke_3d)-1)])
            ps.register_curve_network("cand", nodes=cand_0.stroke_3d, edges=edges_array)
            not_masked_candidates = []
            for i in range(len(per_stroke_candidates[corr.stroke_ids[0]])):
                if i not in masked_candidates_0:
                    not_masked_candidates.append(i)
            for i in masked_candidates_0:
                cand = per_stroke_candidates[corr.stroke_ids[0]][i]
                edges_array = np.array([[i, i+1] for i in range(len(cand.stroke_3d)-1)])
                ps.register_curve_network("masked_cand_"+str(i), nodes=cand.stroke_3d, edges=edges_array,
                                          color=(1, 0, 0))
            for i in not_masked_candidates:
                cand = per_stroke_candidates[corr.stroke_ids[0]][i]
                edges_array = np.array([[i, i+1] for i in range(len(cand.stroke_3d)-1)])
                ps.register_curve_network("not_masked_cand_"+str(i), nodes=cand.stroke_3d, edges=edges_array,
                                          color=(0, 0, 1))
            ps.show()

            if corr.stroke_ids[0] == corr.stroke_ids[1]:
                continue
            ps.remove_all_structures()
            edges_array = np.array([[i, i+1] for i in range(len(cand_1.stroke_3d)-1)])
            ps.register_curve_network("cand", nodes=cand_1.stroke_3d, edges=edges_array)
            not_masked_candidates = []
            for i in range(len(per_stroke_candidates[corr.stroke_ids[1]])):
                if i not in masked_candidates_1:
                    not_masked_candidates.append(i)
            for i in masked_candidates_1:
                cand = per_stroke_candidates[corr.stroke_ids[1]][i]
                edges_array = np.array([[i, i+1] for i in range(len(cand.stroke_3d)-1)])
                ps.register_curve_network("masked_cand_"+str(i), nodes=cand.stroke_3d, edges=edges_array,
                                          color=(1, 0, 0))
            for i in not_masked_candidates:
                cand = per_stroke_candidates[corr.stroke_ids[1]][i]
                edges_array = np.array([[i, i+1] for i in range(len(cand.stroke_3d)-1)])
                ps.register_curve_network("not_masked_cand_"+str(i), nodes=cand.stroke_3d, edges=edges_array,
                                          color=(0, 0, 1))
            ps.show()

    return correspondences, per_stroke_candidates

bezier_coeff_mat = np.array([[1, 0, 0, 0],
                             [-3, 3, 0, 0],
                             [3, -6, 3, 0],
                             [-1, 3, -3, 1]])
def cubic_bezier_eval_paralllel(ctrl_pts, a, b, n):
    t_coeffs = np.ones([n, 4], dtype=float)
    t_simple = np.linspace(a, b, n).transpose()
    t_coeffs[:, 1] = t_simple
    t_coeffs[:, 2] = t_simple*t_simple
    t_coeffs[:, 3] = t_simple*t_simple*t_simple
    return np.matmul(t_coeffs, np.matmul(bezier_coeff_mat, ctrl_pts))


def equi_resample_parallel(ctrl_pts, a, b, N):
    if np.isclose(np.abs(a - b), 0.0):
        return np.array([])
    pts = cubic_bezier_eval_paralllel(ctrl_pts, a, b, 100)
    dists = np.linalg.norm(pts[1:] - pts[:len(pts)-1], axis=1)
    if np.isclose(np.sum(dists), 0.0):
        return []
    chord_lengths = np.cumsum(dists)/np.sum(dists)
    arc_lengths = np.zeros(len(pts))
    arc_lengths[1:] = chord_lengths
    arc_lengths = a + (b-a)*arc_lengths
    equi_pts = np.array([pts[np.argmin(np.abs(arc_lengths - t))]
                         for t in np.linspace(a, b, N)])
    return equi_pts

def equi_resample(ctrl_pts, a, b, N):
    if np.isclose(np.abs(a - b), 0.0):
        return np.array([])
    pts = np.array([bezier.q(ctrl_pts, t) for t in np.linspace(a, b, 100)])
    dists = np.linalg.norm(pts[1:] - pts[:len(pts)-1], axis=1)
    chord_lengths = np.cumsum(dists)/np.sum(dists)
    arc_lengths = np.zeros(len(pts))
    arc_lengths[1:] = chord_lengths
    arc_lengths = a + (b-a)*arc_lengths
    equi_pts = np.array([pts[np.argmin(np.abs(arc_lengths - t))]
                         for t in np.linspace(a, b, N)])
    return equi_pts

def equi_resample_polyline(poly, dist):
    #print("equi_resample_polyline")
    #print(poly)
    #print("dist", dist)
    dists = np.linalg.norm(poly[1:] - poly[:len(poly)-1], axis=1)
    poly_len = np.sum(dists)
    if poly_len/dist > 100:
        dist = poly_len/100
    #print(len(np.linspace(0, 1, int(poly_len/dist))))
    chord_lengths = np.cumsum(dists)/poly_len
    arc_lengths = np.zeros(len(poly))
    arc_lengths[1:] = chord_lengths
    equi_pts = []
    for t in np.linspace(0, 1, int(poly_len/dist)):
        if np.isclose(t, 0.0):
            equi_pts.append(poly[0])
            continue
        elif np.isclose(t, 1.0):
            equi_pts.append(poly[-1])
            continue
        closest_ids = np.argsort(np.abs(arc_lengths-t))
        min_id = np.min(closest_ids[:2])
        max_id = np.max(closest_ids[:2])
        equi_pts.append(poly[min_id] +
                        (t - arc_lengths[min_id])/(arc_lengths[max_id] - arc_lengths[min_id])*\
                        (poly[max_id] - poly[min_id]))
    return np.array(equi_pts)

def symmetric_point_cloud(pc, refl_mat, threshold=0.01, VERBOSE=False):

    reflected_points = tools_3d.apply_hom_transform_to_points(pc, refl_mat)
    #dist = distance.cdist(pc, pc)
    #for i in range(len(pc)):
    #    dist[i, i] = 100.0
    #closest = np.min(dist, axis=1)
    #mean_dist = 2*np.mean(closest)
    dist = distance.cdist(pc, reflected_points)
    close_pts = np.zeros(len(pc), dtype=np.int)

    for i in range(len(pc)):
        dist[i, i] = 100.0
    for i in range(len(pc)):
        close_pts[i] = np.sum(dist[i, :] < threshold)
    if VERBOSE:
        ps.remove_all_structures()
        pc_ps = ps.register_point_cloud("pts", points=pc)
        ps.register_point_cloud("reflected_pts", points=reflected_points)
        pc_ps.add_scalar_quantity("close_pts", close_pts, enabled=True, vminmax=(0, 1))
        ps.show()
    return close_pts

def copy_correspondences_batch(correspondences, batch, fixed_strokes, refl_mats,
                               tmp_planes_scale_factors, camera, sketch,
                               ref_correspondences=[]):

    batch_correspondences = []
    batch_correspondence_ids = []
    for corr_id, corr in enumerate(correspondences):
        s_id_0 = corr[0]
        s_id_1 = corr[1]
        if len(ref_correspondences) > 0:
            if not ref_correspondences[corr[4]].has_edge(s_id_0, s_id_1):
                continue
        # old correspondences
        if len(fixed_strokes[s_id_0]) > 0 and len(fixed_strokes[s_id_1]) > 0:
            continue
        # correspondence involving future strokes
        if s_id_0 > batch[1] or s_id_1 > batch[1]:
            continue
        # correspondence involving future strokes
        if corr[7] > batch[1] or corr[8] > batch[1]:
            continue

        if corr[7] != -1 and len(fixed_strokes[corr[7]]) > 0 and \
            corr[8] != -1 and len(fixed_strokes[corr[8]]) > 0:
            first_stroke = np.array(fixed_strokes[corr[7]])
            first_stroke_refl = np.array(tools_3d.apply_hom_transform_to_points(first_stroke, refl_mats[corr[4]]))
            first_len = tools_3d.line_3d_length(first_stroke)
            snd_stroke = np.array(fixed_strokes[corr[8]])
            snd_len = tools_3d.line_3d_length(snd_stroke)
            acc_radius = 0.1*min(first_len, snd_len)
            dist = np.min(distance.cdist(equi_resample_polyline(first_stroke_refl, acc_radius),
                                         equi_resample_polyline(snd_stroke, acc_radius)))
            #dist = np.max(np.min(distance.cdist(equi_resample_polyline(first_stroke_refl, acc_radius),
            #                                    equi_resample_polyline(snd_stroke, acc_radius)), axis=-1))
            #print(distance.cdist(equi_resample_polyline(first_stroke_refl, acc_radius),
            #                            equi_resample_polyline(snd_stroke, acc_radius)))
            #print(np.min(distance.cdist(equi_resample_polyline(first_stroke_refl, acc_radius),
            #                      equi_resample_polyline(snd_stroke, acc_radius)), axis=-1))
            #print(np.max(np.min(distance.cdist(equi_resample_polyline(first_stroke_refl, acc_radius),
            #                            equi_resample_polyline(snd_stroke, acc_radius)), axis=-1)))
            #exit()
            #first_stroke_refl_resampled = equi_resample_polyline(first_stroke_refl, acc_radius)
            #snd_stroke_resampled = equi_resample_polyline(snd_stroke, acc_radius)
            #ps.register_point_cloud("first_stroke", points=first_stroke)
            #ps.register_point_cloud("first_stroke_refl", points=first_stroke_refl_resampled)
            #ps.register_point_cloud("snd_stroke", points=snd_stroke_resampled)
            #ps.show()
            if dist < acc_radius:
                batch_correspondences.append([corr[0], corr[1],
                                              camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[2]) - camera.cam_pos),
                                              camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[3]) - camera.cam_pos),
                                              corr[4], corr[5], corr[6], corr[7], corr[8]])
                batch_correspondence_ids.append(corr_id)
            continue

        # correspondences involving non-fixed strokes
        if s_id_0 <= batch[1] and len(fixed_strokes[s_id_0]) == 0 and \
                s_id_1 <= batch[1] and len(fixed_strokes[s_id_1]) == 0:
            #if corr[0] == 0 or corr[1] == 0:
            #    print(corr)
            batch_correspondences.append([corr[0], corr[1],
                                          camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[2]) - camera.cam_pos),
                                          camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[3]) - camera.cam_pos),
                                          corr[4], corr[5], corr[6], corr[7], corr[8]])
            #if corr[0] == 0 or corr[1] == 0:
            #    print(batch_correspondences[-1])
            batch_correspondence_ids.append(corr_id)
            continue

        # correspondence involving a fixed stroke and a stroke within batch
        if len(fixed_strokes[s_id_0]) > 0:
            s_0 = np.array(fixed_strokes[s_id_0])
            s_0_refl = tools_3d.apply_hom_transform_to_points(s_0, refl_mats[corr[4]])
            s_0_refl_resampled = equi_resample_polyline(s_0_refl, 0.1*tools_3d.line_3d_length(s_0_refl))
            s_0_proj = np.array(camera.project_polyline(s_0_refl_resampled))
            s_1 = np.array([p.coords for p in sketch.strokes[s_id_1].points_list])
            #dist = np.min(distance.cdist(s_0_proj, s_1))
            dist = LineString(s_0_proj).distance(LineString(s_1))
            #dist = LineString(s_0_proj).hausdorff_distance(LineString(s_1))
            #hauss_dist = LineString(s_0_proj).hausdorff_distance(LineString(s_1))
            #if hauss_dist > 2*max(sketch.strokes[s_id_0].acc_radius,sketch.strokes[s_id_1].acc_radius) \
            #        and dist <2*max(sketch.strokes[s_id_0].acc_radius,sketch.strokes[s_id_1].acc_radius):
            #    print(corr)
            #    plt.plot(s_0_proj[:, 0], s_0_proj[:, 1], c="r")
            #    plt.plot(s_1[:, 0], s_1[:, 1], c="g")
            #    plt.show()
            if dist < 2*max(sketch.strokes[s_id_0].acc_radius,
                          sketch.strokes[s_id_1].acc_radius):
                batch_correspondences.append([corr[0], corr[1],
                                              np.array(fixed_strokes[s_id_0]),
                                              camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[3]) - camera.cam_pos),
                                              corr[4], corr[5], corr[6], corr[7], corr[8]])
                batch_correspondence_ids.append(corr_id)

            #if s_id_0 == 4 and s_id_1 == 3 and corr[4] == 0:
            #    print(dist)
            #    print(sketch.strokes[s_id_0].acc_radius)
            #    print(sketch.strokes[s_id_1].acc_radius)
            #    s_0 = np.array([p.coords for p in sketch.strokes[s_id_0].points_list])
            #    plt.plot(s_0[:, 0], s_0[:, 1], c="b")
            #    plt.plot(s_0_proj[:, 0], s_0_proj[:, 1], c="r")
            #    plt.plot(s_1[:, 0], s_1[:, 1], c="g")
            #    plt.show()
            continue

        if len(fixed_strokes[s_id_1]) > 0:
            #print(corr)
            #print("passed by")
            s_1 = np.array(fixed_strokes[s_id_1])
            s_1_refl = tools_3d.apply_hom_transform_to_points(s_1, refl_mats[corr[4]])
            s_1_refl_resampled = equi_resample_polyline(s_1_refl, 0.1*tools_3d.line_3d_length(s_1_refl))
            s_1_proj = np.array(camera.project_polyline(s_1_refl_resampled))
            s_0 = np.array([p.coords for p in sketch.strokes[s_id_0].points_list])
            dist = LineString(s_0).distance(LineString(s_1_proj))
            hauss_dist = LineString(s_0).hausdorff_distance(LineString(s_1_proj))
            #if hauss_dist > 2*max(sketch.strokes[s_id_0].acc_radius,sketch.strokes[s_id_1].acc_radius) \
            #        and dist <2*max(sketch.strokes[s_id_0].acc_radius,sketch.strokes[s_id_1].acc_radius):
            #    print(corr)
            #    plt.plot(s_1_proj[:, 0], s_1_proj[:, 1], c="r")
            #    plt.plot(s_0[:, 0], s_0[:, 1], c="g")
            #    plt.show()
            if dist < 2*max(sketch.strokes[s_id_0].acc_radius,
                          sketch.strokes[s_id_1].acc_radius):
                batch_correspondences.append([corr[0], corr[1],
                                              camera.cam_pos + tmp_planes_scale_factors[corr[4]]*(np.array(corr[2]) - camera.cam_pos),
                                              np.array(fixed_strokes[s_id_1]),
                                              corr[4], corr[5], corr[6], corr[7], corr[8]])
                batch_correspondence_ids.append(corr_id)
            continue

    return batch_correspondences, batch_correspondence_ids

def update_candidate_strokes(fixed_strokes, correspondences, batch, nb_strokes):
    per_axis_per_stroke_candidate_reconstructions = [[[] for s_id in range(nb_strokes)]
                                                     for plane_id in range(3)]

    for corr in correspondences:
        if corr[0] > batch[1] or corr[1] > batch[1]:
            continue
        if int(corr[7]) != -1 and corr[7] > batch[1]:
            continue
        if int(corr[8]) != -1 and corr[8] > batch[1]:
            continue
        if len(fixed_strokes[corr[0]]) > 0:
            if not len(per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[0]]) > 0:
                per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[0]] = [fixed_strokes[corr[0]]]
        else:
            per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[0]].append(corr[2])
        if int(corr[0]) == int(corr[1]):
            continue
        if len(fixed_strokes[corr[1]]) > 0:
            if not len(per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[1]]) > 0:
                per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[1]] = [fixed_strokes[corr[1]]]
        else:
            per_axis_per_stroke_candidate_reconstructions[corr[4]][corr[1]].append(corr[3])

    return per_axis_per_stroke_candidate_reconstructions

# planes: list of (plane_p, plane_n)
def cluster_planes(planes, epsilon=-1):
    # two stage clustering: first along normals, then along points
    # i)Normal clustering
    planes_cluster_data = np.array([plane[1] for plane in planes])
    normal_quantile = 0.1
    if len(planes) < 10:
        normal_quantile = 0.2
        print("len(planes)")
        print(len(planes))
    bandwidth = estimate_bandwidth(planes_cluster_data, quantile=0.2)
    if np.isclose(bandwidth, 0.0):
        bandwidth = estimate_bandwidth(planes_cluster_data, quantile=0.5)
    #print(bandwidth)
    #bandwidth = 1.1
    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    ms.fit(planes_cluster_data)
    labels = ms.labels_
    clustered_normals = ms.cluster_centers_
    nb_labels = np.unique(labels)
    #print(cluster_centers)
    #cluster_planes = [[[center[0], 0, 0], center[1:]]
    #                  for center in cluster_centers]

    final_clusters = []
    final_cluster_ids = np.zeros(len(planes), dtype=np.int)
    # ii) Point clustering
    x_vec = np.array([1, 0, 0])
    y_vec = np.array([0, 1, 0])
    z_vec = np.array([0, 0, 1])
    axes_vec = [x_vec, y_vec, z_vec]
    origin = np.zeros(3)

    for label in range(len(nb_labels)):
        chosen_axis = np.argmax(np.abs([clustered_normals[label].dot(p)
                            for p in axes_vec]))
        plane_ids = np.argwhere(labels == label).flatten()
        if len(plane_ids) == 1:
            final_cluster_ids[plane_ids[0]] = len(final_clusters)
            final_clusters.append(planes[plane_ids[0]])
            continue
        plane_points = planes[plane_ids, 0]
        axis_vec = np.zeros(3)
        axis_vec[chosen_axis] = 1.0
        bandwidth = epsilon
        axis_inters = np.array([tools_3d.line_plane_collision(
            clustered_normals[label], p, axis_vec, origin) for p in plane_points])
        cluster_data = axis_inters[:, chosen_axis]
        cluster_data = cluster_data.reshape(-1, 1)
        ms = MeanShift(bandwidth=bandwidth)
        ms.fit(cluster_data)
        point_labels = ms.labels_
        clustered_points = ms.cluster_centers_
        for cluster_p_id, cluster_p in enumerate(clustered_points):
            cluster_plane_point = [0, 0, 0]
            cluster_plane_point[chosen_axis] = cluster_p[0]

            for p_l in plane_ids[np.argwhere(point_labels == cluster_p_id).flatten()]:
                final_cluster_ids[p_l] = len(final_clusters)
            final_clusters.append(np.array([cluster_plane_point, clustered_normals[label]]))
    final_clusters = np.array(final_clusters)
    final_cluster_ids = np.array(final_cluster_ids)
    return final_clusters, final_cluster_ids

def get_plane_triplet(cam_pos, x_scale, y_scale, z_scale):
    x_plane_point = np.zeros(3, dtype=np.float)
    x_plane_point = cam_pos + x_scale*(np.array(x_plane_point) - cam_pos)
    y_plane_point = np.zeros(3, dtype=np.float)
    y_plane_point = cam_pos + y_scale*(np.array(y_plane_point) - cam_pos)
    z_plane_point = np.zeros(3, dtype=np.float)
    z_plane_point = cam_pos + z_scale*(np.array(z_plane_point) - cam_pos)
    x_plane = Plane(x_plane_point, np.array([1, 0, 0]))
    y_plane = Plane(y_plane_point, np.array([0, 1, 0]))
    z_plane = Plane(z_plane_point, np.array([0, 0, 1]))
    xyz_inter = z_plane.intersect_line(x_plane.intersect_plane(y_plane))
    return np.array(xyz_inter)

def assign_ellipse_minor_axes(sketch, camera, alignment_threshold=30.0,
                              VERBOSE=True, data_folder=""):
    if VERBOSE:

        #angle_cmap = sns.color_palette("viridis", n_colors=90, as_cmap=True)
        plt.rcParams["figure.figsize"] = (10, 10)
        fig, axes = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=1.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        sketch.display_strokes_2(fig=fig, ax=axes, color_process=lambda s: "#0000008D")

        #sm = plt.cm.ScalarMappable(cmap=angle_cmap)
        #sm.set_array([])
        #cax = fig.add_axes([axes.get_position().x1+0.05, axes.get_position().y0, 0.06, axes.get_position().height / 2])
        #plt.colorbar(sm, cax=cax)

    for s_id, s in enumerate(sketch.strokes):
        if not (s.axis_label == 5 and s.is_ellipse()):
            continue
        pts = np.array([p.coords for p in s.points_list])

        ell = EllipseModel()
        ell.estimate(pts)

        xc, yc, a, b, theta_orig = ell.params
        theta = np.rad2deg(theta_orig)
        center = np.array([xc, yc])

        minor_axis_length = min(a, b)
        minor_axis_id = np.argmin([a, b])

        if minor_axis_id == 1:
            theta += 90.0
        rotated_x = np.array([np.cos(np.deg2rad(theta)), np.sin(np.deg2rad(theta))])
        rotated_x /= np.linalg.norm(rotated_x)
        vanishing_lines = np.array([(camera.vanishing_points_coords[i]-center)/np.linalg.norm((camera.vanishing_points_coords[i]-center))
                                    for i in range(3)])
        angles = np.arccos(np.abs(np.dot(vanishing_lines, rotated_x)))
        s.minor_axis = rotated_x
        min_angle = np.min(np.rad2deg(angles))
        s.closest_major_axis_id = np.argmin(angles)
        s.closest_major_axis_deviation = min_angle

        if VERBOSE:

            #plt.plot(pts[:, 0], pts[:, 1], c="red", lw=3)
            #plt.scatter([center[0]], [center[1]], c="red")
            #print(scale)
            #print(rot_mat)
            #print(pts)

            #print(rotated_x)
            x_axis = np.array([1, 0])
            minor_axis = np.array([center - minor_axis_length*rotated_x,
                                   center + minor_axis_length*rotated_x])
            plt.plot(minor_axis[:, 0], minor_axis[:, 1], c="#377eb8", lw=3)

            #print(np.rad2deg(angles))
            closest_vanishing_line_id = np.argmin(angles)
            closest_vanishing_line = np.array([center, center + \
                                               minor_axis_length*vanishing_lines[closest_vanishing_line_id]])
            minor_angle = np.rad2deg(np.arccos(np.dot(x_axis, rotated_x)))
            cross = np.cross(rotated_x, x_axis)
            #print(x_axis, rotated_x)
            #print(cross)
            #print(np.dot(x_axis, cross))
            if cross < 0:
                #minor_angle *= -1
                minor_angle = 180 - minor_angle
                minor_angle += 180

            vanishing_line_angle = np.rad2deg(np.arccos(np.dot(x_axis, vanishing_lines[closest_vanishing_line_id])))
            cross = np.cross(vanishing_lines[closest_vanishing_line_id], x_axis)
            if cross < 0:
                #vanishing_line_angle *= -1
                vanishing_line_angle = 180 - vanishing_line_angle
                vanishing_line_angle += 180

            if abs(minor_angle-vanishing_line_angle) > 90.0:
                minor_angle = np.rad2deg(np.arccos(np.dot(-rotated_x, x_axis)))
                cross = np.cross(-rotated_x, x_axis)
                if cross < 0:
                    minor_angle = 180 - minor_angle
                    minor_angle += 180
                    #minor_angle *= -1

            print("angles", minor_angle, vanishing_line_angle)
            axes.arrow(center[0], center[1], minor_axis_length*vanishing_lines[closest_vanishing_line_id][0],
                      minor_axis_length*vanishing_lines[closest_vanishing_line_id][1],
                      width=2, color="#4daf4a")
            #arc_patch = Arc(center, 1.5*minor_axis_length, 1.5*minor_axis_length, color="red",
            #                theta1=-max(minor_angle, vanishing_line_angle),
            #                theta2=-min(minor_angle, vanishing_line_angle),
            #                linewidth=5)
            #axes.add_patch(arc_patch)
            #angle_cmap(int(abs(minor_angle-vanishing_line_angle))),
            theta_1 = -max(minor_angle, vanishing_line_angle)
            theta_2 = -min(minor_angle, vanishing_line_angle)
            #theta_1 = min(minor_angle, vanishing_line_angle)
            #theta_2 = max(minor_angle, vanishing_line_angle)
            #if minor_angle > 0.0 and vanishing_line_angle > 0.0:
            #    theta_1 = min(minor_angle, vanishing_line_angle)
            #    theta_2 = max(minor_angle, vanishing_line_angle)

            wedge_patch = Wedge(center, (1.5*minor_axis_length)/2, color="red",
                                #theta1=-max(minor_angle, vanishing_line_angle),
                                #theta2=-min(minor_angle, vanishing_line_angle),
                                theta1=theta_1,
                                theta2=theta_2,
                                alpha=0.8, zorder=10.0)
            axes.add_patch(wedge_patch)
            #plt.plot(closest_vanishing_line[:, 0], closest_vanishing_line[:, 1],
            #         c="#4daf4a", lw=2, path_effects=[pe.Stroke(linewidth=5, foreground='black'), pe.Normal()])
            c = "#377eb8"
            if min_angle > alignment_threshold:
                c = "#e41a1c"
            ell_patch = Ellipse((xc, yc), 2*a, 2*b, theta_orig*180.0/np.pi,
                                edgecolor=c, facecolor='none', linewidth=3)
            axes.add_patch(ell_patch)


    if VERBOSE:
        axes.set_xlim(0, sketch.width)
        axes.set_ylim(sketch.height, 0)
        axes.axis("equal")
        axes.axis("off")
        #plt.show()
        if data_folder != "":
            plt.savefig(os.path.join(data_folder, "ellipses_alignment.png"))
        else:
            plt.show()

def get_max_intersection_length(sketch, s_id):
    intersections = sketch.intersection_graph.get_intersections_by_stroke_id(s_id)
    if len(intersections) < 2:
        return sketch.strokes[s_id].linestring.linestring.length
    projected_distances = np.array([sketch.strokes[s_id].linestring.linestring.project(Point(inter.inter_coords))
                                    for inter in intersections])
    max_intersections = np.array([intersections[np.argmin(projected_distances)].inter_coords,
                                  intersections[np.argmax(projected_distances)].inter_coords])

    #print(intersections)
    #print(max_intersections)
    if len(max_intersections) > 1:
        return np.linalg.norm(max_intersections[0]-max_intersections[-1])
    else:
        return s.linestring.linestring.length

def get_median_line_lengths(sketch):
    axes_distances = []
    for i in range(3):
        line_lengths = []
        for s_id, s in enumerate(sketch.strokes):
            if s.axis_label != i:
                continue
            line_lengths.append(get_max_intersection_length(sketch, s_id))
        axes_distances.append(np.median(line_lengths))
    return axes_distances

def get_vanishing_triangle(s, vp, epsilon=0.0):
    s_coords = np.array(s.linestring.linestring).tolist()
    vanishing_triangle = MultiPoint(s_coords + [vp]).convex_hull
    for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
        found_vp = False
        if np.isclose(np.linalg.norm(p - vp), 0.0):
            vp_id = p_id
            break
    outer_1 = np.array(vanishing_triangle.exterior.coords)[:-1][(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]
    outer_2 = np.array(vanishing_triangle.exterior.coords)[:-1][(vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]
    outer_vec = outer_1 - outer_2
    outer_1 += epsilon*outer_vec/2
    outer_2 -= epsilon*outer_vec/2
    triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]),
                                                                  vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
    triangle = np.array([outer_1, vp, outer_2])
    # extend vanishing triangle to "infinity"
    triangle[0] += (triangle[0] - triangle[1])
    triangle[-1] += (triangle[-1] - triangle[1])
    vanishing_triangle = Polygon(triangle)
    return vanishing_triangle

def rotate_angle(pts, angle):
    rot_mat = np.array([[np.cos(angle), -np.sin(angle)],
                        [np.sin(angle), np.cos(angle)]])
    return np.matmul(rot_mat, pts.T).T

def curve_symmetry_candidate(s1, s2, vanishing_triangle, axis_id, sketch, cam, angle_threshold=15.0,
                             dist_threshold=0.05, VERBOSE=False):
    axes_distances = get_median_line_lengths(sketch)
    # spatial distance
    center_1 = np.mean(np.array(s1.linestring.linestring), axis=0)
    center_2 = np.mean(np.array(s2.linestring.linestring), axis=0)
    spatial_dist = min(1.0, np.linalg.norm(center_1-center_2)/axes_distances[axis_id])
    spatial_dist = min(1.0, s1.linestring.linestring.distance(s2.linestring.linestring)/axes_distances[axis_id])

    if not vanishing_triangle.intersects(s2.linestring.linestring):
        return False
    vp = np.array(cam.vanishing_points_coords[axis_id])
    #print("here")
    #print(vanishing_triangle)
    #print(vp)
    # check for translational and reflective symmetry
    translational_symmetry = True
    reflectional_symmetry = True
    if VERBOSE:
        print("spatial_dist", spatial_dist)
    if spatial_dist < 0.1:
        translational_symmetry = False

    max_length = max(s1.length(), s2.length())
    max_length = min(s1.length(), s2.length())

    vanishing_triangle_pts = np.array(vanishing_triangle.exterior.coords)
    #print(vanishing_triangle_pts)
    v1 = np.array([vanishing_triangle_pts[1], vanishing_triangle_pts[0]])
    v2 = np.array([vanishing_triangle_pts[1], vanishing_triangle_pts[2]])
    v_mid = np.array([(v1[1]+v2[1])/2, vanishing_triangle_pts[1]])

    s1_pts = np.array(s1.linestring.linestring)
    s2_intersection = vanishing_triangle.intersection(s2.linestring.linestring)
    if type(s2_intersection) == MultiLineString:
        s2_pts = np.concatenate([np.array(l) for l in s2_intersection.geoms])
    else:
        s2_pts = np.array(s2_intersection)

    # check if strokes are almost overlapping
    registered_s1_pts, angle, reflection = icp_registration(s1_pts, s2_pts)
    if VERBOSE:
        print("icp angle", angle, "reflection", reflection)
        print(reflection, angle < angle_threshold, np.linalg.norm(registered_s1_pts[0]-s1_pts[0])/axes_distances[axis_id])
    if not reflection and angle < angle_threshold and \
        np.linalg.norm(registered_s1_pts[0]-s1_pts[0])/axes_distances[axis_id] < 0.15:
        return False

    proj_center_1 = LineString(v_mid).interpolate(LineString(v_mid).project(MultiPoint(np.array(s1.linestring.linestring)).centroid))
    proj_center_2 = LineString(v_mid).interpolate(LineString(v_mid).project(MultiPoint(s2_pts).centroid))
    in_between_center = (np.array(proj_center_1)+np.array(proj_center_2))/2
    v_mid_vec = v_mid[1] - v_mid[0]
    v_mid_vec /= np.linalg.norm(v_mid_vec)
    #in_between_center = np.array(proj_center_1)-v_mid_vec*20
    #print(v_mid_vec)
    v_mid_ortho_vec = np.array([-v_mid_vec[1], v_mid_vec[0]])

    scale_1_1 = LineString(v1).interpolate(LineString(v1).project(proj_center_1))
    scale_1_2 = LineString(v2).interpolate(LineString(v2).project(proj_center_1))
    scale_1 = np.linalg.norm(np.array(scale_1_1) - np.array(scale_1_2))
    scale_2_1 = LineString(v1).interpolate(LineString(v1).project(proj_center_2))
    scale_2_2 = LineString(v2).interpolate(LineString(v2).project(proj_center_2))
    scale_2 = np.linalg.norm(np.array(scale_2_1) - np.array(scale_2_2))
    scaling_factor = scale_2/scale_1
    #print("scales", scale_1, scale_2, scaling_factor)
    s1_center = np.array(MultiPoint(s1_pts).centroid)
    recentered_s1_pts = s1_pts - s1_center
    recentered_s1_pts = s1_pts - np.array(proj_center_1)
    x_axis = np.array([1, 0])
    primal_ortho_vec_angle = np.sign(np.cross(x_axis, v_mid_ortho_vec)) * np.arccos(np.abs(np.dot(x_axis, v_mid_ortho_vec)))
    if VERBOSE:
        print("ortho_vec_angle", primal_ortho_vec_angle, np.rad2deg(primal_ortho_vec_angle), np.cross(x_axis, v_mid_ortho_vec))
    rotated_s1_pts = rotate_angle(recentered_s1_pts, -primal_ortho_vec_angle)
    #print(rotated_s1_pts)
    rescaled_s1_pts = scaling_factor*rotated_s1_pts
    rerotated_s1_pts = rotate_angle(rescaled_s1_pts, primal_ortho_vec_angle)
    #s2_pts = np.array(s2.linestring.linestring)
    retranslated_s1_pts = rerotated_s1_pts + np.array(proj_center_2)
    registered_s1_pts, angle, reflection = icp_registration(retranslated_s1_pts, s2_pts)
    if VERBOSE:
        print("icp angle", angle, "reflection", reflection)
    #print(registered_s1_pts, s2_pts)
    dist_1, pointwise_dists = chamfer_distance(registered_s1_pts, s2_pts, return_pointwise_distances=True)
    #print(dist_1, max_length)
    if reflection:
        translational_symmetry = False
    if angle > angle_threshold:
        translational_symmetry = False
    if dist_1 > max_length*dist_threshold:
        translational_symmetry = False
    if VERBOSE:
        print("dist", dist_1, hausdorff_distance(registered_s1_pts, s2_pts))
    #print(pointwise_dists)
    #print(np.var(pointwise_dists[0]))
    #print(np.var(pointwise_dists[1]))

    # reflection
    reflection_axes = []
    reflectional_symmetries = []
    if axis_id == 0:
        reflection_axes = [1, 2]
    elif axis_id == 1:
        reflection_axes = [0, 2]
    elif axis_id == 2:
        reflection_axes = [0, 1]
    tmp_reflectional_symmetries = [True, True]
    for vec_id, reflection_axis_id in enumerate(reflection_axes):
        if VERBOSE:
            plt.rcParams["figure.figsize"] = (10, 10)
            x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
            fig, ax = plt.subplots(nrows=1, ncols=1)
            fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                                bottom=0.0,
                                top=1.0)
            sketch.display_strokes_2(fig, ax, color_process=lambda s: "#0000008D")
            ax.scatter([center_1[0]], [center_1[1]])
            ax.scatter([center_2[0]], [center_2[1]])

            #ax.add_patch(PolygonPatch(vanishing_triangle, fc=patch_red, ec=patch_red, alpha=0.2, zorder=0))

        v_mid_ortho_vec = np.array(cam.vanishing_points_coords[reflection_axis_id]) - in_between_center
        v_mid_ortho_vec /= np.linalg.norm(v_mid_ortho_vec)
        reflection_line = LineString([in_between_center-1000.0*v_mid_ortho_vec,
                                      in_between_center+1000.0*v_mid_ortho_vec])
        if VERBOSE:
            ax.plot(np.array(reflection_line)[:, 0], np.array(reflection_line)[:, 1], c="red", lw=2)

        reflected_pts = []
        for p_id, p in enumerate(s1_pts):
            p_vanishing_vec = np.array(cam.vanishing_points_coords[axis_id]) - p
            p_vanishing_vec /= np.linalg.norm(p_vanishing_vec)
            if np.dot(in_between_center-p, p_vanishing_vec) < 0:
                p_vanishing_vec *= -1
            p_vanishing_line = LineString([p, p+1000.0*p_vanishing_vec])
            #ax.plot(np.array(p_vanishing_line)[:, 0], np.array(p_vanishing_line)[:, 1], c="cyan")
            if p_vanishing_line.intersects(reflection_line):
                dist_to_midline = np.linalg.norm(np.array(p_vanishing_line.intersection(reflection_line)) - p)
                refl_p = p + 2.0*dist_to_midline*p_vanishing_vec
                mid_p = p + 1.0*dist_to_midline*p_vanishing_vec
                reflected_pts.append(refl_p)
                if VERBOSE:
                    #ax.scatter([mid_p[0]], [mid_p[1]])
                    refl_line = np.array([p, refl_p])
                    ax.plot(refl_line[:, 0], refl_line[:, 1], c="green", alpha=0.3)
        if len(reflected_pts) == 0:
            tmp_reflectional_symmetries[vec_id] = False
            continue
        reflected_pts = np.array(reflected_pts)
        reflected_registered_s1_pts, angle, reflection = icp_registration(reflected_pts, s2_pts)
        if np.linalg.norm(reflected_pts[0]-reflected_registered_s1_pts[0]) > 0.25*max_length:
            tmp_reflectional_symmetries[vec_id] = False
        if VERBOSE:
            print("icp angle", angle, "reflection", reflection)
        dist_2, pointwise_dists = chamfer_distance(reflected_registered_s1_pts, s2_pts, return_pointwise_distances=True)
        if VERBOSE:
            print(dist_2, max_length)
        if reflection:
            tmp_reflectional_symmetries[vec_id] = False
        if angle > angle_threshold:
            tmp_reflectional_symmetries[vec_id] = False
        if dist_2 > max_length*dist_threshold:
           tmp_reflectional_symmetries[vec_id] = False

        if VERBOSE:
            print("dist", dist_2, hausdorff_distance(reflected_registered_s1_pts, s2_pts))
        if VERBOSE:
            ax.scatter(reflected_pts[:, 0], reflected_pts[:, 1], c="red")
            ax.plot(reflected_pts[:, 0], reflected_pts[:, 1], c="red")
            ax.plot(reflected_registered_s1_pts[:, 0], reflected_registered_s1_pts[:, 1], c="pink", lw=3)

            ax.plot(s1_pts[:, 0], s1_pts[:, 1], lw=3, c="blue")
            ax.plot(s2_pts[:, 0], s2_pts[:, 1], lw=3, c="green")

            ax.set_xlim(x_lim)
            ax.set_ylim(y_lim)
            ax.set_aspect("equal")
            ax.axis("off")
            plt.show()

    if VERBOSE:
        print(translational_symmetry, tmp_reflectional_symmetries)
    return translational_symmetry or np.any(tmp_reflectional_symmetries)

def icp_registration(s1_pts, s2_pts, with_scaling=False):

    initial_T = np.identity(4) # Initial transformation for ICP
    source = np.zeros([len(s1_pts), 3])
    source[:, :2] = s1_pts
    target = np.zeros([len(s2_pts), 3])
    target[:, :2] = s2_pts
    source_pc = o3d.geometry.PointCloud()
    source_pc.points = o3d.utility.Vector3dVector(source)
    target_pc = o3d.geometry.PointCloud()
    target_pc.points = o3d.utility.Vector3dVector(target)

    distance = 1000.0 # The threshold distance used for searching correspondences
    icp_type = o3d.pipelines.registration.TransformationEstimationPointToPoint(with_scaling=with_scaling)
    iterations = o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration = 10000)
    result = o3d.pipelines.registration.registration_icp(source_pc, target_pc, distance, initial_T, icp_type, iterations)
    trans = np.asarray(deepcopy(result.transformation))
    #if trans[0, 0]*trans[1, 1] < 0:
    #    trans[:, 0] *= -1
    source_pc.transform(trans)
    scale_x = np.linalg.norm(trans[:, 0])
    scale_y = np.linalg.norm(trans[:, 1])
    scale_z = np.linalg.norm(trans[:, 2])
    scale_z = trans[2, 2]
    reflection = scale_z < 0
    rot_mat = deepcopy(trans)
    rot_mat[:, 0] /= scale_z
    rot_mat[:, 1] /= scale_z
    rot_mat[:, 2] /= scale_z
    angle = np.rad2deg(np.arccos(rot_mat[0, 0]))

    return np.array(source_pc.points)[:, :2], angle, reflection

def tangents(c):
    tans = []
    for p_id in range(len(c)):
        if p_id == 0:
            vec = c[p_id+1] - c[p_id]
        elif p_id == len(c)-1:
            vec = c[p_id-1] - c[p_id]
        else:
            vec = c[p_id+1] - c[p_id-1]
        vec /= np.linalg.norm(vec)
        tans.append(vec)
    return tans

def angle(v1, v2):
    return np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(v1, v2)))))

def two_curves_tan_score(s1, s2, sketch, VERBOSE=True):
    s1_part = s2.linestring.linestring.buffer(max(s1.acc_radius, s2.acc_radius)).intersection(s1.linestring.linestring)
    s2_part = s1.linestring.linestring.buffer(max(s1.acc_radius, s2.acc_radius)).intersection(s2.linestring.linestring)
    #print(s1_part)
    #print(s2_part)
    if np.isclose(s1_part.length, 0.0) or np.isclose(s2_part.length, 0.0):
        return -1.0
    if type(s1_part) == MultiLineString:
        s1_part = np.concatenate([np.array(l) for l in s1_part.geoms])
    else:
        s1_part = np.array(s1_part)
    if type(s2_part) == MultiLineString:
        s2_part = np.concatenate([np.array(l) for l in s2_part.geoms])
    else:
        s2_part = np.array(s2_part)
    s1_tans = tangents(s1_part)
    s2_tans = tangents(s2_part)
    #print(s1_part)
    #print(s2_part)
    #print(s1_tans)
    #print(s2_tans)

    x_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric="l2").fit(s1_part)
    min_x_to_y = x_nn.kneighbors(s2_part)[1].flatten()
    #print(min_x_to_y)
    tan_score = []
    prev_id = -1
    for p_id in range(len(s2_part)):
        #print(min_x_to_y)
        #print(prev_id, min_x_to_y[p_id])
        if prev_id == min_x_to_y[p_id]:
            continue
        tan_angle = angle(s2_tans[p_id], s1_tans[min_x_to_y[p_id]])
        #print("tan_angle", tan_angle)
        prev_id = min_x_to_y[p_id]
        #tan_score.append((90.0-tan_angle)/90.0)
        tan_score.append(tan_angle)

    if VERBOSE:
        #print(tan_score)
        #print(np.mean(tan_score))
        plt.rcParams["figure.figsize"] = (10, 10)
        fig, ax = plt.subplots(nrows=1, ncols=1)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        ax.plot(np.array(s1_part)[:, 0], np.array(s1_part)[:, 1], c="red", lw=3)
        ax.plot(np.array(s2_part)[:, 0], np.array(s2_part)[:, 1], c="red", lw=3)
        sketch.display_strokes_2(fig, ax, color_process=lambda s: "#0000008d")
        sketch.display_strokes_2(fig, ax, color_process=lambda s: "green",
                                 display_strokes=[s1.id, s2.id],
                                 linewidth_process=lambda s: 2)
        x_lim, y_lim = utils_plot.get_sketch_limits(sketch)
        ax.set_xlim(x_lim)
        ax.set_ylim(y_lim)
        ax.set_aspect("equal")
        ax.axis("off")
        plt.show()

    return np.mean(tan_score)


def relift_intersections(intersections, fixed_strokes, proxies, sketch, cam):

    intersections_3d = []

    for inter in intersections:
        #print(inter)
        lifted_inter_0 = []
        lifted_inter_1 = []
        sketch_inter = sketch.intersection_graph.get_intersections([inter[2]])[0]
        s_0_id = sketch_inter.stroke_ids[0]
        s_0 = fixed_strokes[s_0_id]
        if proxies[s_0_id] is not None:
            s_0 = proxies[s_0_id]
        if len(s_0) > 0:
            if sketch.strokes[s_0_id].axis_label < 5:
                p = s_0[0]
                vec = s_0[-1] - s_0[0]
                vec /= np.linalg.norm(vec)
                lifted_inter_0 = cam.lift_point_close_to_line(sketch_inter.inter_coords, p, vec)
            else:
                lifted_inter_0 = cam.lift_point_close_to_polyline(sketch_inter.inter_coords, s_0)
            #inter_pc.append(lifted_inter_0)
            intersections_3d.append([list(lifted_inter_0), int(inter[2])])
        s_1_id = sketch_inter.stroke_ids[1]
        s_1 = fixed_strokes[s_1_id]
        if proxies[s_1_id] is not None:
            s_1 = proxies[s_1_id]
        if len(s_1) > 0:
            #if final_proxis[s_1_id] is not None:
            if sketch.strokes[s_1_id].axis_label < 5:
                p = s_1[0]
                vec = s_1[-1] - s_1[0]
                vec /= np.linalg.norm(vec)
                lifted_inter_1 = cam.lift_point_close_to_line(sketch_inter.inter_coords, p, vec)
            else:
                lifted_inter_1 = cam.lift_point_close_to_polyline(sketch_inter.inter_coords, s_1)
            #inter_pc.append(lifted_inter_1)
            intersections_3d.append([list(lifted_inter_1), int(inter[2])])
    return intersections_3d

def prepare_triple_intersections(sketch):
    per_stroke_triple_intersections = []
    for s_id, s in enumerate(sketch.strokes):
        stroke_dict = {"s_id": s_id,
                       "i_triple_intersections": []}
        for inter in sketch.intersection_graph.get_intersections_by_stroke_id(s_id):
            i_triple_dict = {"inter_id": inter.inter_id,
                             "k_axes": [[], [], [], [], [], []]}
            if len(inter.adjacent_inter_ids) > 1:
                neigh_inters = sketch.intersection_graph.get_intersections(inter.adjacent_inter_ids)
                for neigh_inter in neigh_inters:
                    if neigh_inter.is_extended:
                        continue
                    if not s_id in neigh_inter.stroke_ids:
                        continue
                    i_triple_dict["k_axes"][sketch.strokes[neigh_inter.stroke_ids[0]].axis_label].append(neigh_inter.inter_id)
                    i_triple_dict["k_axes"][sketch.strokes[neigh_inter.stroke_ids[1]].axis_label].append(neigh_inter.inter_id)
                for axis_label in range(len(i_triple_dict["k_axes"])):
                    i_triple_dict["k_axes"][axis_label] = np.unique(i_triple_dict["k_axes"][axis_label]).tolist()
            if np.sum([len(i_triple_dict["k_axes"][i]) > 0 for i in range(len(i_triple_dict["k_axes"]))]) > 2:
                stroke_dict["i_triple_intersections"].append(i_triple_dict)
        if len(stroke_dict["i_triple_intersections"]) > 0:
            per_stroke_triple_intersections.append(stroke_dict)
    return per_stroke_triple_intersections

def filter_weak_curve_correspondences(per_correspondence_sym_scores, global_candidate_correspondences, sketch, threshold=0.5):
    del_ids = []
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if len(per_correspondence_sym_scores[corr_id]) > 0:
            if np.max(per_correspondence_sym_scores[corr_id]) < threshold:
                del_ids.append(corr_id)
    print("removed", len(del_ids), "weak curve correspondences")
    # also filter weak ellipse correspondences
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if not(sketch.strokes[corr[0]].axis_label == 5 and sketch.strokes[corr[0]].is_ellipse()):
            continue
        if corr[0] == corr[1]:
            continue
        area_1 = sketch.strokes[corr[0]].linestring.linestring.convex_hull.area
        area_2 = sketch.strokes[corr[1]].linestring.linestring.convex_hull.area
        if area_1/area_2 > 3.0 or area_1/area_2:
            del_ids.append(corr_id)
    for del_id in reversed(del_ids):
        #corr = global_candidate_correspondences[del_id]
        #print(corr[0], corr[1], corr[4])
        del global_candidate_correspondences[del_id]
        del per_correspondence_sym_scores[del_id]

def get_per_correspondence_sym_scores(sketch, curve_epsilons, global_candidate_correspondences):
    # account for actual point-to-point symmetry for curves
    per_correspondence_sym_scores = []
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if sketch.strokes[corr[0]].axis_label != 5 or sketch.strokes[corr[0]].is_ellipse():
            per_correspondence_sym_scores.append([])
            continue
        if corr[0] == corr[1]:
            per_correspondence_sym_scores.append([])
            continue
        # equidistant resampling
        all_points = []
        point_stroke_ids = []
        for i in range(2):
            equi_seg = equi_resample_polyline(corr[2+i], curve_epsilons[corr[4]])
            for p in equi_seg:
                all_points.append(p)
                point_stroke_ids.append(i)
        all_points = np.array(all_points)
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[corr[4]] = 1.0
        refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
        min_length = np.min([tools_3d.line_3d_length(corr[2+i]) for i in range(2)])

        #symmetric_points = symmetry_tools.symmetric_point_cloud(all_points, refl_mat,
        #                                                        threshold=0.05*bbox_diags[corr[4]],
        #                                                        VERBOSE=True)
        symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                                threshold=0.1*min_length,
                                                                VERBOSE=False)
        sym_scores = [[] for l in range(2)]
        for pt_id, sym_score in enumerate(symmetric_points):
            s_id = point_stroke_ids[pt_id]
            sym_scores[s_id].append(min(sym_score, 1.0))
        for s_id, scores in enumerate(sym_scores):
            sym_scores[s_id] = np.mean(scores)
        #print(sym_scores)
        per_correspondence_sym_scores.append(sym_scores)
    return per_correspondence_sym_scores

def get_intersections_scale_factors(per_axis_per_stroke_candidate_reconstructions,
                                    reference_plane_id, aligned_plane_id, sketch,
                                    camera, batch=[]):
    scale_factors = []
    intersections_3d = []
    for inter in sketch.intersection_graph.get_intersections():
        if len(batch) > 0 and (inter.stroke_ids[0] > batch[1] or inter.stroke_ids[1] > batch[1]):
            continue
        stroke_id_0 = -1
        stroke_id_1 = -1
        if (len(per_axis_per_stroke_candidate_reconstructions[reference_plane_id][inter.stroke_ids[0]]) > 0 and \
                len(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][inter.stroke_ids[1]]) > 0):
            stroke_id_0 = inter.stroke_ids[0]
            stroke_id_1 = inter.stroke_ids[1]
        elif (len(per_axis_per_stroke_candidate_reconstructions[reference_plane_id][inter.stroke_ids[1]]) > 0 and \
              len(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][inter.stroke_ids[0]]) > 0):
            stroke_id_0 = inter.stroke_ids[1]
            stroke_id_1 = inter.stroke_ids[0]
        else:
            continue
        for s_0_id, s_0 in enumerate(per_axis_per_stroke_candidate_reconstructions[reference_plane_id][stroke_id_0]):
            if sketch.strokes[stroke_id_0].axis_label == 5:
                if sketch.strokes[stroke_id_0].is_ellipse():
                    inter_3d_0 = camera.lift_point_close_to_polyline_v2(inter.inter_coords, s_0)
                else:
                    inter_3d_0 = camera.lift_point_close_to_polyline(inter.inter_coords, s_0)
            else:
                dir_vec_0 = s_0[-1] - s_0[0]
                dir_vec_0 /= np.linalg.norm(dir_vec_0)
                inter_3d_0 = camera.lift_point_close_to_line(inter.inter_coords, s_0[0], dir_vec_0)
            if inter_3d_0 is None:
                continue
            inter_3d_0_cam_dist = np.linalg.norm(inter_3d_0 - camera.cam_pos)
            #s_0_cand_corr_id = per_axis_per_stroke_candidate_correspondences[0][stroke_id_0][s_0_id]
            for s_1_id, s_1 in enumerate(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][stroke_id_1]):
                if sketch.strokes[stroke_id_1].axis_label == 5:
                    if sketch.strokes[stroke_id_1].is_ellipse():
                        inter_3d_1 = camera.lift_point_close_to_polyline_v2(inter.inter_coords, s_1)
                    else:
                        inter_3d_1 = camera.lift_point_close_to_polyline(inter.inter_coords, s_1)
                else:
                    dir_vec_1 = s_1[-1] - s_1[0]
                    dir_vec_1 /= np.linalg.norm(dir_vec_1)
                    inter_3d_1 = camera.lift_point_close_to_line(inter.inter_coords, s_1[0], dir_vec_1)
                if inter_3d_1 is None:
                    continue
                inter_3d_1_cam_dist = np.linalg.norm(inter_3d_1 - camera.cam_pos)
                #s_1_cand_corr_id = per_axis_per_stroke_candidate_correspondences[1][stroke_id_1][s_1_id]

                scale_factor = inter_3d_0_cam_dist / inter_3d_1_cam_dist
                #temporal_distance = np.abs(inter.stroke_ids[0] - inter.stroke_ids[1])
                #print(inter.inter_id, scale_factor, stroke_id_0, stroke_id_1, s_0, s_1, inter_3d_0)
                scale_factors.append(scale_factor)
                intersections_3d.append(
                    Intersection3d(inter_id=inter.inter_id,
                                   inter_3d=inter_3d_0,
                                   stroke_ids=[stroke_id_0, stroke_id_1],
                                   stroke_candidates=[s_0, s_1]))
                                   #candidate_correspondence_ids=[s_0_cand_corr_id, s_1_cand_corr_id]))

    return intersections_3d, scale_factors

# fixed_strokes: array of size len(sketch.strokes)
def get_intersections_scale_factors_fixed_strokes(
        per_axis_per_stroke_candidate_reconstructions, fixed_strokes, batch,
        aligned_plane_id, sketch, camera):
    scale_factors = []
    intersections_3d = []
    for inter in sketch.intersection_graph.get_intersections():
        if inter.stroke_ids[0] > batch[1] or inter.stroke_ids[1] > batch[1]:
            continue
        stroke_id_0 = -1
        stroke_id_1 = -1
        if (len(fixed_strokes[inter.stroke_ids[0]]) > 0 and \
            len(fixed_strokes[inter.stroke_ids[1]]) == 0):# and \
                #len(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][inter.stroke_ids[1]]) > 0):
            stroke_id_0 = inter.stroke_ids[0]
            stroke_id_1 = inter.stroke_ids[1]
        elif (len(fixed_strokes[inter.stroke_ids[1]]) > 0 and \
              len(fixed_strokes[inter.stroke_ids[0]]) == 0):# and \
              #len(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][inter.stroke_ids[0]]) > 0):
            stroke_id_0 = inter.stroke_ids[1]
            stroke_id_1 = inter.stroke_ids[0]
        else:
            continue
        s_0 = fixed_strokes[stroke_id_0]
        if sketch.strokes[stroke_id_0].axis_label == 5:
            if sketch.strokes[stroke_id_0].is_ellipse():
                inter_3d_0 = camera.lift_point_close_to_polyline_v2(inter.inter_coords, s_0)
            else:
                inter_3d_0 = camera.lift_point_close_to_polyline(inter.inter_coords, s_0)
        else:
            dir_vec_0 = s_0[-1] - s_0[0]
            if np.isclose(np.linalg.norm(dir_vec_0), 0.0):
                continue
            dir_vec_0 /= np.linalg.norm(dir_vec_0)
            inter_3d_0 = camera.lift_point_close_to_line(inter.inter_coords, s_0[0], dir_vec_0)
        if inter_3d_0 is None:
            print("is None", inter.inter_coords, s_0[0], dir_vec_0, stroke_id_0, stroke_id_1, inter.inter_id)
            continue
        inter_3d_0_cam_dist = np.linalg.norm(inter_3d_0 - camera.cam_pos)
        for s_1_id, s_1 in enumerate(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][stroke_id_1]):
            #if stroke_id_1 == 61:
            #    print(aligned_plane_id)
            #    print(per_axis_per_stroke_candidate_reconstructions[aligned_plane_id][stroke_id_1])
            if sketch.strokes[stroke_id_1].axis_label == 5:
                if sketch.strokes[stroke_id_1].is_ellipse():
                    inter_3d_1 = camera.lift_point_close_to_polyline_v2(inter.inter_coords, s_1)
                else:
                    inter_3d_1 = camera.lift_point_close_to_polyline(inter.inter_coords, s_1)
            else:
                dir_vec_1 = s_1[-1] - s_1[0]
                if np.isclose(np.linalg.norm(dir_vec_1), 0.0):
                    continue
                dir_vec_1 /= np.linalg.norm(dir_vec_1)
                inter_3d_1 = camera.lift_point_close_to_line(inter.inter_coords, s_1[0], dir_vec_1)
            if inter_3d_1 is None:
                #print("is None", inter.inter_coords, dir_vec_1, stroke_id_0, stroke_id_1, inter.inter_id)
                #print(s_1)
                print("is None", inter.inter_coords, s_1[0], dir_vec_1, stroke_id_0, stroke_id_1, inter.inter_id)
                continue
            inter_3d_1_cam_dist = np.linalg.norm(inter_3d_1 - camera.cam_pos)

            #if len(intersections_3d) == 6:
            #    print(inter.inter_id, inter.stroke_ids)
            #    print(s_0, s_1)
            #    print(inter_3d_0)
            scale_factor = inter_3d_0_cam_dist / inter_3d_1_cam_dist
            scale_factors.append(scale_factor)
            intersections_3d.append(
                Intersection3d(inter_id=inter.inter_id,
                               inter_3d=inter_3d_0,
                               stroke_ids=[stroke_id_0, stroke_id_1],
                               stroke_candidates=[s_0, s_1]))

    return intersections_3d, scale_factors